<?php 

	class Tcpf {
	
		private $cookieFile;
	    private $token;
	    private $imgCaptcha;

	    public function __construct(){

	    	Yii::import('application.vendor.*');

	    	$this->cookieFile = 'cookie/' . session_id();

	    	if (!file_exists($this->cookieFile)) {
            
            	$file = fopen($this->cookieFile, 'w');
            	fclose($file);
        	}

        	$ch = curl_init('http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/consultapublica.asp');
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
		    $html = curl_exec($ch);

		    if (!$html) {
            	return false;
        	}

        	$html = new Simple_html_dom($html);
        	$url_imagem = $tokenValue = '';
        	$imgcaptcha = $html->find('img[id=imgcaptcha]');

        	if ( count($imgcaptcha) ) {
        		
        		foreach($imgcaptcha as $imgAttr)
        			$url_imagem = $imgAttr->src;

        		if ( preg_match('#guid=(.*)$#', $url_imagem, $arr) ) {
        			
        			$idCaptcha = $arr[1];
                	$viewstate = $html->find('input[id=viewstate]');

                	if ( count($viewstate) ) {
                    	foreach ($viewstate as $inputViewstate)
                        	$tokenValue = $inputViewstate->value;
                	}

                	if (!empty($idCaptcha) && !empty($tokenValue)) {
                		
                		$this->token = array(
	                        $idCaptcha,
	                        $tokenValue
                    	);
                	}
                	else{
                		$this->token = false;
                	}
        		}
        	}

        	//echo $idCaptcha;
        	$this->imgCaptcha = $this->getCaptcha($idCaptcha);

	    }

	    public function getCaptcha( $idCaptcha ){

	    	if ( preg_match('#^[a-z0-9-]{36}$#', $idCaptcha) ) {

	    		$url = 'http://www.receita.fazenda.gov.br/scripts/captcha/Telerik.Web.UI.WebResource.axd?type=rca&guid=' . $idCaptcha;
            	$ch  = curl_init( $url );

            	curl_setopt($ch, CURLOPT_HEADER, false);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	            $imgsource = curl_exec($ch);
	            curl_close($ch);

	            if ( !empty($imgsource) ) {

	            	if ( file_exists( 'images/captchas/'.Yii::app()->session->getSessionId().'.jpg' ) ) {
                    	unlink('images/captchas/'.Yii::app()->session->getSessionId().'.jpg');
                	}

                	$fp = fopen('images/captchas/'.Yii::app()->session->getSessionId().'.jpg', 'x');
                	fwrite($fp, $imgsource);
                	fclose($fp);
	            }
	    	}

	    }
	}