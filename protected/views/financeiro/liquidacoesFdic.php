
<div class="content" style="padding-bottom:500px;">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Gestor de Filiais - Produção</h4> 
            </div>
        </div>
        
        <div id="indicativos-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">  
                        <h4 class="m-t-0 header-title"><b>FILTROS INDICATIVOS</b></h4>
                        <form class="">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                            <h5><b>De:</b></h5>
                                            <input readonly id="de" type="text" class="form-control date" value="<?php echo date('01/m/Y') ?>">
                                    </div>
                                    <div class="col-md-3">
                                            <h5><b>Até:</b></h5>
                                            <input readonly id="ate" type="text" class="form-control date" value="<?php echo date('t/m/Y') ?>">
                                    </div>
                                    <div class="col-md-3">
                                        <h5><b>&nbsp;</b></h5>
                                        <button type="button" id="botaoGerar" class="btn btn-success waves-effect waves-light">Gerar arquivo de baixas</button>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">  
                        <h4 class="m-t-0 header-title"><b>Arquivos</b></h4>
                        <table id="grid_gerados" class="table table-striped table-hover dataTable">
                            <thead>
                                <th class="no-orderable" width="33%">Data de Geração</th>
                                <th class="no-orderable" width="33%">Qtd. Titulos</th>
                                <th class="no-orderable" width="33%">Usuário</th>
                                <th class="no-orderable" width="1%"></th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
            <?php $this->renderPartial('/user/form_mudar_primeira_senha'); ?>
        <?php endif ?>
    </div>
    <?php $this->renderPartial('/user/menu', [ 'css_menu' => 'display:none']); ?>
</div>

<style type="text/css">
    #grid_gerados_filter,
    #grid_gerados_length
    {
        display: none;
    }
    .btn-custom.btn-inverse
    {
        color: #FFF !important;
    }
    .card-box:hover
    {
        border:2px solid rgba(255, 255, 255, 0.1);
    }
    @media (min-width: 992px){
        .col-md-2 {
            width: 11%!important;
        }
    }
    input.filter{
        border:none!important;
        background: transparent!important;
    }
    ::-webkit-input-placeholder {
        font-style: italic;
    }
    tfoot tr th{
        font-size: 11px!important;
    }
</style>