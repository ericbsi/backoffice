<div class="content">
   <?php $this->renderPartial('/navigation/navigation_menu'); ?>
   <div class="container" id="action-content">
      <div id="wrapper-politicas-menus">
         <div class="row">
            <div class="col-sm-12">
               <h4 class="page-header  header-title m-b-30">ADMINISTRAR POLÍTICAS DE CRÉDITO</h4>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="btn-group btn-group-justified m-b-20">
                  <a data-input-focus="#PoliticaCredito_nome" data-bind-show="#wrapper-politicas-index" data-bind-hide="" class="btn-move-panel btn btn-success waves-effect waves-light" role="button">ADICIONAR</a>
                  <a data-bind-show="#wrapper-politicas-grid" data-bind-hide="" class="btn-move-panel btn btn-primary waves-effect waves-light" role="button">DISPONÍVEIS</a>
                  <!--<a data-bind-show="#wrapper-politicas-regras" data-bind-hide="" class="btn-move-panel btn btn-warning waves-effect waves-light" role="button">REGRAS</a>-->
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-politicas-index" style="display:none">
         <div class="row">
            <div class="col-lg-12">
               <div class="portlet" id="portlet-form">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Incluir Nova Política</h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <form id="form-politica" class="form-horizontal group-border-dashed" action="#">
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Descrição</label>
                              <div class="col-sm-6">
                                 <input data-nome-msg="Descrição"  name="PoliticaCredito[descricao]" type="text" id="PoliticaCredito_nome" class="form-control" required placeholder="Descrição" />
                              </div>
                           </div>
                           <div class="form-group m-b-0">
                              <label class="col-sm-3 control-label">Legenda</label>
                              <div class="col-sm-6">
                                 <div data-color-format="rgb" data-color="rgb(255, 146, 180)" class="colorpicker-default input-group">
                                    <input data-nome-msg="Legenda" type="text" readonly="readonly" class="form-control" required name="PoliticaCredito[cor]">
                                    <span class="input-group-btn add-on">
                                    <button class="btn btn-white" type="button">
                                    <i style="width:150px;background-color: rgb(124, 66, 84);margin-top: 2px;"></i>
                                    </button>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                 <button type="submit" class="btn btn-login-credshow">Salvar</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-politicas-grid" style="display:none">
         <div class="row">
            <div class="col-lg-12">
               <div class="portlet" id="portlet-form3">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Políticas cadastradas</h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default2" aria-expanded="false" class="collapsed"><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default2" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <table id="datatable-politicas" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th>Legenda</th>
                                 <th>Descrição</th>
                                 <th width="150">Regras de análise</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-politicas-regras" style="display:none;">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
         <div class="row">
           <div id="erros" class="col-sm-12"></div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="portlet" id="portlet-form2">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">
                        Regras cadastradas para a política <span class="span_politica_nome"></span>
                     </h3>

                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default2" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div style="padding-bottom:500px;" id="bg-default2" class="panel-collapse collapse in table-responsive">
                     <div class="portlet-body">
                        <form id="form-add-regra">
                           <input name="pid" type="hidden"  id="pid" class="form-control" value="0" />
                           <input name="id" type="hidden"   id="id" class="form-control"  value="0" />
                           <table style="table-layout: fixed;" id="grid_regras_form" class="table table-striped">
                              <thead>
                                 <tr>
                                    <th width="25">
                                       <select data-nome-msg="Score" data-style="btn-default btn-custom" name="Regra[score]" required="required" class="input-small show-tick form-control selectpicker required" id="score">
                                          <option value="">SCORE</option>
                                          <option value="A">A</option>
                                          <option value="B">B</option>
                                          <option value="C">C</option>
                                          <option value="D">D</option>
                                          <option value="E">E</option>
                                          <option value="F">F</option>
                                       </select>
                                    </th>
                                    <th width="25">
                                       <select data-nome-msg="Ação" data-style="btn-default btn-custom" name="Regra[status]" required="required" class="show-tick form-control selectpicker required" id="status">
                                          <option value="">AÇÃO</option>
                                          <option value="4">MESA</option>
                                          <option value="2">APROVAR</option>
                                          <option value="3">NEGAR</option>
                                       </select>
                                    </th>
                                    <th width="35">
                                       <select data-nome-msg="Histórico" data-style="btn-default btn-custom" name="Regra[historico]" class="show-tick form-control selectpicker" id="historico">
                                          <option value="" >HISTORICO?</option>
                                          <option value="1">SIM</option>
                                          <option value="0">NÃO</option>
                                       </select>
                                    </th>
                                    <th width="25">
                                       <select data-nome-msg="Valor Mínimo" data-style="btn-default btn-custom" name="Regra[valor_de]" class="show-tick form-control selectpicker" id="valor_de">
                                          <option value="" >R$ DE</option>
                                          <option value="0">0,00</option>
                                          <option value="50">50,00</option>
                                          <option value="100">100,00</option>
                                          <option value="150">150,00</option>
                                          <option value="200">200,00</option>
                                          <option value="250">250,00</option>
                                          <option value="300">300,00</option>
                                          <option value="350">350,00</option>
                                          <option value="400">400,00</option>
                                          <option value="450">450,00</option>
                                          <option value="500">500,00</option>
                                          <option value="550">550,00</option>
                                          <option value="600">600,00</option>
                                          <option value="650">650,00</option>
                                          <option value="700">700,00</option>
                                          <option value="750">750,00</option>
                                          <option value="800">800,00</option>
                                          <option value="850">850,00</option>
                                          <option value="900">900,00</option>
                                          <option value="950">950,00</option>
                                          <option value="1000">1000,00</option>
                                          <option value="1100">1100,00</option>
                                          <option value="1200">1200,00</option>
                                          <option value="1300">1300,00</option>
                                          <option value="1400">1400,00</option>
                                          <option value="1500">1500,00</option>
                                       </select>
                                    </th>
                                    <th width="30">
                                       <select data-nome-msg="Valor Máximo" data-style="btn-default btn-custom" name="Regra[valor_ate]" class="show-tick form-control selectpicker" id="valor_ate">
                                          <option value="" >R$ DE</option>
                                          <option value="0">0,00</option>
                                          <option value="50">50,00</option>
                                          <option value="100">100,00</option>
                                          <option value="150">150,00</option>
                                          <option value="200">200,00</option>
                                          <option value="250">250,00</option>
                                          <option value="300">300,00</option>
                                          <option value="350">350,00</option>
                                          <option value="400">400,00</option>
                                          <option value="450">450,00</option>
                                          <option value="500">500,00</option>
                                          <option value="550">550,00</option>
                                          <option value="600">600,00</option>
                                          <option value="650">650,00</option>
                                          <option value="700">700,00</option>
                                          <option value="750">750,00</option>
                                          <option value="800">800,00</option>
                                          <option value="850">850,00</option>
                                          <option value="900">900,00</option>
                                          <option value="900">950,00</option>
                                          <option value="1000">1000,00</option>
                                          <option value="1100">1100,00</option>
                                          <option value="1200">1200,00</option>
                                          <option value="1300">1300,00</option>
                                          <option value="1400">1400,00</option>
                                          <option value="1500">1500,00</option>
                                       </select>
                                    </th>
                                    <th width="35">
                                       <select data-nome-msg="Restrição" data-style="btn-default btn-custom" name="Regra[restricao]" class="show-tick form-control selectpicker" id="restricao">
                                          <option value="">RESTRIÇÃO?</option>
                                          <option value="1">SIM</option>
                                          <option value="0">NÃO</option>
                                       </select>
                                    </th>
                                    <th width="30">
                                       <select data-nome-msg="Comprometimento da renda" data-style="btn-default btn-custom" name="Regra[comprometimento_da_renda]" class="show-tick form-control selectpicker" id="comprometimento_da_renda">
                                          <option value="" >% RENDA</option>
                                          <option value="5">5%</option>
                                          <option value="10">10%</option>
                                          <option value="15">15%</option>
                                          <option value="20">20%</option>
                                          <option value="25">25%</option>
                                          <option value="30">30%</option>
                                          <option value="35">35%</option>
                                          <option value="40">40%</option>
                                          <option value="45">45%</option>
                                          <option value="50">50%</option>
                                       </select>
                                    </th>
                                    <th width="45">
                                       <select data-nome-msg="Contratos em aberto" data-style="btn-default btn-custom" name="Regra[contratos_em_aberto]" class="show-tick form-control selectpicker" id="contratos_em_aberto">
                                          <option value="" >CONTRA. ABERTOS</option>
                                          <option value="5">5%</option>
                                          <option value="10">10%</option>
                                          <option value="15">15%</option>
                                          <option value="20">20%</option>
                                          <option value="25">25%</option>
                                          <option value="30">30%</option>
                                          <option value="35">35%</option>
                                          <option value="40">40%</option>
                                          <option value="45">45%</option>
                                          <option value="50">50%</option>
                                       </select>
                                    </th>
                                    <th width="30">
                                       <select data-nome-msg="Alerta" data-style="btn-default btn-custom" name="Regra[alerta]" class="show-tick form-control selectpicker" id="alerta">
                                          <option value="" >ALERTA</option>
                                          <?php foreach (Alerta::model()->findAll() as $alerta ): ?>
                                                <option value="<?php echo $alerta->id ?>" ><?php echo mb_strtoupper($alerta->alerta) ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </th>
                                    <th width="40">
                                       <select data-nome-msg="Máximo de Passagens pela casa" data-style="btn-default btn-custom" name="Regra[max_passagens_casa]" class="show-tick form-control selectpicker" id="max_passagens_casa">
                                          <option value="" >PASS. CASA</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                       </select>
                                    </th>
                                    <th width="35">
                                       <select data-nome-msg="Vendas negadas" data-style="btn-default btn-custom" name="Regra[vendas_negadas]"class="show-tick form-control selectpicker" id="vendas_negadas">
                                          <option value="">NEGAÇÕES?</option>
                                          <option value="1">SIM</option>
                                          <option value="0">NÃO</option>
                                       </select>
                                    </th>
                                    <th width="30">
                                       <select data-nome-msg="RG Anexado?" data-style="btn-default btn-custom" name="Regra[rg_anexado]" class="show-tick form-control selectpicker" id="rg_anexado">
                                          <option value="" >RG?</option>
                                          <option value="1">SIM</option>
                                          <option value="0">NÃO</option>
                                       </select>
                                    </th>
                                    <th width="35">
                                       <select data-nome-msg="Máximo de Passagens no SPC" data-style="btn-default btn-custom" name="Regra[max_passagens_spc]" class="show-tick form-control selectpicker" id="max_passagens_spc">
                                          <option value="" >PASS. SPC</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                       </select>
                                    </th>
                                    <th width="10">
                                       <button class="btn btn-icon waves-effect waves-light btn-success"> <i class="fa fa-plus"></i> </button>
                                    </th>
                                 </tr>
                              </thead>
                           </table>
                        </form>
                        <table style="table-layout:fixed;" id="grid_regras" class="table table-striped">
                           <thead>
                              <tr>
                                 <th width="25">Score</th>
                                 <th width="25">Ação</th>
                                 <th width="35">Cli. com hist?</th>
                                 <th width="25">Valor de</th>
                                 <th width="30">Valor até</th>
                                 <th width="35">Restrição?</th>
                                 <th width="30">% da Renda</th>
                                 <th width="40">Contrato em aberto</th>
                                 <th width="30">Alerta</th>
                                 <th width="40">Pass. Casa</th>
                                 <th width="35">Negações?</th>
                                 <th width="30" >RG?</th>
                                 <th width="35">Pass. SPC</th>
                                 <th width="20"></th>
                              </tr>
                           </thead>
                           <tbody></tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->renderPartial('/user/menu', ['css_menu' => 'display:none']); ?>
</div>
<style type="text/css">
   #datatable-politicas_filter, 
   #datatable-politicas_length, 
   #grid_regras_length, 
   #grid_regras_filter
   {
      display: none;
   }
   .btn-list button{
      float: left!important;
      display: inline!important;
   }
   input.filter, select.filter{
      background: transparent!important;
   }
   ::-webkit-input-placeholder {
      font-style: italic;
   }
   table.dataTable thead > tr > th {
      padding-left: 0px!important;
      padding-right: 0px!important;
   }

   #datatable-politicas thead > tr > th {
      padding-left: 10px!important;
   }

   #portlet-form2{
      border: none!important;
   }
</style>