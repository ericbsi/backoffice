<?php 
	$util 				= new Util;
	$totalFinanciado 	= 0;
	$totalRepasse 		= 0;
        $qtd = 0;
        foreach ($contratos as $c){
            $qtd ++;
        }
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/vendor/modernizr.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation/foundation.css" >
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">DATA DE PROCESSAMENTO: <?php echo $util->bd_date_to_view(substr($contratos[0]['data_processo'], 0, 10)) . ' às ' . substr($contratos[0]['data_processo'], 11) ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">CÓDIGO DO PROCESSO: <?php echo str_pad($contratos[0]['codigo_processo'], 8, '0', STR_PAD_LEFT) ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:center"><?php echo $qtd . " " ?>Contratos</th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;">
        <thead>
            <tr width>
                <th  width="410" style="text-align:left">COMPRADOR</th>
                <th  width="80" style="text-align:left">CPF</th>
                <th  width="80" style="text-align:left">CONTRATO</th>
                <th  width="100" style="text-align:left">VAL FINANCIADO</th>
                <th  width="80" style="text-align:left">PARCELAS</th>
                <th  width="80" style="text-align:left">PRIMEIRO VENC.</th>
                <th  width="80" style="text-align:left">ULTIMO VENC.</th>
            </tr>
        </thead>
        <tbody>
	
            <?php foreach ($contratos as $hasProposta): ?>
                	<?php 
                		$totalFinanciado 	+=  ($hasProposta['valor_financiado']);
                		$totalRepasse 		+=  ($hasProposta['valor_repasse']);
                		$proposta = Proposta::model()->find('habilitado AND codigo = ' . $hasProposta['codigo']);
                	?>
                    <tr>
                        <td><?php echo substr(strtoupper($hasProposta['nome']), 0,70) ?></td>
                        <td><?php echo $hasProposta['cpf'] ?></td>
                        <td><?php echo $hasProposta['codigo'] ?></td>
                        <td><?php echo "R$ " . number_format($hasProposta['valor_financiado'], 2, ',', '.') ?></td>
                        <td><?php echo $hasProposta['parcelas'] ?></td>
                        <td><?php echo $proposta->getDataPrimeiraParcela() ?></td>
                        <td><?php echo $proposta->getDataUltimaParcela() ?></td>
                    </tr>
                
            <?php endforeach; ?>
        </tbody>
</table>
<table role="grid" style="margin:0!important ">
    <thead>
            <tr>
                <th width="410"></th>
                <th width="80" ></th>
                <th width="80"></th>
                <th width="100">TOTAL: <?php echo "R$ " . number_format($totalFinanciado, 2, ',','.'); ?></th>
                <th width="80"></th>
                <th width="80"></th>
                <th width="80"></th>
            </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important">
	<thead>
		<tr>
			<th  width="460" style="text-align:left">ASS: ___________________________________</th>
			<th  width="460" style="text-align:left">DOC: ___________________________________, ASS: ___________________________________</th>
		</tr>
	</thead>
</table>
<style type="text/css">
	table tr th, table tr td{
		font-size: 0.400rem;
	}
	

    @media all{

    }
    
</style>