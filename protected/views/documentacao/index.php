<div class="content">

    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Gerenciamento de Contratos</h4>
            </div>
        </div>
        <div class="card-box" style="border:none;">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group btn-group-justified">
                        <button id="recebimento_btn"   
                                style="width: 33.333333333333333333333333333333333333333%" 
                                type="button" 
                                class="btn btnDiv btn-success waves-effect waves-light btn-custom">
                            <b>Recebimento de Documentação</b>
                        </button>
                        <button id="borderos_btn"         
                                style="width: 33.333333333333333333333333333333333333333%" 
                                type="button" 
                                class="btn btnDiv btn-danger waves-effect waves-light btn-custom">
                            <b>Protocolos Gerados</b>
                        </button>
                        <button id="malotes_btn"         
                                style="width: 33.333333333333333333333333333333333333333%" 
                                type="button" 
                                class="btn btnDiv btn-warning waves-effect waves-light btn-custom">
                            <b>Protocolos Consolidados</b>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row divRecebimentos" id="all" style="display: none;">
            <div class="col-sm-12">
                <div class="card-box" style="border:none;  margin-top: -40px;">
                    </br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input onkeydown="bloquear_ctrl_j();" placeholder="Contrato" type="text" id="contrato"  class="form-control" style="height: 30px!important;">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <button id="gerar_recebimento" class="btn btn-primary btn-sm" style="width: 100%!important">Gerar Recebimento</button>
                        </div>
                    </div>
                </div>
                <div class="card-box" style="border: none; margin-top: -50px!important">
                    <div class="row" id="propostas">
                        <div class="col-md-12">
                            <table style="font-size: 11px!important;" id="grid_propostas" class="table table-full-width dataTable">
                                <thead>
                                <th>Modalidade</th>
                                <th>Código</th>
                                <th>Filial</th>
                                <th>Cliente</th>
                                <th>CPF</th>
                                <th>Data</th>
                                <th>Financiado</th>
                                <th>Parcelas</th>
                                <th>Repasse</th>
                                </thead>
                                <thead style="background-color: #1db198">
                                <th id="qtd_props"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th width="11%" id="total_fin"></th>
                                <th></th>
                                <th width="11%" id="total_rep"></th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row divBorderos" id="all" style="display: none;">
            <form id="form_print" method="post" target="_blank" action="/documentacao/parceiroConsolidada/">
                <input id="id_rebs" name="id_rebs" type="hidden">
                <button type="submit" class="floating_btn btn btn-danger btn-lg waves-effect waves-light"><i class="glyphicon glyphicon-print"></i></button>
            </form>
            <div class="col-sm-12">
                <div class="card-box" style="border:none;">
                    <div class="row">
                        <div class="col-sm-4">
                            <select id="nucleos" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count"
                                    data-url="/gestorDeFiliais/listarFiliais/"
                                    data-alvo="filiais">

                                <?php foreach (NucleoFiliais::model()->findAll('habilitado') as $nucleo) { ?>
                                    <option value="<?php echo $nucleo->id; ?>" >
                                        <?php echo strtoupper($nucleo->nome); ?>
                                    </option>
                                <?php } ?>

                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select id="filiais" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count">
                                <?php foreach (Filial::model()->findAll('habilitado') as $filial) { ?>
                                    <option value="<?php echo $filial->id; ?>" >
                                        <?php echo $filial->getConcat(); ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button id="filtrar_recebidos" class="btn btn-primary" style="width: 100%!important">Filtrar</button>
                        </div>
                    </div>    
                </div>
                <div class="card-box" style="border:none; margin-top: -50px!important">
                    </br>
                    <div class="row">
                        <div class="col-md-12">
                            <button id="gerar_malotes" class="btn btn-danger btn-sm" style="width: 100%!important">Gerar Relatórios de Impressão</button>
                        </div>
                    </div>
                </div>
                <div class="card-box" style="border: none; margin-top: -50px!important">
                    <div class="row" id="recebidos">
                        <div class="col-md-12">
                            <input id="marcar_todos" data-plugin="switchery" data-size="small" data-color="#f05050" class="checkall" type="checkbox">
                            <label>Marcar Todos</label>
                            <table style="font-size: 11px!important;" id="grid_recebidos" class="table table-full-width dataTable">
                                <thead>
                                <th></th>
                                <th>Código</th>
                                <th>Filial</th>
                                <th>Usuário</th>
                                <th>Data</th>
                                <th width="1%"></th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row divMalotes" id="all" style="display: none;">
            <div class="col-sm-12">
                <div class="card-box" style="border: none;">
                    <div class="row" id="recebidos">
                        <div class="col-md-12">
                            <table style="font-size: 11px!important;" id="grid_documentacao" class="table table-full-width dataTable">
                                <thead>
                                <th>Código</th>
                                <th>Data</th>
                                <th>Usuário</th>
                                <th width="1%"></th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
            <?php $this->renderPartial('/user/form_mudar_primeira_senha'); ?>
        <?php endif ?>
    </div>
    <?php $this->renderPartial('/user/menu', ['css_menu' => 'display:none']); ?>
</div>
<style>
    .dropdown-toggle > span{
        text-align: center!important;
    }
    .bootstrap-select.btn-group .dropdown-menu {
        max-width: 100%!important;
    }
    .has-bigode .select2-container-multi .select2-choices{
        border : 2px solid #76b6fe!important;
        min-height: 38px!important;
        max-height: 100px!important;
        overflow-y: auto;
    }
    #grid_propostas_length, #grid_propostas_filter,
    #grid_propostas_info, #grid_propostas_paginate{
        display: none;
    }
    #grid_recebidos_length, #grid_recebidos_filter,
    #grid_recebidos_info, #grid_recebidos_paginate{
        display: none;
    }
    #grid_documentacao_length, #grid_documentacao_filter,
    #grid_documentacao_info, #grid_documentacao_paginate{
        display: none;
    }

    .dataTables_empty{
        text-align: center;
    }
    .floating_btn{
        z-index: 1;
        position: fixed;
        right: 2.1%;
        bottom: 2%;
        border-radius: 50%;
        display: none;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        padding: 20px 23px !important;
    }

    .floating_btn:hover{
        border: #ffffff 1px solid!important;
        box-shadow: 0 10px 13px rgba(0,0,0,0.16), 0 10px 13px rgba(0,0,0,0.23);
    }
    
    .glyphicon {
        margin-top: 2px;
    }    
</style>
