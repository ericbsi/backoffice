<?php
$util = new Util;
$indice = 1;
$id_atual = 0;
?>
<html>
    <head></head>
    <body>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/vendor/modernizr.js"></script>

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation/foundation.css" >
        <?php
        foreach ($recebimentos as $rec) {
            $processo = Recebimento::model()->findByPk($rec['rec']);
            $totalFinanciado = 0;
            $totalRepasse = 0;

            $id_next = $rec['nf'];
            if ($rec['nf'] != $id_atual) {

                $totalFinanciado2 = 0;
                $totalRepasse2 = 0;
                ?>

                <hr />
                <table role="grid" style="margin:0!important;border-bottom:none!important">
                    <thead>
                        <tr>
                            <th  width="923" style="text-align:center">RECEBIMENTO DE CONTRATOS: <?php echo strtoupper($processo->filial->nucleoFiliais->nome) ?></th>
                        </tr>
                    </thead>
                </table>
                <table role="grid" style="margin:0!important;border-bottom:none!important">
                    <thead>
                        <tr>
                            <th  width="923" style="text-align:left">DATA DE PROCESSAMENTO: <?php echo $util->bd_date_to_view(substr($processo->data_cadastro, 0, 10)) . ' às ' . substr($processo->data_cadastro, 11) ?> </th>
                        </tr>
                    </thead>
                </table>
                <table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
                    <thead>
                        <tr>
                            <th  width="923" style="text-align:center">Protocolos</th>
                        </tr>
                    </thead>
                </table>
                <table role="grid" style="margin:0!important;">
                    <thead>
                        <tr>
                            <th  width="80" style="text-align:left">DATA:</th>
                            <th  width="151" style="text-align:left">CÓDIGO:</th>
                            <th  width="336" style="text-align:left">FILIAL:</th>
                            <th  width="70" style="text-align:left">USUÁRIO:</th>
                            <th  width="139" style="text-align:left">VAL FIN:</th>
                            <th  width="137" style="text-align:left">VAL REPASSE:</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php } ?>
                    <?php
                    foreach ($processo->itemRecebimentos as $item) {
                        $totalRepasse += +$item->proposta->valorRepasse();
                    }

                    foreach ($processo->itemRecebimentos as $item) {
                        $totalFinanciado += ($item->proposta->valor - $item->proposta->valor_entrada);
                    }
                    foreach ($processo->itemRecebimentos as $item) {
                        $totalRepasse2 += +$item->proposta->valorRepasse();
                    }

                    foreach ($processo->itemRecebimentos as $item) {
                        $totalFinanciado2 += ($item->proposta->valor - $item->proposta->valor_entrada);
                    }
                    ?>
                    <tr>
                        <td><?php echo $util->bd_date_to_view(substr($processo->data_cadastro, 0, 10)) ?></td>
                        <td><?php echo date('Ymd', strtotime($processo->data_cadastro)) . str_pad($processo->id, 6, "0", STR_PAD_LEFT) ?></td>
                        <td><?php echo $processo->filial->getConcat() ?></td>
                        <td><?php echo strtoupper($processo->usuario->nome_utilizador) ?></td>
                        <td><?php echo "R$ " . number_format($totalFinanciado, 2, ',', '.') ?></td>
                        <td><?php echo "R$ " . number_format($totalRepasse, 2, ',', '.') ?></td>
                    </tr>
                    <?php if ($indice < count($recebimentos)) { ?>
                        <?php if ($id_next != $recebimentos[$indice]['nf']) { ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>TOTAL: <?php echo "R$ " . number_format($totalFinanciado2, 2, ',', '.'); ?></th>
                                <th>TOTAL: <?php echo "R$ " . number_format($totalRepasse2, 2, ',', '.'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                    <table role="grid" style="margin:0!important">
                        <thead>
                            <tr>
                                <th  width="481" style="text-align:left">GERADOR POR <?php echo strtoupper($processo->usuario->nome_utilizador); ?> EM <?php echo $util->bd_date_to_view(substr($processo->data_cadastro, 0, 10)) . ' às ' . substr($processo->data_cadastro, 11) ?></th>
                                <th  width="440" style="text-align:left">RECEBIDO POR ___________________________________ EM <?php echo date('d/m/Y') ?> às <?php echo date('H:i:s') ?> </th>
                            </tr>
                            <tr>
                                <th  width="475" style="text-align:left">ASS: _________________________________</th>
                                <th  width="440" style="text-align:left">DOC: _____________________________, ASS: _____________________________</th>
                            </tr>
                        </thead>
                    </table>
                    <hr />
                    <span class="page-break"></span>
                <?php } ?>
            <?php } else { ?>
            </tbody>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>TOTAL: <?php echo "R$ " . number_format($totalFinanciado2, 2, ',', '.'); ?></th>
                <th>TOTAL: <?php echo "R$ " . number_format($totalRepasse2, 2, ',', '.'); ?></th>
            </tr>
        </tfoot>
        </table>
        <table role="grid" style="margin:0!important">
            <thead>
                <tr>
                    <th  width="481" style="text-align:left">GERADOR POR <?php echo strtoupper($processo->usuario->nome_utilizador); ?> EM <?php echo $util->bd_date_to_view(substr($processo->data_cadastro, 0, 10)) . ' às ' . substr($processo->data_cadastro, 11) ?></th>
                    <th  width="440" style="text-align:left">RECEBIDO POR ___________________________________ EM <?php echo date('d/m/Y') ?> às <?php echo date('H:i:s') ?> </th>
                </tr>
                <tr>
                    <th  width="475" style="text-align:left">ASS: _________________________________</th>
                    <th  width="440" style="text-align:left">DOC: _______________________________, ASS: _______________________________</th>
                </tr>
            </thead>
        </table>
        <hr />
    <?php } ?>
    <?php
    $id_atual = $rec['nf'];
    $indice++;
}
?>
</body>      </html>
<style type="text/css">

    table tr th, table tr td{
        font-size: 0.480rem;
    }

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

    thead {display: table-header-group;}
    tfoot {display: table-header-group;}

    #grid_recebido_filter {
        display: none;
    }

</style>