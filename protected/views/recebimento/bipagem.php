<div class="content">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Formalização de Propostas</h4>
            </div>
        </div>
        <div class="row" id="all">
            <div class="col-sm-12">
                <div class="card-box" style="border:none;">
                    </br>
                    <div class="row">
                        <div class="col-md-12">
                            <select id="select-modo" class="selectpicker" data-style="btn-default btn-custom">
                                <option value="0" disabled="" selected>Selecione o Modo de Bipagem:</option>
                                <option value="1">Imagem</option>
                                <option value="2">Físico</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-box" style="border: none;">
                    <div class="row" id="gcpl" style="display:none;">
                        <div class="col-md-12">
                            <div class="btn-group btn-group-justified">
                                <button id="geracao_cnabs"   
                                        style="width: 33.333333333333333%" 
                                        type="button" 
                                        class="btn btnDiv btn-success waves-effect waves-light btn-custom">
                                    <b>Geração de CNABs</b>
                                </button>
                                <button id="pre_lotes"         
                                        style="width: 33.333333333333333%" 
                                        type="button" 
                                        class="btn btnDiv btn-warning waves-effect waves-light btn-custom">
                                    <b>Pré Lotes</b>
                                </button>
                                <button id="dev_propostas"         
                                        style="width: 33.333333333333333%" 
                                        type="button" 
                                        class="btn btnDiv btn-danger waves-effect waves-light btn-custom">
                                    <b>Devolver Contratos</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-box" style="margin-top: -50px!important; border:none;display:none;" id="filtros_imagem">
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-bigode">
                                <select id="selectNucleo" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count"
                                        data-url="/gestorDeFiliais/listarFiliais/"
                                        data-alvo="selectFilial">

                                    <?php foreach (NucleoFiliais::model()->findAll('habilitado') as $nucleo) { ?>
                                        <option value="<?php echo $nucleo->id; ?>" >
                                            <?php echo strtoupper($nucleo->nome); ?>
                                        </option>
                                    <?php } ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-bigode">
                                <select id="selectFilial" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count">
                                    <?php foreach (Filial::model()->findAll('habilitado') as $filial) { ?>
                                        <option value="<?php echo $filial->id; ?>" >
                                            <?php echo $filial->getConcat(); ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <br>                        
                    </div>
                </div>
                <div class="card-box" style="margin-top: -50px!important; border:none;display:none;" id="filtros_imagem2">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input placeholder="Data Inicial" type="text" id="data_de" name="data_de" class="form-control" onfocus="(this.type = 'date')" onblur="(this.type = 'text')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input placeholder="Data Final" type="text" id="data_ate" name="data_ate" class="form-control" onfocus="(this.type = 'date')" onblur="(this.type = 'text')">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input onkeydown="bloquear_ctrl_j();" placeholder="Contrato" type="text" id="contrato"  class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input placeholder="Valor Max (R$)" type="text" id="valor_max" name="valor_max" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button id="filtrar" class="btn btn-primary btn-sm" style="width: 100%!important">Filtrar</button>
                        </div>
                        <div class="col-md-6">
                            <button id="gerar_cnab" style="width: 100%" class="btn btn-danger btn-sm">Gerar CNAB</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget-inline-box text-center">
                                <h3><i class="text-primary md md-list"></i> <b id="props_selects">0</b></h3>
                                <h4 class="text-muted">Qtd. Propostas</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget-inline-box text-center">
                                <h3><i class="text-custom md md-attach-money"></i> <b id="props_fin">0,00</b></h3>
                                <h4 class="text-muted">Total Financiado</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget-inline-box text-center">
                                <h3><i class="text-pink md md-attach-money"></i> <b id="props_rep">0,00</b></h3>
                                <h4 class="text-muted">Total Repasse</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="widget-inline-box text-center b-0">
                                <h3><i class="text-purple md md-pin-drop"></i> <b id="props_ncl">0</b></h3>
                                <h4 class="text-muted">Qtd. Núcleos</h4>
                            </div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-12">
                            <input id="marcar_todos" class="checkall" type="checkbox">
                            <label>Marcar Todas</label>
                        </div>
                    </div>
                    <table style="font-size: 11px!important;" id="grid_propostas" class="table table-full-width dataTable">
                        <thead>
                        <th></th>
                        <th>Modalidade</th>
                        <th>Código</th>
                        <th>Filial</th>
                        <th>Cliente</th>
                        <th>CPF</th>
                        <th>Data</th>
                        <th>Financiado</th>
                        <th>Parcelas</th>
                        <th>Repasse</th>
                        </thead>
                        <thead style="background-color: #1db198">
                        <th colspan="2" id="qtd_props"></th>
                        <th width="1%"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th width="10%" id="total_fin"></th>
                        <th width="8%"></th>
                        <th width="10%" id="total_rep"></th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="card-box" style="margin-top: -50px!important;border:none;display:none;" id="filtros_pl2">
                    <div class="row">
                        <div class="col-md-8"></div>
                        <form action="/recebimento/importarDP" method="post" id="form_importacao" class="form-horizontal" enctype="multipart/form-data">
                            <div style="padding-top: 18px;" class="col-md-2">
                                <input class="anexo_importacao" name="anexo_importacao" id="anexo_importacao" type="file" class="form-control">
                                <label id="anexo_label" for="anexo_importacao"><i class="fa fa-refresh" aria-hidden="true"></i> Selecionar Arquivo</label>
                            </div>
                            <div style="padding-top: 18px;" class="col-md-2">
                                <button style="width: 100%; border-radius: 3px" id="submit_arquivo" class="btn btn-success btn-sm" type="submit"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Importar DP</button>
                            </div>
                        </form>
                    </div>
                    <table style="font-size: 11px!important;" id="grid_plotes" class="table table-full-width dataTable">
                        <thead>
                        <th></th>
                        <th>Data</th>
                        <th>Qtd Borderos</th>
                        <th>Qtd Propostas</th>
                        <th>Usuário</th>
                        <th>Financiado</th>
                        <th>Repasse</th>
                        <th>Status</th>
                        <th></th>
                        <th width="1%"></th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="card-box" style="margin-top: -25px!important;border:none;display:none;" id="filtros_contratos">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group has-bigode">
                                <input onkeydown="bloquear_ctrl_j();" placeholder="Contrato" type="text" id="contrato_dev"  class="form-control input-sm">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group has-bigode">
                                <button id="filtrar_dev" class="btn btn-primary btn-sm" style="width: 100%!important">Pesquisar</button>
                            </div>
                        </div>
                    </div>
                    <table style="font-size: 11px!important;" id="grid_devs" class="table table-full-width dataTable">
                        <thead>
                        <th>Data (Pre Lote)</th>
                        <th>Codigo</th>
                        <th>Parceiro</th>
                        <th>Cliente</th>
                        <th>CPF</th>
                        <th>Financiado</th>
                        <th>Repasse</th>
                        <th></th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
            <?php $this->renderPartial('/user/form_mudar_primeira_senha'); ?>
        <?php endif ?>
    </div>
    <?php $this->renderPartial('/user/menu', ['css_menu' => 'display:none']); ?>
</div>
<!--CSS-->
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/css/login/font-awesome.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/select2/select2.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/css/multipleselect.css'); ?>
<?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css'); ?>
<!--JS-->
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile('/assets/ubold/js/formalizacao/fn-formalizacao-de-propostas-v11.js', CClientScript::POS_END); ?>
<style>
    .dropdown-toggle > span{
        text-align: center!important;
    }
    .bootstrap-select.btn-group .dropdown-menu {
        max-width: 100%!important;
    }
    .has-bigode .select2-container-multi .select2-choices{
        border : 2px solid #76b6fe!important;
        min-height: 38px!important;
        max-height: 100px!important;
        overflow-y: auto;
    }
    
    #grid_devs_length, #grid_devs_filter,
    #grid_devs_info, #grid_devs_paginate{
        display: none;
    }
    #grid_propostas_length, #grid_propostas_filter,
    #grid_propostas_info, #grid_propostas_paginate{
        display: none;
    }
    #grid_plotes_length, #grid_plotes_filter,
    #grid_plotes_info, #grid_plotes_paginate{
        display: none;
    }
    #arquivo-detalhado_length, #arquivo-detalhado_filter,
    #arquivo-detalhado_info, #arquivo-detalhado_paginate{
        display: none;
    }
    #pl-detalhado_length, #pl-detalhado_filter,
    #pl-detalhado_info, #pl-detalhado_paginate{
        display: none;
    }
    #bordero-detalhado_length, #bordero-detalhado_filter,
    #bordero-detalhado_info, #bordero-detalhado_paginate{
        display: none;
    }
    #arquivo-detalhado, #pl-detalhado{
        border:none;
    }
    #arquivo-detalhado .even, #pl-detalhado .even{
        background: #364554 !important;
    }
    #arquivo-detalhado thead tr, #pl-detalhado thead tr{
        background: #364554 !important;
    }
    #arquivo-detalhado .odd, #pl-detalhado .odd{
        background: #364554 !important;
    }
    #arquivo-detalhado tr:not(.odd), #pl-detalhado tr:not(.odd){
        background: #364554!important;
    }
    input#anexo_importacao {
        position: absolute;
        filter: alpha(opacity=0);
        opacity: 0;
    }
    input#anexo_importacao + label {
        border: 1px solid #CCC;
        border-radius: 3px;
        text-align: left;
        padding: 6px;
        width: 100%;
        height: 30px;
        left: 0;
        position: relative;
        text-align: center;
        background: #ffbd4a;
        color: #fff;
        border: none;
        cursor: pointer;
    }
    .dataTables_empty{
        text-align: center;
    }
    ul.dropdown-menu{
        text-align: center;
    }
    .sorting_asc:after{
        display: none!important;
    }
</style>
