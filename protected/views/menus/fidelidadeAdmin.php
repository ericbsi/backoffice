<?php ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->request->baseUrl; ?>/premios/">
         <i class="fa fa-home">
         </i>
         <span class="title"> 
            Inicial 
         </span>
         <span class="selected">
         </span>
      </a>
   </li>
   <li>
      <a href="/premios/adminPremios">
         <i class="clip-cart">
         </i>
         <span class="title">
            Administrar de Prêmios
         </span>
         <span class="selected">
         </span>
      </a>
   </li>
   <li>
      <a href="/venda/adminResgates/">
         <i class="clip-list-4"></i>
         <span class="title">Administrar Resgates</span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="/venda/adminOrdemEntrega/">
         <i class="clip-truck"></i>
         <span class="title">Administrar Entregas</span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="/usuario/admin/">
         <i class="fa fa-group"></i>
         <span class="title">Administrar Usuários</span>
         <span class="selected"></span>
      </a>
   </li>
</ul>