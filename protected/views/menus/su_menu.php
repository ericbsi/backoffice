<?php $util = new Util; ?>
<ul class="main-navigation-menu">
   
   <li>
      <a href="javascript:void(0)"><i class="fa fa-home"></i>
      <span class="title"> Empresas </span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/empresa/create">
               <i class="fa fa-edit"></i>
               <span class="title"> Cadastrar Empresa </span>
            </a>
        </li>
        <li>
         <a href="<?php echo Yii::app()->request->baseUrl;?>/empresa/admin">
            <i class="clip-list"></i>
            <span class="title"> Administrar Empresas </span>
         </a>
        </li>
      </ul>
   </li>
   <li>
      <a href="javascript:void(0)">
         <i class="fa fa-sitemap"></i>
         <span class="title">Financeiras</span><i class="icon-arrow"></i>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeira/create">
               <i class="fa fa-edit"></i>
               <span class="title"> Cadastrar Financeira </span>
            </a>
        </li>        
        <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeira/admin">
               <i class="fa fa-edit"></i>
               <span class="title"> Administrar Financeira </span>
            </a>
        </li>
      </ul>
   </li>
   <?php if ( $util->countEmpresas() > 0 ) { ?>
     <li>
        <a href="javascript:void(0)"><i class="clip-users"></i>
        <span class="title"> Usuarios </span><i class="icon-arrow"></i>
        <span class="selected"></span>
        </a>
        <ul class="sub-menu">
          <li>
              <a href="<?php echo Yii::app()->request->baseUrl;?>/usuario/create">
                 <i class="clip-user-plus"></i>
                 <span class="title"> Cadastrar Usuário </span>
              </a>
          </li>
          <li>
           <a href="<?php echo Yii::app()->request->baseUrl;?>/usuario/admin">
              <i class="clip-list"></i>
              <span class="title"> Administrar Usuários </span>
           </a>
          </li>
        </ul>
     </li>
   <?php } ?>
</ul>