<div class="content">
   <?php $this->renderPartial( '/navigation/navigation_menu' ); ?>
   <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30">Resumo de informações: <?= mb_strtoupper($perfil->label); ?></h4>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-6 col-sm-6">
            <div class="widget-panel widget-style-2 card-box">
               <i class="md md-account-child text-custom"></i>
               <button class="btn btn-icon waves-effect waves-light btn-credshow-orange"><?= count( $perfil->getUsuarios(1,1) );  ?></button>
               <div class="text-muted m-t-5">Usuários Ativos</div>
            </div>
         </div>
         <div class="col-lg-6 col-sm-6">
            <div class="widget-panel widget-style-2 card-box">
               <i class="md md-sort text-custom"></i>
               <button id="label-niveis-count" data-toggle="modal" data-target="#con-close-modal"  class="btn btn-icon waves-effect waves-light btn-credshow-orange"> <?= count( $perfil->getNiveisDeAcesso(1,[1,2]) ) ?></button>
               <div class="text-muted m-t-5">Níveis de Acesso</div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <input type="hidden" value="<?= $perfil->id ?>" id="Role_Id">
            <div class="portlet">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Níveis de Acesso - <?= mb_strtoupper($perfil->label); ?>
                  </h3>
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="false" class="collapsed"><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default2" class="panel-collapse collapse out">
                  <div class="portlet-body">
                     <table id="datatable-niveis-acessos" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <th width="40"></th>
                              <th>Nome</th>
                              <th>Descrição</th>
                              <th>Status</th>
                              <th width="40">Editar</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                  <h4 class="modal-title"> <?= mb_strtoupper('Cadastrar Novo Nível de Acesso - ' . $perfil->label); ?></h4>
               </div>
               <form id="form-add-novo-nivel">
	               <div class="modal-body">
	               	
	                  <div class="row">
	                     <div class="col-md-12">
	                        <div class="form-group"> 
	                           <label for="field-3" class="control-label">Nome</label> 
	                           <input id="nivelrole_nome" required name="NivelRole[nome]" type="text" class="form-control" placeholder="Nome"> 
	                           <input name="NivelRole[Role_id]" type="hidden" class="form-control" value="<?= $perfil->id ?>">
	                           <input id="nivelrole_id" name="Nivel_Role_id" type="hidden" class="form-control" value="0">
	                        </div>
	                     </div>
	                  </div>
	                  
	                  <div class="row">
	                     <div class="col-md-12">
	                        <div class="form-group no-margin"> 
	                           <label for="field-7" class="control-label">Descrição</label> 
	                           <textarea id="nivelrole_descricao" required name="NivelRole[descricao]" class="form-control autogrow" placeholder="Descreve em poucas palavras o novo nível de acesso" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
	                        </div>
	                     </div>
	                  </div>
	                
	               </div>
	               <div class="modal-footer"> 
	                  <button type="button" class="btn btn-login-credshow waves-effect" data-dismiss="modal">Fechar</button> 
	                  <button type="submit" class="btn btn-credshow-orange waves-effect waves-light">Salvar Informações</button> 
	               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>
</div>
<style type="text/css">
   #datatable-niveis-acessos_length, #funcoesGrid_length{
      display: none;
   }
   td.details-control{
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
   #funcoesGrid{
      border:none;
   }
   #funcoesGrid .even{
      background: #424e5b !important;
   }
   #funcoesGrid thead tr{
      background: #303841 !important;
   }
   #funcoesGrid .odd{
      background: #424E5B !important;
   }
   .checkbox label::before {
   -o-transition: 0.3s ease-in-out;
   -webkit-transition: 0.3s ease-in-out;
   background-color: transparent;
   border-radius: 3px;
   border: 2px solid #98a6ad;
   content: "";
   display: inline-block;
   height: 17px;
   left: 0;
   margin-left: -20px;
   position: absolute;
   transition: 0.3s ease-in-out;
   width: 17px;
   outline: none !important;
   }
</style>