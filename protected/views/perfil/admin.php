<div class="content">
	
	<?php $this->renderPartial( '/navigation/navigation_menu' ); ?>
    <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            <div class="portlet" id="portlet-form">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Incluir Novo Perfil de Acesso
                  </h3>

                  <div class="portlet-widgets">
                     <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class="collapsed"><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default" class="panel-collapse collapse out">
                  <div class="portlet-body">
                     <form id="form-role" class="form-horizontal group-border-dashed" action="#">
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Descrição do Perfil</label>
                           <div class="col-sm-6">
                              <textarea id="role_descricao" placeholder="Descrição" name="Role[descricao]" required class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Etiqueta</label>
                           <div class="col-sm-6">
                            
                              <input  name="Role[label]" type="text" id="role_label" class="form-control" required placeholder="Etiqueta" />
                              <input  name="RoleId" id="role_id" type="hidden" value="0" />
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-3 col-sm-9 m-t-15">
                              <button type="submit" class="btn btn-login-credshow">Salvar</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="portlet">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Perfis Disponíveis
                  </h3>
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default2" class="panel-collapse collapse in">
                  <div class="portlet-body">

                     <table id="datatable-roles" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <th width="40">Funções</th>
                              <th>Etiqueta</th>
                              <th>Descrição</th>
                              <th>Status</th>
                              <th width="40">Editar</th>
                              <th width="40">Configurar</th>
                           </tr>
                        </thead>

                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
	<?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>
</div>

<style type="text/css">
   td.details-control{
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
   #datatable-roles_length, #funcoesGrid_length{
      display: none;
   }
   #funcoesGrid{
      border:none;
   }
   #funcoesGrid .even{
      background: #424e5b !important;
   }
   #funcoesGrid thead tr{
      background: #303841 !important;
   }
   #funcoesGrid .odd{
      background: #424E5B !important;
   }
   .checkbox label::before {
   -o-transition: 0.3s ease-in-out;
   -webkit-transition: 0.3s ease-in-out;
   background-color: transparent;
   border-radius: 3px;
   border: 2px solid #98a6ad;
   content: "";
   display: inline-block;
   height: 17px;
   left: 0;
   margin-left: -20px;
   position: absolute;
   transition: 0.3s ease-in-out;
   width: 17px;
   outline: none !important;
   }
</style>