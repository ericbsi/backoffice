<div class="content">

    <?php $this->renderPartial('/navigation/navigation_menu'); ?>


    <div class="container" id="action-content">

        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Relatório Fidc - Recebimentos</h4>
            </div>
        </div>

        <div class="row" id="select-status-wrapper" style="">
            <div class="col-sm-12">
                <form class="form-inline">
                    <div class="form-group m-r-10">
                        <label for="exampleInputName2">Situação:</label>
                        <select name="situacao" required="" class="form-control" id="selectSituacao">
                            <option value="0">SELECIONE:</option>
                            <option value="1">PAGO</option>
                            <option value="2">EM ABERTO</option>
                        </select>
                    </div>
                    <div class="form-group m-r-10">
                        <label for="de">De?</label>
                        <input readonly id="data_de" type="text" class="datepicker form-control" value="<?php echo date('01/01/Y') ?>">
                    </div>
                    <div class="form-group m-r-10">
                        <label for="ate">Até:</label>
                        <input id="data_ate" type="text" class="datepicker form-control" value="<?php echo date('t/12/Y') ?>">
                    </div>
                    <div class="form-group m-r-10">
                        <button id="exportbtn" type="button" class="btn btn-success waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-file-excel-o"></i> </span>Exportar
                        </button>
                    </div>
                </form>
                <form method="POST" id="exportform" action="https://s1.sigacbr.com.br/printer/recebimentoFidc/" target="_blank">
                    <input type="hidden" name="queryXlsExport" id="queryXlsExport">
                </form>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="grid_recebimentos" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>CPF</th>
                                <th>Sacado</th>
                                <th>Nosso Número</th>
                                <th>Numero do Documento</th>
                                <th>Tipo Recebivel</th>
                                <th>Valor Nominal</th>
                                <th>Valor Pago</th>
                                <th>Data Venci.</th>
                                <th>Data Baixa</th>
                                <th>Situação recebível</th>                               
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css'); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/assets/ubold/js/fidc/fn-recebimentos.js?version=1.3', CClientScript::POS_END); ?>

</div>
<style type="text/css">
    #grid_recebimentos_length, #grid_recebimentos_filter{
        display: none;
    }
</style>