<div class="content">
   <?php $this->renderPartial( '/navigation/navigation_menu' ); ?>
   <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
         <!---->
         <div class="row">
            <div class="col-lg-12">
               <div class="portlet" id="portlet-form">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Incluir Novo Usuário</h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class="collapsed"><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default" class="panel-collapse collapse out">
                     <div class="portlet-body">
                        <form id="form-user" class="form-horizontal group-border-dashed" action="#">
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Nome / CPF</label>
                              <div class="col-sm-3">
                                 <input  name="Usuario[nome_utilizador]" type="text" id="usuario_nome_utilizador" class="form-control" required placeholder="Nome" />
                              </div>
                              <div class="col-sm-3">
                                 <input  name="Documento[cpf]" type="text" id="usuario_cpf" class="form-control cpf" required placeholder="CPF" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Username / E-mail</label>
                              <div class="col-sm-3">
                                 <input  name="Usuario[username]" type="text" id="usuario_username" class="form-control" required placeholder="Username" />
                                 <input  name="UsuarioId" id="usuario_id" type="hidden" value="0" />
                              </div>
                              <div class="col-sm-3">
                                 <input  name="Contato[email]" type="text" id="usuario_email" class="form-control email" required placeholder="E-mail" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Senha / Confirmar Senha</label>
                              <div class="col-sm-3">
                                 <input name="Usuario[password]" type="password" id="senha" class="form-control" required="" placeholder="Senha">
                              </div>
                              <div class="col-sm-3">
                                 <input name="senha2" id="senha2" type="password" class="form-control" required="" placeholder="Re-digite sua Senha">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Perfil Principal / Outros perfis</label>
                              <div class="col-sm-3">
                                 <select name="perfil_principal" required="" class="form-control" id="select_main_role" data-placeholder="Perfil principal ...">
                                    <option></option>
                                    <?php foreach( Role::model()->findAll() as $perfil ): ?>
                                    <optgroup label="<?= mb_strtoupper($perfil->label)  ?>">
                                       <?php foreach( $perfil->getNiveisDeAcesso(1) as $nivel ): ?>
                                       <option value="<?= $nivel->id; ?>" ><?= mb_strtoupper($perfil->label).' - '.mb_strtoupper($nivel->nome); ?></option>
                                       <?php endforeach; ?>
                                    </optgroup>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                              <div class="col-sm-3">
                                 <select name="perfis_secundarios[]" id="select_outros_roles" class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Perfis secundários ..." >
                                    <option></option>
                                    <?php foreach( Role::model()->findAll() as $perfil ): ?>
                                    <optgroup label="<?= mb_strtoupper($perfil->label)  ?>">
                                       <?php foreach( $perfil->getNiveisDeAcesso(1) as $nivel ): ?>
                                       <option value="<?= $nivel->id; ?>" ><?= mb_strtoupper($perfil->label).' - '.mb_strtoupper($nivel->nome); ?></option>
                                       <?php endforeach; ?>
                                    </optgroup>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                 <button type="submit" class="btn btn-login-credshow">Salvar</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>
      <div class="row">
         <div class="row">
            <div class="col-sm-12">
               <div class="portlet">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">
                        Usuários cadastrados
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default2" class="panel-collapse in">
                     <div class="portlet-body">
                        <table id="datatable-usuarios" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th class="searchable">
                                    <input style="width:100%!important" name="nome_utilizador" type="text" id="usuario_nome_utilizador_filter" class="form-control filter" required placeholder="Filtrar por Nome" />
                                 </th>
                                 <th class="searchable">
                                    <input style="width:100%!important" name="username" type="text" id="usuario_username_filter" class="form-control filter" required placeholder="Filtrar por Username" />
                                 </th>
                                 <th></th>
                                 <th></th>
                              </tr>
                              <tr>
                                 <th width="40"></th>
                                 <th>Nome</th>
                                 <th>Username</th>
                                 <th>E-mail</th>
                                 <th width="20">Hab/Des</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!---->
      </div>
   </div>
   <?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>
</div>
<style type="text/css">

#datatable-usuarios_length, #papeisGrid_length{
  display: none;
}

td.details-control{
   background: url('../../images/details_open.png') no-repeat center center;
   cursor: pointer;
}
tr.shown td.details-control {
   background: url('../../images/details_close.png') no-repeat center center;
}

#papeisGrid{
   border:none;
}
#papeisGrid .even{
   background: #424e5b !important;
}
#papeisGrid thead tr{
   background: #303841 !important;
}
#papeisGrid .odd{
   background: #424E5B !important;
}
input.filter{
   border:none!important;
   background: transparent!important;
}
::-webkit-input-placeholder {
   font-style: italic;
}

</style>