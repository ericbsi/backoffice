<div class="content">

	<?php $this->renderPartial( '/navigation/navigation_menu' ); ?>

	<div class="container" id="action-content">

		<div class="row">
	        <div class="col-sm-12">
	        	<h4 class="page-header  header-title m-b-30">Vita Promotora - Propostas</h4>
	        </div>
      	</div>

      	<div class="row">
         <div class="col-md-12">
            <div class="portlet" id="portlet-filtros">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Filtros
                  </h3>
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" href="#bg-filtros" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-filtros" class="panel-collapse collapse in table-responsive">
                  <div class="portlet-body">
                     <form class="form-inline" role="form">
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>Status:</b></h5>
                              <select name="situacao" required="" class="form-control" id="selectStatus">
                                 <option value="0">SELECIONE:</option>
                                 <?php foreach (StatusProposta::model()->findAll( 'id IN(1,2,3,8,9)' ) as $status): ?>
                                 <option value="<?php echo $status->id ?>">
                                    <?php echo mb_strtoupper($status->status); ?>
                                 </option>
                                 <?php endforeach ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>De:</b></h5>
                              <input readonly id="de" type="text" class="form-control date" value="<?php echo date('01/m/Y') ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>Até:</b></h5>
                              <input readonly id="ate" type="text" class="form-control date" value="<?php echo date('d/m/Y') ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>Usuário:</b></h5>
                              <input id="f_usuario" type="text" class="form-control">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>CPF:</b></h5>
                              <input id="f_cpf" type="text" class="form-control">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12">
                              <h5><b>&nbsp;</b></h5>
                              <button id="btn-update" type="button" class="btn btn-icon waves-effect waves-light btn-success">Filtrar</button>
                           </div>
                        </div>
                        
                     </form>
                  </div>
               </div>
            </div>
         </div>
      	</div>

      	<div class="row">
         <div class="col-sm-12">
            <div class="portlet" id="portlet-form">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Propostas
                  </h3>
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default" class="panel-collapse collapse in table-responsive">
                  <div class="portlet-body">
                     <table id="grid_producao" class="table table-striped">
                        <thead>
                           <tr>
                              <th width="40">Diálogo</th>
                              <th width="">Código</th>
                              <th width="">Cliente</th>
                              <th width="">CPF</th>
                              <th>Valor</th>
                              <th>Usuário</th>
                              <th>Loja</th>
                              <th>Data</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

	</div>
	<?php if (Yii::app()->session['usuario']->primeira_senha): ?>
    	<?php $this->renderPartial( '/user/form_mudar_primeira_senha' ); ?>
    <?php endif ?>
	<?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>

</div>

<style type="text/css">
	#grid_producao_filter, #grid_producao_length, #mensagensGrid_filter, #mensagensGrid_length
	{
		display: none;
	}
   td.details-control{
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
   #grid_producao{
      border:none;
   }
   #grid_producao .even{
      background: #424e5b !important;
   }
   #grid_producao thead tr{
      background: #303841 !important;
   }
   #grid_producao .odd{
      background: #424E5B !important;
   }
</style>