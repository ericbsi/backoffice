<?php
   setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
   date_default_timezone_set('America/Sao_Paulo');

   $util = new Util;

?>
<div class="content">
   
   <?php $this->renderPartial( '/navigation/navigation_menu' ); ?>

   <?php

      $dados = Titulo::model()->listarLotesPagamento(1, 0, 5, '1', NULL, NULL, '0');

   ?>

   <div class="container" id="action-content">
      
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30">Gestor de Filiais - Painel Inicial</h4>
         </div>
      </div>
      
      <!--Linha 1-->
      <div class="row">
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box fadeInDown animated">
               <div class="bg-icon bg-info pull-left">
                  <i class="md md-attach-money text-white"></i>
               </div>
               <div class="text-right">
                  <h3 class=""><b class="counter">Vendas / Mês</b></h3>
                  <h3 class=""><b id="indicador_vendas_mes" class="counter">0,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-pink pull-left">
                  <i class="md md-add-shopping-cart text-white"></i>
               </div>
               <div class="text-right">
                  <h3 class=""><b class="counter">N° de Vendas</b></h3>
                  <h3 class=""><b id="indicador_n_vendas_mes" class="counter">0,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-purple pull-left">
                  <i class="md md-equalizer text-white"></i>
               </div>
               <div class="text-right">
                  <h3 class=""><b class="counter">Venda Média</b></h3>
                  <h3 class=""><b id="indicador_valor_medio_venda" class="counter">0,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="col-md-6 col-lg-3">
            <div class="widget-bg-color-icon card-box">
               <div class="bg-icon bg-success pull-left">
                  <i class="md md-remove-red-eye text-white"></i>
               </div>
               <div class="text-right">
                  <h3 class=""><b class="counter">Vendas / Dia</b></h3>
                  <h3 class=""><b id="indicador_vendas_dia" class="counter">0,00</b></h3>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      
      <!--Linha 2-->
      <div class="row">

         <div class="col-lg-4">
            <div class="card-box">
               <h4 class="m-t-0 header-title"><b>Aprovado / Negado <?php //echo strftime('%B', strtotime('today'));?> </b></h4>
               
               <ul class="list-inline chart-detail-list text-center">
                  <li>
                     <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec"></i>Aprovadas</h5>
                  </li>
                  <li>
                     <h5><i class="fa fa-circle m-r-5" style="color: #f05050"></i>Negadas</h5>
                  </li>
               </ul>
               <canvas id="lineChart" height="300"></canvas>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="card-box">
               <h4 class="m-t-0 header-title"><b>Análise de vendas / Dia <?php //echo strftime('%Y', strtotime('today'));?> </b></h4>
               <ul class="list-inline chart-detail-list text-center">
                  <li>
                     <h5>&nbsp;</h5>
                  </li>
               </ul>
            <canvas id="bar" height="300"></canvas>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="card-box" style="height:420px;">
               <h4 class="m-t-0 header-title"><b>Lotes a receber (Resumo) </b></h4>

               <div class="p-20">
                  <table class="table table-striped m-0">
                     <thead>
                        <tr>
                           <th>Lote</th>
                           <th>Data</th>
                           <th>Valor</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ( $dados['data'] as $lote ): ?>
                           <tr>
                              <td><?php echo $lote['codigo']; ?></td>
                              <td><?php echo $lote['dataCadastro'];  ?></td>
                              <td><?php echo $lote['valor']; ?></td>
                           </tr>
                        <?php endforeach; ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th></th>
                           <th></th>
                           <th><?php echo "R$ " .$dados['customReturn']['total']; ?></th>
                        </tr>
                     </tfoot>
                  </table>
               </div>

            </div>

         </div>
        
      </div>
      
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box">
               <h4 class="m-t-0 header-title"><b>Análise anual <?php //echo strftime('%Y', strtotime('today'));?> </b></h4>
               
               <ul class="list-inline chart-detail-list text-center">
                  <li>
                     <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec"></i>Aprovadas</h5>
                  </li>
                  
                  <li>
                     <h5><i class="fa fa-circle m-r-5" style="color: #f05050"></i>Negadas</h5>
                  </li>
                  
               </ul>
               <canvas id="lineChart2" height="300"></canvas>
            </div>
         </div>
      </div>

      <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
         <?php $this->renderPartial( '/user/form_mudar_primeira_senha' ); ?>
      <?php endif ?>
   </div>
   <?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>
</div>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/js/blockInterface.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/plugins/Chart.js/Chart.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/pages/jquery.nvd3.init.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/plugins/rickshaw-chart/rickshaw.min.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/js/gestorDeFiliais/fn-gestor-de-filiais-index.js', CClientScript::POS_END); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl. '/assets/ubold/js/gestorDeFiliais/fn-gestor-de-filiais-index.js', CClientScript::POS_END); ?>

<?php //Yii::app()->clientScript->registerCssFile('/assets/ubold/plugins/nvd3/build/nv.d3.min.css'); ?>

<style type="text/css">
   #datatable-usuarios_length, #papeisGrid_length{
   display: none;
   }
   td.details-control{
   background: url('../../images/details_open.png') no-repeat center center;
   cursor: pointer;
   }
   tr.shown td.details-control {
   background: url('../../images/details_close.png') no-repeat center center;
   }
   #papeisGrid{
   border:none;
   }
   #papeisGrid .even{
   background: #424e5b !important;
   }
   #papeisGrid thead tr{
   background: #303841 !important;
   }
   #papeisGrid .odd{
   background: #424E5B !important;
   }
   input.filter{
   border:none!important;
   background: transparent!important;
   }
   ::-webkit-input-placeholder {
   font-style: italic;
   }
</style>