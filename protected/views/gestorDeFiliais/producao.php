<?php
    $queryNucleos  = " SELECT DISTINCT (NF.id) as 'NfId', NF.nome as 'Nucleo'  FROM `AdminHasParceiro` AS AHP";
    $queryNucleos .= " INNER JOIN Filial AS F ON F.id = AHP.Parceiro";
    $queryNucleos .= " INNER JOIN NucleoFiliais AS NF ON NF.id = F.NucleoFiliais_id";
    $queryNucleos .= " WHERE AHP.`Administrador` = " . Yii::app()->session['usuario']->id . " AND AHP.habilitado ";


    $queryGrupos   = " SELECT DISTINCT (GF.id) as 'GfId', GF.nome_fantasia as 'Grupo'  FROM `AdminHasParceiro` AS AHP ";
    $queryGrupos  .= " INNER JOIN Filial AS F ON F.id = AHP.Parceiro ";
    $queryGrupos  .= " INNER JOIN NucleoFiliais AS NF ON NF.id = F.NucleoFiliais_id ";
    $queryGrupos  .= " INNER JOIN GrupoFiliais AS GF ON GF.id = NF.GrupoFiliais_id ";
    $queryGrupos  .= " WHERE AHP.`Administrador` = " . Yii::app()->session['usuario']->id . " AND AHP.habilitado AND GF.habilitado ";

    $nucleos    = Yii::app()->db->createCommand($queryNucleos)->queryAll();
    $grupos     = Yii::app()->db->createCommand($queryGrupos)->queryAll();
?>
<div class="content" style="padding-bottom:500px;">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Gestor de Filiais - Produção</h4> 
            </div>
        </div>
        
        <form action="/reports/index" id="form-export-aprovadas" target="_blank" method="post">
            <!--Campo que irá guardar a query para exportacao-->
            <input type="hidden" name="query" id="queryExportAprovadas">
        </form>
        <form action="/reports/index" id="form-export-negadas" target="_blank" method="post">
            <!--Campo que irá guardar a query para exportacao-->
            <input type="hidden" name="query" id="queryExportNegadas">
        </form>
        <form action="/reports/index" id="form-export-canceladas" target="_blank" method="post">
            <!--Campo que irá guardar a query para exportacao-->
            <input type="hidden" name="query" id="queryExportCanceladas">
        </form>
        
        <div id="indicativos-wrapper">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">  
                        <h4 class="m-t-0 header-title"><b>FILTROS INDICATIVOS</b></h4>
                        <form class="">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-2">
                                            <h5><b>De:</b></h5>
                                            <input readonly id="de" type="text" class="form-control date" value="<?php echo date('01/m/Y') ?>">
                                    </div>
                                    <div class="col-md-2">
                                            <h5><b>Até:</b></h5>
                                            <input readonly id="ate" type="text" class="form-control date" value="<?php echo date('t/m/Y') ?>">
                                    </div>
                                    <div class="col-md-3">
                                            <h5><b>Grupos:</b></h5>
                                            <select data-live-search="true" title="SELECIONE GRUPOS..." multiple data-selected-text-format="count > 1" data-style="btn-inverse btn-custom" name="grupos" required="" class="show-tick bootstrap-select selectpicker multiselectCustom" id="selectGrupo">
                                                <?php foreach( $grupos as $g ): ?>
                                                    <option value="<?php echo $g['GfId']; ?>" >
                                                        <?php echo mb_strtoupper($g['Grupo']); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                    </div>
                                    <div class="col-md-3">
                                            <h5><b>Núcleos:</b></h5>
                                            <select data-live-search="true" title="SELECIONE NÚCLEOS..." multiple data-selected-text-format="count > 1" data-style="btn-inverse btn-custom" name="nucleo" required="" class="show-tick bootstrap-select selectpicker multiselectCustom" id="selectNucleo">
                                                <?php foreach (NucleoFiliais::model()->findAllAdmFiliais() as $nucleo) { ?>
                                                    <option value="<?php echo $nucleo->id; ?>" >
                                                        <?php echo mb_strtoupper($nucleo->nome); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                    </div>
                                    <div class="col-md-3">
                                            <h5><b>Filiais:</b></h5>
                                            <select data-live-search="true" title="SELECIONE FILIAIS..." multiple data-selected-text-format="count > 2" data-style="btn-inverse btn-custom" name="filiais" required="" class="show-tick bootstrap-select selectpicker multiselectCustom" id="selectFilial">
                                                <?php foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro): ?>
                                                    <option value="<?php echo $hasParceiro->parceiro->id ?>">
                                                        <?php echo mb_strtoupper($hasParceiro->parceiro->getConcat()); ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <h5><b>&nbsp;</b></h5>
                                        <button type="button" id="btn_filter" class="btn btn-success waves-effect waves-light">Aplicar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="portlet" id="portlet-form">
                        <div class="portlet-heading portlet-default">
                            <h3 class="portlet-title">
                                Indicativos
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" href="#windicativos" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="windicativos" class="panel-collapse collapse in table-responsive">
                            <div class="portlet-body">
                                <table id="grid_indicativos" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Quantidade</th>
                                            <th>Valor</th>
                                            <th>Porcentagem por quantidade</th>
                                            <th>Porcentagem por valor</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper-grid-aprovadas" style="display:none" class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b>Filtrar Propostas aprovadas</b></h4>
                            <div class="row">
                                <form class="">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <h5><b>&nbsp;</b></h5>
                                            <button data-bind-hide="#wrapper-grid-aprovadas" data-bind-show="#indicativos-wrapper" type="button" class="btn btn-default waves-effect waves-light btn-move-panel">
                                                <span class="btn-label"><i class="fa fa-arrow-left"></i></span>Voltar
                                            </button>
                                        </div>
                                        <div class="col-md-2">
                                            <h5><b>&nbsp;</b></h5>
                                            <button id="btn-export-aprovadas" type="button" class="btn btn-success waves-effect waves-light btn-move-panel">
                                                <span class="btn-label"><i class="fa fa-file-excel-o"></i></span>Exportar
                                            </button>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <h5><b>Status:</b></h5>
                                            <select title="SELECIONE O STATUS..."  data-style="btn-inverse btn-custom" name="situacao" required="" class="show-tick form-control selectpicker" id="selectStatus">
                                                <option selected value="0">SELECIONE O STATUS...</option>
                                                <?php foreach (StatusProposta::model()->findAll('id IN(2,7)') as $status): ?>
                                                    <option value="<?php echo $status->id ?>">
                                                        <?php echo mb_strtoupper($status->status); ?>
                                                    </option>
                                                <?php endforeach ?>
                                                <option value="200">CONTRATO RECEBIDO</option>
                                                <option value="100">PAGAMENTO AUTORIZADO</option>
                                                <option value="101">PAGAMENTO EFETUADO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <h5><b>Operação:</b></h5>
                                            <select title="SELECIONE A OPERAÇÃO..."  data-style="btn-inverse btn-custom" name="selectModalidade" required="" class="show-tick form-control selectpicker" id="selectModalidade">
                                                <option selected value="0">SELECIONE A OPERAÇÃO...</option>
                                                <option value="1">CDC FDIC</option>
                                                <option value="2">CDC</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <h5><b>Modalidade:</b></h5>
                                            <select title="SELECIONE A MODALIADADE..."  data-style="btn-inverse btn-custom" name="selectOperacao" required="" class="show-tick form-control selectpicker" id="selectOperacao">
                                                <option selected value="0">SELECIONE A MODALIADADE...</option>
                                                <option value="1">COM JUROS</option>
                                                <option value="2">SEM JUROS</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="portlet" id="portlet-form">
                    <div class="portlet-heading portlet-default">
                        <h3 class="portlet-title">
                            Propostas aprovadas
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default" class="panel-collapse collapse in table-responsive">
                        <div class="portlet-body">
                            <table id="grid_producao_aprovadas" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th width=""></th>
                                        <th width="125">
                                            <input style="width:100%!important" name="codigo_filter" type="text" id="codigo_filter" class="form-control filter" required placeholder="Filtrar por Código" />
                                        </th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                    </tr>
                                    <tr>
                                        <th width="">Data</th>
                                        <th width="">Código</th>
                                        <th width="">Filial</th>
                                        <th width="">Cliente</th>
                                        <th width="">Vendedor</th>
                                        <th width="">Inicial</th>
                                        <th width="">Entrada</th>
                                        <th width="">Carência</th>
                                        <th width="">Financiado</th>
                                        <th width="">Repasse</th>
                                        <th width="20">Parcelas</th>
                                        <th width="">Status</th>
                                        <th width="">Operação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th id="th_total_inicial"></th>
                                        <th id="th_total_entrada"></th>
                                        <th id=""></th>
                                        <th id="th_total_financiado"></th>
                                        <th id="th_total_repasse"></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                        <th width=""></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper-grid-negadas" style="display:none" class="row">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <form class="">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <h5><b>&nbsp;</b></h5>
                                        <button data-bind-hide="#wrapper-grid-negadas" data-bind-show="#indicativos-wrapper" type="button" class="btn btn-default waves-effect waves-light btn-move-panel">
                                            <span class="btn-label"><i class="fa fa-arrow-left"></i></span>Voltar
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <h5><b>&nbsp;</b></h5>
                                        <button id="btn-export-negadas" type="button" class="btn btn-success waves-effect waves-light btn-move-panel">
                                            <span class="btn-label"><i class="fa fa-file-excel-o"></i></span>Exportar
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <h5><b>Modalidade:</b></h5>
                                        <select title="SELECIONE A MODALIADADE..."  data-style="btn-inverse btn-custom" name="selectModalidade" required="" class="show-tick form-control selectpicker" id="selectModalidade2">
                                            <option selected value="0">SELECIONE A MODALIADADE...</option>
                                            <option value="1">CDC FDIC</option>
                                            <option value="2">CDC</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="portlet" id="portlet-form">
                    <div class="portlet-heading portlet-default">
                        <h3 class="portlet-title">
                            Propostas negadas
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default" class="panel-collapse collapse in table-responsive">
                        <div class="portlet-body">
                            <table id="grid_producao_negadas" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="125">Código</th>
                                        <th width="">Cliente</th>
                                        <th width="50">CPF</th>
                                        <th width="">Filial</th>
                                        <th width="70">Valor</th>
                                        <th width="60">Entrada</th>
                                        <th width="70">Financiado</th>
                                        <th width="100">Data</th>
                                        <th width="100">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper-grid-canceladas" style="display:none" class="row">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <form class="">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <h5><b>&nbsp;</b></h5>
                                        <button data-bind-hide="#wrapper-grid-canceladas" data-bind-show="#indicativos-wrapper" type="button" class="btn btn-default waves-effect waves-light btn-move-panel">
                                            <span class="btn-label"><i class="fa fa-arrow-left"></i></span>Voltar
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <h5><b>&nbsp;</b></h5>
                                        <button id="btn-export-canceladas" type="button" class="btn btn-success waves-effect waves-light btn-move-panel">
                                            <span class="btn-label"><i class="fa fa-file-excel-o"></i></span>Exportar
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <h5><b>Modalidade:</b></h5>
                                        <select title="SELECIONE A MODALIADADE..."  data-style="btn-inverse btn-custom" name="selectModalidade" required="" class="show-tick form-control selectpicker" id="selectModalidade3">
                                            <option selected value="0">SELECIONE A MODALIADADE...</option>
                                            <option value="1">CDC FDIC</option>
                                            <option value="2">CDC</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="portlet" id="portlet-form">
                    <div class="portlet-heading portlet-default">
                        <h3 class="portlet-title">
                            Propostas pendentes
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default" class="panel-collapse collapse in table-responsive">
                        <div class="portlet-body">
                            <table id="grid_producao_canceladas" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="125">Código</th>
                                        <th width="">Cliente</th>
                                        <th width="50">CPF</th>
                                        <th width="">Filial</th>
                                        <th width="70">Valor</th>
                                        <th width="60">Entrada</th>
                                        <th width="70">Financiado</th>
                                        <th width="100">Data</th>
                                        <th width="100">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (Yii::app()->session['usuario']->primeira_senha): ?>
            <?php $this->renderPartial('/user/form_mudar_primeira_senha'); ?>
        <?php endif ?>
    </div>
    <?php $this->renderPartial('/user/menu', [ 'css_menu' => 'display:none']); ?>
</div>

<style type="text/css">
    #grid_producao_negadas_filter,
    #grid_producao_aprovadas_filter,
    #grid_producao_aprovadas_length,
    #grid_producao_canceladas_length,
    #grid_producao_negadas_length,
    #grid_indicativos_filter,
    #grid_indicativos_length
    {
        display: none;
    }
    .btn-custom.btn-inverse
    {
        color: #FFF !important;
    }
    .card-box:hover
    {
        border:2px solid rgba(255, 255, 255, 0.1);
    }
    @media (min-width: 992px){
        .col-md-2 {
            width: 11%!important;
        }
    }
    input.filter{
        border:none!important;
        background: transparent!important;
    }
    ::-webkit-input-placeholder {
        font-style: italic;
    }
    tfoot tr th{
        font-size: 11px!important;
    }
</style>