<?php
$queryNucleos = " SELECT DISTINCT (NF.id) as 'NfId', NF.nome as 'Nucleo'  FROM `AdminHasParceiro` AS AHP";
$queryNucleos .= " INNER JOIN Filial AS F ON F.id = AHP.Parceiro";
$queryNucleos .= " INNER JOIN NucleoFiliais AS NF ON NF.id = F.NucleoFiliais_id";
$queryNucleos .= " WHERE AHP.`Administrador` = " . Yii::app()->session['usuario']->id . " AND AHP.habilitado ";
$nucleos = Yii::app()->db->createCommand($queryNucleos)->queryAll();
?>
<div class="content">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">Gestor de Filiais - Valores a receber</h4>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>FILTROS INDICATIVOS</b></h4>
                    <div class="row">
                        <form class="">
                            <div class="form-group">
                                <div class="col-md-1">
                                    <h5><b>De:</b></h5>
                                    <input readonly id="de" type="text" class="form-control date" value="<?php echo date('01/m/Y') ?>">
                                </div>
                                <div class="col-md-1">
                                    <h5><b>Até:</b></h5>
                                    <input readonly id="ate" type="text" class="form-control date" value="<?php echo date('t/m/Y') ?>">
                                </div>
                                <div class="col-md-4">
                                    <h5><b>Núcleos:</b></h5>
                                    <select title="SELECIONE NÚCLEOS..." multiple data-selected-text-format="count > 1" data-style="btn-inverse btn-custom" name="nucleo" required="" class="show-tick form-control selectpicker" id="selectNucleo">
                                        <?php foreach ($nucleos as $nucleo): ?>
                                            <option value="<?php echo $nucleo['NfId'] ?>">
                                                <?php $n = NucleoFiliais::model()->findByPk($nucleo['NfId']); ?>

                                                <?php echo mb_strtoupper($n->nome); ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <h5><b>Situação:</b></h5>
                                    <select title="SELECIONE NÚCLEOS..." data-selected-text-format="count > 1" data-style="btn-inverse btn-custom" name="status" required="" class="show-tick form-control selectpicker" id="selectStatus">
                                        <?php foreach (StatusLotePagamento::model()->findAll() as $status): ?>
                                            <option value="<?php echo $status->id ?>">
                                                <?php echo mb_strtoupper($status->descricao); ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <h5><b>&nbsp;</b></h5>
                                    <button type="button" id="btn-filtro" class="btn btn-success waves-effect waves-light">Aplicar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="grid_lotes_a_receber" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="20">Borderôs</th>
                                <th width="100">Lote</th>
                                <th width="80">Data</th>
                                <th>Núcleo</th>
                                <th>Valor</th>
                                <th>Comprovante</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th id="th-total"></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php $this->renderPartial('/user/menu', [ 'css_menu' => 'display:none']); ?>
</div>
<style type="text/css">
    #grid_lotes_a_receber_filter, #grid_lotes_a_receber_length, #borderosGrid_length, #boderosGrid_filter, #propostasGrid_filter, #propostasGrid_length
    {
        display: none;
    }
    /*td.details-control, td.details-control2{
       background: url('../../images/details_open.png') no-repeat center center;
       cursor: pointer;
    }*/
    /*tr.shown td.details-control, tr.shown td.details-control2 {
       background: url('../../images/details_close.png') no-repeat center center;
    }*/
    #borderosGrid, #propostasGrid{
        border:none;
    }
    #borderosGrid .even{
        background: #424e5b !important;
    }
    #borderosGrid thead tr{
        background: #303841 !important;
    }
    #borderosGrid .odd{
        background: #424E5B !important;
    }
    #borderosGrid tr:not(.odd){
        background: #303841!important;
    }
    .btn-custom.btn-inverse {
        color: #FFF !important;
    }
</style>