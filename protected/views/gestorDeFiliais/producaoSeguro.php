<?php
   $queryNucleos = " SELECT DISTINCT (NF.id) as 'NfId', NF.nome as 'Nucleo'  FROM `AdminHasParceiro` AS AHP";
   $queryNucleos .= " INNER JOIN Filial AS F ON F.id = AHP.Parceiro";
   $queryNucleos .= " INNER JOIN NucleoFiliais AS NF ON NF.id = F.NucleoFiliais_id";
   $queryNucleos .= " WHERE AHP.`Administrador` = " . Yii::app()->session['usuario']->id . " AND AHP.habilitado ";
   
   $nucleos = Yii::app()->db->createCommand($queryNucleos)->queryAll();
   ?>
<div class="content" style="padding-bottom:500px;">

   <?php $this->renderPartial('/navigation/navigation_menu'); ?>
   <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30">Gestor de Filiais - Produção de Seguro</h4>
         </div>
      </div>
      <div id="indicativos-wrapper">
         <div class="row">
            <div class="col-sm-12">
               <div class="card-box">
                  <h4 class="m-t-0 header-title"><b>FILTROS INDICATIVOS</b></h4>
                  <div class="row">
                     <form class="">
                        <div class="form-group">
                           <div class="col-md-2">
                              <h5><b>De:</b></h5>
                              <input readonly id="de" type="text" class="form-control date" value="<?php echo date('01/m/Y') ?>">
                           </div>
                           <div class="col-md-2">
                              <h5><b>Até:</b></h5>
                              <input readonly id="ate" type="text" class="form-control date" value="<?php echo date('t/m/Y') ?>">
                           </div>
                           <div class="col-md-3">
                              <h5><b>Núcleos:</b></h5>
                              <select data-live-search="true" title="SELECIONE NÚCLEOS..." multiple data-selected-text-format="count > 1" data-style="btn-inverse btn-custom" name="nucleo" required="" class="show-tick bootstrap-select selectpicker multiselectCustom" id="selectNucleo">
                                 <?php foreach (NucleoFiliais::model()->findAllAdmFiliais() as $nucleo) { ?>
                                 <option value="<?php echo $nucleo->id; ?>" >
                                    <?php echo mb_strtoupper($nucleo->nome); ?>
                                 </option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="col-md-3">
                              <h5><b>Filiais:</b></h5>
                              <select data-live-search="true" title="SELECIONE FILIAIS..." multiple data-selected-text-format="count > 2" data-style="btn-inverse btn-custom" name="filiais" required="" class="show-tick bootstrap-select selectpicker multiselectCustom" id="selectFilial">
                                 <?php foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro): ?>
                                 <option value="<?php echo $hasParceiro->parceiro->id ?>">
                                    <?php echo mb_strtoupper($hasParceiro->parceiro->getConcat()); ?>
                                 </option>
                                 <?php endforeach ?>
                              </select>
                           </div>
                           <div class="col-md-2">
                              <h5><b>&nbsp;</b></h5>
                              <button type="button" id="btn_filter" class="btn btn-success waves-effect waves-light">Aplicar</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="portlet" id="portlet-form">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">
                        Propostas aprovadas
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default" class="panel-collapse collapse in table-responsive">
                     <div class="portlet-body">
                        <table id="grid_producao" class="table table-striped">
                           <thead>
                              <tr>
                                 <th width="125">Código</th>
                                 <th width="">Filial</th>
                                 <th width="100">Data</th>
                                 <th width="">Crediarista</th>
                                 <th width="70">R$ Solicitado</th>
                                 <th width="60">R$ Seguro</th>
                                 <th width="70">R$ Financiado</th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th width="125"></th>
                                 <th width=""></th>
                                 <th width="50"></th>
                                 <th width=""></th>
                                 <th id="th-total-soli" width="70"></th>
                                 <th id="th-total-seguro" width="60"></th>
                                 <th id="th-total-financiado" width="70"></th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->renderPartial('/user/menu', [ 'css_menu' => 'display:none']); ?>
</div>
<style type="text/css">
   #grid_producao_filter,
   #grid_producao_length{
      display: none;
   }
   .btn-custom.btn-inverse{
      color: #FFF !important;
   }
   .card-box:hover{
      border:2px solid rgba(255, 255, 255, 0.1);
   }
   #grid_producao tfoot th{
      font-size: 10px
   }
</style>