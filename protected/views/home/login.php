<form id="form-login" class="form-horizontal m-t-20" action="<?php echo Yii::app()->getBaseUrl(true) ?>/home/login/" method="POST">
   <div class="form-group ">
      <div class="col-xs-12">
         <input class="form-control" required type="text" placeholder="Usuário" name="LoginForm[username]">
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <input class="form-control" required type="password" placeholder="Senha" name="LoginForm[password]">
      </div>
   </div>
   <div class="form-group ">
      <div class="col-xs-12">
         <div class="checkbox checkbox-primary">
            <input id="checkbox-signup" type="checkbox">
            <label for="checkbox-signup">
            Manter-me contectado</label>
         </div>
      </div>
   </div>
   <div class="form-group text-center m-t-40">
      <div class="col-xs-12">
         <button name="LoginForm[SubmitButton]" class="btn btn-login-credshow btn-block text-uppercase waves-effect waves-light" type="submit">Entrar</button>
         <?php if ( isset( $loginError ) ): ?>
            <label style="margin-top:25px" for="LoginForm[SubmitButton]" class="error"><?= $loginError ?></label>
         <?php endif ?>
      </div>
   </div>
   <div class="form-group m-t-30 m-b-0">
      <div class="col-sm-12">
         <a href="#" class="text-muted"><i class="fa fa-lock m-r-5"></i> Esqueceu sua senha?</a>
      </div>
   </div>
</form>