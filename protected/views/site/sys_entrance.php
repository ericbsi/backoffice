<?php $form                   = $this->beginWidget('CActiveForm', array(
   'id'                       => 'login-form',
   'enableClientValidation'   => true,
   'clientOptions'            => array(
   	'validateOnSubmit'      => true,
   ),
   'htmlOptions'              => array(
      'class'                 =>'form-horizontal',
   ),
)); ?>
<div class="content">
   <h4 class="title">Informe o nome do usuário:</h4>
   <div class="form-group">
      <div class="col-sm-12">
         <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            
            <?php 
               echo $form->textField($model,'username',array('class'=>'form-control','id'=>'username', 'placeholder'=>'Nome do usuário')); 
            ?>
         </div>
            <?php echo $form->error($model,'username'); ?> 
      </div>
   </div>
</div>
<div class="foot">
   <button class="btn btn-primary" data-dismiss="modal" type="submit">PROSSEGUIR</button>
</div>
<?php $this->endWidget(); ?>