<div class="content">
   <?php $this->renderPartial('/navigation/navigation_menu'); ?>
   <div class="container" id="action-content" style="padding-bottom:400px;">
      <div id="wrapper-politicas-menus">
         <div class="row">
            <div class="col-sm-12">
               <h4 class="page-header  header-title m-b-30">ADMINISTRAR TABELAS</h4>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="btn-group btn-group-justified m-b-20">
                  <a data-input-focus="#tabela_descricao" data-bind-show="#wrapper-politicas-index" data-bind-hide="" class="btn-move-panel btn btn-success waves-effect waves-light" role="button">ADICIONAR</a>
                  <a data-bind-show="#wrapper-politicas-grid" data-bind-hide="" class="btn-move-panel btn btn-primary waves-effect waves-light" role="button">DISPONÍVEIS</a>
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-politicas-index" style="display:none">
         <div class="row">
            <div class="col-lg-12">
               <div class="portlet" id="portlet-form">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Incluir Nova Tabela</h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class=""></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <form id="form-add-tabela" class="form-horizontal group-border-dashed" action="#">
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Descrição / Modalidade</label>
                              <div class="col-sm-4">
                                 <input name="TabelaCotacao[descricao]" type="text" id="tabela_descricao" class="form-control" required placeholder="Descrição"/>
                              </div>
                              <div class="col-sm-2">
                                 <select data-style="btn-custom" name="TabelaCotacao[ModalidadeId]" required="required" class="input-small show-tick form-control selectpicker required">
                                    <option value="">MODALIDADE</option>
                                    <?php foreach (ModalidadeTabela::model()->findAll() as $modalidade): ?>
                                    <option value="<?php echo $modalidade->id ?>"><?php echo mb_strtoupper($modalidade->descricao) ?></option>
                                    <?php endforeach ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Parcela De / Parcela Até / T.a / T.m / Carências</label>
                              <div class="col-sm-1">
                                 <input  name="TabelaCotacao[parcela_inicio]" type="text" id="" class="form-control number" required placeholder="Parcela Início" />
                              </div>
                              <div class="col-sm-1">
                                 <input  name="TabelaCotacao[parcela_fim]" type="text" id="" class="form-control number" required placeholder="Parcela Fim" />
                              </div>
                              <div class="col-sm-1">
                                 <input  name="TabelaCotacao[taxa]" type="text" id="" class="form-control number" required placeholder="Taxa" />
                              </div>
                              <div class="col-sm-1">
                                 <input  name="TabelaCotacao[taxa_anual]" type="text" id="" class="form-control number" required placeholder="Taxa Anual" />
                              </div>
                              <div class="col-sm-2">
                                 <select title="CARÊNCIAS..." required="required" data-selected-text-format="count > 3" multiple data-style="btn-custom" name="Carencias[]" required="required" class="multiple input-small show-tick form-control selectpicker required">
                                    <?php foreach (Carencia::model()->findAll() as $c): ?>
                                    <option value="<?php echo $c->id ?>"><?php echo $c->id; ?></option>
                                    <?php endforeach ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                 <button type="submit" class="btn btn-login-credshow">Salvar</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-politicas-grid" style="">
         <div class="row">
            <div class="col-lg-12">
               <div class="portlet" id="portlet-form3">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Tabelas cadastradas</h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default2" aria-expanded="false" class=""></a>
                     </div>
                     <div class="clearfix"></div>
                     <br>
                     <div class="row">
                        
                        <div class="col-lg-3">
                           <select id="ativos-filter" data-style="btn-custom" name="tipo" required="required" class="input-small show-tick form-control selectpicker required">
                              <option value="1">ATIVAS:</option>
                              <option value="0">INATIVAS:</option>
                           </select>
                        </div>

                        <div id="wrapper-nucleo-filter" class="col-lg-3" style="display:none">
                           <select id="nucleo-filter" data-style="btn-custom" name="tipo" required="required" class="input-small show-tick form-control selectpicker required">
                              <option value="0">SELECIONE O NÚCLEO:</option>
                              <?php foreach (NucleoFiliais::model()->findAll() as $nucleo): ?>
                                 <option value="<?php echo $nucleo->id ?>" ><?php echo mb_strtoupper($nucleo->nome) ?></option>
                              <?php endforeach ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div id="bg-default2" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <table id="datatable-tabelas" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th width="20">Parceiros</th>
                                 <th>Descrição</th>
                                 <th width="110">Modalidade</th>
                                 <th width="75">Configurar</th>
                                 <th width="85">Ativar/Desativar</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="wrapper-tabela-fatores" style="display:none;">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
         
         <div class="row">
            <div class="col-sm-12">
               <div class="portlet" id="portlet-form2">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">Configurações da tabela <span class="span_tabela_descricao"></span></h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default4" aria-expanded="false" class=""></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default4" class="panel-collapse collapse in">
                     <div class="portlet-body">
                        <div class="row">
                           <div class="col-md-12"></div>
                           <form action="/tabelaCotacao/atualizarFatores" method="post" id="form_importacao" class="form-horizontal" enctype="multipart/form-data">
                              <div class="col-md-2">
                                 <select data-style="btn-custom" name="tipo" required="required" class="input-small show-tick form-control selectpicker required">
                                    <option value="1">FATOR</option>
                                    <option value="2">IOF</option>
                                 </select>
                              </div>
                              <div class="col-md-2">
                                 <input class="anexo_importacao" name="anexo_importacao" id="anexo_importacao" type="file" class="form-control">
                                 <label id="anexo_label" for="anexo_importacao"><i class="fa fa-refresh" aria-hidden="true"></i> Selecionar Arquivo</label>
                                 <input value="0" name="tabelaId" type="hidden" id="tabelaId" class="form-control"/>
                              </div>
                              <div class="col-md-2">
                                 <button style="width: 100%; border-radius: 3px" id="submit_arquivo" class="btn btn-success btn-sm" type="submit"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Atualizar fatores</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="portlet" id="portlet-form2">
                  <div class="portlet-heading portlet-default">
                     <h3 class="portlet-title">
                        Fatores da tabela <span class="span_tabela_descricao"></span>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" href="#bg-default3" aria-expanded="false" class=""></a>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div id="bg-default3" class="panel-collapse collapse in table-responsive">
                     <div class="portlet-body">
                        <table style="table-layout:fixed;" id="grid_fatores" class="table table-striped">
                           <thead>
                              <tr>
                                 <th>Carência</th>
                                 <th>Fator</th>
                                 <th>Fator IOF</th>
                                 <th>Retenção</th>
                                 <th>Sequencial da Parcela</th>
                              </tr>
                           </thead>
                           <tbody></tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->renderPartial('/user/menu', ['css_menu' => 'display:none']); ?>
</div>
<style type="text/css">
   #datatable-tabelas_filter, 
   #datatable-tabelas_length, 
   #grid_fatores_length, 
   #grid_fatores_filter,
   #gridParceiros_wrapper,#funcoesGrid_length
   {
      display: none;
   }
   .btn-list button{
   float: left!important;
   display: inline!important;
   }
   input.filter, select.filter{
   background: transparent!important;
   }
   ::-webkit-input-placeholder {
   font-style: italic;
   }
   input#anexo_importacao {
        position: absolute;
        filter: alpha(opacity=0);
        opacity: 0;
    }
   #submit_arquivo{
      text-align        : left;
      padding           : 9px;
      width             : 100%;
      height            : 39px;
      left              : 0;
      position          : relative;
      text-align        : center;
      color             : #fff;
      background-color: #81c868 !important;
      border: 1px solid #81c868 !important;
      cursor            : pointer;
   }
   input#anexo_importacao + label {
      border            : 1px solid #CCC;
      border-radius     : 3px;
      text-align        : left;
      padding           : 9px;
      width             : 100%;
      height            : 39px;
      left              : 0;
      position          : relative;
      text-align        : center;
      background        : #ffbd4a;
      color             : #fff;
      border            : none;
      cursor            : pointer;
   }
   #funcoesGrid{
      border:none;
   }
   #funcoesGrid .even{
      background: #424e5b !important;
   }
   #funcoesGrid thead tr{
      background: #303841 !important;
   }
   #funcoesGrid .odd{
      background: #424E5B !important;
   }
</style>