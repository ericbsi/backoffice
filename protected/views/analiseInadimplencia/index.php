
<div class="content">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div id="action-content" class="container">       

        <div class="row">
            
            <h4 class="page-header  header-title"  style="text-align: center; margin-top: 0px">
                Análise de Inadimplência
            </h4>
            
        </div>

        <div class="row">

            <div class="btn-group btn-group-justified">

                <button id="trimestreBtn"   
                        style="width: 25%" 
                        type="button" 
                        class="btn btnDiv btn-success waves-effect waves-light" 
                        data-divNome="divTrimestre" 
                        data-urlTable="getTableTrimestre"
                        data-tituloTable="Trimestre"
                        data-filtroPre="">
                    <b>TRIMESTRE</b>
                </button>

                <button id="pnpBtn"         
                        style="width: 25%" 
                        type="button" 
                        class="btn btnDiv btn-primary waves-effect waves-light" 
                        data-divNome="divPNP"
                        data-urlTable="getTablePNP"
                        data-tituloTable="PNP"
                        data-filtroPre="">
                    <b>PNP</b>
                </button>

                <button id="safraBtn"       
                        style="width: 25%" 
                        type="button" 
                        class="btn btnDiv btn-warning waves-effect waves-light" 
                        data-divNome="divSafra"
                        data-urlTable="getTableSafra"
                        data-tituloTable="Safra"
                        data-filtroPre="">
                    <b>SAFRA</b>
                </button>

                <button id="analistasBtn"       
                        style="width: 25%" 
                        type="button" 
                        class="btn btnDiv btn-info waves-effect waves-light" 
                        data-divNome="divAnalistas"
                        data-urlTable="getTableAnalistas"
                        data-tituloTable="Analistas"
                        data-filtroPre="">
                    <b>ANALISTAS</b>
                </button>

            </div>

        </div>
        
        <br>
        
        <div class="row">
                
            <div class="portlet">

                <div class="row">

                    <div class="col-sm-12">

                        <button class="btn btnMostrarFiltro" 
                                style="width: 100%; background-color: orangered;" 
                                id="btnFiltroSafra" 
                                data-class-filtro="filtroSafra">

                            <b>
                                Exibir Filtro
                            </b>

                        </button>

                    </div>

                </div>

                <div class="row filtroSafra" style="display: none">

                    <div class="col-sm-12">

                        <div class="row" id="divFiltroSafraCabec">

                            <div class="col-sm-3">
                                Meses de Partida
                            </div>

                            <div class="col-sm-3">
                                Financeira
                            </div>

                            <div class="col-sm-2">
                                Modalidade
                            </div>

                            <div class="col-sm-2">
                                Política de Crédito
                            </div>

                            <div class="col-sm-2">
                                Score
                            </div>

                        </div>

                        <div class="row" id="divFiltroSafra">

                            <div class="col-sm-3">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>

                                    <select id="mesesPartidaSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count > 0">

                                        <?php foreach ($this->getMesesPartida() as $mes) { ?>
                                            <option value="<?php echo $mes["value"]; ?>" >
                                                <?php echo $mes["text"]; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-bank"></i>
                                    </span>

                                    <select id="financeiraSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count">

                                        <?php foreach (Financeira::model()->findAll("id IN (5,10,11)") as $financeira) { ?>
                                            <option value="<?php echo $financeira->id; ?>" >
                                                <?php echo $financeira->nome; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-2">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-adjust"></i>
                                    </span>

                                    <select id="modalidadeSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count">

                                        <?php foreach (ModalidadeTabela::model()->findAll() as $modalidade) { ?>
                                            <option value="<?php echo $modalidade->id; ?>" >
                                                <?php echo $modalidade->descricao; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-2">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-align-center"></i>
                                    </span>

                                    <select id="politicaSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count">

                                        <?php foreach (PoliticaCredito::model()->findAll() as $politica) { ?>
                                            <option value="<?php echo $politica->id; ?>" >
                                                <?php echo $politica->descricao; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-2">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </span>

                                    <select id="scoreSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count">

                                        <option value="'A'" >
                                            A
                                        </option>

                                        <option value="'B'" >
                                            B
                                        </option>

                                        <option value="'C'" >
                                            C
                                        </option>

                                        <option value="'D'" >
                                            D
                                        </option>

                                        <option value="'E'" >
                                            E
                                        </option>

                                        <option value="'F'" >
                                            F
                                        </option>

                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <br>

                <div class="row filtroSafra" style="display: none">

                    <div class="col-sm-12">

                        <div class="row" id="divFiltroSafraCabec">

                            <div class="col-sm-3">
                                Grupos
                            </div>

                            <div class="col-sm-3">
                                Núcleos
                            </div>

                            <div class="col-sm-4">
                                Filiais
                            </div>

                            <div class="col-sm-2">
                                Cidades
                            </div>

                        </div>

                        <div class="row" id="divFiltroSafra">

                            <div class="col-sm-3">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-th"></i>
                                    </span>

                                    <select id="gruposSafra" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count"
                                            data-url="/gestorDeFiliais/listarNucleosGrupos/"
                                            data-alvo="nucleosSafra"
                                            data-variavel="gruposId">

                                        <?php foreach (GrupoFiliais::model()->findAllAdmFiliais() as $grupo) { ?>
                                            <option value="<?php echo $grupo->id; ?>" >
                                                <?php echo $grupo->nome_fantasia; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-th-large"></i>
                                    </span>

<!--                                            <input type="hidden" class="show-tick selectpicker multiselectCustom" id="nucleosSafra" />-->

                                    <select id="nucleosSafra" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count"
                                            data-url="/gestorDeFiliais/listarFiliaisFiltro/"
                                            data-alvo="filiaisSafra">

                                        <?php foreach (NucleoFiliais::model()->findAllAdmFiliais() as $nucleo) { ?>
                                            <option value="<?php echo $nucleo->id; ?>" >
                                                <?php echo $nucleo->nome; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-4">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-building"></i>
                                    </span>

                                    <select id="filiaisSafra" multiple="multiple" class="show-tick bootstrap-select selectpicker multiselectCustom" data-selected-text-format="count"
                                            data-url="/analiseInadimplencia/listarCidades/"
                                            data-alvo="cidadesSafra">

                                        <?php foreach (Filial::model()->findAllAdmFiliais() as $filial) { ?>
                                            <option value="<?php echo $filial->id; ?>" >
                                                <?php echo $filial->getConcat(); ?>
                                            </option>
                                        <?php } ?>

                                        <?php /*foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro) { ?>
                                            <option value="<?php echo $hasParceiro->parceiro->id; ?>" >
                                                <?php echo $hasParceiro->parceiro->getConcat(); ?>
                                            </option>
                                        <?php }*/ ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-sm-2">

                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </span>

                                    <select id="cidadesSafra" multiple="multiple" class="show-tick selectpicker multiselectCustom" data-selected-text-format="count">

                                        <?php foreach ($this->listarCidades() as $cidade) { ?>
                                            <option value="'<?php echo $cidade["nome"]; ?>'" >
                                                <?php echo $cidade["nome"]; ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>


                <div class="row filtroSafra" style="display: none">

                    <br>

                    <div class="col-sm-12">

                        <button class="btn btn-success btnFiltrar" style="width : 100%" data-grid-responsavel="gridSafra">
                            <i id="btnFiltroSafra" class="fa fa-filter iBtnFiltro"></i> <b>Filtrar</b>
                        </button>

                    </div>

                    <br>
                    <br>

                </div>

                <div class="portlet-heading">

                    <h3 class="portlet-title" style="float: none; text-align: center" id="divTituloGrid">
                        
                    </h3>

                </div>

                <div id="bg-default2">

                    <div class="portlet-body">
                        
                        <!--<div class="row" id="divAnaliticoSintetico">
                            
                            <div class="col-sm-3">
                            
                                <table>
                                    <tr>
                                        <td>
                                            <input id="analiticoSintetico" 
                                                   type="checkbox" 
                                                   data-plugin="switchery" 
                                                   data-color="#f05050" 
                                                   data-size="small" 
                                                   data-switchery="true" 
                                                   >
                                        </td>
                                        <td>
                                            <h4 id="analiticoText" style="padding: 2px">
                                                Sintético
                                            </h4>
                                        </td>
                                    </tr>
                                </table>
                                
                            </div>
                            
                            <div class="col-sm-8">
                            </div>
                            
                            <div class="col-sm-1" style="text-align: right">
                                <button id="exportarXLS" class="btn btn-success">
                                    <i class="fa fa-file-excel-o"></i>
                                </button>
                            </div>
                            
                        </div>-->
                        
                        <!--<div class="row divTableCustom" id="divSafra"       style="display: none">-->
                        
                        <div class="row divTableCustom" id="divSafra">
                            
                            <table id="gridSafra"  data-order='[[ 0, "asc" ]]' class="table table-striped tableCustom table-responsive" cellspacing="0" width="100%">

                                <thead>
                                    <tr class="trCabec">
                                        <th class="no-orderable" style="z-index: -1">
                                        </th>
                                        <th>
                                            Safra
                                        </th>
                                        <th>
                                            1x
                                        </th>
                                        <th>
                                            2x
                                        </th>
                                        <th>
                                            3x
                                        </th>
                                        <th>
                                            4x
                                        </th>
                                        <th>
                                            5x
                                        </th>
                                        <th>
                                            6x
                                        </th>
                                        <th>
                                            7x
                                        </th>
                                        <th>
                                            8x
                                        </th>
                                        <th>
                                            9x
                                        </th>
                                        <th>
                                            10x
                                        </th>
                                        <th>
                                            11x
                                        </th>
                                        <th>
                                            12x
                                        </th>
                                        <th>
                                            13x
                                        </th>
                                        <th>
                                            14x
                                        </th>
                                        <th>
                                            15x
                                        </th>
                                        <th>
                                            16x
                                        </th>
                                        <th>
                                            17x
                                        </th>
                                        <th>
                                            18x
                                        </th>
                                        <th>
                                            Geral
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>

                            </table>
                            
                        </div>
                        
                        <div class="row divTableCustom" id="divPNP">

                            <table id="gridPNP" class="table table-striped table-bordered nowrap tableCustom" cellspacing="0" width="100%">

                                <thead>
                                    <tr>
                                        <th class="no-orderable" style="z-index: -1">
                                        </th>
                                        <th>
                                            Safra
                                        </th>
                                        <th>
                                            5
                                        </th>
                                        <th>
                                            10
                                        </th>
                                        <th>
                                            15
                                        </th>
                                        <th>
                                            20
                                        </th>
                                        <th>
                                            25
                                        </th>
                                        <th>
                                            30
                                        </th>
                                        <th>
                                            35
                                        </th>
                                        <th>
                                            40
                                        </th>
                                        <th>
                                            45
                                        </th>
                                        <th>
                                            50
                                        </th>
                                        <th>
                                            55
                                        </th>
                                        <th>
                                            60
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>

                            </table>

                        </div>
        
                        <div class="row divTableCustom" id="divTrimestre">

                            <table id="gridTrimestre" class="table table-striped table-bordered tableCustom nowrap" cellspacing="0" width="100%">

                                <thead>
                                    
                                    <tr>
                                        
                                        <th class="no-orderable" style="z-index: -1">
                                        </th>
                                        
                                        <th>
                                            Safra
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Meta 1x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            1x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Deve 1x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Base 1x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Meta 123x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            123x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Deve 123x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Base 123x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Meta 456x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            456x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Deve 456x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Base 456x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Meta 789x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            789x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Deve 789x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Base 789x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Meta 101112x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            101112x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Deve 101112x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Base 101112x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Meta 131415x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            131415x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Deve 131415x
                                        </th>
                                        
                                        <th style="background-color: darkgreen; width: 100px">
                                            Base 131415x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Meta 161718x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            161718x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Deve 161718x
                                        </th>
                                        
                                        <th style="background-color: green; width: 100px">
                                            Base 161718x
                                        </th>
                                        
                                        <th>
                                            Geral
                                        </th>
                                        
                                    </tr>
                                    
                                </thead>

                            </table>

                        </div>
                        
                        <div class="row divTableCustom" id="divAnalistas">
                            
                            <div class="col-sm-12">

                                <div class="input-group" style="width: 100%">

                                    <span class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </span>

                                    <select id="analistasSafra" multiple="multiple" class="show-tick selectpicker" data-selected-text-format="count>10">

                                        <?php foreach (Usuario::model()->findAll("habilitado AND tipo_id = 4") as $analista) { ?>
                                            <option value="'<?php echo $analista->id; ?>'" >
                                                <?php echo strtoupper($analista->nome_utilizador); ?>
                                            </option>
                                        <?php } ?>

                                    </select>

                                </div>
                                
                            </div>
                            
                            <table id="gridAnalistas"  data-order='[[ 0, "asc" ]]' class="table table-striped tableCustom table-responsive" cellspacing="0" width="100%">

                                <thead>
                                    <tr style="background-color: #34d3eb;">
                                        <th class="no-orderable" style="z-index: -1">
                                        </th>
                                        <th>
                                            Safra
                                        </th>
                                        <th>
                                            1x
                                        </th>
                                        <th>
                                            2x
                                        </th>
                                        <th>
                                            3x
                                        </th>
                                        <th>
                                            4x
                                        </th>
                                        <th>
                                            5x
                                        </th>
                                        <th>
                                            6x
                                        </th>
                                        <th>
                                            7x
                                        </th>
                                        <th>
                                            8x
                                        </th>
                                        <th>
                                            9x
                                        </th>
                                        <th>
                                            10x
                                        </th>
                                        <th>
                                            11x
                                        </th>
                                        <th>
                                            12x
                                        </th>
                                        <th>
                                            13x
                                        </th>
                                        <th>
                                            14x
                                        </th>
                                        <th>
                                            15x
                                        </th>
                                        <th>
                                            16x
                                        </th>
                                        <th>
                                            17x
                                        </th>
                                        <th>
                                            18x
                                        </th>
                                        <th>
                                            Geral
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>

                            </table>
                            
                        </div>
                        
                    </div>

                </div>

            </div>
            
        </div>

    </div>
    <?php $this->renderPartial('/user/menu', ['css_menu' => 'display:none']); ?>
</div>

<style type="text/css">
    .btn-group
    {
        width       : 100%!important                ;
    }
    .multiselect
    {
        text-align  : left                          ;
    }
    
    .dropdown-menu.open
    {
        max-height   : 250px;
    }
/*    input[type="checkbox"]
    {
        opacity: 10;
    }*/
</style>