<div class="content">
    <?php $this->renderPartial('/navigation/navigation_menu'); ?>
    <div class="container" id="action-content">       

        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-header  header-title m-b-30">
                    Análise de Inadimplência
                </h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="portlet">
                    <div class="portlet-heading portlet-default">
                        <h3 class="portlet-title">
                            Bads
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="true">
                                <i class="ion-minus-round"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default2" class="panel-collapse collapse in">
                        <div class="portlet-body">

                            <table id="gridBads" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr id="cabecBads">
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php $this->renderPartial('/user/menu', [ 'css_menu' => 'display:none']); ?>

</div>

<style type="text/css">
    #gridBads_length, #gridBads_filter, #gridBads_info, #gridBads_paginate
    {
        display: none;
    }
</style>