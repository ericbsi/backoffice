<div class="content">
   <?php $this->renderPartial( '/navigation/navigation_menu' ); ?>
   <div class="container" id="action-content">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-header  header-title m-b-30"></h4>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            <div class="portlet" id="portlet-form">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Incluir Nova Função
                  </h3>
                  <!--<div class="loader-1"></div></div>-->
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" href="#bg-default" aria-expanded="false" class="collapsed"><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default" class="panel-collapse collapse out">
                  <div class="portlet-body">
                     <form id="form-funcao" class="form-horizontal group-border-dashed" action="#">
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Descrição da Função</label>
                           <div class="col-sm-6">
                              <textarea id="funcao_descricao" placeholder="Descrição" name="Funcao[descricao]" required class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Etiqueta</label>
                           <div class="col-sm-6">
                              <input  name="Funcao[label]" type="text" id="funcao_label" class="form-control" required placeholder="Etiqueta" />
                              <input  name="FuncaoId" id="funcao_id" type="hidden" value="0" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Controller</label>
                           <div class="col-sm-6">
                              <input  name="Funcao[controller]" type="text" id="funcao_label" class="form-control" required placeholder="Controller" />
                              <input  name="FuncaoId" id="funcao_id" type="hidden" value="0" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">Action</label>
                           <div class="col-sm-6">
                              <input  name="Funcao[nome]" type="text" id="funcao_label" class="form-control" required placeholder="Action" />
                              <input  name="FuncaoId" id="funcao_id" type="hidden" value="0" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 control-label">View</label>
                           <div class="col-sm-6">
                              <input  name="Funcao[view]" type="text" id="funcao_label" class="form-control" required placeholder="View" />
                              <input  name="FuncaoId" id="funcao_id" type="hidden" value="0" />
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-offset-3 col-sm-9 m-t-15">
                              <button type="submit" class="btn btn-login-credshow">Salvar</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="portlet">
               <div class="portlet-heading portlet-default">
                  <h3 class="portlet-title">
                     Funções Disponíveis
                  </h3>
                  <div class="portlet-widgets">
                     <a data-toggle="collapse" data-parent="#accordion2" href="#bg-default2" aria-expanded="false" class=""><i class="ion-minus-round"></i></a>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div id="bg-default2" class="panel-collapse collapse in">
                  <div class="portlet-body">

                     <table id="datatable-funcoes" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <thead>
                           <tr>
                              <th>Etiqueta</th>
                              <th>Descrição</th>
                              <th>Editar</th>
                           </tr>
                        </thead>

                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->renderPartial( '/user/menu', [ 'css_menu' => 'display:none'] ); ?>
</div>

<style type="text/css">
   #datatable-funcoes_length{
      display: none;
   }
</style>