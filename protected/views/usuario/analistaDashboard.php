<?php ?>

<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Propostas   
                </a>
            </li>
            <li class="active">
                Últimas propostas
            </li>
        </ol>
        <div class="page-header">
            <h1>Últimas propostas</h1>
        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-sm-10">
    </div>
    
    <div class="col-sm-2">
        <label class="checkbox-inline">
            <input type="checkbox" 
                   class="square-green" 
                   value=""
                   checked="checked"
                   id="minhasPropostas"
                   name="minhasPropostas"
                   style="position: absolute; 
                          top: -10%; 
                          left: -10%; 
                          display: block; 
                          width: 120%; 
                          height: 120%; 
                          margin: 0px; 
                          padding: 0px; 
                          border: 0px; 
                          opacity: 0; 
                          background: rgb(255, 255, 255);" />
            Só Minhas Análises?
        </label>
    </div>
    
</div>

<div class="row">
    
    
    
</div>

<div class="row">

    <div class="col-sm-12">
        <table id="table_id" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Analista</th>
                    <th>Cliente</th>
                    <th>Financeira</th>
                    <th>Parceiro</th>
                    <th>Política de Crédito</th> <!-- ---inicio fim alteracao--- -->
                    <th>R$ Financiado</th>
                    <th>Parcelamento</th>
                    <th>Atualizações</th>
                    <th>Status</th>
                    <th>Data cadastro</th>
                    <th>Espera</th>
                </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
    </div>

</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
            Deseja iniciar esta análise ?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/analise">
            <div class="form-group">
                <div class="col-sm-3">
                    <input id="input_proposta_id" type="hidden" name="id">
                </div>             
            </div>                              
            <div class="alert alert-success">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Analisar'</strong> Para iniciar a análise.
            </div>              
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-green" value="Analisar">
            </div>
        </form>
    </div>     
</div>


<style type="text/css">
    span.label{
        padding: 0.3em 0.5em !important;
    }
    .modal_toggle:hover, .modal_toggle:focus{
        text-decoration: none;
    }
</style>

<?php if (Yii::app()->session['usuario']->primeira_senha) { ?>

    <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-body">
            <p>
                O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
                Por medidas de segurança, solicitamos que você altere a sua senha.
            </p>
        </div>
        <div class="modal-footer">
            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
                <button type="submit"  class="btn btn-primary">
                    Mudar senha
                </button>
            </form>
        </div>      
    </div>

<?php } ?>


<style type="text/css">
    #table_id_filter, #table_id_length{
        display: none
    }
    .buttonpulsate {

        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;

        cursor: pointer;
        display: inline-block;
        font-family: Arial;
        font-size: 20px;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
    }
    @-webkit-keyframes glowing {
        0% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; -webkit-box-shadow: 0 0 60px #b33426; }
        100% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
    }

    @-moz-keyframes glowing {
        0% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; -moz-box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
    }

    @-o-keyframes glowing {
        0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    @keyframes glowing {
        0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    .buttonpulsate {
        -webkit-animation: glowing 1200ms infinite;
        -moz-animation: glowing 1200ms infinite;
        -o-animation: glowing 1200ms infinite;
        animation: glowing 1200ms infinite;
    }
</style>