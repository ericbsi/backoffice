<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Administradores
            </a>
         </li>
         <li class="active">
            Gerenciar
         </li>
      </ol>
<!--      <div class="page-header">
         <h1>
         Administradores
      </div>-->
   </div>
</div>
<!--<p> 
   <a class="btn btn-success" href="#modal_form_new_admin" data-toggle="modal" id="btn_modal_form_new_tabela">
     Cadastrar Administrador <i class="fa fa-plus"></i>
   </a>
</p>-->

<div class="row">
   <div class="col-sm-12">
      <table id="grid_admins" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
             <tr>
               <th class="no-orderable">
                   <input class="form form-control" id="inputNomeUtilizador"    type="text"/>
               </th>
               <th class="no-orderable">
                   <input class="form form-control" id="inputUsername"          type="text"/>
               </th>
               <th class="no-orderable">
                   <input class="form form-control" id="inputSenha"             type="password"/>
               </th>
               <th class="no-orderable">
                    <select multiple="multiple" required="required" id="parceiros_select" class="form-control multipleselect" name="Parceiros[]">
                       <?php foreach ( Filial::model()->findAll() as $filial ) { ?>
                          <option value="<?php echo $filial->id ?>">
                              <?php echo strtoupper( $filial->getConcat() ); ?>
                          </option>
                       <?php } ?>
                    </select>
               </th>
               <th width="3px">
                   <button id="btnSalvar" class="btn btn-green">
                       <i class="fa fa-save"></i>
                   </button>
               </th>
            </tr>
            <tr>
               <th class="no-orderable">
                   Nome Utilizador
               </th>
               <th class="no-orderable">
                   Username
               </th>
               <th class="no-orderable">
                   Senha
               </th>
               <th class="no-orderable">
                   Parceiros Administrados
               </th>
               <th>
               </th>
            </tr>
         </thead>
      </table>
   </div>
</div>
<!--
<div id="modal_form_new_admin" class="modal fade" tabindex="-1" data-width="780" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Administrador</h4>
   </div>
   <form id="form-add-admin">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Nome do Utilizador <span class="symbol required"></span>
                           </label>
                           <input required name="Admin[nome_utilizador]" type="text" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Username <span class="symbol required"></span>
                           </label>
                           <input id="Usuario_username" required name="Admin[username]" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Senha <span class="symbol required"></span>
                           </label>
                           <input id="Usuario_password" required name="Admin[password]" type="password" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Confirme a senha <span class="symbol required"></span>
                           </label>
                           <input required name="password_again" type="password" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">
                              Parceiros <span class="symbol required"></span>
                           </label>
                           <br>
                           <select multiple="multiple" required="required" id="parceiros_select" class="form-control multipleselect" name="Parceiros[]">
                              <?php foreach ( Filial::model()->findAll() as $filial ) { ?>
                                 <option value="<?php echo $filial->id ?>"><?php echo strtoupper( $filial->getConcat() ); ?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <!--<label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>-->
<!--         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>
-->

<style>
   .panel{
      background: transparent!important;
      border:none!important;
   }
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
   #grid_admins_length, #grid_admins_filter, #filiaisAdmin_length {
      display: none;
   }
   td.details-control {
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
</style>