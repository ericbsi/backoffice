<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Usuários
            </a>
         </li>
         <li class="active">
            Listar
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Usuários
      </div>
   </div>
</div>
<p>
<form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/create/">
   <button class="btn btn-success" type="submit">
      Cadastrar Usuário <i class="fa fa-plus"></i>
   </button>
</form>
</p>
<p>

</p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_usuarios" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th                      >Nome Utilizador</th>
               <th                      >Username</th>
               <th class="no-orderable" >Tipo</th>
               <th class="no-orderable" >Empresa / Filial</th>
               <th class="no-orderable" >Senha</th>
               <th class="no-orderable" >Última ação no sistema</th>
               <th class="no-orderable" >Saldo pontos</th>
            </tr>
         </thead>
         <tfoot>
            <tr>
               <th class="searchable"   >Nome utilizador</th>
               <th class="searchable"   >Username</th>
               <th                      ></th>
               <th                      ></th>
               <th                      ></th>
               <th                      ></th>
               <th                      ></th>
            </tr>
         </tfoot>
      </table>
      <input type="hidden" value="<?php echo Yii::app()->session['usuario']->tipo_id ?>" name="" id="tipo_de_usuario_logado_id">
   </div>
</div>
<style type="text/css">
   #grid_usuarios_filter, #grid_usuarios_length{
      display: none;
   }
</style>