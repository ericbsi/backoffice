<?php

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Cadastrar',
);

?>

<?php 
	$this->renderPartial('_form', 
		array(
			'model'=>$model,
			'empresaHasUsuario'=>$empresaHasUsuario,
			'filialHasUsuario'=>$filialHasUsuario,
		)
	); 
?>
