<?php ?>
<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Propostas   
                </a>
            </li>
            <li class="active">
                Minhas propostas
            </li>
        </ol>
        <div class="page-header">
            <h1>Minhas propostas</h1>
            <a class="btn btn-primary" href="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/iniciarAnalise/"><i class="fa fa-plus"></i>
                Adicionar Proposta
            </a>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom:30px;">
    <div class="col-sm-12">
        <table id="table_id" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th></th>
                    <th width="125">Cód</th>
                    <th>Cliente</th>
                    <th width="100">Financeira</th>
                    <th width="75">R$ Inicial</th>
                    <th width="80">R$ Entrada</th>
                    <th width="70">R$ Seguro</th>
                    <th width="100">R$ Financiado</th>
                    <th width="100">Parcelamento</th>
                    <th>R$ Final</th>
                    <th>Atualizações</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
    </div>
</div>
<?php if (Yii::app()->session['usuario']->primeira_senha) { ?>

    <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-body">
            <p>
                O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
                Por medidas de segurança, solicitamos que você altere a sua senha.
            </p>
        </div>
        <div class="modal-footer">
            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
                <button type="submit"  class="btn btn-primary">
                    Mudar senha
                </button>
            </form>
        </div>      
    </div>

<?php } ?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fn-dashboard-crediaristav2.js"></script>

<style>
    #table_id_length,#table_id_filter{display: none!important}

    table.table thead .sorting_asc, table.dataTable thead .sorting{
      background: none!important
    }

    .buttonpulsate {
          
          -webkit-border-radius: 10px;
          border-radius: 10px;
          border: none;
          
          cursor: pointer;
          display: inline-block;
          font-family: Arial;
          font-size: 20px;
          padding: 5px 10px;
          text-align: center;
          text-decoration: none;
    }
    @-webkit-keyframes glowing {
      0% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
      50% { background-color: #d9534f; -webkit-box-shadow: 0 0 60px #b33426; }
      100% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
    }

    @-moz-keyframes glowing {
      0% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
      50% { background-color: #d9534f; -moz-box-shadow: 0 0 10px #b33426; }
      100% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
    }

    @-o-keyframes glowing {
      0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
      50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
      100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    @keyframes glowing {
      0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
      50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
      100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    .buttonpulsate {
      -webkit-animation: glowing 1200ms infinite;
      -moz-animation: glowing 1200ms infinite;
      -o-animation: glowing 1200ms infinite;
      animation: glowing 1200ms infinite;
    }
</style>