<?php

/**
 * This is the model class for table "Tipo_Conta_Bancaria".
 *
 * The followings are the available columns in table 'Tipo_Conta_Bancaria':
 * @property integer $id
 * @property string $tipo
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property DadosBancarios[] $dadosBancarioses
 */
class TipoContaBancariaBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TipoContaBancariaBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Tipo_Conta_Bancaria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('tipo', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro, observacao', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tipo, habilitado, data_cadastro, data_cadastro_br, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dadosBancarioses' => array(self::HAS_MANY, 'DadosBancariosBeta', 'Tipo_Conta_Bancaria_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo' => 'Tipo',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}