<?php


class LotePagamentoHasStatusLotePagamento extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'LotePagamento_has_StatusLotePagamento';
	}

	public function rules()
	{
		return array(
			array('LotePagamento_id, StatusLotePagamento_id, data_cadastro, Usuario_id', 'required'),
			array('LotePagamento_id, StatusLotePagamento_id, habilitado, Usuario_id', 'numerical', 'integerOnly'=>true),
			array('LotePagamento_id, StatusLotePagamento_id, habilitado, data_cadastro, id, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'lotePagamento'			 	=> array(self::BELONGS_TO, 'LotePagamento', 		'LotePagamento_id'),
			'statusLotePagamento' 		=> array(self::BELONGS_TO, 'StatusLotePagamento', 	'StatusLotePagamento_id'),
			'usuario' 					=> array(self::BELONGS_TO, 'Usuario', 				'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LotePagamento_id' => 'Lote Pagamento',
			'StatusLotePagamento_id' => 'Status Lote Pagamento',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'id' => 'ID',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LotePagamento_id',$this->LotePagamento_id);
		$criteria->compare('StatusLotePagamento_id',$this->StatusLotePagamento_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}