<?php

/**
 * This is the model class for table "Telefone".
 *
 * The followings are the available columns in table 'Telefone':
 * @property integer $id
 * @property string $numero
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property string $discagem_direta_distancia
 * @property string $ramal
 * @property integer $Tipo_Telefone_id
 * @property integer $telefone_proprio
 *
 * The followings are the available model relations:
 * @property ContatoHasTelefone[] $contatoHasTelefones
 */
class Telefone extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Telefone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo_Telefone_id', 'required'),
			array('habilitado, Tipo_Telefone_id, telefone_proprio', 'numerical', 'integerOnly'=>true),
			array('numero, ramal', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('discagem_direta_distancia', 'length', 'max'=>4),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, habilitado, data_cadastro, data_cadastro_br, discagem_direta_distancia, ramal, Tipo_Telefone_id, telefone_proprio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contatoHasTelefones' => array(self::HAS_MANY, 'ContatoHasTelefone', 'Telefone_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Numero',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'discagem_direta_distancia' => 'Discagem Direta Distancia',
			'ramal' => 'Ramal',
			'Tipo_Telefone_id' => 'Tipo Telefone',
			'telefone_proprio' => 'Telefone Proprio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('discagem_direta_distancia',$this->discagem_direta_distancia,true);
		$criteria->compare('ramal',$this->ramal,true);
		$criteria->compare('Tipo_Telefone_id',$this->Tipo_Telefone_id);
		$criteria->compare('telefone_proprio',$this->telefone_proprio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Telefone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
