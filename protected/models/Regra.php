<?php


class Regra extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'Regra';
	}

	public function rules()
	{
		return array(
			array('score, status', 'required'),
			array('status, historico, restricao, alerta, max_passagens_casa, vendas_negadas, rg_anexado, max_passagens_spc, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor_de, valor_ate, comprometimento_da_renda, contratos_em_aberto', 'numerical'),
			array('score', 'length', 'max'=>10),
			array('id, score, status, historico, valor_de, valor_ate, restricao, comprometimento_da_renda, contratos_em_aberto, alerta, max_passagens_casa, vendas_negadas, rg_anexado, max_passagens_spc, habilitado', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'politicaCreditoHasRegras' => array(self::HAS_MANY, 'PoliticaCreditoHasRegra', 'Regra_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' 						=> 'ID',
			'score' 					=> 'Score',
			'status' 					=> 'Status',
			'historico' 				=> 'Historico',
			'valor_de' 					=> 'Valor De',
			'valor_ate' 				=> 'Valor Ate',
			'restricao' 				=> 'Restricao',
			'comprometimento_da_renda' 	=> 'Comprometimento Da Renda',
			'contratos_em_aberto' 		=> 'Contratos Em Aberto',
			'alerta' 					=> 'Alerta',
			'max_passagens_casa' 		=> 'Max Passagens Casa',
			'vendas_negadas' 			=> 'Vendas Negadas',
			'rg_anexado' 				=> 'Rg Anexado',
			'max_passagens_spc' 		=> 'Max Passagens Spc',
			'habilitado' 				=> 'Habilitado',
		);
	}

	public function search()
	{

		$criteria 										= new CDbCriteria;

		$criteria->compare('id',						$this->id);
		$criteria->compare('score',						$this->score,true);
		$criteria->compare('status',					$this->status);
		$criteria->compare('historico',					$this->historico);
		$criteria->compare('valor_de',					$this->valor_de);
		$criteria->compare('valor_ate',					$this->valor_ate);
		$criteria->compare('restricao',					$this->restricao);
		$criteria->compare('comprometimento_da_renda',	$this->comprometimento_da_renda);
		$criteria->compare('contratos_em_aberto',		$this->contratos_em_aberto);
		$criteria->compare('alerta',					$this->alerta);
		$criteria->compare('max_passagens_casa',		$this->max_passagens_casa);
		$criteria->compare('vendas_negadas',			$this->vendas_negadas);
		$criteria->compare('rg_anexado',				$this->rg_anexado);
		$criteria->compare('max_passagens_spc',			$this->max_passagens_spc);
		$criteria->compare('habilitado',				$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}