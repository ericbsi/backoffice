<?php


class Bordero extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bordero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Bordero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dataCriacao, destinatario, criadoPor, Status', 'required'),
			array('destinatario, criadoPor, Status, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dataCriacao, destinatario, criadoPor, Status, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'destinatarioObj' => array(self::BELONGS_TO, 'Filial', 'destinatario'),
			'status' => array(self::BELONGS_TO, 'StatusDoBordero', 'Status'),
			'criadoPor0' => array(self::BELONGS_TO, 'Usuario', 'criadoPor'),
			'borderoHasParcelas' => array(self::HAS_MANY, 'BorderoHasParcela', 'Bordero_id'),
			'itemDoBorderos' => array(self::HAS_MANY, 'ItemDoBordero', 'Bordero', 'condition' => 'itemDoBorderos.habilitado'),
			'loteHasBorderos' => array(self::HAS_MANY, 'LoteHasBordero', 'Bordero_id'),
			'recebimentoDeDocumentacao' => array(self::HAS_ONE, 'RecebimentoDeDocumentacao', 'Bordero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dataCriacao' => 'Data Criacao',
			'destinatario' => 'Destinatario',
			'criadoPor' => 'Criado Por',
			'Status' => 'Status',
			'habilitado' => 'Habilitado',
		);
	}


	public function getValor()
	{
		$valorTotal = 0;

		foreach(  $this->itemDoBorderos as $item )
		{
			$valorTotal += $item->proposta->valorRepasse();
		}

		return $valorTotal;
	}

	public function getPropostas( $draw, $start, $length, $borderoId )
	{
		$data 					= [];
		$bordero 				= Bordero::model()->findByPk($borderoId);

		$total 					= count($bordero->itemDoBorderos);

		foreach( $bordero->itemDoBorderos as $item )
		{
			$data[]				= [
				'codigo'		=> $item->proposta->codigo,
				'valor'			=> "R$ ".number_format($item->proposta->valorRepasse(), 2, ',', '.'),
				'cliente' 		=> strtoupper($item->proposta->analiseDeCredito->cliente->pessoa->nome),
				'cpf'			=> $item->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
				'filial'		=> strtoupper($item->proposta->analiseDeCredito->filial->getConcat()),
			];
		}

		return [
			'draw'				=> $draw,
			'recordsTotal'		=> $total,
			'recordsFiltered'	=> $length,
			'data' 				=> $data
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dataCriacao',$this->dataCriacao,true);
		$criteria->compare('destinatario',$this->destinatario);
		$criteria->compare('criadoPor',$this->criadoPor);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}