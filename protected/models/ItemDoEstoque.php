<?php

/**
 * This is the model class for table "Item_do_Estoque".
 *
 * The followings are the available columns in table 'Item_do_Estoque':
 * @property integer $id
 * @property string $descricao
 * @property integer $Produto_id
 * @property integer $Estoque_id
 * @property double $saldo
 * @property integer $habilitado
 * @property string $codigo_de_barras
 *
 * The followings are the available model relations:
 * @property FinanceiraHasItemDoEstoque[] $financeiraHasItemDoEstoques
 * @property ItemDaVenda[] $itemDaVendas
 * @property Estoque $estoque
 * @property Produto $produto
 * @property ItemDoInventario[] $itemDoInventarios
 * @property ItemHasUM[] $itemHasUMs
 */
class ItemDoEstoque extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemDoEstoque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_do_Estoque';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, saldo, codigo_de_barras', 'required'),
			array('Produto_id, Estoque_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('saldo', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, Produto_id, Estoque_id, saldo, habilitado, codigo_de_barras', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'financeiraHasItemDoEstoques' => array(self::HAS_MANY, 'FinanceiraHasItemDoEstoque', 'ItemDoEstoque_id'),
			'itemDaVendas' => array(self::HAS_MANY, 'ItemDaVenda', 'Item_do_Estoque_id'),
			'estoque' => array(self::BELONGS_TO, 'Estoque', 'Estoque_id'),
			'produto' => array(self::BELONGS_TO, 'Produto', 'Produto_id'),
			'itemDoInventarios' => array(self::HAS_MANY, 'ItemDoInventario', 'Item_do_Estoque_id'),
			'itemHasUMs' => array(self::HAS_MANY, 'ItemHasUM', 'Item_do_Estoque_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'Produto_id' => 'Produto',
			'Estoque_id' => 'Estoque',
			'saldo' => 'Saldo',
			'habilitado' => 'Habilitado',
			'codigo_de_barras' => 'Codigo De Barras',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('Produto_id',$this->Produto_id);
		$criteria->compare('Estoque_id',$this->Estoque_id);
		$criteria->compare('saldo',$this->saldo);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('codigo_de_barras',$this->codigo_de_barras,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}