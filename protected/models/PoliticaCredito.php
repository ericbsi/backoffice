<?php

/**
 * This is the model class for table "PoliticaCredito".
 *
 * The followings are the available columns in table 'PoliticaCredito':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property string $data_cadatro
 * @property string $cor
 *
 * The followings are the available model relations:
 * @property Filial[] $filials
 */
class PoliticaCredito extends CActiveRecord
{

   /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return PoliticaCredito the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'PoliticaCredito';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
          array('descricao, cor' , 'required'                              ),
          array('habilitado'     , 'numerical'  , 'integerOnly'   => true  ),
          array('descricao, cor' , 'length'     , 'max'           => 200   ),
          array('data_cadatro'   , 'safe'                                  ),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('id, descricao, habilitado, data_cadastro, cor', 'safe', 'on' => 'search'),
      );
   }

   /**
    * @return array relational rules.
    */
   public function relations()
   {
      // NOTE: you may need to adjust the relation name and the related
      // class name for the relations automatically generated below.
      return array(
            'filials' => array(self::HAS_MANY, 'Filial', 'PoliticaCredito_id'),
            'filialHasPoliticaCredito'  =>  array
                                            (
                                                self::HAS_ONE                                                                                                                                   , 
                                                'FilialHasPoliticaCredito'                                                                                                                      , 
                                                'PoliticaCredito_id'                                                                                                                            ,    
                                                'on'                        => 'habilitado AND ' . date('Y-m-d') . ' >= dataInicio AND ('  . date('Y-m-d') . ' <= dataFim OR dataFim IS NULL)'
                                            ),
      );
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'descricao' => 'Descricao',
          'habilitado' => 'Habilitado',
          'data_cadatro' => 'Data Cadatro',
          'cor' => 'Cor',
      );
   }

   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('descricao', $this->descricao, true);
      $criteria->compare('habilitado', $this->habilitado);
      $criteria->compare('data_cadatro', $this->data_cadatro, true);
      $criteria->compare('cor', $this->cor, true);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }
   
   public function salvar($PoliticaCredito)
   {

      $arrayRetorno = array(
         'hasErrors'    => 0                                ,
         'msg'          => 'Política de Cŕedito Cadastrada com Sucesso.' ,
         'pntfyClass'   => 'success'
      );

      $politica 		= new PoliticaCredito ;
      $politica->attributes 	= $PoliticaCredito    ;
      $politica->habilitado 	= 1                   ;
      $politica->data_cadastro 	= date('Y-m-d H:i:S') ;

      if( !$politica->save() )
      {
         
         ob_start();
         var_dump($politica->getErrors());
         $result = ob_get_clean();
         
         $arrayRetorno['msg'        ]  = 'Ocorreu um erro. Tente novamente.' . $result;
         $arrayRetorno['pntfyClass' ]  = 'error';
      }
      
      return $arrayRetorno;
      
   }

}
