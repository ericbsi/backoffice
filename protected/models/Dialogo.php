<?php

/**
 * This is the model class for table "Dialogo".
 *
 * The followings are the available columns in table 'Dialogo':
 * @property integer $id
 * @property string $assunto
 * @property string $data_criacao
 * @property integer $habilitado
 * @property string $Entidade_Relacionamento
 * @property integer $Entidade_Relacionamento_id
 * @property integer $criado_por
 *
 * The followings are the available model relations:
 * @property Usuario $criadoPor
 */
class Dialogo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Dialogo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Dialogo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, Entidade_Relacionamento_id, criado_por', 'numerical', 'integerOnly'=>true),
			array('Entidade_Relacionamento', 'length', 'max'=>100),
			array('assunto, data_criacao', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, assunto, data_criacao, habilitado, Entidade_Relacionamento, Entidade_Relacionamento_id, criado_por', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'criadoPor' => array(self::BELONGS_TO, 'Usuario', 'criado_por'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'assunto' => 'Assunto',
			'data_criacao' => 'Data Criacao',
			'habilitado' => 'Habilitado',
			'Entidade_Relacionamento' => 'Entidade Relacionamento',
			'Entidade_Relacionamento_id' => 'Entidade Relacionamento',
			'criado_por' => 'Criado Por',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('assunto',$this->assunto,true);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Entidade_Relacionamento',$this->Entidade_Relacionamento,true);
		$criteria->compare('Entidade_Relacionamento_id',$this->Entidade_Relacionamento_id);
		$criteria->compare('criado_por',$this->criado_por);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}