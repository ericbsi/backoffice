<?php

/**
 * This is the model class for table "Contato".
 *
 * The followings are the available columns in table 'Contato':
 * @property integer $id
 * @property string $facebook
 * @property string $skype
 * @property string $twitter
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ContatoHasEmail[] $contatoHasEmails
 * @property ContatoHasTelefone[] $contatoHasTelefones
 * @property DadosProfissionais[] $dadosProfissionaises
 * @property EmpresaHasContato[] $empresaHasContatos
 * @property Filial[] $filials
 * @property FornecedorHasContato[] $fornecedorHasContatos
 * @property SequenciaHasContato[] $sequenciaHasContatos
 */
class Contato extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contato the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Contato';
	}

	public function getEmail()
	{	
		return ( $this->contatoHasEmail !== NULL ) ? $this->contatoHasEmail->email : NULL;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('facebook, skype, twitter', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, facebook, skype, twitter, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'contatoHasEmail' 		=> array(self::HAS_ONE,  'ContatoHasEmail', 		'Contato_id'),
			'contatoHasEmails' 		=> array(self::HAS_MANY, 'ContatoHasEmail', 		'Contato_id'),
			'contatoHasTelefones' 	=> array(self::HAS_MANY, 'ContatoHasTelefone', 		'Contato_id'),
			'dadosProfissionaises' 	=> array(self::HAS_MANY, 'DadosProfissionais', 		'Contato_id'),
			'empresaHasContatos' 	=> array(self::HAS_MANY, 'EmpresaHasContato', 		'Contato_id'),
			'filials' 				=> array(self::HAS_MANY, 'Filial', 					'Contato_id'),
			'fornecedorHasContatos' => array(self::HAS_MANY, 'FornecedorHasContato', 	'Contato_id'),
			'sequenciaHasContatos' 	=> array(self::HAS_MANY, 'SequenciaHasContato', 	'Contato_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 					=> 'ID',
			'facebook' 				=> 'Facebook',
			'skype' 				=> 'Skype',
			'twitter' 				=> 'Twitter',
			'data_cadastro' 		=> 'Data Cadastro',
			'data_cadastro_br' 		=> 'Data Cadastro Br',
			'habilitado' 			=> 'Habilitado',
		);
	}

	public function novo( $Contato )
	{
		$nContato 				= new Contato;
		$nContato->attributes 	= $Contato;
		$nContato->save();

		return $nContato;
	}
}