<?php

/**
 * This is the model class for table "PropostaOmniConfig".
 *
 * The followings are the available columns in table 'PropostaOmniConfig':
 * @property integer $id
 * @property string $urlContrato
 * @property string $urlBoleto
 * @property string $codigoOmni
 * @property string $codigoSigac
 * @property integer $habilitado
 * @property integer $Proposta
 * @property integer $interno
 * @property integer $numPropLoj
 * @property string $urlCG
 * @property string $usuarioHost
 * @property string $tabelaHost
 * @property string $data_cadastro
 */
class PropostaOmniConfig extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PropostaOmniConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PropostaOmniConfig';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Proposta, tabelaHost', 'required'),
			array('habilitado, Proposta, interno, numPropLoj', 'numerical', 'integerOnly'=>true),
			array('urlCG', 'length', 'max'=>500),
			array('usuarioHost', 'length', 'max'=>256),
			array('tabelaHost', 'length', 'max'=>45),
			array('urlContrato, urlBoleto, codigoOmni, codigoSigac, data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, urlContrato, urlBoleto, codigoOmni, codigoSigac, habilitado, Proposta, interno, numPropLoj, urlCG, usuarioHost, tabelaHost, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'urlContrato' => 'Url Contrato',
			'urlBoleto' => 'Url Boleto',
			'codigoOmni' => 'Codigo Omni',
			'codigoSigac' => 'Codigo Sigac',
			'habilitado' => 'Habilitado',
			'Proposta' => 'Proposta',
			'interno' => 'Interno',
			'numPropLoj' => 'Num Prop Loj',
			'urlCG' => 'Url Cg',
			'usuarioHost' => 'Usuario Host',
			'tabelaHost' => 'Tabela Host',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('urlContrato',$this->urlContrato,true);
		$criteria->compare('urlBoleto',$this->urlBoleto,true);
		$criteria->compare('codigoOmni',$this->codigoOmni,true);
		$criteria->compare('codigoSigac',$this->codigoSigac,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Proposta',$this->Proposta);
		$criteria->compare('interno',$this->interno);
		$criteria->compare('numPropLoj',$this->numPropLoj);
		$criteria->compare('urlCG',$this->urlCG,true);
		$criteria->compare('usuarioHost',$this->usuarioHost,true);
		$criteria->compare('tabelaHost',$this->tabelaHost,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}