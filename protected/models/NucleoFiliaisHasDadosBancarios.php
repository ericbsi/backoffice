<?php

class NucleoFiliaisHasDadosBancarios extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'NucleoFiliais_has_Dados_Bancarios';
	}

	public function rules()
	{

		return array(
			array('NucleoFiliais_id, Dados_Bancarios_id', 'required'),
			array('NucleoFiliais_id, Dados_Bancarios_id', 'numerical', 'integerOnly'=>true),
			array('NucleoFiliais_id, Dados_Bancarios_id, id', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'dadosBancarios' => array(self::BELONGS_TO, 'DadosBancarios', 'Dados_Bancarios_id'),
			'nucleoFiliais' => array(self::BELONGS_TO, 'NucleoFiliais', 'NucleoFiliais_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'NucleoFiliais_id' => 'Nucleo Filiais',
			'Dados_Bancarios_id' => 'Dados Bancarios',
			'id' => 'ID',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);
		$criteria->compare('Dados_Bancarios_id',$this->Dados_Bancarios_id);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}