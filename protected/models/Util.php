<?php 

class Util {
	

	public function diffTime( $d1, $d2 )
	{
		$d1 		= strtotime( $d1 );
		$d2 		= strtotime( $d2 );

		return 		$d2 - $d1;
	}

	public function callSoapOmni($xmlString,$apiUrl) //string xml, endereco da url
	{
        $objProposta  = new SoapVar($xmlString, XSD_ANYXML);
      
        $soapCliente  = new SoapClient( $apiUrl, array("soap_version" => 1, "trace" => 1, "exceptions" => 1,'features' => SOAP_SINGLE_ELEMENT_ARRAYS ) );

        try
        {
        	$result   = $soapCliente->CM_PROPOSTA($objProposta, null);

            return simplexml_load_string($result->propostaResponse);
        }

        catch(SoapFault $e)
        {
        	return $e;
        }
	}

	public function arrayToXML($array, SimpleXMLElement $xml, $child_name)
	{
	    foreach ($array as $k => $v){

	        if( is_array( $v ) )
	        {
	          	( is_int( $k ) ) ? $this->arrayToXML( $v, $xml->addChild( $child_name ), $v ) : $this->arrayToXML( $v, $xml->addChild( strtolower( $k ) ), $child_name );
	        }

	        else{
	            ( is_int( $k ) ) ? $xml->addChild( $child_name, $v ) : $xml->addChild( strtolower( $k ), $v );
	        }
	    }

	    return $xml;
	}

	public function limparCPFPJ ( $string )
	{
		$string = str_replace('-', '', $string);

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

	public function clienteIdade( $nascimento )
	{
		$nascimento 	= $this->view_date_to_bd($nascimento);

		$d1     		= new DateTime( date('Y-m-d') );
        $d2     		= new DateTime($nascimento);

        $diff   		= $d2->diff($d1);

        if( $diff->y <= 90 && $diff->y >= 18 )
        {
        	echo "true";
        }
        else
        {
        	echo "false";
        }
	}

	public function getMeses()
	{
		return array(
			array(1,	'Janeiro'		,'Jan'),
			array(2,	'Fevereiro'		,'Fev'),
			array(3,	'Março'			,'Mar'),
			array(4,	'Abril'			,'Abr'),
			array(5,	'Maio'			,'Mai'),
			array(6,	'Junho'			,'Jun'),
			array(7,	'Julho'			,'Jul'),
			array(8,	'Agosto'		,'Ago'),
			array(9, 	'Setembro'		,'Set'),
			array(10, 	'Outubro'		,'Out'),
			array(11, 	'Novembro'		,'Nov'),
			array(12,	'Dezembro'		,'Dez'),
		);
	}

	public function checkDiaFinalDeSemana( $data )
	{
		$dataFds 	= date('w',strtotime( $data ) );

		//Sábado ou Domingo...
		if( $dataFds == 0 || $dataFds == 6 )
		{
			return TRUE;
		}
		
		//Dia útil
		else 
		{
			return FALSE;
		}
	}

	public function validarCPF($cpf = null){

		// Verifica se um número foi informado
    	if( empty( $cpf ) )
    	{
        	return false;
    	}

    	// Elimina possivel mascara
    	$cpf = preg_replace('[^0-9]', '', $cpf);
    	//$cpf = ereg_replace('[^0-9]', '', $cpf);
    	$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    	// Verifica se o numero de digitos informados é igual a 11
	    if ( strlen( $cpf ) != 11 )
	    {
	        return false;
	    }

	    // Verifica se nenhuma das sequências invalidas abaixo 
    	// foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
	    {
        	return false;
		    // Calcula os digitos verificadores para verificar se o
		    // CPF é válido
     	}
     	else
     	{
     		for ($t = 9; $t < 11; $t++)
     		{
     			for ($d = 0, $c = 0; $c < $t; $c++)
     			{
                	$d += $cpf{$c} * (($t + 1) - $c);
            	}
            	
            	$d = ((10 * $d) % 11) % 10;
            	
            	if ($cpf{$c} != $d)
            	{
            		return false;
            	}
     		}

     		return true;
     	}
	}

	public function adicionarMesMantendoDia( $date, $months, $format = "Y-m-d" ){
                
        $date = \DateTime::createFromFormat( $format, $date );

        for( $i = 0;$i < $months; $i++ )
        {
        	$date->modify('+ ' . date("t", $date->getTimestamp())  . ' day');
        }

    	return $date;
	}

	public function montaContratoAss($str){

		$linha = '';

		for ( $i = 0; $i < strlen($str); $i++ ) {
			$linha .='_';
		}
		
		return $linha;		

	}

	public function dataPorExtenso($dia, $mes, $ano){

		$semana = array(
        	'Sun' => 'Domingo',
        	'Mon' => 'Segunda-Feira',
        	'Tue' => 'Terca-Feira',
        	'Wed' => 'Quarta-Feira',
        	'Thu' => 'Quinta-Feira',
        	'Fri' => 'Sexta-Feira',
        	'Sat' => 'Sábado'
 	   );
 
	    $mes_extenso = array(
	        'Jan' => 'Janeiro',
	        'Feb' => 'Fevereiro',
	        'Mar' => 'Marco',
	        'Apr' => 'Abril',
	        'May' => 'Maio',
	        'Jun' => 'Junho',
	        'Jul' => 'Julho',
	        'Aug' => 'Agosto',
	        'Nov' => 'Novembro',
	        'Sep' => 'Setembro',
	        'Oct' => 'Outubro',
	        'Dec' => 'Dezembro'
	    );

	    return "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";

	}

	public function valor_extenso($valor = 0, $maiusculas = false)
	{
	    // verifica se tem virgula decimal
	    if (strpos($valor,",") > 0)
	    {
	      // retira o ponto de milhar, se tiver
	      $valor = str_replace(".","",$valor);
	 
	      // troca a virgula decimal por ponto decimal
	      $valor = str_replace(",",".",$valor);
	    }

		$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
		$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
 
		$c 		= array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
		$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
		$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
		$u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
 
        $z = 0;
 
        $valor = number_format($valor, 2, ".", ".");

        $inteiro = explode(".", $valor);
		
		$cont = count($inteiro);
		
		for( $i = 0; $i < $cont; $i++ ){
            for( $ii = strlen( $inteiro[$i] ); $ii < 3; $ii++ ){
            	$inteiro[$i] = "0".$inteiro[$i];
        	}
		}
 		

        $fim = $cont - ($inteiro[$cont-1] > 0 ? 1 : 2);
		$rt = '';
        
        for ( $i = 0;$i < $cont; $i++ ) {

                $valor = $inteiro[$i];
                $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
                $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
                $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

				 
                $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
                $t = $cont-1-$i;
                $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
                
                if ($valor == "000")
                	$z++; elseif ($z > 0) $z--;

                if ( ($t==1) && ($z>0) && ($inteiro[0] > 0) ) 
                	$r .= (($z>1) ? " de " : "").$plural[$t];
				
                if ( $r ){
                	$rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
                }
        }

 		
        if( !$maiusculas )
        {
        	return($rt ? $rt : "zero");
        }

        elseif( $maiusculas == "2" ) 
        {
        	return (strtoupper($rt) ? strtoupper($rt) : "Zero");
        } 

        else
        {
        	return (ucwords($rt) ? ucwords($rt) : "Zero");
        } 	
	}

	public function getValorMonetarioCnab( $valor ){}

	public function countDigits( $str )
	{
    	return preg_match_all( "/[0-9]/", $str );
	}

	public function cleanStr($string, $case) {

	   $map = array(
		    'á' => 'a',
		    'à' => 'a',
		    'ã' => 'a',
		    'â' => 'a',
		    'é' => 'e',
		    'ê' => 'e',
		    'í' => 'i',
		    'ó' => 'o',
		    'ô' => 'o',
		    'õ' => 'o',
		    'ú' => 'u',
		    'ü' => 'u',
		    'ç' => 'c',
		    'Á' => 'A',
		    'À' => 'A',
		    'Ã' => 'A',
		    'Â' => 'A',
		    'É' => 'E',
		    'Ê' => 'E',
		    'Í' => 'I',
		    'Ó' => 'O',
		    'Ô' => 'O',
		    'Õ' => 'O',
		    'Ú' => 'U',
		    'Ü' => 'U',
		    'Ç' => 'C',
		    '(' => '',
		    ')' => '',
	   		'-'	=> '',
	   		' ' => '',
	   		'&' => 'e',
	   		'/' => '',
	   		'.' => '',
	   		'-' => '',
		);

	    if ( $case == 'upp' )	
	   		return strtoupper(strtr($string, $map));
	   	
	   	return strtolower(strtr($string, $map));

	}

	public function nomeSemAcentos($string, $case){
	
		$map 	= [
			'á' => 'a',
		    'à' => 'a',
		    'ã' => 'a',
		    'â' => 'a',
		    'é' => 'e',
		    'ê' => 'e',
		    'í' => 'i',
		    'ó' => 'o',
		    'ô' => 'o',
		    'õ' => 'o',
		    'ú' => 'u',
		    'ü' => 'u',
		    'ç' => 'c',
		    'Á' => 'A',
		    'À' => 'A',
		    'Ã' => 'A',
		    'Â' => 'A',
		    'É' => 'E',
		    'Ê' => 'E',
		    'Í' => 'I',
		    'Ó' => 'O',
		    'Ô' => 'O',
		    'Õ' => 'O',
		    'Ú' => 'U',
		    'Ü' => 'U',
		    'Ç' => 'C',
		];

		if ( $case == 'upp' )	
	   		return strtoupper(strtr($string, $map));
	   	
	   	return strtolower(strtr($string, $map));

	}

	public function applyMonetaryMask( $val, $type ){

		switch ( $type ) {
			case 'echo':
				echo "R$ " . number_format($val, 2, ",", ".");
				break;
			
			default:
				return  "R$ " . number_format($val, 2, ",", ".");
				break;
		}

	}

	public function view_date_to_bd($data){

		$dia 					= '01';
		$mes 					= '01';
		$ano 					= '1990';

		$partes 			   	= explode('/', $data);

		if( count( $partes ) >= 3 ){
			list($dia, $mes, $ano) 	= explode('/', $data);
		}

        $valor 					= $ano . '-' . $mes . '-' . $dia;

        return $valor;
	}

	public function bd_date_to_view($data){

		list($ano, $mes, $dia) = explode('-', $data);

        $valor = $dia . '/' . $mes . '/' . $ano;

        return $valor;

	}

	public function strIsNull( $value ){

		if ( trim( $value ) == "" && $value == NULL)
			return true;
		return false;
	}

	public function countEmpresas (){

		return sizeof(Empresa::model()->findAll());

	}

	public function crypto_rand_secure($min, $max){
	        $range = $max - $min;
	        if ($range < 0) return $min; // not so random...
	        $log = log($range, 2);
	        $bytes = (int) ($log / 8) + 1; // length in bytes
	        $bits = (int) $log + 1; // length in bits
	        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	        do {
	            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
	            $rnd = $rnd & $filter; // discard irrelevant bits
	        } while ($rnd >= $range);
	        return $min + $rnd;
	}

	public function getToken($length){
	    
	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	    $codeAlphabet.= "0123456789";
	    
	    for( $i = 0; $i < $length;$i++ ){
	        $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }

    	return $token;
	}

	public function getCodeVenda ( $codeAlphabet, $length ) {
		
		$codigo = "";
		
		for( $i = 0; $i < $length;$i++ ){
	        $codigo .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
	    }

	    return $codigo;
	}

	public function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    	$numbers = range($min, $max);
    	shuffle($numbers);
    	return array_slice($numbers, 0, $quantity);
	}

	public function curlCEP( $url, $post = array(), $get = array() ){

		$url = explode('?',$url,2);

		if( count($url) === 2 ){

			$temp_get = array();

			parse_str($url[1],$temp_get);

			$get = array_merge($get,$temp_get);

		}

		$ch = curl_init($url[0]."?".http_build_query($get));
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		return curl_exec ($ch);
	}

	public function buscarCEP($cep){

		libxml_use_internal_errors(true);

		include('protected/vendor/phpQuery-onefile.php');

		$html = $this->curlCEP('http://m.correios.com.br/movel/buscaCepConfirma.do',array(
			'cepEntrada'=>$cep,
			'tipoCep'=>'',
			'cepTemp'=>'',
			'metodo'=>'buscarCep'
		));

		phpQuery::newDocumentHTML($html, $charset = 'utf-8');

		$dados = array(
			'logradouro'=> trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
			'bairro'=> trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html()),
			'cidade/uf'=> trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
			'cep'=> trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
		);

		$dados['cidade/uf'] = explode('/',$dados['cidade/uf']);
		$dados['cidade'] = trim($dados['cidade/uf'][0]);
		$dados['uf'] = trim($dados['cidade/uf'][1]);

		unset($dados['cidade/uf']);
		
		echo ( json_encode($dados) );
	}

	public function moedaViewToBD($val)
	{
		return str_replace(array('.',','), array('','.'), $val);
	}

	public function round_up($value, $places = 0 )
	{

        if ( $places < 0 ) { $places = 0; }
        
        $mult = pow(10, $places);
    	
    	return ceil( $value * $mult ) / $mult;
	}

	public function round_down($value, $precision = 0)
	{

		if ( $precision < 0 ) { $precision = 0; }

	    $power = pow(10, $precision);

	    return floor( $value * $power ) / $power;
	}

	public function arredondar($valor)
	{
		$valores 			= explode('.', $valor);

		if( sizeof( $valores ) > 1 )
		{
			$ultimoDigito 		= substr($valores[1], -1);

			if( $ultimoDigito < 5 )
			{
				return $this->round_down( $valor, 2 );
			}
			else
			{
				return $this->round_up( $valor, 2 );
			}
		}
		else
		{
			return $valor;
		}
	}

	public function consertarTitulosSantander( $titulos )
	{
		$arrReturn 		= array(
			'hasErrors' => 0, 
			'errors' 	=> array(
			)
		);

		$valorTotalDeRegistros	= 0;

		$arquivoRemessa             				= 'remessas/santander/consertos/'.date('YmdHis').'.txt';

		/*Verifica se o arquivo já existe. Se não existir, prossegue...*/
		if ( !file_exists( $arquivoRemessa ) )
		{
			/*
				-Primeiro, verificar se existem detalhes na data informada
				-O processo só seguirá caso existam detalhes.
			*/

			$criteriaDetalhes 								= new CDbCriteria;
			$criteriaDetalhes->addInCondition('t.id', 		$titulos);
			
			$registros                						= CnabDetalhe::model()->findAll($criteriaDetalhes);

			if( count( $registros ) > 0 )
			{
				$valorTotalDeRegistros						= 0;
				$confgCamposSantander       				= CampoCnabConfig::model()->cnab400SantanderConfig(); //Carrega as configurações do CNAB400 Santander
				$numSequencial 								= 1;
				$nSpacesAdd 								= 0;
		        $handle                     				= fopen($arquivoRemessa, 'w+'); //Cria o arquivo
		        $headerBaseSantander    					= CnabHeader::model()->findByPk(3); //Header Base do Santander
		        $trailer    								= new CnabTrailer;
		        $headerBaseSantander->data_gravacao_texto 	= date('dmy');
		       
		        $camposHeader             					= CampoCnabConfig::model()->returnCampos( $confgCamposSantander['header'] );
		        $camposDetalhe             					= CampoCnabConfig::model()->returnCampos( $confgCamposSantander['detalhe'] );
		        $camposTrailer             					= CampoCnabConfig::model()->returnCampos( $confgCamposSantander['trailer'] );

		        /*INICIO HEADER*/
		        $linhaHeader 								= '';

		        for ( $i = 0; $i < count( $camposHeader ); $i++ )
		        {
		        	$campoHeader 							= CampoCnabConfig::model()->findByPk( $camposHeader[$i] );
		        	$nSpacesAdd         					= ( $campoHeader->posi_fim - $campoHeader->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaHeader          				.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader               			 	= substr_replace($linhaHeader, $headerBaseSantander->getAttribute($campoHeader->sigla), $campoHeader->posi_ini-1,1);
		        }

		        fwrite($handle, substr($linhaHeader,0,400).PHP_EOL);
		        /*FIM HEADER*/
		        	
		        /*INICIO REGISTROS*/
		        foreach( $registros as $r )
		        {
		        	$numSequencial++;
		        	$nSpacesAdd 							= 0;
		        	$linhaDetalhe                  			= '';
		        	$valorTotalDeRegistros					+= $r->valor_do_titulo;

		        	$r->numero_sequencial 					= CampoCnabConfig::model()->getCodigoSequencial('detalhe',date('dmy'), $numSequencial);

		        	for ( $i = 0; $i < sizeof( $camposDetalhe ); $i++ )
		        	{
		        		$campoDetalhe              			= CampoCnabConfig::model()->findByPk( $camposDetalhe[$i] );

		                $nSpacesAdd         				= ( $campoDetalhe->posi_fim - $campoDetalhe->posi_ini ) + 1;

		                for ( $y = 0; $y < $nSpacesAdd; $y++ )
		                {
		                	$linhaDetalhe          			.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		                }

		                $linhaDetalhe               		= substr_replace($linhaDetalhe, $r->getAttribute($campoDetalhe->sigla), $campoDetalhe->posi_ini-1,1);
		        	}

		        	fwrite($handle, substr($linhaDetalhe,0,400).PHP_EOL);
		        }
		        /*FIM REGISTROS*/

		       	/*INICIO TRAILER*/
		        $nSpacesAdd 										= 0;
		        $linhaTrailer 										= '';

		        $trailer->cod_registro				 				= '9';
		        $trailer->quantidade_de_documentos_no_arquivo_sant	= CampoCnabConfig::model()->getCodigoSequencial('detalhe', date('dmy'), count($registros)+2);
		        $trailer->valor_total_dos_titulos_sant				= CampoCnabConfig::model()->cnabValorMonetario($valorTotalDeRegistros,13);
		        $trailer->numero_sequencial				 			= CampoCnabConfig::model()->getCodigoSequencial('detalhe', date('dmy'), count($registros)+2);
		        $trailer->zeros1_sant				 				= '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

		        for( $x = 0; $x < count( $camposTrailer ); $x ++ )
		        {
			    	$campoTrailer 			    					= CampoCnabConfig::model()->findByPk( $camposTrailer[$x] );
			    	$nSpacesAdd         							= ( $campoTrailer->posi_fim - $campoTrailer->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaTrailer          						.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaTrailer               			 		= substr_replace($linhaTrailer, $trailer->getAttribute($campoTrailer->sigla), $campoTrailer->posi_ini-1,1);
		        }
		        
		        fwrite($handle, substr($linhaTrailer,0,400).PHP_EOL);
		        /*FIM TRAILER*/

		        fclose($handle);

		        $arrReturn['hasErrors'] = 0;
				$arrReturn['errors'][] 	= 'Tudo ok.';
			}

			/*Não existem registros para o critério informado*/
			else
			{
				$arrReturn['hasErrors'] = 1;
				$arrReturn['errors'][] 	= 'Nenhum registro encontrado';
			}
	    }

	    /*Usuário informou um nome de arquivo que já existe*/
	    else
		{
			$arrReturn['hasErrors'] = 1;
			$arrReturn['errors'][] 	= 'Arquivo já existente.';
		}

		return $arrReturn;
	}
}