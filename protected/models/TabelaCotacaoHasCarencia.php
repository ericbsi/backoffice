<?php

class TabelaCotacaoHasCarencia extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'TabelaCotacaoHasCarencia';
	}

	public function rules()
	{
		return array(
			array('CarenciaId, TabelaCotacaoId, habilitado', 'required'),
			array('CarenciaId, TabelaCotacaoId, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, CarenciaId, TabelaCotacaoId, habilitado', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'carencia' => array(self::BELONGS_TO, 'Carencia', 'CarenciaId'),
			'tabelaCotacao' => array(self::BELONGS_TO, 'TabelaCotacao', 'TabelaCotacaoId'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'CarenciaId' => 'Carencia',
			'TabelaCotacaoId' => 'Tabela Cotacao',
			'habilitado' => 'Habilitado',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('CarenciaId',$this->CarenciaId);
		$criteria->compare('TabelaCotacaoId',$this->TabelaCotacaoId);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}