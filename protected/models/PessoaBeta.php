<?php

class PessoaBeta extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getCPF() {

        $pessoaHasDocumento = PessoaHasDocumentoBeta::model()->findAll('Pessoa_id = ' . $this->id);

        if (count($pessoaHasDocumento) > 0) {
            foreach ($pessoaHasDocumento as $phd) {

                $doc = DocumentoBeta::model()->findByPk($phd->Documento_id);

                if ($doc != NULL) {
                    if ($doc->tipoDocumento->id == 1)
                        return $doc;
                }
            }
        }

        return null;
    }
	
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	public function tableName()
	{
		return 'Pessoa';
	}

	public function rules()
	{
		
		return array(
			array('Contato_id, nome, Estado_Civil_id', 'required'),
			array('Contato_id, habilitado, naturalidade_cidade, Estado_Civil_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>150),
			array('razao_social', 'length', 'max'=>200),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('sexo', 'length', 'max'=>5),
			array('naturalidade', 'length', 'max'=>4),
			array('nacionalidade', 'length', 'max'=>100),
			array('estado_civil', 'length', 'max'=>45),
			array('nascimento, data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Contato_id, nome, razao_social, nascimento, habilitado, data_cadastro, data_cadastro_br, sexo, naturalidade, naturalidade_cidade, nacionalidade, estado_civil, Estado_Civil_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'clientes' => array(self::HAS_MANY, 'ClienteBeta', 'Pessoa_id'),
			'dadosProfissionaises' => array(self::HAS_MANY, 'DadosProfissionais', 'Pessoa_id'),
			'familiars' => array(self::HAS_MANY, 'Familiar', 'Pessoa_id'),
			'fornecedors' => array(self::HAS_MANY, 'Fornecedor', 'Pessoa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Contato_id' => 'Contato',
			'nome' => 'Nome',
			'razao_social' => 'Razao Social',
			'nascimento' => 'Nascimento',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'sexo' => 'Sexo',
			'naturalidade' => 'Naturalidade',
			'naturalidade_cidade' => 'Naturalidade Cidade',
			'nacionalidade' => 'Nacionalidade',
			'estado_civil' => 'Estado Civil',
			'Estado_Civil_id' => 'Estado Civil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Contato_id',$this->Contato_id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('razao_social',$this->razao_social,true);
		$criteria->compare('nascimento',$this->nascimento,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('naturalidade',$this->naturalidade,true);
		$criteria->compare('naturalidade_cidade',$this->naturalidade_cidade);
		$criteria->compare('nacionalidade',$this->nacionalidade,true);
		$criteria->compare('estado_civil',$this->estado_civil,true);
		$criteria->compare('Estado_Civil_id',$this->Estado_Civil_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}