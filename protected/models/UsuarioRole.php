<?php

class UsuarioRole extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsuarioRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Usuario_Role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, Role_id, data_cadastro', 'required'),
			array('Usuario_id, Role_id, habilitado, Nivel_Role_id', 'numerical', 'integerOnly'=>true),
			array('data_cadastro_br', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Usuario_id, principal, Role_id, habilitado, Nivel_Role_id, data_cadastro, data_cadastro_br', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nivelRole' => array(self::BELONGS_TO, 'NivelRole', 'Nivel_Role_id', 	'on' 	=>	'nivelRole.Status_Nivel_Role_id = 2 AND nivelRole.habilitado = 1'),
			'role' 		=> array(self::BELONGS_TO, 'Role', 		'Role_id', 			'on'	=>	'role.habilitado = 1 AND role.Status_Role_id = 2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'ID',
			'Usuario_id' 		=> 'Usuario',
			'Role_id' 			=> 'Role',
			'habilitado' 		=> 'Habilitado',
			'principal' 		=> 'Principal',
			'Nivel_Role_id' 	=> 'Nivel Role',
			'data_cadastro' 	=> 'Data Cadastro',
			'data_cadastro_br' 	=> 'Data Cadastro Br'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('Role_id',$this->Role_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Nivel_Role_id',$this->Nivel_Role_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function novo($UsuarioRole)
	{
		$ur 				= new UsuarioRole('search');
		$ur->attributes 	= $UsuarioRole;
		$ur->save();
		return $ur;
	}
}