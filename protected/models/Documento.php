<?php

class Documento extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Documento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo_documento_id', 'required'),
			array('habilitado, Tipo_documento_id', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>100),
			array('orgao_emissor, uf_emissor, tipo_documento', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_emissao, data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, numero, orgao_emissor, data_emissao, habilitado, data_cadastro, data_cadastro_br, uf_emissor, tipo_documento, Tipo_documento_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'Tipo_documento_id'),
			'documentoHasSituacaoCadastrals' => array(self::HAS_MANY, 'DocumentoHasSituacaoCadastral', 'Documento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Numero',
			'orgao_emissor' => 'Orgao Emissor',
			'data_emissao' => 'Data Emissao',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'uf_emissor' => 'Uf Emissor',
			'tipo_documento' => 'Tipo Documento',
			'Tipo_documento_id' => 'Tipo Documento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('orgao_emissor',$this->orgao_emissor,true);
		$criteria->compare('data_emissao',$this->data_emissao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('uf_emissor',$this->uf_emissor,true);
		$criteria->compare('tipo_documento',$this->tipo_documento,true);
		$criteria->compare('Tipo_documento_id',$this->Tipo_documento_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}