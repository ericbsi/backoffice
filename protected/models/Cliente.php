<?php

class Cliente extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Pessoa_id', 'required'),
			array('Pessoa_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Pessoa_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'analiseDeCreditos' => array(self::HAS_MANY, 'AnaliseDeCredito', 'Cliente_id'),
			'baixas' => array(self::HAS_MANY, 'Baixa', 'Cliente_id'),
			'cadastros' => array(self::HAS_MANY, 'Cadastro', 'Cliente_id'),
			'pessoa' => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id'),
			'familiars' => array(self::HAS_MANY, 'Familiar', 'Cliente_id'),
			'fichamentoHasStatusFichamentos' => array(self::HAS_MANY, 'FichamentoHasStatusFichamento', 'Cliente'),
			'listaNegras' => array(self::MANY_MANY, 'ListaNegra', 'ListaNegra_has_Cliente(Cliente_id, ListaNegra_id)'),
			'vendaWs' => array(self::HAS_MANY, 'VendaW', 'Cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Pessoa_id' => 'Pessoa',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}