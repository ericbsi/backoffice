<?php

class Funcao extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Funcao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['label, descricao', 'required'],
            ['habilitado', 'numerical', 'integerOnly' => true],
            ['controller', 'length', 'max' => 200],
            ['nome', 'length', 'max' => 45],
            ['view, template', 'length', 'max' => 100],
            ['data_cadastro', 'safe'],
            ['id, controller, nome, view, template, descricao, habilitado, data_cadastro, ultima_atualizacao', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'nivelRoleHasFuncaos'   => array(self::HAS_MANY,    'NivelRoleHasFuncao',   'Funcao_id'),
            'roleHasFuncaos'        => array(self::HAS_MANY,    'RoleHasFuncao',        'Funcao_id'),
            'status'                => array(self::BELONGS_TO,  'StatusFuncao',         'Status_Funcao_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'controller' => 'Controller',
            'nome' => 'Nome',
            'view' => 'View',
            'template' => 'Template',
            'descricao' => 'Descricao',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
        );
    }

    /**/

    public function loadStyle($nivelId)
    {
        return NivelRoleHasFuncaoHasStyleConfig::model()->find('Nivel_Role_has_Funcao_id = ' . $nivelId . ' AND habilitado');
    }

    public function carregarEstilo($nivelId)
    {
        return NivelRoleHasRoleHasFuncaoHasStyleConfig::model()->find('habilitado AND Nivel_Role_has_Role_has_Funcao_id = ' . $nivelId);
    }

    /**
     * Inclui e editar informações sobre as funções
     * @param POST['Funcao']
     * @param INT FuncaoId
     * @return [bool result, array [errors, msgconfig]]
     * @author Eric
     * @version 1.0
     */
    public function persist($Funcao, $FuncaoId)
    {
        $retorno['hasErrors']       = false;
        $F                          = new Funcao;
    
        /* Se houver um id, trata-se de uma atualização nos dados */
        if ($FuncaoId !== '0')
        {
            $F = Funcao::model()->findByPk($FuncaoId);
        }

        /* Se não, trata-se de um novo registro */ 
        else
        {
            $F->data_cadastro       = date('Y-m-d H:i:s');
            $F->Status_Funcao_id    = 1;
        }

        $F->attributes          = $Funcao;
        $F->ultima_atualizacao  = date('Y-m-d H:i:s');

        /* Se houver erro ao salvar/atualizar, configura o retorno de acordo */
        if (!$F->save())
        {
            $retorno['hasErrors'] = true;
            $retorno['errors'] = $F->getErrors();
        }

        return $retorno;
    }

    /**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model Funcao]
     * @author Eric
     * @version 1.0
     */
    public function search($limit)
    {
        $criteria = new CDbCriteria;

        /* Campos texto, usando LIKE */
        $criteria->addSearchCondition('controller', $this->controller);
        $criteria->addSearchCondition('nome', $this->nome);
        $criteria->addSearchCondition('template', $this->template);
        $criteria->addSearchCondition('descricao', $this->descricao);

        $criteria->compare('habilitado', $this->habilitado);
        $criteria->offset = $limit['start'];
        $criteria->limit = $limit['limit'];

        return Funcao::model()->findAll($criteria);
    }

    /**
     * Monta a estrutura necessária para apresentação em uma jQuery DataTable
     * @param INT $draw - variável padrão do plugin dataTables
     * @param ARRAY $limit 
     * @return [json result]
     * @author Eric
     * @version 1.0
     */
    public function dataTable($draw, $limit, $idPerfil = '0')
    {
        $fun                    = new Funcao;
        $funcoesCollection      = [];
        $recordsTotal           = count(Funcao::model()->findAll());

        $i                      = 1;
        $hab                    = 0;
        $eHab                   = 0;
        
        foreach ($fun->search($limit) as $funcao)
        {   
            $checked             = "";
            $checkedExibir       = "";
            $eHab                = 0;

            if( $idPerfil !== '0' )
            {
                $nivelRHasFuncao = RoleHasFuncao::model()->find('t.Funcao_id = ' . $funcao->id . ' AND t.Role_id = ' . $idPerfil . ' AND t.habilitado');

                if( $nivelRHasFuncao !== null )
                {
                    if( $nivelRHasFuncao->show_menu )
                    {
                        $checkedExibir = "checked";
                    }
                    else
                    {
                        $eHab          = 1;
                    }

                    $checked    = "checked";
                    $hab        = 0;
                }
                else
                {
                    $checked    = "";
                    $hab        = 1;
                }
            }

            $funcoesCollection[] =  [
                'checkFilial'   => '<div class="checkbox checkbox-primary"><input data-hab="'.$hab.'" data-funcao-id="'.$funcao->id.'" data-nivel-id="'.$idPerfil.'" class="input_check" '.$checked.' id="checkbox'.$i.'" type="checkbox"><label for="checkbox'.$i.'"></label></div>',
                'exibirMenu'    => '<div class="checkbox checkbox-primary"><input data-hab="'.$eHab.'" data-funcao-id="'.$funcao->id.'" data-nivel-id="'.$idPerfil.'" class="input_check_exibir" '.$checkedExibir.' id="checkbox_'.$i.'" type="checkbox"><label for="checkbox_'.$i.'"></label></div>',
                'label'         => mb_strtoupper($funcao->label),
                'descricao'     => mb_strtoupper($funcao->descricao),
                'btn_editar'    => '<a data-form-wrapper="#portlet-form" data-source="/funcao/loadFormEdit/" data-entity-id="' . $funcao->id . '" data-form-load="#form-funcao" data-collapse-div="#bg-default" class="btn btn-credshow-orange waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-edit"></i></a>',
            ];

            $i++;
        }

        return ['collection'    =>  $funcoesCollection, 'total'  => $recordsTotal];
    }

    /**
     * Retorna um array com as informações necessárias para carregar um formulário de edição
     * de um obejto 
     * @param INT $id
     * @return [arr result]
     * @author Eric
     * @version 1.0
     */
    public function loadFormEdit($id)
    {
        $funcao = Funcao::model()->findByPk($id);

        return [
            'fields' => [
                'id' => [
                    'value' => $funcao->id,
                    'type' => 'text',
                    'input_bind' => 'funcao_id',
                ],
                'descricao' => [
                    'value' => mb_strtoupper($funcao->descricao),
                    'type' => 'text',
                    'input_bind' => 'funcao_descricao',
                ],
                'label' => [
                    'value' => mb_strtoupper($funcao->label),
                    'type' => 'text',
                    'input_bind' => 'funcao_label',
                ],
            ]
        ];
    }
}


//$funcao->roleHasFuncao->funcao->loadStyle( $funcao->id )->styleConfig->icon_class