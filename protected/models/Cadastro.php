<?php

/**
 * This is the model class for table "Cadastro".
 *
 * The followings are the available columns in table 'Cadastro':
 * @property integer $id
 * @property integer $conjugue_compoe_renda
 * @property string $nome_do_pai
 * @property string $nome_da_mae
 * @property integer $titular_do_cpf
 * @property integer $Cliente_id
 * @property integer $Empresa_id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $numero_de_dependentes
 * @property integer $atualizar
 * @property integer $cliente_da_casa
 * @property integer $analfabeto
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 * @property Cliente $cliente
 * @property Confirmacao[] $confirmacaos
 */
class Cadastro extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadastro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Cliente_id, Empresa_id', 'required'),
			array('conjugue_compoe_renda, titular_do_cpf, Cliente_id, Empresa_id, habilitado, numero_de_dependentes, atualizar, cliente_da_casa, analfabeto', 'numerical', 'integerOnly'=>true),
			array('nome_do_pai, nome_da_mae', 'length', 'max'=>150),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, conjugue_compoe_renda, nome_do_pai, nome_da_mae, titular_do_cpf, Cliente_id, Empresa_id, habilitado, data_cadastro, data_cadastro_br, numero_de_dependentes, atualizar, cliente_da_casa, analfabeto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
			'confirmacaos' => array(self::HAS_MANY, 'Confirmacao', 'Cadastro_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'conjugue_compoe_renda' => 'Conjugue Compoe Renda',
			'nome_do_pai' => 'Nome Do Pai',
			'nome_da_mae' => 'Nome Da Mae',
			'titular_do_cpf' => 'Titular Do Cpf',
			'Cliente_id' => 'Cliente',
			'Empresa_id' => 'Empresa',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'numero_de_dependentes' => 'Numero De Dependentes',
			'atualizar' => 'Atualizar',
			'cliente_da_casa' => 'Cliente Da Casa',
			'analfabeto' => 'Analfabeto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('conjugue_compoe_renda',$this->conjugue_compoe_renda);
		$criteria->compare('nome_do_pai',$this->nome_do_pai,true);
		$criteria->compare('nome_da_mae',$this->nome_da_mae,true);
		$criteria->compare('titular_do_cpf',$this->titular_do_cpf);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('Empresa_id',$this->Empresa_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('numero_de_dependentes',$this->numero_de_dependentes);
		$criteria->compare('atualizar',$this->atualizar);
		$criteria->compare('cliente_da_casa',$this->cliente_da_casa);
		$criteria->compare('analfabeto',$this->analfabeto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cadastro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
