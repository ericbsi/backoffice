<?php

/**
 * This is the model class for table "Mensagem".
 *
 * The followings are the available columns in table 'Mensagem':
 * @property integer $id
 * @property integer $Dialogo_id
 * @property string $conteudo
 * @property integer $lida
 * @property integer $habilitado
 * @property integer $editada
 * @property string $data_criacao
 * @property string $data_ultima_edicao
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 * @property UsuarioLeuMensagem[] $usuarioLeuMensagems
 */
class Mensagem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mensagem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Mensagem';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Dialogo_id, conteudo', 'required'),
			array('Dialogo_id, lida, habilitado, editada, Usuario_id', 'numerical', 'integerOnly'=>true),
			array('data_criacao, data_ultima_edicao', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Dialogo_id, conteudo, lida, habilitado, editada, data_criacao, data_ultima_edicao, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'usuarioLeuMensagems' => array(self::HAS_MANY, 'UsuarioLeuMensagem', 'Mensagem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Dialogo_id' => 'Dialogo',
			'conteudo' => 'Conteudo',
			'lida' => 'Lida',
			'habilitado' => 'Habilitado',
			'editada' => 'Editada',
			'data_criacao' => 'Data Criacao',
			'data_ultima_edicao' => 'Data Ultima Edicao',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Dialogo_id',$this->Dialogo_id);
		$criteria->compare('conteudo',$this->conteudo,true);
		$criteria->compare('lida',$this->lida);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('editada',$this->editada);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_ultima_edicao',$this->data_ultima_edicao,true);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}