<?php

class Excel {

    public function __construct() {
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('ext.excel_Classes.PHPExcel', true);
        spl_autoload_register(array('YiiBase', 'autoload'));
    }

    public function setHeaders() {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . date('Ymdis') . '.xls"');
        header('Cache-Control: max-age=0');
    }

    public function export($colunas, $linhas, $rodape = NULL) {
        /* Sem limite para execução, pois geralmente teremos a exportação de grandes volumes de dados */
        set_time_limit(-1);

        /* Setando os headers http para disponibilizar o tipo certo de conteúdo no navegador */
        $this->setHeaders();

        $objPHPExcel = new PHPExcel();

        /* Montando o cabeçalho; colunas consequentemente */
        for ($i = 0; $i < count($colunas); $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($colunas[$i][0] . '1', $colunas[$i][1]);

            $objPHPExcel->getActiveSheet()->getStyle($colunas[$i][0] . '1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colunas[$i][0])->setAutoSize(true);
        }

        /* Setando a altura do cabeçalho */
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(22);

        /*
          Montandos as linhas
          array no padrão -> ['A1','foo']
         */
        for ($x = 0; $x < count($linhas); $x++) {
            for ($c = 0; $c < count($colunas); $c++) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($linhas[$x][$c][0], $linhas[$x][$c][1]);
            }
        }

        $ultimaLinha = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow() + 1;

        /* Montando o rodapé, se ele foi informado */
        if ($rodape != NULL) {
            for ($r = 0; $r < count($rodape); $r++) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($rodape[$r][0] . ($ultimaLinha), $rodape[$r][1]);

                $objPHPExcel->getActiveSheet()->getStyle($rodape[$r][0] . ($ultimaLinha))->getFont()->setBold(true);
            }
        }

        for ($h = 0; $h <= $objPHPExcel->setActiveSheetIndex(0)->getHighestRow(); $h++) {
            $objPHPExcel->getActiveSheet()->getRowDimension($h)->setRowHeight(22);
        }

        /* Definições de estilos */
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );

        $objPHPExcel->getDefaultStyle()->applyFromArray($style);

        /* Exibindo a planilha para download */
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        /*
          $objPHPExcel->getProperties()->setCreator("Autor do Documento")
          ->setLastModifiedBy("Modificado por...")
          ->setTitle("O Título")
          ->setSubject("O Assunto")
          ->setDescription("A Descrição")
          ->setKeywords("As Palavras Chave")
          ->setCategory("A Categoria");
         */
    }

}
