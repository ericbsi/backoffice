<?php

/**
 * This is the model class for table "Parcela".
 *
 * The followings are the available columns in table 'Parcela':
 * @property integer $id
 * @property integer $seq
 * @property string $vencimento
 * @property double $valor
 * @property string $valor_texto
 * @property double $valor_atual
 * @property integer $Titulo_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property AtendimentoHasParcela[] $atendimentoHasParcelas
 * @property Baixa[] $baixas
 * @property BorderoHasParcela[] $borderoHasParcelas
 * @property Cobranca[] $cobrancas
 * @property Fichamento[] $fichamentos
 * @property ItemDoBordero[] $itemDoBorderos
 * @property LinhaCNAB[] $linhaCNABs
 */
class Parcela extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Parcela';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('seq, valor, valor_atual, Titulo_id', 'required'),
			array('seq, Titulo_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor, valor_atual', 'numerical'),
			array('vencimento, valor_texto', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, seq, vencimento, valor, valor_texto, valor_atual, Titulo_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'atendimentoHasParcelas' => array(self::HAS_MANY, 'AtendimentoHasParcela', 'Parcela_id'),
			'baixas' => array(self::HAS_MANY, 'Baixa', 'Parcela_id'),
			'borderoHasParcelas' => array(self::HAS_MANY, 'BorderoHasParcela', 'Parcela_id'),
			'cobrancas' => array(self::HAS_MANY, 'Cobranca', 'Parcela_id'),
			'fichamentos' => array(self::HAS_MANY, 'Fichamento', 'Parcela'),
			'itemDoBorderos' => array(self::HAS_MANY, 'ItemDoBordero', 'Parcela_id'),
			'linhaCNABs' => array(self::HAS_MANY, 'LinhaCNAB', 'Parcela_id'),
			'titulo'        => array(self::BELONGS_TO  , 'Titulo'          , 'Titulo_id'     ),
			'rangesGerados' => array(self::HAS_ONE     , 'RangesGerados'   , 'Parcela_id'    ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'seq' => 'Seq',
			'vencimento' => 'Vencimento',
			'valor' => 'Valor',
			'valor_texto' => 'Valor Texto',
			'valor_atual' => 'Valor Atual',
			'Titulo_id' => 'Titulo',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('seq',$this->seq);
		$criteria->compare('vencimento',$this->vencimento,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('valor_texto',$this->valor_texto,true);
		$criteria->compare('valor_atual',$this->valor_atual);
		$criteria->compare('Titulo_id',$this->Titulo_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Parcela the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
