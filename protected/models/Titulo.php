<?php

class Titulo extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'Titulo';
    }

    public function rules() {

        return array(
            array('Proposta_id, Tributo_id, NaturezaTitulo_id, VendaW_id, Venda_has_FP_has_CP_FP_has_CP_id, Compra_has_FP_has_CP_FP_has_CP_id, habilitado', 'numerical', 'integerOnly' => true),
            array('prefixo', 'length', 'max' => 45),
            array('emissao', 'safe'),
            array('id, prefixo, emissao, Proposta_id, Tributo_id, NaturezaTitulo_id, VendaW_id, Venda_has_FP_has_CP_FP_has_CP_id, Compra_has_FP_has_CP_FP_has_CP_id, habilitado', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'despesas' => array(self::HAS_MANY, 'Despesa', 'Titulo_id'),
            'naturezaTitulo' => array(self::BELONGS_TO, 'NaturezaTitulo', 'NaturezaTitulo_id'),
            'compraHasFPHasCPFPHasCP' => array(self::BELONGS_TO, 'CompraHasFormaDePagamentoHasCondicaoDePagamento', 'Compra_has_FP_has_CP_FP_has_CP_id'),
            'vendaHasFPHasCPFPHasCP' => array(self::BELONGS_TO, 'VendaHasFormaDePagamentoHasCondicaoDePagamento', 'Venda_has_FP_has_CP_FP_has_CP_id'),
            'parcelas' => array(self::HAS_MANY, 'Parcela', 'Titulo_id'),
            'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'prefixo' => 'Prefixo',
            'emissao' => 'Emissao',
            'Proposta_id' => 'Proposta',
            'Tributo_id' => 'Tributo',
            'NaturezaTitulo_id' => 'Natureza Titulo',
            'VendaW_id' => 'Venda W',
            'Venda_has_FP_has_CP_FP_has_CP_id' => 'Venda Has Fp Has Cp Fp Has Cp',
            'Compra_has_FP_has_CP_FP_has_CP_id' => 'Compra Has Fp Has Cp Fp Has Cp',
            'habilitado' => 'Habilitado',
        );
    }

    public function getLotesPagamento($start, $limit, $situacao = '0', $de, $ate, $nucleo) {
        $filiais = [];
        $filiaisString = "";
        $util = new Util;

        foreach (Yii::app()->session['usuario']->adminHasParceiros as $f) {
            if ($f->habilitado) {
                $filiais[] = $f->parceiro->id;
            }
        }

        if (!empty($filiais)) {
            $filiaisString = join(',', $filiais);
        }

        $query = "SELECT "
                . "LP.id, LP.data_cadastro, NC.nome as 'Nucleo' "
                . "FROM        LotePagamento                          AS LP "
                . "INNER JOIN  NucleoFiliais                          AS NC ON NC.id          = LP.NucleoFiliais_id         AND NC.habilitado ";

        $query .= "LEFT JOIN LotePagamento_has_StatusLotePagamento AS LS ON LP.id = LS.LotePagamento_id AND LS.StatusLotePagamento_id ='2' ";

        $query .= "INNER JOIN  Lote_has_Bordero                      AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado "
                . "INNER JOIN  Bordero                                AS  B ON  B.id          = LB.Bordero_id               AND  B.habilitado "
                . "INNER JOIN  ItemDoBordero                          AS IB ON  B.id          = IB.Bordero                  AND IB.habilitado "
                . "INNER JOIN  Proposta                               AS Pr ON Pr.id          = IB.Proposta_id "
                . "INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id "
                . "WHERE LP.habilitado ";

        if ($de !== NULL && $ate !== NULL) {
            $query .= " AND LP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59'  ";
        }

        if (!empty($filiaisString)) {
            $query .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        if ($situacao == '1') {
            $query .= " AND LS.id is null ";
        } else if ($situacao == '2') {
            $query .= " AND LS.id is not null ";
        }

        if ($nucleo != '0') {
            $query .= " AND LP.NucleoFiliais_id = " . $nucleo . " ";
        }

        $query .= "GROUP BY LP.id ";


        $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "ORDER BY LP.id DESC LIMIT " . $start . " ," . $limit;

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        if (empty($filiais)) {
            return [null, 0];
        } else {
            return [$resultado, $qtdTotal];
        }
    }

    public function listarLotesPagamento($draw, $start, $limit, $situacao, $de, $ate, $nucleo) {
        $codigosNucleos = [];
        $linhas = [];
        $util = new Util;

        /**/
        $queryNucleos = " SELECT DISTINCT (NF.id) as 'NfId', NF.nome as 'Nucleo'  FROM `AdminHasParceiro` AS AHP";
        $queryNucleos .= " INNER JOIN Filial AS F ON F.id = AHP.Parceiro";
        $queryNucleos .= " INNER JOIN NucleoFiliais AS NF ON NF.id = F.NucleoFiliais_id";
        $queryNucleos .= " WHERE AHP.`Administrador` = " . Yii::app()->session['usuario']->id . " AND AHP.habilitado ";
        /**/

        $dBancarios = "";
        $cgcBancarios = "";
        $total = 0;

        $wherePgtoStatusNr = "";
        $wherePgtoStatusBt = "";

        $whereNucleoNr = "";
        $whereNucleoBt = "";

        if (!empty($nucleo) && isset($nucleo)) {
            for ($i = 0; $i < count($nucleo); $i++) {
                $n = NucleoFiliais::model()->findByPk($nucleo[$i]);
                $codigosNucleos[] = "'" . $n->codigo . "'";
            }
        } else {
            foreach (Yii::app()->db->createCommand($queryNucleos)->queryAll() as $nq) {
                $n = NucleoFiliais::model()->findByPk($nq['NfId']);
                $codigosNucleos[] = "'" . $n->codigo . "'";
            }
        }

        $codigosNucleos = join(',', $codigosNucleos);

        if (isset($situacao) && !empty($situacao) && trim($situacao) != '') {
            if ($situacao == '1') {
                $wherePgtoStatusNr = " AND LPHS.id IS NULL  ";
                $wherePgtoStatusBt = " AND bLPHS.id IS NULL ";
            } else if ($situacao == '2') {
                $wherePgtoStatusNr = " AND LPHS.id IS NOT NULL  ";
                $wherePgtoStatusBt = " AND bLPHS.id IS NOT NULL ";
            }
        }

        /* Query que seleciona os lotes dos dois bancos de dados */
        $Query = " SELECT DISTINCT(LoteId), LoteId, DataCadastro, Nucleo, ambiente ";
        $Query .= " FROM ( ";
        $Query .= " SELECT Lote.id AS `LoteId`, Lote.data_cadastro AS `DataCadastro`, NucleoF.nome AS `Nucleo`, 'n' AS `ambiente` ";
        $Query .= " FROM nordeste2.`LotePagamento` AS Lote ";
        $Query .= " INNER JOIN nordeste2.Lote_has_Bordero AS LHB ON LHB.Lote_id = Lote.id AND LHB.habilitado ";
        $Query .= " LEFT JOIN  nordeste2.LotePagamento_has_StatusLotePagamento AS LPHS ON LPHS.LotePagamento_id = Lote.id AND LPHS.StatusLotePagamento_id = 2 AND LPHS.habilitado ";
        $Query .= " INNER JOIN nordeste2.Bordero AS B ON B.id = LHB.Bordero_id AND B.habilitado ";
        $Query .= " INNER JOIN nordeste2.NucleoFiliais AS NucleoF ON NucleoF.id = Lote.NucleoFiliais_id AND NucleoF.habilitado ";
        $Query .= " WHERE Lote.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' AND ";
        $Query .= " Lote.habilitado " . $wherePgtoStatusNr . " AND NucleoF.codigo IN(" . $codigosNucleos . ")";
        $Query .= " UNION ";
        $Query .= " SELECT bLote.id AS `LoteId`, bLote.data_cadastro AS `DataCadastro`, bNucleoF.nome AS `Nucleo`, 'b' AS `ambiente` ";
        $Query .= " FROM beta.`LotePagamento` AS bLote ";
        $Query .= " INNER JOIN beta.Lote_has_Bordero AS bLHB ON bLHB.Lote_id = bLote.id AND bLHB.habilitado ";
        $Query .= " LEFT JOIN  beta.LotePagamento_has_StatusLotePagamento AS bLPHS ON bLPHS.LotePagamento_id = bLote.id AND bLPHS.StatusLotePagamento_id = 2 AND bLPHS.habilitado ";
        $Query .= " INNER JOIN beta.Bordero AS bB ON bB.id = bLHB.Bordero_id AND bB.habilitado ";
        $Query .= " INNER JOIN beta.NucleoFiliais AS bNucleoF ON bNucleoF.id = bLote.NucleoFiliais_id AND bNucleoF.habilitado ";
        $Query .= " WHERE bLote.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' AND ";
        $Query .= " bLote.habilitado " . $wherePgtoStatusBt . " AND bNucleoF.codigo IN(" . $codigosNucleos . ")";
        $Query .= " ) AS TEMP ";

        $countReg = count(Yii::app()->db->createCommand($Query)->queryAll());

        $Query .= " LIMIT $start, $limit ";
        /* FIM Query */

        if ($countReg > 0) {
            foreach (Yii::app()->db->createCommand($Query)->queryAll() as $r) {
                $objLote = NULL;
                $urlComprovante = 'https://credshow.sigacbr.com.br';
                $comprovante = '<a href="#" class="btn btn-danger waves-effect waves-light btn-xs">Sem comprovante</a>';

                if ($r['ambiente'] == 'b') {
                    $objLote = LotePagamentoBeta::model()->findByPk($r['LoteId']);
                    $urlComprovante = 'https://s1.sigacbr.com.br';
                } else {
                    $objLote = LotePagamento::model()->findByPk($r['LoteId']);
                }

                $dadosPgto = LotePagamento::model()->getDadosBancariosPagos($objLote->id);

                if (isset($dadosPgto['dados']['linkComprovante'])) {
                    $comprovante = '<a target="_blank" href=' . $urlComprovante . $dadosPgto['dados']['linkComprovante'] . ' class="btn btn-success waves-effect waves-light btn-xs">Abrir comprovante</a>';
                }

                $linhas[] = [
                    'btn-more' => '<button style="padding:3px 8px!important;" type="button" class="btopen btn-xs btn-success btn-rounded"><i class="fa fa-plus"></i></button>',
                    'ambiente' => $r['ambiente'],
                    'loteId' => $r['LoteId'],
                    'codigo' => str_pad($r['LoteId'], 10, '0', STR_PAD_LEFT),
                    'dataCadastro' => $util->bd_date_to_view(substr($r['DataCadastro'], 0, 10)),
                    'nucleo' => mb_strtoupper($r['Nucleo']),
                    'valor' => "R$ " . number_format($objLote->getValor(), 2, ',', '.'),
                    'comprovante' => $comprovante
                ];

                $total += $objLote->getValor();
            }
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($linhas),
            "recordsFiltered" => $countReg,
            "data" => $linhas,
            "customReturn" => [
                'total' => number_format($total, 2, ',', '.'),
                'codigosNucleos' => $codigosNucleos
            ]
        );
    }

    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('prefixo', $this->prefixo, true);
        $criteria->compare('emissao', $this->emissao, true);
        $criteria->compare('Proposta_id', $this->Proposta_id);
        $criteria->compare('Tributo_id', $this->Tributo_id);
        $criteria->compare('NaturezaTitulo_id', $this->NaturezaTitulo_id);
        $criteria->compare('VendaW_id', $this->VendaW_id);
        $criteria->compare('Venda_has_FP_has_CP_FP_has_CP_id', $this->Venda_has_FP_has_CP_FP_has_CP_id);
        $criteria->compare('Compra_has_FP_has_CP_FP_has_CP_id', $this->Compra_has_FP_has_CP_FP_has_CP_id);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
