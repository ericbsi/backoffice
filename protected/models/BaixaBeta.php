<?php

/**
 * This is the model class for table "Baixa".
 *
 * The followings are the available columns in table 'Baixa':
 * @property integer $id
 * @property string $valor_pago
 * @property string $data_da_ocorrencia
 * @property string $data_da_baixa
 * @property string $codigo_banco_cobrador
 * @property string $agencia_cobradora
 * @property string $valor_multa
 * @property string $valor_iof
 * @property string $valor_mora
 * @property string $tarifa_custas
 * @property string $valor_abatimento
 * @property string $valor_desconto
 * @property integer $baixado
 * @property integer $Parcela_id
 * @property integer $Filial_id
 * @property integer $Cliente_id
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property Parcela $parcela
 * @property Filial $filial
 * @property Cliente $cliente
 */
class BaixaBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaixaBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Baixa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Parcela_id, Filial_id', 'required'),
			array('baixado, Parcela_id, Filial_id, Cliente_id', 'numerical', 'integerOnly'=>true),
			array('valor_pago, data_da_ocorrencia, data_da_baixa, codigo_banco_cobrador, agencia_cobradora, valor_multa, valor_iof, valor_mora, tarifa_custas, valor_abatimento, valor_desconto, observacao', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, valor_pago, data_da_ocorrencia, data_da_baixa, codigo_banco_cobrador, agencia_cobradora, valor_multa, valor_iof, valor_mora, tarifa_custas, valor_abatimento, valor_desconto, baixado, Parcela_id, Filial_id, Cliente_id, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'valor_pago' => 'Valor Pago',
			'data_da_ocorrencia' => 'Data Da Ocorrencia',
			'data_da_baixa' => 'Data Da Baixa',
			'codigo_banco_cobrador' => 'Codigo Banco Cobrador',
			'agencia_cobradora' => 'Agencia Cobradora',
			'valor_multa' => 'Valor Multa',
			'valor_iof' => 'Valor Iof',
			'valor_mora' => 'Valor Mora',
			'tarifa_custas' => 'Tarifa Custas',
			'valor_abatimento' => 'Valor Abatimento',
			'valor_desconto' => 'Valor Desconto',
			'baixado' => 'Baixado',
			'Parcela_id' => 'Parcela',
			'Filial_id' => 'Filial',
			'Cliente_id' => 'Cliente',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('valor_pago',$this->valor_pago,true);
		$criteria->compare('data_da_ocorrencia',$this->data_da_ocorrencia,true);
		$criteria->compare('data_da_baixa',$this->data_da_baixa,true);
		$criteria->compare('codigo_banco_cobrador',$this->codigo_banco_cobrador,true);
		$criteria->compare('agencia_cobradora',$this->agencia_cobradora,true);
		$criteria->compare('valor_multa',$this->valor_multa,true);
		$criteria->compare('valor_iof',$this->valor_iof,true);
		$criteria->compare('valor_mora',$this->valor_mora,true);
		$criteria->compare('tarifa_custas',$this->tarifa_custas,true);
		$criteria->compare('valor_abatimento',$this->valor_abatimento,true);
		$criteria->compare('valor_desconto',$this->valor_desconto,true);
		$criteria->compare('baixado',$this->baixado);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}