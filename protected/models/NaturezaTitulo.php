<?php

/**
 * This is the model class for table "NaturezaTitulo".
 *
 * The followings are the available columns in table 'NaturezaTitulo':
 * @property integer $id
 * @property string $descricao
 * @property string $codigo
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $NaturezaTitulo_id
 *
 * The followings are the available model relations:
 * @property NaturezaTitulo $naturezaTitulo
 * @property NaturezaTitulo[] $naturezaTitulos
 * @property Titulo[] $titulos
 */
class NaturezaTitulo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NaturezaTitulo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'NaturezaTitulo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao', 'required'),
			array('habilitado, NaturezaTitulo_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>200),
			array('codigo', 'length', 'max'=>45),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, codigo, data_cadastro, habilitado, NaturezaTitulo_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'naturezaTitulo' => array(self::BELONGS_TO, 'NaturezaTitulo', 'NaturezaTitulo_id'),
			'naturezaTitulos' => array(self::HAS_MANY, 'NaturezaTitulo', 'NaturezaTitulo_id'),
			'titulos' => array(self::HAS_MANY, 'Titulo', 'NaturezaTitulo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'codigo' => 'Codigo',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'NaturezaTitulo_id' => 'Natureza Titulo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('NaturezaTitulo_id',$this->NaturezaTitulo_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}