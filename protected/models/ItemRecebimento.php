<?php

/**
 * This is the model class for table "ItemRecebimento".
 *
 * The followings are the available columns in table 'ItemRecebimento':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $Proposta_id
 * @property integer $Recebimento_id
 *
 * The followings are the available model relations:
 * @property Proposta $proposta
 * @property Recebimento $recebimento
 */
class ItemRecebimento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemRecebimento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, habilitado, Proposta_id, Recebimento_id', 'required'),
			array('habilitado, Proposta_id, Recebimento_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, Proposta_id, Recebimento_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
			'recebimento' => array(self::BELONGS_TO, 'Recebimento', 'Recebimento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'Proposta_id' => 'Proposta',
			'Recebimento_id' => 'Recebimento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('Recebimento_id',$this->Recebimento_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemRecebimento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
