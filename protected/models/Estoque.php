<?php

/**
 * This is the model class for table "Estoque".
 *
 * The followings are the available columns in table 'Estoque':
 * @property integer $id
 * @property integer $vende
 * @property integer $Local_id
 * @property integer $Filial_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Filial $filial
 * @property Local $local
 * @property ItemDoEstoque[] $itemDoEstoques
 */
class Estoque extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Estoque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Estoque';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vende, Local_id, Filial_id', 'required'),
			array('vende, Local_id, Filial_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vende, Local_id, Filial_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'local' => array(self::BELONGS_TO, 'Local', 'Local_id'),
			'itemDoEstoques' => array(self::HAS_MANY, 'ItemDoEstoque', 'Estoque_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vende' => 'Vende',
			'Local_id' => 'Local',
			'Filial_id' => 'Filial',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vende',$this->vende);
		$criteria->compare('Local_id',$this->Local_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}