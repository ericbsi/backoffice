<?php

/**
 * This is the model class for table "Anexo".
 *
 * The followings are the available columns in table 'Anexo':
 * @property integer $id
 * @property string $descricao
 * @property string $relative_path
 * @property string $absolute_path
 * @property string $entidade_relacionamento
 * @property integer $id_entidade_relacionamento
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $extensao
 */
class AnexoBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnexoBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Anexo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_entidade_relacionamento, habilitado', 'numerical', 'integerOnly'=>true),
			array('entidade_relacionamento, extensao', 'length', 'max'=>100),
			array('descricao, relative_path, absolute_path, data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, relative_path, absolute_path, entidade_relacionamento, id_entidade_relacionamento, habilitado, data_cadastro, extensao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'relative_path' => 'Relative Path',
			'absolute_path' => 'Absolute Path',
			'entidade_relacionamento' => 'Entidade Relacionamento',
			'id_entidade_relacionamento' => 'Id Entidade Relacionamento',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'extensao' => 'Extensao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('relative_path',$this->relative_path,true);
		$criteria->compare('absolute_path',$this->absolute_path,true);
		$criteria->compare('entidade_relacionamento',$this->entidade_relacionamento,true);
		$criteria->compare('id_entidade_relacionamento',$this->id_entidade_relacionamento);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('extensao',$this->extensao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}