<?php

/**
 * This is the model class for table "Recebimento".
 *
 * The followings are the available columns in table 'Recebimento':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $Filial_id
 * @property integer $Documentacao_id
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property ItemRecebimento[] $itemRecebimentos
 * @property Documentacao $documentacao
 * @property Filial $filial
 * @property Usuario $usuario
 */
class Recebimento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Recebimento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, habilitado, Filial_id, Usuario_id', 'required'),
			array('habilitado, Filial_id, Documentacao_id, Usuario_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, Filial_id, Documentacao_id, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemRecebimentos' => array(self::HAS_MANY, 'ItemRecebimento', 'Recebimento_id'),
			'documentacao' => array(self::BELONGS_TO, 'Documentacao', 'Documentacao_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'Filial_id' => 'Filial',
			'Documentacao_id' => 'Documentacao',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('Documentacao_id',$this->Documentacao_id);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recebimento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
