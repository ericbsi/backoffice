<?php

/**
 * This is the model class for table "NucleoFiliais".
 *
 * The followings are the available columns in table 'NucleoFiliais':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $GrupoFiliais_id
 * @property string $nome
 *
 * The followings are the available model relations:
 * @property Filial[] $filials
 * @property LotePagamento[] $lotePagamentos
 * @property GrupoFiliais $grupoFiliais
 * @property NucleoFiliaisHasDadosBancarios[] $nucleoFiliaisHasDadosBancarioses
 * @property Regional[] $regionals
 */
class NucleoFiliaisBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NucleoFiliaisBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'NucleoFiliais';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, GrupoFiliais_id, nome', 'required'),
			array('habilitado, GrupoFiliais_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, GrupoFiliais_id, nome', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filials' => array(self::HAS_MANY, 'Filial', 'NucleoFiliais_id'),
			'lotePagamentos' => array(self::HAS_MANY, 'LotePagamento', 'NucleoFiliais_id'),
			'grupoFiliais' => array(self::BELONGS_TO, 'GrupoFiliais', 'GrupoFiliais_id'),
			'nucleoFiliaisHasDadosBancarioses' => array(self::HAS_MANY, 'NucleoFiliaisHasDadosBancarios', 'NucleoFiliais_id'),
			'regionals' => array(self::HAS_MANY, 'Regional', 'NucleoFiliais_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'GrupoFiliais_id' => 'Grupo Filiais',
			'nome' => 'Nome',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('GrupoFiliais_id',$this->GrupoFiliais_id);
		$criteria->compare('nome',$this->nome,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}