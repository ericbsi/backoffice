<?php

/**
 * This is the model class for table "Filial_has_Endereco".
 *
 * The followings are the available columns in table 'Filial_has_Endereco':
 * @property integer $id
 * @property integer $Filial_id
 * @property integer $Endereco_id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 *
 * The followings are the available model relations:
 * @property Endereco $endereco
 * @property Filial $filial
 */
class FilialHasEndereco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FilialHasEndereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Filial_has_Endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Filial_id, Endereco_id', 'required'),
			array('Filial_id, Endereco_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Filial_id, Endereco_id, habilitado, data_cadastro, data_cadastro_br', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'endereco' => array(self::BELONGS_TO, 'Endereco', 'Endereco_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Filial_id' => 'Filial',
			'Endereco_id' => 'Endereco',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('Endereco_id',$this->Endereco_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}