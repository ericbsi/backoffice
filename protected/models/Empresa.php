<?php

/**
 * This is the model class for table "Empresa".
 *
 * The followings are the available columns in table 'Empresa':
 * @property integer $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property string $inscricao_estadual
 * @property integer $Unidade_de_negocio_id
 * @property string $data_criacao
 * @property string $data_de_abertura
 *
 * The followings are the available model relations:
 * @property Cadastro[] $cadastros
 * @property UnidadeDeNegocio $unidadeDeNegocio
 * @property EmpresaHasAtividadeEconomicaSecundaria[] $empresaHasAtividadeEconomicaSecundarias
 * @property EmpresaHasContato[] $empresaHasContatos
 * @property EmpresaHasDadosBancarios[] $empresaHasDadosBancarioses
 * @property EmpresaHasEndereco[] $empresaHasEnderecos
 * @property EmpresaHasFinanceira[] $empresaHasFinanceiras
 * @property EmpresaHasUsuario[] $empresaHasUsuarios
 * @property Filial[] $filials
 * @property GrupoDeAnalistas[] $grupoDeAnalistases
 * @property Representante[] $representantes
 */
class Empresa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Empresa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Empresa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, Unidade_de_negocio_id', 'numerical', 'integerOnly'=>true),
			array('razao_social', 'length', 'max'=>120),
			array('nome_fantasia', 'length', 'max'=>100),
			array('cnpj, inscricao_estadual', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro, data_criacao, data_de_abertura', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, razao_social, nome_fantasia, cnpj, habilitado, data_cadastro, data_cadastro_br, inscricao_estadual, Unidade_de_negocio_id, data_criacao, data_de_abertura', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cadastros' => array(self::HAS_MANY, 'Cadastro', 'Empresa_id'),
			'unidadeDeNegocio' => array(self::BELONGS_TO, 'UnidadeDeNegocio', 'Unidade_de_negocio_id'),
			'empresaHasAtividadeEconomicaSecundarias' => array(self::HAS_MANY, 'EmpresaHasAtividadeEconomicaSecundaria', 'Empresa_id'),
			'empresaHasContatos' => array(self::HAS_MANY, 'EmpresaHasContato', 'Empresa_id'),
			'empresaHasDadosBancarioses' => array(self::HAS_MANY, 'EmpresaHasDadosBancarios', 'Empresa_id'),
			'empresaHasEnderecos' => array(self::HAS_MANY, 'EmpresaHasEndereco', 'Empresa_id'),
			'empresaHasFinanceiras' => array(self::HAS_MANY, 'EmpresaHasFinanceira', 'Empresa_id'),
			'empresaHasUsuarios' => array(self::HAS_MANY, 'EmpresaHasUsuario', 'Empresa_id'),
			'filials' => array(self::HAS_MANY, 'Filial', 'Empresa_id'),
			'grupoDeAnalistases' => array(self::HAS_MANY, 'GrupoDeAnalistas', 'Empresa_id'),
			'representantes' => array(self::HAS_MANY, 'Representante', 'Empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'razao_social' => 'Razao Social',
			'nome_fantasia' => 'Nome Fantasia',
			'cnpj' => 'Cnpj',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'inscricao_estadual' => 'Inscricao Estadual',
			'Unidade_de_negocio_id' => 'Unidade De Negocio',
			'data_criacao' => 'Data Criacao',
			'data_de_abertura' => 'Data De Abertura',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('razao_social',$this->razao_social,true);
		$criteria->compare('nome_fantasia',$this->nome_fantasia,true);
		$criteria->compare('cnpj',$this->cnpj,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('inscricao_estadual',$this->inscricao_estadual,true);
		$criteria->compare('Unidade_de_negocio_id',$this->Unidade_de_negocio_id);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_de_abertura',$this->data_de_abertura,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}