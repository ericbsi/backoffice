<?php

/**
 * This is the model class for table "Tabela_Cotacao".
 *
 * The followings are the available columns in table 'Tabela_Cotacao':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $ModalidadeId
 * @property integer $parcela_inicio
 * @property integer $parcela_fim
 * @property double $taxa
 * @property double $taxa_anual
 *
 * The followings are the available model relations:
 * @property Fator[] $fators
 * @property FilialHasTabelaCotacao[] $filialHasTabelaCotacaos
 * @property FinanceiraHasTabelaCotacao[] $financeiraHasTabelaCotacaos
 * @property Proposta[] $propostas
 * @property TabelaCotacaoHasCarencia[] $tabelaCotacaoHasCarencias
 * @property ModalidadeTabela $modalidade
 */
class TabelaCotacaoBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TabelaCotacaoBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Tabela_Cotacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, habilitado, data_cadastro, taxa', 'required'),
			array('habilitado, ModalidadeId, parcela_inicio, parcela_fim', 'numerical', 'integerOnly'=>true),
			array('taxa, taxa_anual', 'numerical'),
			array('descricao', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, data_cadastro, ModalidadeId, parcela_inicio, parcela_fim, taxa, taxa_anual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fators' => array(self::HAS_MANY, 'FatorBeta', 'Tabela_Cotacao_id'),
			'filialHasTabelaCotacaos' => array(self::HAS_MANY, 'FilialHasTabelaCotacao', 'Tabela_Cotacao_id'),
			'financeiraHasTabelaCotacaos' => array(self::HAS_MANY, 'FinanceiraHasTabelaCotacao', 'Tabela_Cotacao_id'),
			'propostas' => array(self::HAS_MANY, 'Proposta', 'Tabela_id'),
			'tabelaCotacaoHasCarencias' => array(self::HAS_MANY, 'TabelaCotacaoHasCarencia', 'TabelaCotacaoId'),
			'modalidade' => array(self::BELONGS_TO, 'ModalidadeTabela', 'ModalidadeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'ModalidadeId' => 'Modalidade',
			'parcela_inicio' => 'Parcela Inicio',
			'parcela_fim' => 'Parcela Fim',
			'taxa' => 'Taxa',
			'taxa_anual' => 'Taxa Anual',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('ModalidadeId',$this->ModalidadeId);
		$criteria->compare('parcela_inicio',$this->parcela_inicio);
		$criteria->compare('parcela_fim',$this->parcela_fim);
		$criteria->compare('taxa',$this->taxa);
		$criteria->compare('taxa_anual',$this->taxa_anual);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}