<?php

/**
 * This is the model class for table "Cliente".
 *
 * The followings are the available columns in table 'Cliente':
 * @property integer $id
 * @property integer $Pessoa_id
 * @property integer $habilitado
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property AnaliseDeCredito[] $analiseDeCreditos
 * @property Baixa[] $baixas
 * @property Cadastro[] $cadastros
 * @property Pessoa $pessoa
 * @property Usuario $usuario
 * @property Familiar[] $familiars
 * @property FichamentoHasStatusFichamento[] $fichamentoHasStatusFichamentos
 * @property ListaNegra[] $listaNegras
 * @property VendaW[] $vendaWs
 */
class ClienteBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ClienteBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Pessoa_id', 'required'),
			array('Pessoa_id, habilitado, Usuario_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Pessoa_id, habilitado, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'analiseDeCreditos' 				=> array(self::HAS_MANY, 	'AnaliseDeCredito', 				'Cliente_id'),
			'baixas' 							=> array(self::HAS_MANY, 	'Baixa', 							'Cliente_id'),
			'cadastros' 						=> array(self::HAS_MANY, 	'Cadastro', 						'Cliente_id'),
			'pessoa' 							=> array(self::BELONGS_TO, 	'PessoaBeta', 						'Pessoa_id'),
			'usuario' 							=> array(self::BELONGS_TO, 	'Usuario', 							'Usuario_id'),
			'familiars' 						=> array(self::HAS_MANY, 	'Familiar', 						'Cliente_id'),
			'fichamentoHasStatusFichamentos' 	=> array(self::HAS_MANY, 	'FichamentoHasStatusFichamento', 	'Cliente'),
			'listaNegras' 						=> array(self::MANY_MANY, 	'ListaNegra', 						'ListaNegra_has_Cliente(Cliente_id, ListaNegra_id)'),
			'vendaWs' 							=> array(self::HAS_MANY, 	'VendaW', 							'Cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Pessoa_id' => 'Pessoa',
			'habilitado' => 'Habilitado',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}