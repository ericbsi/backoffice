<?php

/**
 * This is the model class for table "Produto".
 *
 * The followings are the available columns in table 'Produto':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property integer $TipoProduto_id
 * @property integer $Categoria_id
 * @property integer $Modelo_id
 * @property integer $Ncm_id
 *
 * The followings are the available model relations:
 * @property ItemDoEstoque[] $itemDoEstoques
 * @property Ncm $ncm
 * @property ProdutoHasValoresCaracteristica[] $produtoHasValoresCaracteristicas
 */
class Produto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Produto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Produto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, TipoProduto_id, Categoria_id, Modelo_id', 'required'),
			array('habilitado, TipoProduto_id, Categoria_id, Modelo_id, Ncm_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, TipoProduto_id, Categoria_id, Modelo_id, Ncm_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemDoEstoques' => array(self::HAS_MANY, 'ItemDoEstoque', 'Produto_id'),
			'ncm' => array(self::BELONGS_TO, 'Ncm', 'Ncm_id'),
			'produtoHasValoresCaracteristicas' => array(self::HAS_MANY, 'ProdutoHasValoresCaracteristica', 'Produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'TipoProduto_id' => 'Tipo Produto',
			'Categoria_id' => 'Categoria',
			'Modelo_id' => 'Modelo',
			'Ncm_id' => 'Ncm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('TipoProduto_id',$this->TipoProduto_id);
		$criteria->compare('Categoria_id',$this->Categoria_id);
		$criteria->compare('Modelo_id',$this->Modelo_id);
		$criteria->compare('Ncm_id',$this->Ncm_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}