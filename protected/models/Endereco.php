<?php

/**
 * This is the model class for table "Endereco".
 *
 * The followings are the available columns in table 'Endereco':
 * @property integer $id
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $cidade
 * @property string $bairro
 * @property string $uf
 * @property string $tempo_de_residencia
 * @property string $cep
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $Tipo_Endereco_id
 * @property integer $endereco_de_cobranca
 * @property integer $Tipo_Moradia_id
 * @property integer $entrega
 *
 * The followings are the available model relations:
 * @property DadosProfissionais[] $dadosProfissionaises
 * @property EmpresaHasEndereco[] $empresaHasEnderecos
 * @property TipoEndereco $tipoEndereco
 * @property TipoMoradia $tipoMoradia
 * @property FilialHasEndereco[] $filialHasEnderecos
 * @property FinanceiraHasEndereco[] $financeiraHasEnderecos
 * @property FornecedorHasEndereco[] $fornecedorHasEnderecos
 */
class Endereco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Endereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo_Endereco_id', 'required'),
			array('habilitado, Tipo_Endereco_id, endereco_de_cobranca, Tipo_Moradia_id, entrega', 'numerical', 'integerOnly'=>true),
			array('logradouro', 'length', 'max'=>145),
			array('numero, cep', 'length', 'max'=>45),
			array('complemento, cidade, bairro', 'length', 'max'=>100),
			array('uf', 'length', 'max'=>2),
			array('tempo_de_residencia', 'length', 'max'=>20),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, logradouro, numero, complemento, cidade, bairro, uf, tempo_de_residencia, cep, habilitado, data_cadastro, data_cadastro_br, Tipo_Endereco_id, endereco_de_cobranca, Tipo_Moradia_id, entrega', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dadosProfissionaises' => array(self::HAS_MANY, 'DadosProfissionais', 'Endereco_id'),
			'empresaHasEnderecos' => array(self::HAS_MANY, 'EmpresaHasEndereco', 'Endereco_id'),
			'tipoEndereco' => array(self::BELONGS_TO, 'TipoEndereco', 'Tipo_Endereco_id'),
			'tipoMoradia' => array(self::BELONGS_TO, 'TipoMoradia', 'Tipo_Moradia_id'),
			'filialHasEnderecos' => array(self::HAS_MANY, 'FilialHasEndereco', 'Endereco_id'),
			'financeiraHasEnderecos' => array(self::HAS_MANY, 'FinanceiraHasEndereco', 'Endereco_id'),
			'fornecedorHasEnderecos' => array(self::HAS_MANY, 'FornecedorHasEndereco', 'Endereco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'logradouro' => 'Logradouro',
			'numero' => 'Numero',
			'complemento' => 'Complemento',
			'cidade' => 'Cidade',
			'bairro' => 'Bairro',
			'uf' => 'Uf',
			'tempo_de_residencia' => 'Tempo De Residencia',
			'cep' => 'Cep',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'Tipo_Endereco_id' => 'Tipo Endereco',
			'endereco_de_cobranca' => 'Endereco De Cobranca',
			'Tipo_Moradia_id' => 'Tipo Moradia',
			'entrega' => 'Entrega',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('logradouro',$this->logradouro,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('complemento',$this->complemento,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('uf',$this->uf,true);
		$criteria->compare('tempo_de_residencia',$this->tempo_de_residencia,true);
		$criteria->compare('cep',$this->cep,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('Tipo_Endereco_id',$this->Tipo_Endereco_id);
		$criteria->compare('endereco_de_cobranca',$this->endereco_de_cobranca);
		$criteria->compare('Tipo_Moradia_id',$this->Tipo_Moradia_id);
		$criteria->compare('entrega',$this->entrega);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}