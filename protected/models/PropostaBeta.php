<?php

class PropostaBeta extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	public function tableName()
	{
		return 'Proposta';
	}

	public function rules()
	{
		return array(
			array('codigo, Analise_de_Credito_id, valor_entrada, valor_final, Status_Proposta_id', 'required'),
			array('Analise_de_Credito_id, Financeira_id, Cotacao_id, Tabela_id, habilitado, qtd_parcelas, carencia, Status_Proposta_id, titulos_gerados, segurada, Banco_id', 'numerical', 'integerOnly'=>true),
			array('valor, valor_parcela, valor_entrada, valor_final', 'numerical'),
			array('codigo', 'length', 'max'=>240),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro, valor_texto, valor_parcela_texto', 'safe'),
			array('id, codigo, Analise_de_Credito_id, Financeira_id, Cotacao_id, Tabela_id, habilitado, data_cadastro, data_cadastro_br, valor, valor_texto, qtd_parcelas, carencia, valor_parcela, valor_parcela_texto, valor_entrada, valor_final, Status_Proposta_id, titulos_gerados, segurada, Banco_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'analistaHasPropostaHasStatusPropostas' 	=> array(self::HAS_MANY, 	'AnalistaHasPropostaHasStatusProposta', 'Proposta_id'),
			'apolices' 									=> array(self::HAS_MANY, 	'Apolice', 								'Proposta_id'),
			'itemDoBorderos' 							=> array(self::HAS_MANY, 	'ItemDoBordero', 						'Proposta_id'),
			'itemPendentes' 							=> array(self::HAS_MANY, 	'ItemPendente', 						'ItemPendente'),
			'negacaoDeCreditos' 						=> array(self::HAS_MANY, 	'NegacaoDeCredito', 					'Proposta'),
			'banco' 									=> array(self::BELONGS_TO, 	'Banco', 								'Banco_id'),
			'analiseDeCredito' 							=> array(self::BELONGS_TO, 	'AnaliseDeCreditoBeta',					'Analise_de_Credito_id'),
			'tabelaCotacao' 							=> array(self::BELONGS_TO, 	'TabelaCotacaoBeta',					'Tabela_id'),
			'reanalises' 								=> array(self::HAS_MANY, 	'Reanalise', 							'Proposta'),
			'solicitacaoDeCancelamentos' 				=> array(self::HAS_MANY, 	'SolicitacaoDeCancelamento', 			'Proposta_id'),
			'venda' 									=> array(self::HAS_ONE, 	'Venda', 								'Proposta_id')
		);
	}

	public function valorRepasse()
	{

        $valorFinanciado = $this->valor - $this->valor_entrada;

        $fator = FatorBeta::model()->find('Tabela_Cotacao_id = ' . $this->tabelaCotacao->id . ' AND parcela = ' . $this->qtd_parcelas . ' AND carencia = ' . $this->carencia . ' AND habilitado');

        if ($fator != NULL) {
            return $valorFinanciado - $fator->getValorRetido($valorFinanciado);
        } {
            return $valorFinanciado; //mudanca inserida 11/02/2016. Retorne o valor financiado
        }
    }

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'Analise_de_Credito_id' => 'Analise De Credito',
			'Financeira_id' => 'Financeira',
			'Cotacao_id' => 'Cotacao',
			'Tabela_id' => 'Tabela',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'valor' => 'Valor',
			'valor_texto' => 'Valor Texto',
			'qtd_parcelas' => 'Qtd Parcelas',
			'carencia' => 'Carencia',
			'valor_parcela' => 'Valor Parcela',
			'valor_parcela_texto' => 'Valor Parcela Texto',
			'valor_entrada' => 'Valor Entrada',
			'valor_final' => 'Valor Final',
			'Status_Proposta_id' => 'Status Proposta',
			'titulos_gerados' => 'Titulos Gerados',
			'segurada' => 'Segurada',
			'Banco_id' => 'Banco',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('Analise_de_Credito_id',$this->Analise_de_Credito_id);
		$criteria->compare('Financeira_id',$this->Financeira_id);
		$criteria->compare('Cotacao_id',$this->Cotacao_id);
		$criteria->compare('Tabela_id',$this->Tabela_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('valor_texto',$this->valor_texto,true);
		$criteria->compare('qtd_parcelas',$this->qtd_parcelas);
		$criteria->compare('carencia',$this->carencia);
		$criteria->compare('valor_parcela',$this->valor_parcela);
		$criteria->compare('valor_parcela_texto',$this->valor_parcela_texto,true);
		$criteria->compare('valor_entrada',$this->valor_entrada);
		$criteria->compare('valor_final',$this->valor_final);
		$criteria->compare('Status_Proposta_id',$this->Status_Proposta_id);
		$criteria->compare('titulos_gerados',$this->titulos_gerados);
		$criteria->compare('segurada',$this->segurada);
		$criteria->compare('Banco_id',$this->Banco_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function calcularValorDoSeguro()
	{
        $valorDoSeguro = 0;

        /* Possui seguro */
        if ($this->segurada) {
            /* Proposta segurada nos novos moldes */
            if (( $this->venda !== NULL ) && (count($this->venda->itemDaVendas) > 0)) {
                foreach ($this->venda->itemDaVendas as $item) {
                    /* É o seguro */
                    if (($item->itemDoEstoque->produto->id == 1) && ($item->habilitado)) {
                        return $item->preco;
                    }
                }
            }
            /* Trata-se de uma proposta com o modelo antigo de seleção de seguro */ else {
                return ( ( $this->valor - $this->valor_entrada ) / 100 ) * 10;
            }
        }

        /* Não possui seguro */ else {
            return 0;
        }
    }

    public function renderBotaoStatus(){

        $itemDoBordero = NULL;

        if( $this->Status_Proposta_id == 2 )
        {
            $itemDoBordero                 = ItemDoBorderoBeta::model()->find('Proposta_id = '.$this->id . ' AND t.habilitado');

            if( $itemDoBordero             != NULL )
            {
                $loteHasBordero             = LoteHasBorderoBeta::model()->find('Bordero_id = '. $itemDoBordero->bordero->id . ' AND t.habilitado');

                if( $loteHasBordero != NULL )
                {
                    $statusLote             = LotePagamentoHasStatusLotePagamentoBeta::model()->find('LotePagamento_id = ' . $loteHasBordero->Lote_id . ' AND StatusLotePagamento_id = 2 AND t.habilitado');

                    if( $statusLote != NULL )
                    {
                        return "<button type='button' class='btn label label-success'>PAGAMENTO EFETUADO</button>";                        
                    }
                    else
                    {
                        return "<button type='button' class='btn label label-success'>PAGAMENTO AUTORIZADO</button>";
                    }
                }
                else
                {
                    return "<button type='button' class='btn label label-success'>PAGAMENTO AUTORIZADO</button>";
                }
            }
            else
            {
                return "<button type='button' class='btn label label-success'>APROVADA</button>";
            }

        }
        else if( $this->Status_Proposta_id == 7 )
        {
            return "<button type='button' class='btn label label-warning'>PENDENTE</button>";
        }
        else if( $this->Status_Proposta_id == 8 )
        {
            return "<button type='button' class='btn label label-danger'>NEGADO</button>";
        }
        else{
            return "<button type='button' class='btn label label-danger'>INDEFINIDO</button>";   
        }
    }

}