<?php


class DadosBancariosBeta extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}
	public function tableName()
	{
		return 'Dados_Bancarios';
	}
	public function rules()
	{
		return array(
			array('habilitado, Tipo_Conta_Bancaria_id, Banco_id', 'numerical', 'integerOnly'=>true),
			array('agencia, operacao, numero, data_abertura', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			array('id, agencia, operacao, numero, data_abertura, habilitado, data_cadastro, data_cadastro_br, Tipo_Conta_Bancaria_id, Banco_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'dadosPagamentos' 					=> array(self::HAS_MANY, 	'DadosPagamentoBeta', 					'Dados_Bancarios_Debito'	),
			'dadosPagamentos1'				 	=> array(self::HAS_MANY, 	'DadosPagamentoBeta', 					'Dados_Bancarios_Credito'	),
			'banco' 							=> array(self::BELONGS_TO, 	'BancoBeta',							'Banco_id'					),
			'tipoContaBancaria' 				=> array(self::BELONGS_TO, 	'TipoContaBancariaBeta',				'Tipo_Conta_Bancaria_id'	),
			'empresaHasDadosBancarioses' 		=> array(self::HAS_MANY, 	'EmpresaHasDadosBancariosBeta',			'Dados_Bancarios_id'		),
			'financeiraHasDadosBancarioses' 	=> array(self::HAS_MANY, 	'FinanceiraHasDadosBancariosbeta',		'Dados_Bancarios_id'		),
			'nucleoFiliaisHasDadosBancarioses' 	=> array(self::HAS_MANY, 	'NucleoFiliaisHasDadosBancariosBeta',	'Dados_Bancarios_id'		),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' 								=> 'ID',
			'agencia' 							=> 'Agencia',
			'operacao' 							=> 'Operacao',
			'numero' 							=> 'Numero',
			'data_abertura' 					=> 'Data Abertura',
			'habilitado' 						=> 'Habilitado',
			'data_cadastro' 					=> 'Data Cadastro',
			'data_cadastro_br' 					=> 'Data Cadastro Br',
			'Tipo_Conta_Bancaria_id' 			=> 'Tipo Conta Bancaria',
			'Banco_id' 							=> 'Banco',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agencia',$this->agencia,true);
		$criteria->compare('operacao',$this->operacao,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('data_abertura',$this->data_abertura,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('Tipo_Conta_Bancaria_id',$this->Tipo_Conta_Bancaria_id);
		$criteria->compare('Banco_id',$this->Banco_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}