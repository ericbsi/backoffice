<?php

/**
 * This is the model class for table "ConfiguracoesLecca".
 *
 * The followings are the available columns in table 'ConfiguracoesLecca':
 * @property integer $id
 * @property string $cod_lojista
 * @property string $cod_tabJuros
 * @property string $cod_tab15
 * @property string $cod_tab30
 * @property string $cod_tab45
 * @property string $cod_tabJuros90
 * @property string $cod_tabPromo
 * @property integer $habilitado
 * @property integer $NucleoFiliais_id
 */
class ConfiguracoesLecca extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ConfiguracoesLecca';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, NucleoFiliais_id', 'required'),
			array('habilitado, NucleoFiliais_id', 'numerical', 'integerOnly'=>true),
			array('cod_lojista, cod_tabJuros, cod_tab15, cod_tab30, cod_tab45, cod_tabJuros90, cod_tabPromo', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cod_lojista, cod_tabJuros, cod_tab15, cod_tab30, cod_tab45, cod_tabJuros90, cod_tabPromo, habilitado, NucleoFiliais_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod_lojista' => 'Cod Lojista',
			'cod_tabJuros' => 'Cod Tab Juros',
			'cod_tab15' => 'Cod Tab15',
			'cod_tab30' => 'Cod Tab30',
			'cod_tab45' => 'Cod Tab45',
			'cod_tabJuros90' => 'Cod Tab Juros90',
			'cod_tabPromo' => 'Cod Tab Promo',
			'habilitado' => 'Habilitado',
			'NucleoFiliais_id' => 'Nucleo Filiais',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cod_lojista',$this->cod_lojista,true);
		$criteria->compare('cod_tabJuros',$this->cod_tabJuros,true);
		$criteria->compare('cod_tab15',$this->cod_tab15,true);
		$criteria->compare('cod_tab30',$this->cod_tab30,true);
		$criteria->compare('cod_tab45',$this->cod_tab45,true);
		$criteria->compare('cod_tabJuros90',$this->cod_tabJuros90,true);
		$criteria->compare('cod_tabPromo',$this->cod_tabPromo,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConfiguracoesLecca the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
