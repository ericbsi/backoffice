<?php

class PoliticaCreditoHasRegra extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
	public function tableName()
	{
		return 'PoliticaCredito_has_Regra';
	}

	
	public function rules()
	{
		return array(
			array('PoliticaCredito_id, Regra_id', 'required'),
			array('id, PoliticaCredito_id, Regra_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, PoliticaCredito_id, Regra_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'politicaCredito' => array(self::BELONGS_TO, 'PoliticaCredito', 'PoliticaCredito_id'),
			'regra' => array(self::BELONGS_TO, 'Regra', 'Regra_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'PoliticaCredito_id' => 'Politica Credito',
			'Regra_id' => 'Regra',
			'habilitado' => 'Habilitado',
		);
	}


	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('PoliticaCredito_id',$this->PoliticaCredito_id);
		$criteria->compare('Regra_id',$this->Regra_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}