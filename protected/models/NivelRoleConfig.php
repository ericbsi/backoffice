<?php

/**
 * This is the model class for table "Nivel_Role_Config".
 *
 * The followings are the available columns in table 'Nivel_Role_Config':
 * @property integer $id
 * @property integer $Nivel_Role_id
 * @property string $index_view
 * @property string $template
 * @property integer $index_on
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property NivelRole $nivelRole
 */
class NivelRoleConfig extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NivelRoleConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Nivel_Role_Config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nivel_Role_id, index_view, template', 'required'),
			array('Nivel_Role_id, index_on, habilitado', 'numerical', 'integerOnly'=>true),
			array('index_view, template', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Nivel_Role_id, index_view, template, index_on, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nivelRole' => array(self::BELONGS_TO, 'NivelRole', 'Nivel_Role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Nivel_Role_id' => 'Nivel Role',
			'index_view' => 'Index View',
			'template' => 'Template',
			'index_on' => 'Index On',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Nivel_Role_id',$this->Nivel_Role_id);
		$criteria->compare('index_view',$this->index_view,true);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('index_on',$this->index_on);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}