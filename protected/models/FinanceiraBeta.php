<?php

/**
 * This is the model class for table "Financeira".
 *
 * The followings are the available columns in table 'Financeira':
 * @property integer $id
 * @property string $cnpj
 * @property string $nome
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $Contato_id
 *
 * The followings are the available model relations:
 * @property CotacaoHasFinanceira[] $cotacaoHasFinanceiras
 * @property EmpresaHasFinanceira[] $empresaHasFinanceiras
 * @property FinanceiraHasTabelaCotacao[] $financeiraHasTabelaCotacaos
 * @property FinanceiraHasDadosBancarios[] $financeiraHasDadosBancarioses
 * @property FinanceiraHasEndereco[] $financeiraHasEnderecos
 */
class FinanceiraBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FinanceiraBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Financeira';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Contato_id', 'required'),
			array('habilitado, Contato_id', 'numerical', 'integerOnly'=>true),
			array('cnpj', 'length', 'max'=>45),
			array('nome', 'length', 'max'=>100),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cnpj, nome, habilitado, data_cadastro, data_cadastro_br, Contato_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cotacaoHasFinanceiras' => array(self::HAS_MANY, 'CotacaoHasFinanceira', 'Financeira_id'),
			'empresaHasFinanceiras' => array(self::HAS_MANY, 'EmpresaHasFinanceira', 'Financeira_id'),
			'financeiraHasTabelaCotacaos' => array(self::HAS_MANY, 'FinanceiraHasTabelaCotacao', 'Financeira_id'),
			'financeiraHasDadosBancarioses' => array(self::HAS_MANY, 'FinanceiraHasDadosBancarios', 'Financeira_id'),
			'financeiraHasEnderecos' => array(self::HAS_MANY, 'FinanceiraHasEndereco', 'Financeira_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cnpj' => 'Cnpj',
			'nome' => 'Nome',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'Contato_id' => 'Contato',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cnpj',$this->cnpj,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('Contato_id',$this->Contato_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}