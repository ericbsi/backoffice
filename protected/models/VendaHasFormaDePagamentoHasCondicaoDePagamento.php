<?php

/**
 * This is the model class for table "Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento".
 *
 * The followings are the available columns in table 'Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento':
 * @property integer $id
 * @property integer $Venda_id
 * @property integer $Forma_de_Pagamento_has_Condicao_de_Pagamento_id
 * @property double $valor
 * @property integer $qtdParcelas
 *
 * The followings are the available model relations:
 * @property Titulo[] $titulos
 * @property FormaDePagamentoHasCondicaoDePagamento $formaDePagamentoHasCondicaoDePagamento
 * @property Venda $venda
 */
class VendaHasFormaDePagamentoHasCondicaoDePagamento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VendaHasFormaDePagamentoHasCondicaoDePagamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas', 'required'),
			array('id, Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, qtdParcelas', 'numerical', 'integerOnly'=>true),
			array('valor', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'titulos' => array(self::HAS_MANY, 'Titulo', 'Venda_has_FP_has_CP_FP_has_CP_id'),
			'formaDePagamentoHasCondicaoDePagamento' => array(self::BELONGS_TO, 'FormaDePagamentoHasCondicaoDePagamento', 'Forma_de_Pagamento_has_Condicao_de_Pagamento_id'),
			'venda' => array(self::BELONGS_TO, 'Venda', 'Venda_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Venda_id' => 'Venda',
			'Forma_de_Pagamento_has_Condicao_de_Pagamento_id' => 'Forma De Pagamento Has Condicao De Pagamento',
			'valor' => 'Valor',
			'qtdParcelas' => 'Qtd Parcelas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Venda_id',$this->Venda_id);
		$criteria->compare('Forma_de_Pagamento_has_Condicao_de_Pagamento_id',$this->Forma_de_Pagamento_has_Condicao_de_Pagamento_id);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('qtdParcelas',$this->qtdParcelas);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}