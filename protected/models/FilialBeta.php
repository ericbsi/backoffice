<?php


class FilialBeta extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	
	public function tableName()
	{
		return 'Filial';
	}

	public function rules()
	{
		return array(
			array('Contato_id, Empresa_id, isMatriz', 'required'),
			array('habilitado, Contato_id, Empresa_id, isMatriz, Unidade_de_negocio_id, PoliticaCredito_id, NucleoFiliais_id, poloNucleo, Regional_id', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>20),
			array('nome_fantasia', 'length', 'max'=>100),
			array('cnpj, inscricao_estadual', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro, data_de_abertura', 'safe'),
			array('id, codigo, nome_fantasia, cnpj, inscricao_estadual, data_cadastro, data_cadastro_br, habilitado, Contato_id, Empresa_id, data_de_abertura, isMatriz, Unidade_de_negocio_id, PoliticaCredito_id, NucleoFiliais_id, poloNucleo, Regional_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminHasParceiros' => array(self::HAS_MANY, 'AdminHasParceiro', 'Parceiro'),
			'analiseDeCreditos' => array(self::HAS_MANY, 'AnaliseDeCredito', 'Filial_id'),
			'arquivoCnabs' => array(self::HAS_MANY, 'ArquivoCnab', 'Filial_id'),
			'baixas' => array(self::HAS_MANY, 'Baixa', 'Filial_id'),
			'borderos' => array(self::HAS_MANY, 'Bordero', 'destinatario'),
			'cnabDetalhes' => array(self::HAS_MANY, 'CnabDetalhe', 'Filial_id'),
			'estoques' => array(self::HAS_MANY, 'Estoque', 'Filial_id'),
			'unidadeDeNegocio' => array(self::BELONGS_TO, 'UnidadeDeNegocio', 'Unidade_de_negocio_id'),
			'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),
			'nucleoFiliais' => array(self::BELONGS_TO, 'NucleoFiliais', 'NucleoFiliais_id'),
			'regional' => array(self::BELONGS_TO, 'FilialBeta', 'Regional_id'),
			'filials' => array(self::HAS_MANY, 'FilialBeta', 'Regional_id'),
			'politicaCredito' => array(self::BELONGS_TO, 'PoliticaCredito', 'PoliticaCredito_id'),
			'filialLoginConfigs' => array(self::HAS_MANY, 'FilialLoginConfig', 'Filial_id'),
			'filialHasTabelaCotacaos' => array(self::HAS_MANY, 'FilialHasTabelaCotacao', 'Filial_id'),
			'filialHasAtividadeEconomicaSecundarias' => array(self::HAS_MANY, 'FilialHasAtividadeEconomicaSecundaria', 'Filial_id'),
			'filialHasEnderecos' => array(self::HAS_MANY, 'FilialHasEndereco', 'Filial_id'),
			'filialHasUsuarios' => array(self::HAS_MANY, 'FilialHasUsuario', 'Filial_id'),
			'filialHasVendedors' => array(self::HAS_MANY, 'FilialHasVendedor', 'Filial_id'),
			'fornecedors' => array(self::HAS_MANY, 'Fornecedor', 'Filial_id'),
			'grupoDeAnalistasHasFilials' => array(self::HAS_MANY, 'GrupoDeAnalistasHasFilial', 'Filial_id'),
			'migracaos' => array(self::HAS_MANY, 'Migracao', 'destino'),
			'recebimentoDeDocumentacaos' => array(self::HAS_MANY, 'RecebimentoDeDocumentacao', 'destinatario'),
			'registroDePendenciases' => array(self::HAS_MANY, 'RegistroDePendencias', 'destinatario'),
			'vendaWs' => array(self::HAS_MANY, 'VendaW', 'Filial_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'nome_fantasia' => 'Nome Fantasia',
			'cnpj' => 'Cnpj',
			'inscricao_estadual' => 'Inscricao Estadual',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'habilitado' => 'Habilitado',
			'Contato_id' => 'Contato',
			'Empresa_id' => 'Empresa',
			'data_de_abertura' => 'Data De Abertura',
			'isMatriz' => 'Is Matriz',
			'Unidade_de_negocio_id' => 'Unidade De Negocio',
			'PoliticaCredito_id' => 'Politica Credito',
			'NucleoFiliais_id' => 'Nucleo Filiais',
			'poloNucleo' => 'Polo Nucleo',
			'Regional_id' => 'Regional',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('nome_fantasia',$this->nome_fantasia,true);
		$criteria->compare('cnpj',$this->cnpj,true);
		$criteria->compare('inscricao_estadual',$this->inscricao_estadual,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Contato_id',$this->Contato_id);
		$criteria->compare('Empresa_id',$this->Empresa_id);
		$criteria->compare('data_de_abertura',$this->data_de_abertura,true);
		$criteria->compare('isMatriz',$this->isMatriz);
		$criteria->compare('Unidade_de_negocio_id',$this->Unidade_de_negocio_id);
		$criteria->compare('PoliticaCredito_id',$this->PoliticaCredito_id);
		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);
		$criteria->compare('poloNucleo',$this->poloNucleo);
		$criteria->compare('Regional_id',$this->Regional_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getEndereco()
    {
      $filialHasEndereco = FilialHasEnderecoBeta::model()->find('Filial_id = ' . $this->id);

      return EnderecoBeta::model()->findByPk($filialHasEndereco->Endereco_id);
    }

    public function getConcat()
    {
      return strtoupper($this->nome_fantasia . ' / ' . $this->getEndereco()->cidade . '-' . $this->getEndereco()->uf);
    }

	/**
     * Retorna indicativos variados sobre as vendas de uma filial, 
     * respeitando parametros enviados, como cnjp da filial, datas de filtro, etc
     * @param DATE [de, ate]
     * @return [json result]
     * @author Eric
     * @version 1.0
    */
    public function actionIndicativosVendas()
    {
    	$totalMes 				= 0;
    	$nDeVendasMes 			= 0;
    	$valorMedioVendasMes	= 0;
    	$totalVendasDia 		= 0;
    	
    	$Dia 					= date('Y-m-01');
    	
    	$meses                  = [
            ['01','Janeiro',  0 ],
            ['02','Fevereiro',0 ],
            ['03','Março',    0 ],
            ['04','Abril',    0 ],
            ['05','Maio',     0 ],
            ['06','Junho',    0 ],
            ['07','Julho',    0 ], 
            ['08','Agosto',   0 ],
            ['09','Setembro', 0 ],
            ['10','Outubro',  0 ],
            ['11','Novembro', 0 ],
            ['12','Dezembro', 0 ]
        ];

    	/*Montando critérios de pesquisa*/
    	$criteria 				= new CDbCriteria;
    	
    	/*
    		* Inner join analise de credito
    		* pois ela possui o id da filial
    	*/

    	$criteria->with 		= ['analiseDeCredito'	=> ['alias'	=>	'ac']	];
    	$criteria->addInCondition('ac.Filial_id', [$this->id], ' AND ');
    	
    	/*Propostas aprovadas e habilitadas, com titulos gerados*/
    	$criteria->addInCondition('t.Status_Proposta_id', [2,7], 	' AND ');
    	$criteria->addInCondition('t.titulos_gerados', [1], 		' AND ');
    	$criteria->addInCondition('t.habilitado', [1], 				' AND ');
	
    	/*Período inicial, primeiro dia do mês atual, até final do mês*/
    	$criteria->addBetweenCondition('t.data_cadastro', date('Y-m-01') . ' 00:00:00', date("Y-m-t", strtotime(date('Y-m-d'))) . ' 23:59:59', 'AND');

    	$propostas 					= PropostaBeta::model()->findAll($criteria);

    	foreach( $propostas as $proposta )
    	{
    		$totalMes 				+= ($proposta->valor - $proposta->valor_entrada);
    		
    		if( $proposta->data_cadastro >= date('Y-m-d 00:00:00') && $proposta->data_cadastro <= date('Y-m-d 23:59:59') )
    		{
    			$totalVendasDia 	+= ($proposta->valor -$proposta->valor_entrada);
    		}
    	}

    	$nDeVendasMes 				= count($propostas);
    	$valorMedioVendasMes 		= ( $nDeVendasMes > 0 ? ($totalMes / $nDeVendasMes) : 0 );

    	/*retorna array configurado com os indicativos*/
    	return [
    		'totalMes'				=> $totalMes,
    		'nDeVendasMes'			=> $nDeVendasMes,
    		'valorMedioVendasMes'	=> $valorMedioVendasMes,
    		'totalVendasDia'		=> $totalVendasDia,
    	];
    }
}