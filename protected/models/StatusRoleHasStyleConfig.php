<?php

/**
 * This is the model class for table "Status_Role_has_Style_Config".
 *
 * The followings are the available columns in table 'Status_Role_has_Style_Config':
 * @property integer $id
 * @property integer $Status_Role_id
 * @property integer $Style_Config_id
 *
 * The followings are the available model relations:
 * @property StatusRole $statusRole
 * @property StyleConfig $styleConfig
 */
class StatusRoleHasStyleConfig extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatusRoleHasStyleConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Status_Role_has_Style_Config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status_Role_id, Style_Config_id', 'required'),
			array('Status_Role_id, Style_Config_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Status_Role_id, Style_Config_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'statusRole' => array(self::BELONGS_TO, 'StatusRole', 'Status_Role_id'),
			'styleConfig' => array(self::BELONGS_TO, 'StyleConfig', 'Style_Config_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Status_Role_id' => 'Status Role',
			'Style_Config_id' => 'Style Config',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Status_Role_id',$this->Status_Role_id);
		$criteria->compare('Style_Config_id',$this->Style_Config_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}