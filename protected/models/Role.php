<?php

class Role extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Role the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, descricao, label, Status_Role_id', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('papel, menu_padrao, login_redirect', 'length', 'max'=>100),
			array('label', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, papel, descricao, Status_Role_id, habilitado, data_cadastro, menu_padrao, label, login_redirect', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'roleConfig'     => array(self::HAS_ONE,     'RoleConfig',       'Role'),
			'roleHasFuncaos' => array(self::HAS_MANY,    'RoleHasFuncao',    'Role'),
            'status'         => array(self::BELONGS_TO,  'StatusRole',       'Status_Role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'papel' => 'Papel',
			'descricao' => 'Descrição',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'menu_padrao' => 'Menu Padrao',
			'label' => 'Label',
			'login_redirect' => 'Login Redirect',
		);
	}


	/**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model Funcao]
     * @author Eric
     * @version 1.0
     */
    public function search($limit)
    {
        $criteria = new CDbCriteria;

        /* Campos texto, usando LIKE*/
        $criteria->addSearchCondition('papel', 		$this->papel);
        $criteria->addSearchCondition('descricao', 	$this->descricao);
        $criteria->addSearchCondition('label', 		$this->label);

        $criteria->compare('habilitado', 			$this->habilitado);


        $criteria->offset = $limit['start'];
        $criteria->limit = $limit['limit'];

        return Role::model()->findAll($criteria);
    }

    /**
     * Inclui e editar informações sobre as funções
     * @param POST['Funcao']
     * @param INT FuncaoId
     * @return [bool result, array [errors, msgconfig]]
     * @author Eric
     * @version 1.0
     */
    public function persist($Role, $RoleId)
    {
        $retorno['hasErrors']   = false;
        $R                      = new Role;

        /* Se houver um id, trata-se de uma atualização nos dados */
        if ($RoleId !== '0')
        {
            $R = Role::model()->findByPk($RoleId);
        }

        /* Se não, trata-se de um novo registro */ else
        {
            $R->data_cadastro   = date('Y-m-d H:i:s');
            $R->Status_Role_id  = 1;
        }

        $R->attributes = $Role;

        /* Se houver erro ao salvar/atualizar, configura o retorno de acordo */
        if (!$R->save())
        {
            $retorno['hasErrors'] = true;
            $retorno['errors'] = $R->getErrors();
        }

        return $retorno;
    }


     /**
     * Monta a estrutura necessária para apresentação em uma jQuery DataTable
     * @param INT $draw - variável padrão do plugin dataTables
     * @param ARRAY $limit 
     * @return [json result]
     * @author Eric
     * @version 1.0
     */
    public function dataTable($draw, $limit)
    {
        $roleCollection         = [];
        $recordsTotal           = count(Role::model()->findAll());

        foreach (Role::model()->search($limit) as $role)
        {
            $roleCollection[]   =  [   
                'label'         => mb_strtoupper($role->label),
                'descricao'     => mb_strtoupper($role->descricao),
                'status'        => mb_strtoupper($role->status->nome),
                'btn_editar'    => '<a data-form-wrapper="#portlet-form" data-source="/perfil/loadFormEdit/" data-entity-id="' . $role->id . '" data-form-load="#form-role" data-collapse-div="#bg-default" class="btn btn-credshow-orange waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-edit"></i></a>',
                'btn_config'    => '<form method="post" action="/perfil/configurar/"><input type="hidden" name="RoleId" value="'.$role->id.'"><button type="submit" class="btn btn-credshow-blue-one waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-cog"></i></button></form>',
            	'idPerfil'      => $role->id
			];
        }

        return [
            'collection'        => $roleCollection,
            'total'             => $recordsTotal
        ];
    }

    /**
     * Retorna um array com as informações necessárias para carregar um formulário de edição
     * de um obejto 
     * @param INT $id
     * @return [arr result]
     * @author Eric
     * @version 1.0
     */
    public function loadFormEdit($id)
    {
        $role = Role::model()->findByPk($id);

        return [
            'fields' => [
                'id' => [
                    'value' => $role->id,
                    'type' => 'text',
                    'input_bind' => 'role_id',
                ],
                'descricao' => [
                    'value' => mb_strtoupper($role->descricao),
                    'type' => 'text',
                    'input_bind' => 'role_descricao',
                ],
                'label' => [
                    'value' => mb_strtoupper($role->label),
                    'type' => 'text',
                    'input_bind' => 'role_label',
                ],
            ]
        ];
    }


    /**
     * Retorna um resultado de uma query com os ids dos usuários [ativos ou não]
     * relacionados ao Role
     * @param INT $ativos
     * @return [OBJ commandQueryResult]
     * @author Eric
     * @version 1.0
    */
    public function getUsuarios( $urativos = 1, $uativos = 1 )
    {
        $sql     = "SELECT U.id AS `UserId` FROM `Usuario` AS U ";
        $sql    .= "INNER JOIN `Usuario_Role` AS UR ";
        $sql    .= "ON UR.Role_id = ".$this->id." AND UR.Usuario_id = U.id AND UR.habilitado = ".$urativos." AND U.habilitado = " . $uativos;

        return Yii::app()->db->createCommand($sql)->queryAll();
    }


    /**
     * Retorna uma coleção de objetos de niveis de acesso
     * relacionados a este Role
     * @param INT $ativos
     * @return [OBJ commandQueryResult]
     * @author Eric
     * @version 1.0
    */
    public function getNiveisDeAcesso( $ativos = 1, $status = [2] )
    {

        $status     = join(',', $status);

        return NivelRole::model()->findAll('t.Role_id = ' . $this->id . ' AND t.habilitado = ' . $ativos . ' AND    Status_Nivel_Role_id IN(' . $status . ')');
    }
}