<?php


class LotePagamento extends CActiveRecord
{

   /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return LotePagamento the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'LotePagamento';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
          array('data_cadastro, Usuario_id, NucleoFiliais_id', 'required'),
          array('habilitado, Usuario_id, NucleoFiliais_id, DadosPagamento_id', 'numerical', 'integerOnly' => true),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('id, data_cadastro, habilitado, Usuario_id, NucleoFiliais_id, DadosPagamento_id', 'safe', 'on' => 'search'),
      );
   }

   /**
    * @return array relational rules.
    */
   public function relations()
   {
      // NOTE: you may need to adjust the relation name and the related
      // class name for the relations automatically generated below.
      return array(
          'nucleoFiliais'                         => array(self::BELONGS_TO,  'NucleoFiliais',                        'NucleoFiliais_id'  ),
          'usuario'                               => array(self::BELONGS_TO,  'Usuario',                              'Usuario_id'        ),
          'lotePagamentoHasStatusLotePagamentos'  => array(self::HAS_MANY,    'LotePagamentoHasStatusLotePagamento',  'LotePagamento_id'  ),
          'loteHasBorderos'                       => array(self::HAS_MANY,    'LoteHasBordero',                       'Lote_id'           ),
          'loteDadosPagamento'                    => array(self::BELONGS_TO,  'DadosPagamento',                       'DadosPagamento_id' ),
      );
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'data_cadastro' => 'Data Cadastro',
          'habilitado' => 'Habilitado',
          'Usuario_id' => 'Usuario',
          'NucleoFiliais_id' => 'Nucleo Filiais',
          'DadosPagamento_id' => 'Dados do Pagamento',
      );
   }

   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('data_cadastro', $this->data_cadastro, true);
      $criteria->compare('habilitado', $this->habilitado);
      $criteria->compare('Usuario_id', $this->Usuario_id);
      $criteria->compare('NucleoFiliais_id', $this->NucleoFiliais_id);
      $criteria->compare('DadosPagamento_id', $this->DadosPagamento_id);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }
   
   public function excluirLote($idLote)
   {
      
      $sigacLogs = [];

      $arrayRetorno = array(
          'hasErrors'   => 0                                ,
          'msg'         => 'Borderô excluido com Sucesso.'  ,
          'pntfyClass'  => 'success'
      );
      
      $transaction = Yii::app()->db->beginTransaction();
      
      $lote = LotePagamento::model()->find('habilitado AND id = ' . $idLote);
      
      if($lote !== null)
      {
         $loteHasBordero = LoteHasBordero::model()->findAll('Lote_id = ' . $lote->id);
         
         foreach ($loteHasBordero as $lb)
         {
            
            $bordero          = Bordero::model()->find('habilitado AND id = ' . $lb->Bordero_id);
            $bordero->Status  = 1                                                               ;
            
            if($bordero->save())
            {
               
               $lb->habilitado = 0;
               
               if($lb->save())
               {
               
                    $obs = "Usuário " . Yii::app()->session['usuario']->username . " alterou o status do borderô de"
                            . "4 para 1. Na desabilitação do Lote de id: " . $lote->id;

                    $sigacLog               = new SigacLog                      ;
                    $sigacLog->acao         = 'Alteração de Status no Borderô'  ;
                    $sigacLog->entidade     = 'Borderô'                         ;
                    $sigacLog->entidade_id  = $bordero->id                      ;
                    $sigacLog->data         = date('Y-m-d H:i:s')               ;
                    $sigacLog->habilitado   = 1                                 ;
                    $sigacLog->observacao   = $obs                              ;
                    $sigacLog->cookie_id    = null                              ;
                    $sigacLogs[]            = $sigacLog                         ;
                   
               }
               else
               {

                    ob_start();
                    var_dump($lb->getErrors());
                    $result = ob_get_clean();

                    $arrayRetorno['hasErrors'  ]  = 1                                                           ;
                    $arrayRetorno['msg'        ] = "Impossível desabilitar loteHasBordero ID: $lb->id. $result" ;
                    $arrayRetorno['pntfyClass' ] = 'error'                                                      ;

                    $transaction->rollBack();

                    return $arrayRetorno;
               }
            
            }
            else
            {

               ob_start();
               var_dump($bordero->getErrors());
               $result = ob_get_clean();

               $arrayRetorno['hasErrors'  ]  = 1                                             ;
               $arrayRetorno['msg'        ] = 'Ocorreu um erro. Tente novamente.' . $result  ;
               $arrayRetorno['pntfyClass' ] = 'error'                                        ;

               $transaction->rollBack();

               return $arrayRetorno;

            }
         }
         
      }
      else
      {

         $arrayRetorno['hasErrors'  ] = 1                                                       ;
         $arrayRetorno['msg'        ] = 'Ocorreu um erro. Lote não encontrado. Id: ' . $idLote  ;
         $arrayRetorno['pntfyClass' ] = 'error'                                                 ;

         $transaction->rollBack();

         return $arrayRetorno;
      }
      
      $lote->habilitado = 0;
      
      if($lote->save())
      {
               
         $obs = "Usuário " . Yii::app()->session['usuario']->username . " desabilitou o Lote de Id: " . $lote->id;

         $sigacLog               = new SigacLog                      ;
         $sigacLog->acao         = 'Alteração de Status no Borderô'  ;
         $sigacLog->entidade     = 'Borderô'                         ;
         $sigacLog->entidade_id  = $bordero->id                      ;
         $sigacLog->data         = date('Y-m-d H:i:s')               ;
         $sigacLog->habilitado   = 1                                 ;
         $sigacLog->observacao   = $obs                              ;
         $sigacLog->cookie_id    = null                              ;
         $sigacLogs[]            = $sigacLog                         ;
      }
      else
      {

         ob_start();
         var_dump($lote->getErrors());
         $result = ob_get_clean();

         $arrayRetorno['hasErrors'  ]  = 1                                             ;
         $arrayRetorno['msg'        ]  = 'Ocorreu um erro. Tente novamente.' . $result ;
         $arrayRetorno['pntfyClass' ]  = 'error'                                       ;

         $transaction->rollBack();

         return $arrayRetorno;
      }
      
      $transaction->commit();
      
      foreach ($sigacLogs as $sl)
      {
         $sl->save(
                     $sl->acao,
                     $sl->entidade,
                     $sl->entidade_id,
                     $sl->data,
                     $sl->habilitado,
                     $sl->Usuario_id,
                     $sl->observacao,
                     $sl->ip,
                     $sl->cookie_id,
                     $sl->session_id
                 );
      }
      
      return $arrayRetorno;
      
      
   }
   
   public function gerarLotes($borderos)
   {
      
      $nucleoLotes = [] ;
      $sigacLogs   = [] ;

      $arrayRetorno = array(
          'hasErrors'   => 0                                                  ,
          'msg'         => 'Borderôs processados com Sucesso. Lotes gerados.' ,
          'pntfyClass'  => 'success'
      );
      
      $transaction = Yii::app()->db->beginTransaction();
      
      foreach ($borderos as $b)
      {
         
         if($b == -1)
         {
            continue;
         }
         
         $bordero = Bordero::model()->find("habilitado AND id = $b AND Status = 1");
         
         if($bordero !== null)
         {
            $loteExistente = null;
                
                $lHbExistente = LoteHasBordero::model()->find("habilitado AND Bordero_id = $bordero->id");
                
                if($lHbExistente !== null)
                {
                    $loteExistente = LotePagamento::model()->find("habilitado AND id = $lHbExistente->id");
                }
                
                if($loteExistente !== null)
                {
                    
                    $arrayRetorno['hasErrors'  ]  = 1                                                               ;
                    $arrayRetorno['msg'        ]  = 'Borderô encontra-se em outro Lote. Id: ' . $loteExistente->id  ;
                    $arrayRetorno['pntfyClass' ]  = 'error'                                                         ;

                    $transaction->rollBack();

                    return $arrayRetorno;
                    
                }
                else
                {
                    
                    if($lHbExistente !== null)
                    {
                        $lHbExistente->habilitado = 0;
                        
                        if($lHbExistente->save())
                        {

                            $sigacLog               = new SigacLog                                                                          ;
                            $sigacLog->acao         = 'Desabilitando LoteHasBordero, pois não existe Lote para ele.'                        ;
                            $sigacLog->entidade     = 'LoteHasBordero'                                                                      ;
                            $sigacLog->entidade_id  = $lHbExistente->id                                                                     ;
                            $sigacLog->data         = date('Y-m-d H:i:s')                                                                   ;
                            $sigacLog->habilitado   = 1                                                                                     ;
                            $sigacLog->observacao   = "Usuário " . Yii::app()->session['usuario']->username . " desabilitou o registro."    ;
                            $sigacLog->cookie_id    = null                                                                                  ;
                            $sigacLogs[]            = $sigacLog                                                                             ;
                    
                        }
                        else
                        {
                            
                            ob_start();
                            var_dump($lHbExistente->getErrors());
                            $resultado = ob_get_clean();
                            
                            $arrayRetorno['hasErrors'  ]  = 1                                                                                           ;
                            $arrayRetorno['msg'        ]  = "Não foi possível desabilitar o loteHasBordero de ID: $lHbExistente->id. Erro: $resultado"  ;
                            $arrayRetorno['pntfyClass' ]  = 'error'                                                                                     ;

                            $transaction->rollBack();

                            return $arrayRetorno;
                       
                        }
                        
                    }
                    
                    $filial  =  Filial::model()->find('habilitado AND id = ' . $bordero->destinatario);

                    if($filial == null)
                    {
                       $arrayRetorno['hasErrors'  ]  = 1                                                      ;
                       $arrayRetorno['msg'        ]  = 'Filial não encontrada. Id: ' . $bordero->destinatario ;
                       $arrayRetorno['pntfyClass' ]  = 'error'                                                ;

                       $transaction->rollBack();

                       return $arrayRetorno;
                    }
                    elseif ($filial->NucleoFiliais_id == null)
                    {
                       $arrayRetorno['hasErrors'  ]  = 1                                                               ;
                       $arrayRetorno['msg'        ]  = 'Filial não tem Núcleo definido. Id: ' . $bordero->destinatario ;
                       $arrayRetorno['pntfyClass' ]  = 'error'                                                         ;

                       $transaction->rollBack();

                       return $arrayRetorno;

                    }
                
                }
            
         }
         else
         {
               
            $transaction->rollBack();

            $arrayRetorno['hasErrors'  ]  = 1                                    ;
            $arrayRetorno['msg'        ] = 'Borderô não encontrado. Id: ' . $b   ;
            $arrayRetorno['pntfyClass' ] = 'error'                               ;
      
            return $arrayRetorno;
               
         }
         
         $posicaoNucleoLote = LotePagamento::model()->getPosicaoNucleo($filial->NucleoFiliais_id,$nucleoLotes);
         
         if($posicaoNucleoLote < 0)
         {
            
            if($filial->NucleoFiliais_id == null or $filial->NucleoFiliais_id == 0)
            {

               $arrayRetorno['hasErrors'  ]  =  1                                                                 ;
               $arrayRetorno['msg'        ]  =  'Núcleo da filial não encontrado. Filial: ' . $filial->id . ' ' . 
                                                $filial->getConcat()                                              ;
               $arrayRetorno['pntfyClass' ]  =  'error'                                                           ;
               
               $transaction->rollBack();
      
               return $arrayRetorno;
            }
            
            $lote = new LotePagamento;
            
            $lote->habilitado       = 1                                    ;
            $lote->data_cadastro    = date('Y-m-d H:i:s')                  ;
            $lote->NucleoFiliais_id = $filial->NucleoFiliais_id            ;
            $lote->Usuario_id       = Yii::app()->session["usuario"]->id   ;
            
            if($lote->save())
            {
            
               $loteHasStatus = new LotePagamentoHasStatusLotePagamento;

               $loteHasStatus->data_cadastro          = date('Y-m-d H:i:s')                  ;
               $loteHasStatus->habilitado             = 1                                    ;
               $loteHasStatus->LotePagamento_id       = $lote->id                            ;
               $loteHasStatus->Usuario_id             = Yii::app()->session["usuario"]->id   ;
               $loteHasStatus->StatusLotePagamento_id = 1                                    ;

               if(!$loteHasStatus->save())
               {

                  ob_start();
                  var_dump($loteHasStatus->getErrors());
                  $result = ob_get_clean();

                  $arrayRetorno['hasErrors'  ]  = 1                                             ;
                  $arrayRetorno['msg'        ]  = 'Ocorreu um erro. Tente novamente.' . $result ;
                  $arrayRetorno['pntfyClass' ]  = 'error'                                       ;

                  $transaction->rollBack();

                  return $arrayRetorno;

               }
               
               $sigacLog               = new SigacLog                                                             ;
               $sigacLog->acao         = 'Criação de Lote de Pagamento'                                           ;
               $sigacLog->entidade     = 'Lote de Pagamentos'                                                     ;
               $sigacLog->entidade_id  = $lote->id                                                                ;
               $sigacLog->data         = date('Y-m-d H:i:s')                                                      ;
               $sigacLog->habilitado   = 1                                                                        ;
               $sigacLog->observacao   = "Usuário " . Yii::app()->session['usuario']->username . " criou o lote." ;
               $sigacLog->cookie_id    = null                                                                     ;
               $sigacLogs[]            = $sigacLog                                                                ;
            
               $nucleoLotes[] = ['nucleo' => $lote->NucleoFiliais_id, 'lote' => $lote->id];
               
            }
            else
            {

               ob_start();
               var_dump($lote->getErrors());
               $result = ob_get_clean();

               $arrayRetorno['hasErrors'  ] = 1                                              ;
               $arrayRetorno['msg'        ] = 'Ocorreu um erro. Tente novamente.' . $result  ;
               $arrayRetorno['pntfyClass' ] = 'error'                                        ;
               
               $transaction->rollBack();
      
               return $arrayRetorno;
         
            }
            
         }
         else
         {
            $lote = LotePagamento::model()->find('habilitado AND id = '. $nucleoLotes[$posicaoNucleoLote]['lote']);
         }
         
         $loteHasBordero = new LoteHasBordero;
         
         $loteHasBordero->Bordero_id   = $b        ;
         $loteHasBordero->Lote_id      = $lote->id ;
         $loteHasBordero->habilitado = 1         ;
         
         if($loteHasBordero->save())
         {
            $bordero->Status = 4;
            
            if($bordero->save())
            {
               
               $obs = "Usuário " . Yii::app()->session['usuario']->username . " alterou o status do borderô de"
                       . "1 para 4. Na gravação do Lote de id: " . $lote->id;
               
               $sigacLog               = new SigacLog                      ;
               $sigacLog->acao         = 'Alteração de Status no Borderô'  ;
               $sigacLog->entidade     = 'Borderô'                         ;
               $sigacLog->entidade_id  = $bordero->id                      ;
               $sigacLog->data         = date('Y-m-d H:i:s')               ;
               $sigacLog->habilitado   = 1                                 ;
               $sigacLog->observacao   = $obs                              ;
               $sigacLog->cookie_id    = null                              ;
               $sigacLogs[]            = $sigacLog                         ;
            }
            
         }
         else
         {

            ob_start();
            var_dump($loteHasBordero->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'  ]  = 1                                             ;
            $arrayRetorno['msg'        ] = 'Ocorreu um erro. Tente novamente.' . $result  ;
            $arrayRetorno['pntfyClass' ] = 'error'                                        ;
               
            $transaction->rollBack();
      
            return $arrayRetorno;

         }
         
      }
      
      $transaction->commit();
      
      foreach ($sigacLogs as $sl)
      {
         $sl->save(
                     $sl->acao,
                     $sl->entidade,
                     $sl->entidade_id,
                     $sl->data,
                     $sl->habilitado,
                     $sl->Usuario_id,
                     $sl->observacao,
                     $sl->ip,
                     $sl->cookie_id,
                     $sl->session_id
                 );
      }
      
      return $arrayRetorno;
      
   }
   
   public function getPosicaoNucleo($idNucleo,$arrayNucleoLote)
   {
      
      for ($i = 0;$i<sizeof($arrayNucleoLote);$i++)
      {
         
         if($arrayNucleoLote[$i]['nucleo'] == $idNucleo)
         {
            return $i;
         }
      }
      
      return -1;
      
   }
   
   public function pagar($id, $contaDebito, $valorPago, $comprovante, $dataPagamento, $contaCredito, $observacao)
   {

      $arrayRetorno = array(
          'hasErrors'   => 0                                                     ,
          'msg'         => 'Lotes baixados com sucesso. Pagamentos efetuados.'   ,
          'pntfyClass'  => 'success'
      );
      
      $transaction = Yii::app()->db->beginTransaction();
      
      $lote = LotePagamento::model()->find('habilitado AND id = ' . $id);
      
      $dadosBancariosDebito   = DadosBancarios::model()->find('habilitado AND id = ' . $contaDebito   )  ;
      $dadosBancariosCredito  = DadosBancarios::model()->find('habilitado AND id = ' . $contaCredito  )  ;
      
      if($dadosBancariosDebito == null)
      {

         $arrayRetorno = array(
                                 'hasErrors'   => 1                                                   ,
                                 'msg'         => 'Conta Débito não encontrada. Id: ' . $contaDebito  ,
                                 'pntfyClass'  => 'error'
                              );

         $transaction->rollBack();

         return $arrayRetorno;
      }
      
      if($dadosBancariosCredito == null)
      {

         $arrayRetorno = array(
                                 'hasErrors'   => 1                                                      ,
                                 'msg'         => 'Conta Crédtio não encontrada. Id: ' . $contaCredito   ,
                                 'pntfyClass'  => 'error'
                              );

         $transaction->rollBack();

         return $arrayRetorno;
      }
      
      if ($lote !== null)
      {
         
         $loteHasBordero = LoteHasBordero::model()->findAll('habilitado AND Lote_id = ' . $id);
         
         foreach ($loteHasBordero as $lhb)
         {
            
            $bordero = Bordero::model()->find('habilitado AND id = ' . $lhb->Bordero_id);
            
            if($bordero == null)
            {

               $arrayRetorno = array(
                                       'hasErrors'   => 1                                                ,
                                       'msg'         => 'Borderô não encontrado. Id: ' . $lhb->Bordero_id,
                                       'pntfyClass'  => 'error'
                                    );
               
               $transaction->rollBack();
               
               return $arrayRetorno;
               
            }
            
            $bordero->Status = 4;
            
            if(!$bordero->update())
            {//parei aqui

               ob_start();
               var_dump($bordero->getErrors());
               $result = ob_get_clean();

               $arrayRetorno = array(
                                       'hasErrors'   => 1                                             ,
                                       'msg'         => 'Ocorreu um erro. Tente novamente.' . $result ,
                                       'pntfyClass'  => 'error'
                                    );
               
               $transaction->rollBack();
               
               return $arrayRetorno;
               
            }
            
            $itensBordero = ItemDoBordero::model()->findAll('habilitado AND Bordero = ' . $bordero->id);
               
            ob_start();
            var_dump($itensBordero);
            $result = ob_get_clean();

            /*$file = fopen('itensBordero', 'w');
            fwrite($file, $result);
            fclose($file);*/
            
            foreach ($itensBordero as $ib)
            {
               
                $titulo   =  Titulo::model()->find('habilitado AND Proposta_id  = ' . $ib->Proposta_id . ' AND NaturezaTitulo_id = 2');
               
                if($titulo == null)
                {

                    $arrayRetorno = array(
                                            'hasErrors'   => 1                                                      ,
                                            'msg'         => "Título da Proposta $ib->Proposta_id não encontrado."  ,
                                            'pntfyClass'  => 'error'
                                         );

                    $transaction->rollBack();
                    
                    return $arrayRetorno;
            
                    
                }
                else
                {
               
                    $parcelas = Parcela::model()->findAll('habilitado AND Titulo_id    = ' . $titulo->id                               );

                    ob_start();
                    var_dump($parcelas);
                    $result = ob_get_clean();

                     /*$file = fopen('parcelas', 'w');
                    fwrite($file, $result);
                    fclose($file);*/

                    foreach ($parcelas as $p)
                    {

                       $baixa = new Baixa;

                       $baixa->Filial_id          = $titulo->proposta->analiseDeCredito->Filial_id   ;
                       $baixa->Parcela_id         = $p->id                                           ;
                       $baixa->data_da_baixa      = date('Y-m-d H:i:s')                              ;
                       $baixa->valor_pago         = $p->valor                                        ;
                       $baixa->valor_abatimento   = $p->valor                                        ;
                       $baixa->baixado            = 1                                                ;

                       if(!$baixa->save())
                       {

                          ob_start();
                          var_dump($baixa->getErrors());
                          $result = ob_get_clean();

                          $transaction->rollBack();

                          $arrayRetorno = array(
                                                  'hasErrors'   => 1                                             ,
                                                  'msg'         => 'Ocorreu um erro. Tente novamente.' . $result ,
                                                  'pntfyClass'  => 'error'
                                               );

                          return $arrayRetorno;

                       }
                       else
                       {
                         //$titulo->proposta->gerarRegistroRetornoParceiro('05', TRUE);
                       }

                    }
                
                }

            }
            
        }
         
        $dadosPagamento                             = new DadosPagamento            ;
        $dadosPagamento->Dados_Bancarios_Credito    = $dadosBancariosCredito->id    ;
        $dadosPagamento->Dados_Bancarios_Debito     = $dadosBancariosDebito->id     ;
        $dadosPagamento->comprovante                = $comprovante                  ;
        $dadosPagamento->dataCompensacao            = $dataPagamento                ;
        $dadosPagamento->valorPago                  = $valorPago                    ;
        $dadosPagamento->data_cadastro              = date('Y-m-d H:i:s')           ;
        $dadosPagamento->habilitado                 = 1                             ;
        $dadosPagamento->observacao                 = $observacao                   ;
         
         if(!$dadosPagamento->save())
         {

            ob_start();
            var_dump($dadosPagamento->getErrors());
            $result = ob_get_clean();

            $arrayRetorno = array(
                                    'hasErrors'   => 1                                             ,
                                    'msg'         => 'Ocorreu um erro. Tente novamente.' . $result ,
                                    'pntfyClass'  => 'error'
                                 );
            
            $transaction->rollBack();

            return $arrayRetorno;
            
         }
         else
         {
            $lote->DadosPagamento_id  = $dadosPagamento->id;

            $arrayRetorno   = array(
              'hasErrors'   => 0                                                     ,
              'msg'         => 'Lotes baixados com sucesso. Pagamentos efetuados.'   ,
              'pntfyClass'  => 'success',
              'DPid'        => $lote->DadosPagamento_id
            );
            
            if(!$lote->update())
            {

               ob_start();
               var_dump($lote->getErrors());
               $result = ob_get_clean();

               $arrayRetorno = array(
                                       'hasErrors'   => 1                                             ,
                                       'msg'         => 'Ocorreu um erro. Tente novamente.' . $result ,
                                       'pntfyClass'  => 'error'
                                    );

               $transaction->rollBack();

               return $arrayRetorno;

            }
            
         }
         
         $loteHasStatus                         = new LotePagamentoHasStatusLotePagamento ;
         $loteHasStatus->LotePagamento_id       = $lote->id                               ;
         $loteHasStatus->StatusLotePagamento_id = 2                                       ;
         $loteHasStatus->Usuario_id             = Yii::app()->session['usuario']->id      ;
         $loteHasStatus->data_cadastro          = date('Y-m-d H:i:s')                     ;
         $loteHasStatus->habilitado             = 1                                       ;
         
         if(!$loteHasStatus->save())
         {

            ob_start();
            var_dump($loteHasStatus->getErrors());
            $result = ob_get_clean();

            $arrayRetorno = array(
                                    'hasErrors'   => 1                                             ,
                                    'msg'         => 'Ocorreu um erro. Tente novamente.' . $result ,
                                    'pntfyClass'  => 'error'
                                 );
            
            $transaction->rollBack();

            return $arrayRetorno;
            
         }
         else
         {
               
            $sigacLog               = new SigacLog                                                             ;
            $sigacLog->acao         = 'Baixa de Lote de Pagamento'                                           ;
            $sigacLog->entidade     = 'Lote de Pagamentos'                                                     ;
            $sigacLog->entidade_id  = $lote->id                                                                ;
            $sigacLog->data         = date('Y-m-d H:i:s')                                                      ;
            $sigacLog->habilitado   = 1                                                                        ;
            $sigacLog->observacao   = "Usuário " . Yii::app()->session['usuario']->username . " baixou o lote." ;
            $sigacLog->cookie_id    = null                                                                     ;
            $sigacLog->save(
                              $sigacLog->acao,
                              $sigacLog->entidade,
                              $sigacLog->entidade_id,
                              $sigacLog->data,
                              $sigacLog->habilitado,
                              $sigacLog->Usuario_id,
                              $sigacLog->observacao,
                              $sigacLog->ip,
                              $sigacLog->cookie_id,
                              $sigacLog->session_id
                           );                                                               ;
            
         }
         
      }
      else
      {
         $transaction->rollBack();

         $arrayRetorno = array(
                                 'hasErrors'   => 1                                      ,
                                 'msg'         => 'Lote não encontrado ou desabilitado.' ,
                                 'pntfyClass'  => 'error'
                              );
         
         return $arrayRetorno;
         
      }
      
      $transaction->commit();
   
      return $arrayRetorno;
      
   }
   
   public function getDadosBancarios($idLote)
   {
      
      $lote = LotePagamento::model()->find('habilitado AND id = ' . $idLote);
      
      $options = array  (
                           'empresa'   => '<option value="0">Selecione uma Conta</option>',
                           'parceiro'  => '<option value="0">Selecione uma Conta</option>',
                           'dataBaixa' => date('Y-m-d')
                        );
      
      if($lote !== null)
      {
         
         $empresaDB = Empresa::model()->getDadosBancarios();
         
         foreach ($empresaDB as $edb)
         {
            $descricao  = 'Banco: ' . $edb['codBanco'] . '-' . $edb['nomeBanco'] . ' ';
            $descricao .= 'Ag.: '   . $edb['agencia' ]                           . ' ';
            $descricao .= 'CC: '    . $edb['numero'  ]                           ; 
            /*DB.agencia					, DB.numero				, DB.operacao*/
            
            $options['empresa'] .= '<option value="' . $edb['id'] . '">' . $descricao . '</option>';
            
         }
         
         $nucleoHasDB = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $lote->NucleoFiliais_id);
         
         if($nucleoHasDB !== null)
         {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDB->Dados_Bancarios_id);
            
            if(
                  (!$dadosBancarios          !== null && $dadosBancarios->Banco_id !== null  ) &&
                  (!$dadosBancarios->agencia !== null && !empty($dadosBancarios->agencia)    ) &&
                  (!$dadosBancarios->numero  !== null && !empty($dadosBancarios->numero)     )
               )
            {
               
               $descricao  = 'Banco: ' . $dadosBancarios->banco->codigo . '-' . $dadosBancarios->banco->nome . ' ' ;
               $descricao .= 'Ag.: '   . $dadosBancarios->agencia                                            . ' ' ;
               $descricao .= 'CC: '    . $dadosBancarios->numero                                                   ; 
               
               $options['parceiro'] .= '<option value="' . $dadosBancarios->id . '">' . $descricao . '</option>';
            }
            
         }
                 
      }
      
      return $options;
      
      
   }
   
   public function getDadosBancariosPagos($idLote) {

        $lote = LotePagamento::model()->find('habilitado AND id = ' . $idLote);
        $utl = '';
        $retorno = array('dados' => [], 'susseco' => true);

        $dadosPagamento = DadosPagamento::model()->findByPk($lote->DadosPagamento_id);

        if ($dadosPagamento !== null) {
            
            $anexo = Anexo::model()->find("habilitado AND entidade_relacionamento = 'DadosPagamento' AND id_entidade_relacionamento = $dadosPagamento->id");
            
            if($anexo == null)
            {
                $linkComprovante = "";
            }
            else
            {
              if( $anexo->url != null ){
                $linkComprovante = $anexo->url;
              }
              else{
                $linkComprovante = $anexo->relative_path;
              }
            }

            $retorno['dados']['empresa'] = 'Banco: ' . $dadosPagamento->dadosBancariosDebito->banco->codigo . '-' .
                    $dadosPagamento->dadosBancariosDebito->banco->nome . ' ';
            $retorno['dados']['empresa'] .= 'Ag.: ' . $dadosPagamento->dadosBancariosDebito->agencia . ' ';
            $retorno['dados']['empresa'] .= 'CC: ' . $dadosPagamento->dadosBancariosDebito->numero;

            $retorno['dados']['parceiro'] = 'Banco: ' . $dadosPagamento->dadosBancariosCredito->banco->codigo . '-' .
                    $dadosPagamento->dadosBancariosCredito->banco->nome . ' ';
            $retorno['dados']['parceiro'] .= 'Ag.: ' . $dadosPagamento->dadosBancariosCredito->agencia . ' ';
            $retorno['dados']['parceiro'] .= 'CC: ' . $dadosPagamento->dadosBancariosCredito->numero;

            $dateTime = new DateTime($dadosPagamento->dataCompensacao);
            $dataCompensacao = $dateTime->format('d/m/Y H:i:s');

            $retorno['dados']['comprovante'] = $dadosPagamento->comprovante;
            $retorno['dados']['linkComprovante'] = $linkComprovante;
            $retorno['dados']['dataPagamento'] = $dataCompensacao;
            $retorno['dados']['valorPago'] = 'R$ ' . number_format($dadosPagamento->valorPago, 2, ',', '.');
        }

        return $retorno;
   }

   public function getValor()
   {
      $valorTotal     = 0;

      foreach( $this->loteHasBorderos as $lhb )
      {
        foreach( $lhb->bordero->itemDoBorderos as $item )
        {
          $valorTotal += $item->proposta->valorRepasse();
        }
      }
      
      return $valorTotal;
   }


   public function getBorderos($draw, $start, $length, $loteId, $ambiente)
   {
      $data = [];

      $criteria = new CDbCriteria;
      $criteria->addInCondition('t.Lote_id', [$loteId], ' AND ');
      $criteria->addInCondition('t.habilitado', [1], ' AND ');

      $totalRegistros         = count( LoteHasBordero::model()->findAll( $criteria ) );

      $criteria->offset       = $start;
      $criteria->limit        = $length;

      foreach( LoteHasBordero::model()->findAll( $criteria ) as $lhb )
      {
        $data[]               = [
          'btn-more'          => '<button style="padding:3px 8px!important;" type="button" class="btopen btn-xs btn-success btn-rounded"><i class="fa fa-plus"></i></button>',
          'codigo'            => str_pad($lhb->bordero->id, 10, '0', STR_PAD_LEFT),
          'valor'             => "R$ ".number_format($lhb->bordero->getValor(), 2, ',', '.'),
          'borderoId'         => $lhb->bordero->id,
          'filial'            => mb_strtoupper($lhb->bordero->destinatarioObj->getConcat()),
          'ambiente'          => $ambiente,
        ];  
      }

      return [
          "draw"              => $draw,
          "recordsTotal"      => $length,
          "recordsFiltered"   => $totalRegistros,
          "data"              => $data,
      ];
   }
}
