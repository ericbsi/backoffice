<?php

/**
 * This is the model class for table "RecebimentoDeDocumentacao".
 *
 * The followings are the available columns in table 'RecebimentoDeDocumentacao':
 * @property integer $id
 * @property string $codigo
 * @property integer $Bordero
 * @property integer $RegistroDePendencias
 * @property string $dataCriacao
 * @property integer $criadoPor
 * @property integer $destinatario
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Bordero $bordero
 * @property RegistroDePendencias $registroDePendencias
 * @property Usuario $criadoPor0
 * @property Filial $destinatario0
 */
class RecebimentoDeDocumentacao extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RecebimentoDeDocumentacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RecebimentoDeDocumentacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, dataCriacao, criadoPor, destinatario', 'required'),
			array('Bordero, RegistroDePendencias, criadoPor, destinatario, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, codigo, Bordero, RegistroDePendencias, dataCriacao, criadoPor, destinatario, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bordero' => array(self::BELONGS_TO, 'Bordero', 'Bordero'),
			'registroDePendencias' => array(self::BELONGS_TO, 'RegistroDePendencias', 'RegistroDePendencias'),
			'criadoPor0' => array(self::BELONGS_TO, 'Usuario', 'criadoPor'),
			'destinatarioObj' => array(self::BELONGS_TO, 'Filial', 'destinatario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'Bordero' => 'Bordero',
			'RegistroDePendencias' => 'Registro De Pendencias',
			'dataCriacao' => 'Data Criacao',
			'criadoPor' => 'Criado Por',
			'destinatario' => 'Destinatario',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('Bordero',$this->Bordero);
		$criteria->compare('RegistroDePendencias',$this->RegistroDePendencias);
		$criteria->compare('dataCriacao',$this->dataCriacao,true);
		$criteria->compare('criadoPor',$this->criadoPor);
		$criteria->compare('destinatario',$this->destinatario);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}