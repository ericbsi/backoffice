<?php

/**
 * This is the model class for table "Role_has_Funcao".
 *
 * The followings are the available columns in table 'Role_has_Funcao':
 * @property integer $id
 * @property integer $Role_id
 * @property integer $Funcao_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property NivelRoleHasRoleHasFuncao[] $nivelRoleHasRoleHasFuncaos
 * @property Funcao $funcao
 * @property Role $role
 */
class RoleHasFuncao extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RoleHasFuncao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Role_has_Funcao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Role_id, Funcao_id', 'required'),
			array('Role_id, Funcao_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Role_id, Funcao_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nivelRoleHasRoleHasFuncaos' => array(self::HAS_MANY, 'NivelRoleHasRoleHasFuncao', 'Role_has_Funcao_id'),
			'funcao' => array(self::BELONGS_TO, 'Funcao', 'Funcao_id'),
			'role' => array(self::BELONGS_TO, 'Role', 'Role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Role_id' => 'Role',
			'Funcao_id' => 'Funcao',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($limit)
    {
        $criteria 				= new CDbCriteria;

        /* Campos texto, usando LIKE */
        $criteria->addInCondition('t.Role_id',[$this->Role_id], ' AND ');


        $criteria->offset 		= $limit['start'];
        $criteria->limit 		= $limit['limit'];

        return RoleHasFuncao::model()->findAll($criteria);
    }


	public function dataTable($draw, $limit, $idPerfil = '0')
    {
        $fun                    = new Funcao;
        $funcoesCollection      = [];
        $recordsTotal           = count(Funcao::model()->findAll());

        $i                      = 1;
        $hab                    = 0;
        
        foreach ($fun->search($limit) as $funcao)
        {   
            $checked             = "";

            if( $idPerfil !== '0' )
            {
                $nivelRHasFuncao = RoleHasFuncao::model()->find('t.Funcao_id = ' . $funcao->id . ' AND t.Role_id = ' . $idPerfil . ' AND t.habilitado');

                if( $nivelRHasFuncao !== null )
                {

                    $checked    = "checked";
                    $hab        = 0;
                }
                else
                {
                    $checked    = "";
                    $hab        = 1;
                }
            }

            $funcoesCollection[] =  [
                'checkFilial'   => '<div class="checkbox checkbox-primary"><input data-hab="'.$hab.'" data-funcao-id="'.$funcao->id.'" data-perfil-id="'.$idPerfil.'" class="input_check" '.$checked.' id="checkbox'.$i.'" type="checkbox"><label for="checkbox'.$i.'"></label></div>',
                'label'         => mb_strtoupper($funcao->label),
                'descricao'     => mb_strtoupper($funcao->descricao),
                'btn_editar'    => '<a data-form-wrapper="#portlet-form" data-source="/funcao/loadFormEdit/" data-entity-id="' . $funcao->id . '" data-form-load="#form-funcao" data-collapse-div="#bg-default" class="btn btn-credshow-orange waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-edit"></i></a>',
            ];

            $i++;
        }

        return ['collection'    =>  $funcoesCollection, 'total'  => $recordsTotal];
    }

    public function roleFuncao( $role_id, $funcao_id, $hab )
    {
		$rf 						= RoleHasFuncao::model()->find('Funcao_id = ' . $funcao_id . ' AND Role_id = ' . $role_id);

		if( $rf 					=== NULL )
		{
			$rf 					= new RoleHasFuncao;
			$rf->Role_id		= $role_id;
			$rf->Funcao_id 			= $funcao_id;
			$rf->save();
		}
		else
		{
			$rf->habilitado 		= $hab;
			$rf->update();
		}
    }
}