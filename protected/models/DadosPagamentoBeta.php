<?php

class DadosPagamentoBeta extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->beta;
	}
	public function tableName()
	{
		return 'DadosPagamento';
	}

	public function rules()
	{
		return array(
			array('Dados_Bancarios_Debito, Dados_Bancarios_Credito, data_cadastro', 'required'),
			array('Dados_Bancarios_Debito, Dados_Bancarios_Credito, habilitado', 'numerical', 'integerOnly'=>true),
			array('valorPago', 'numerical'),
			array('comprovante', 'length', 'max'=>200),
			array('dataCompensacao, observacao', 'safe'),
			array('id, Dados_Bancarios_Debito, Dados_Bancarios_Credito, dataCompensacao, valorPago, comprovante, habilitado, data_cadastro, observacao', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'baixas' 					=> array(self::HAS_MANY, 	'BaixaBeta',			'DadosPagamento_id'			),
			'dadosBancariosDebito' 		=> array(self::BELONGS_TO, 	'DadosBancariosBeta', 	'Dados_Bancarios_Debito'	),
			'dadosBancariosCredito' 	=> array(self::BELONGS_TO, 	'DadosBancariosBeta', 	'Dados_Bancarios_Credito'	),
			'lotePagamentos' 			=> array(self::HAS_MANY, 	'LotePagamentoBeta', 	'DadosPagamento_id'			),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Dados_Bancarios_Debito' => 'Dados Bancarios Debito',
			'Dados_Bancarios_Credito' => 'Dados Bancarios Credito',
			'dataCompensacao' => 'Data Compensacao',
			'valorPago' => 'Valor Pago',
			'comprovante' => 'Comprovante',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Dados_Bancarios_Debito',$this->Dados_Bancarios_Debito);
		$criteria->compare('Dados_Bancarios_Credito',$this->Dados_Bancarios_Credito);
		$criteria->compare('dataCompensacao',$this->dataCompensacao,true);
		$criteria->compare('valorPago',$this->valorPago);
		$criteria->compare('comprovante',$this->comprovante,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}