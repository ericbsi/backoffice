<?php

/**
 * This is the model class for table "PreLote".
 *
 * The followings are the available columns in table 'PreLote':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $Usuario_id
 * @property integer $NucleoFiliais_id
 * @property integer $StatusPreLote_id
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 * @property NucleoFiliais $nucleoFiliais
 * @property StatusPreLote $statusPreLote
 * @property Bordero[] $borderos
 */
class PreLote extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PreLote';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, habilitado, Usuario_id, NucleoFiliais_id, StatusPreLote_id', 'required'),
			array('habilitado, Usuario_id, NucleoFiliais_id, StatusPreLote_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, Usuario_id, NucleoFiliais_id, StatusPreLote_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'nucleoFiliais' => array(self::BELONGS_TO, 'NucleoFiliais', 'NucleoFiliais_id'),
			'statusPreLote' => array(self::BELONGS_TO, 'StatusPreLote', 'StatusPreLote_id'),
			'borderos' => array(self::MANY_MANY, 'Bordero', 'PreLote_has_Bordero(PreLote_id, Bordero_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'Usuario_id' => 'Usuario',
			'NucleoFiliais_id' => 'Nucleo Filiais',
			'StatusPreLote_id' => 'Status Pre Lote',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);
		$criteria->compare('StatusPreLote_id',$this->StatusPreLote_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PreLote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
