<?php

class NivelRoleHasFuncao extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NivelRoleHasFuncao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Nivel_Role_has_Funcao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Funcao_id, Nivel_Role_id', 'required'),
			array('Funcao_id, Nivel_Role_id, habilitado, show_menu', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Funcao_id, Nivel_Role_id, habilitado, show_menu', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'funcao'			 				=> array(self::BELONGS_TO, 	'Funcao',	 						'Funcao_id', 'on' => 'funcao.Status_Funcao_id = 1 AND funcao.habilitado'),
			'nivelRole' 						=> array(self::BELONGS_TO, 	'NivelRole', 						'Nivel_Role_id'),
			'nivelRoleHasFuncaoHasStyleConfigs' => array(self::HAS_MANY, 	'NivelRoleHasFuncaoHasStyleConfig', 'Nivel_Role_has_Funcao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Funcao_id' => 'Funcao',
			'Nivel_Role_id' => 'Nivel Role',
			'habilitado' => 'Habilitado',
			'show_menu' => 'Show Menu',
		);
	}

	/**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model NivelRoleHasFuncao]
     * @author Eric
     * @version 1.0
    */
    public function search($limit 	= array( 'start' => 0, 'limit' => 1 ))
    {
        $criteria 					= new CDbCriteria;
       
        $criteria->addInCondition('Funcao_id', 		$this->Funcao_id ,	  'AND'	);
        $criteria->addInCondition('Nivel_Role_id', 	$this->Nivel_Role_id, 'AND'	);
       
        $criteria->compare('habilitado', 			$this->habilitado 			);
        $criteria->offset 			= $limit['start' 	 								];
        $criteria->limit 			= $limit['limit' 									];

        return NivelRoleHasFuncao::model()->findAll($criteria);
    }

    public function nivelFuncao( $nivel_id, $funcao_id, $hab )
    {
		$anf 						= NivelRoleHasFuncao::model()->find('Funcao_id = ' . $funcao_id . ' AND Nivel_Role_id = ' . $nivel_id);

		if( $anf 					=== NULL )
		{
			$anf 					= new NivelRoleHasFuncao;
			$anf->Nivel_Role_id		= $nivel_id;
			$anf->Funcao_id 		= $funcao_id;
			$anf->save();
		}
		else
		{
			$anf->habilitado 		= $hab;
			$anf->update();
		}
    }
}