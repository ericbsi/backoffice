<?php

class Fator extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fator';
	}



	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('carencia, parcela, habilitado, fator, Tabela_Cotacao_id', 'required'),
			array('carencia, parcela, habilitado, Tabela_Cotacao_id', 'numerical', 'integerOnly'=>true),
			array('fator, porcentagem_retencao', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, carencia, parcela, habilitado, fator, porcentagem_retencao, Tabela_Cotacao_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tabelaCotacao' => array(self::BELONGS_TO, 'TabelaCotacao', 'Tabela_Cotacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'carencia' => 'Carencia',
			'parcela' => 'Parcela',
			'habilitado' => 'Habilitado',
			'fator' => 'Fator',
			'porcentagem_retencao' => 'Porcentagem Retencao',
			'Tabela_Cotacao_id' => 'Tabela Cotacao',
		);
	}

	public function getValorRetido($valor)
    {
        return ($valor / 100) * $this->porcentagem_retencao;
    }
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('carencia',$this->carencia);
		$criteria->compare('parcela',$this->parcela);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('fator',$this->fator);
		$criteria->compare('porcentagem_retencao',$this->porcentagem_retencao);
		$criteria->compare('Tabela_Cotacao_id',$this->Tabela_Cotacao_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}