<?php

/**
 * This is the model class for table "ArquivoCNAB".
 *
 * The followings are the available columns in table 'ArquivoCNAB':
 * @property integer $id
 * @property integer $Usuario_id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property string $urlArquivo
 * @property integer $TipoCNAB_id
 * @property integer $Bordero_id
 *
 * The followings are the available model relations:
 * @property Bordero $bordero
 * @property TipoCNAB $tipoCNAB
 * @property Usuario $usuario
 * @property Bordero[] $borderos
 * @property LinhaCNAB[] $linhaCNABs
 */
class ArquivoCNAB extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ArquivoCNAB';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, data_cadastro, urlArquivo, TipoCNAB_id', 'required'),
			array('Usuario_id, habilitado, TipoCNAB_id, Bordero_id', 'numerical', 'integerOnly'=>true),
			array('urlArquivo', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Usuario_id, data_cadastro, habilitado, urlArquivo, TipoCNAB_id, Bordero_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bordero' => array(self::BELONGS_TO, 'Bordero', 'Bordero_id'),
			'tipoCNAB' => array(self::BELONGS_TO, 'TipoCNAB', 'TipoCNAB_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'borderos' => array(self::HAS_MANY, 'Bordero', 'ArquivoCNAB_id'),
			'linhaCNABs' => array(self::HAS_MANY, 'LinhaCNAB', 'ArquivoExtportado_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Usuario_id' => 'Usuario',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'urlArquivo' => 'Url Arquivo',
			'TipoCNAB_id' => 'Tipo Cnab',
			'Bordero_id' => 'Bordero',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('urlArquivo',$this->urlArquivo,true);
		$criteria->compare('TipoCNAB_id',$this->TipoCNAB_id);
		$criteria->compare('Bordero_id',$this->Bordero_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArquivoCNAB the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
