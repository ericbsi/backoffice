<?php 

/*
	{
	    "Expiration": "",
	    "ETag": "\"159286c209ac34648c58cd21ba9bfa82\"",
	    "ServerSideEncryption": "",
	    "VersionId": "",
	    "SSECustomerAlgorithm": "",
	    "SSECustomerKeyMD5": "",
	    "SSEKMSKeyId": "",
	    "RequestCharged": "",
	    "@metadata": {
	        "statusCode": 200,
	        "effectiveUri": "https:\/\/s3-sa-east-1.amazonaws.com\/uploads-nordeste-sigac\/9519176download.png",
	        "headers": {
	            "x-amz-id-2": "XgO86GShiRHz2vr61FGd8l5opoyUvWfC0HPqKR+zKGeuk4mBdIKByXDuWqONU0VswKg7+vhxceg=",
	            "x-amz-request-id": "51103A98A4D795EE",
	            "date": "Wed, 16 Nov 2016 14:20:38 GMT",
	            "etag": "\"159286c209ac34648c58cd21ba9bfa82\"",
	            "content-length": "0",
	            "server": "AmazonS3"
	        },
	        "transferStats": {
	            "http": [
	                []
	            ]
	        }
	    },
	    "ObjectURL": "https:\/\/s3-sa-east-1.amazonaws.com\/uploads-nordeste-sigac\/9519176download.png"
	}
*/

class S3Client {
	
	public function __construct(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('ext.vendor.autoload', true);
        spl_autoload_register(array('YiiBase', 'autoload'));
    }
	
	public function migrar( $objAnexo, $bucket = null, $er = null ){

		$ext 				= pathinfo($objAnexo->urlArquivo, PATHINFO_EXTENSION);
		$Er 				= 'arquivo';
		$Bucket 			= 'uploads-nordeste-sigac';

		if( $bucket != null ){
			$Bucket 		= $bucket;
		}

		if( $er != null ){
			$Er 		= $er;
		}

		$s3 				= new Aws\S3\S3Client([
	    	'version'     	=> 'latest',
	        'region'      	=> 'sa-east-1',
	        'credentials' 	=> [
	            'key'    	=> 'AKIAIUWLLGQ4EQFZCF5A',
	        	'secret' 	=> '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'
	        ]
	    ]);

		$result           	= $s3->putObject([
			'Bucket'        => $Bucket,
		    'Key'           => $objAnexo->id.$er.'.'.$ext,
		    'Body'          => fopen('/var/www/nordeste'.$objAnexo->urlArquivo, 'r'),
		    'ContentType'   => mime_content_type('/var/www/nordeste'.$objAnexo->urlArquivo),
		    'ACL'           => 'public-read'
		]);
		

	    if( $result['@metadata']['statusCode'] == '200' ){
	    	unlink('/var/www/nordeste'.$objAnexo->urlArquivo);
	    	/*$objAnexo->url 				= $result['@metadata']['effectiveUri'];
	    	$objAnexo->relative_path 	= $result['@metadata']['effectiveUri'];
	    	$objAnexo->absolute_path 	= $result['@metadata']['effectiveUri'];*/
	    	$objAnexo->urlArquivo 		= $result['@metadata']['effectiveUri'];
	    	$objAnexo->update();
	    }

	    return $result;
	}
    
	public function put( $FileInput, $entidade_relacionamento, $id_entidade_relacionamento, $descricao, $bucket = null ){

		$Bucket 			= 'uploads-nordeste-sigac';

		if( $bucket != null ){
			$Bucket 		= $bucket;
		}

    	$s3 				= new Aws\S3\S3Client([
	    	'version'     	=> 'latest',
	        'region'      	=> 'sa-east-1',
	        'credentials' 	=> [
	            'key'    	=> 'AKIAIUWLLGQ4EQFZCF5A',
	        	'secret' 	=> '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'
	        ]
	    ]);
      
	    $result           	= $s3->putObject([
	    	'Bucket'        => $Bucket,
	        'Key'           => rand(0, 9999999).$FileInput["name"],
	        'Body'          => fopen($FileInput["tmp_name"], 'r'),
	        'ContentType'   => $FileInput["type"],
	        'ACL'           => 'public-read'
	    ]);

	    $hasError 			= false;
	    $t 					= Yii::app()->db->beginTransaction();

		//Deu certo
		if( $result['@metadata']['statusCode'] == '200' ){

       		$ext 								= pathinfo($FileInput['name'], PATHINFO_EXTENSION);

			$anexo 								= new Anexo;
		    $anexo->descricao 					= $descricao;
		    $anexo->relative_path 				= $result['@metadata']['effectiveUri'];
		    $anexo->absolute_path 				= $result['@metadata']['effectiveUri'];
		    $anexo->entidade_relacionamento 	= $entidade_relacionamento;
		    $anexo->id_entidade_relacionamento 	= $id_entidade_relacionamento;
		    $anexo->data_cadastro 				= date('Y-m-d H:i:s');
		    $anexo->habilitado 					= 1;
		    $anexo->extensao 					= $ext;
		    $anexo->url 						= $result['@metadata']['effectiveUri'];

		    if( $anexo->save() ){

		       	if( $entidade_relacionamento 	== "cadastro" ){

       				$cadastro 					= Cadastro::model()->findByPk($id_entidade_relacionamento);
		       			
			       	if( $cadastro->cliente->propostasAtivas() != NULL )
		       		{
			       		/*Cria o alerta para a mensagem para disparar o alerta*/
		       			$proposta 					= $cadastro->cliente->propostasAtivas();
		       				
		       			if( $proposta != NULL ){
		       				//$proposta->alterarCondicoesOmni();
			       			$mensagem 					= strtoupper( Yii::app()->session['usuario']->nome_utilizador ) . " anexou novos arquivos ao cadastro do cliente " . strtoupper($cadastro->cliente->pessoa->nome);
				    		Mensagem::model()->nova( $mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id );
		       			}
		       		}
		       	}
		    }
		    else{
		    	$hasError 	= true;
		    }
       		
		}

		else{
			$hasError 		= true;
		}

		if( $hasError ){
			$t->rollBack();
			return[
				'msgReturn'					=> 'Não foi possível enviar o arquivo. Contate o suporte',
				'classNotify'				=> 'error'
			];
		}
		else{
			$t->commit();
			return [
				'msgReturn' 				=> 'Arquivo enviado com sucesso',
				'classNotify' 				=> 'success'
			];
		}
    }

    public function putLecca($file, $nome){
    	
    	$s3 				= new Aws\S3\S3Client([
	    	'version'     	=> 'latest',
	        'region'      	=> 'sa-east-1',
	        'credentials' 	=> [
	            'key'    	=> 'AKIAIUWLLGQ4EQFZCF5A',
	        	'secret' 	=> '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'
	        ]
	    ]);
      
	    $result           	= $s3->putObject([
	    	'Bucket'        => 'remessas-fdic',
	        'Key'           => rand(0, 9999999).$nome,
	        'Body'          => fopen($file, 'r'),
	        'ContentType'   => 'text/plain',
	        'ACL'           => 'public-read'
	    ]);

	    return $result;
    }
}