<?php

/**
 * This is the model class for table "Empresa_has_Dados_Bancarios".
 *
 * The followings are the available columns in table 'Empresa_has_Dados_Bancarios':
 * @property integer $id
 * @property integer $Empresa_id
 * @property integer $Dados_Bancarios_id
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property DadosBancarios $dadosBancarios
 * @property Empresa $empresa
 */
class EmpresaHasDadosBancariosBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmpresaHasDadosBancariosBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Empresa_has_Dados_Bancarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Empresa_id, Dados_Bancarios_id', 'required'),
			array('Empresa_id, Dados_Bancarios_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Empresa_id, Dados_Bancarios_id, habilitado, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dadosBancarios' => array(self::BELONGS_TO, 'DadosBancariosBeta', 'Dados_Bancarios_id'),
			'empresa' => array(self::BELONGS_TO, 'EmpresaBeta', 'Empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Empresa_id' => 'Empresa',
			'Dados_Bancarios_id' => 'Dados Bancarios',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Empresa_id',$this->Empresa_id);
		$criteria->compare('Dados_Bancarios_id',$this->Dados_Bancarios_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}