<?php

class Proposta extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'Proposta';
    }

    public function getDataPrimeiraParcela() {

        $carencia = $this->carencia + 1;

        return date('d/m/Y', strtotime("+" . $carencia . "days", strtotime($this->data_cadastro)));
    }

    public function getDataUltimaParcela() {

        $carencia = $this->carencia;

        $data_ultima_parcela = date('Y-m-d', strtotime($this->data_cadastro . ' +' . $carencia . ' days'));

        $data_ultima_parcela = date('d/m/Y', strtotime("+" . $this->qtd_parcelas - 1 . "months", strtotime($data_ultima_parcela)));

        return $data_ultima_parcela;
    }

    public function calcularValorDoSeguro() {
        $valorDoSeguro = 0;

        /* Possui seguro */
        if ($this->segurada) {
            /* Proposta segurada nos novos moldes */
            if (( $this->venda !== NULL ) && (count($this->venda->itemDaVendas) > 0)) {
                foreach ($this->venda->itemDaVendas as $item) {
                    /* É o seguro */
                    if (($item->itemDoEstoque->produto->id == 1) && ($item->habilitado)) {
                        return $item->preco;
                    }
                }
            }
            /* Trata-se de uma proposta com o modelo antigo de seleção de seguro */ else {
                return ( ( $this->valor - $this->valor_entrada ) / 100 ) * 5;
            }
        }

        /* Não possui seguro */ else {
            return 0;
        }
    }

    public function rules() {

        return array(
            array('codigo, Analise_de_Credito_id, valor_entrada, valor_final, Status_Proposta_id', 'required'),
            array('Analise_de_Credito_id, Financeira_id, Cotacao_id, Tabela_id, habilitado, qtd_parcelas, carencia, Status_Proposta_id, titulos_gerados, segurada, Banco_id, analise_solicitada, dialogo_salvo, ModeloContrato_id, enviando', 'numerical', 'integerOnly' => true),
            array('valor, valor_parcela, valor_entrada, valor_final', 'numerical'),
            array('codigo', 'length', 'max' => 240),
            array('data_cadastro_br', 'length', 'max' => 50),
            array('numero_do_contrato, data_cadastro, valor_texto, valor_parcela_texto', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, codigo, numero_do_contrato, Analise_de_Credito_id, Financeira_id, Cotacao_id, Tabela_id, habilitado, data_cadastro, data_cadastro_br, valor, valor_texto, qtd_parcelas, carencia, valor_parcela, valor_parcela_texto, valor_entrada, valor_final, Status_Proposta_id, titulos_gerados, segurada, Banco_id, analise_solicitada, dialogo_salvo, ModeloContrato_id, enviando', 'safe', 'on' => 'search'),
        );
    }

    public function valorRepasse() {

        $valorFinanciado = $this->valor - $this->valor_entrada + $this->calcularValorDoSeguro();

        $fator = Fator::model()->find('Tabela_Cotacao_id = ' . $this->tabelaCotacao->id . ' AND parcela = ' . $this->qtd_parcelas . ' AND carencia = ' . $this->carencia . ' AND habilitado');

        if ($fator != NULL) {
            return $valorFinanciado - $fator->getValorRetido($valorFinanciado);
        } {
            return $valorFinanciado; //mudanca inserida 11/02/2016. Retorne o valor financiado
        }
    }

    public function relations() {

        return array(
            'analiseDeCredito' => array(self::BELONGS_TO, 'AnaliseDeCredito', 'Analise_de_Credito_id'),
            'analistaHasPropostaHasStatusPropostas' => array(self::HAS_MANY, 'AnalistaHasPropostaHasStatusProposta', 'Proposta_id'),
            'apolices' => array(self::HAS_MANY, 'Apolice', 'Proposta_id'),
            'cartaoHasPropostas' => array(self::HAS_MANY, 'CartaoHasProposta', 'Proposta_id'),
            'emprestimos' => array(self::HAS_MANY, 'Emprestimo', 'Proposta_id'),
            'itemConciliacaos' => array(self::HAS_MANY, 'ItemConciliacao', 'Proposta_id'),
            'itemDoBorderos' => array(self::HAS_MANY, 'ItemDoBordero', 'Proposta_id'),
            'itemPendentes' => array(self::HAS_MANY, 'ItemPendente', 'ItemPendente'),
            'negacaoDeCreditos' => array(self::HAS_MANY, 'NegacaoDeCredito', 'Proposta'),
            'banco' => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
            'modeloContrato' => array(self::BELONGS_TO, 'ModeloContrato', 'ModeloContrato_id'),
            'tabelaCotacao' => array(self::BELONGS_TO, 'TabelaCotacao', 'Tabela_id'),
            'reanalises' => array(self::HAS_MANY, 'Reanalise', 'Proposta'),
            'solicitacaoDeCancelamentos' => array(self::HAS_MANY, 'SolicitacaoDeCancelamento', 'Proposta_id'),
            'venda' => array(self::HAS_ONE, 'Venda', 'Proposta_id'),
            'propostaOmniConfig' => array(self::HAS_ONE, 'PropostaOmniConfig', 'Proposta'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Codigo',
            'numero_do_contrato' => 'Numero Do Contrato',
            'Analise_de_Credito_id' => 'Analise De Credito',
            'Financeira_id' => 'Financeira',
            'Cotacao_id' => 'Cotacao',
            'Tabela_id' => 'Tabela',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data Cadastro Br',
            'valor' => 'Valor',
            'valor_texto' => 'Valor Texto',
            'qtd_parcelas' => 'Qtd Parcelas',
            'carencia' => 'Carencia',
            'valor_parcela' => 'Valor Parcela',
            'valor_parcela_texto' => 'Valor Parcela Texto',
            'valor_entrada' => 'Valor Entrada',
            'valor_final' => 'Valor Final',
            'Status_Proposta_id' => 'Status Proposta',
            'titulos_gerados' => 'Titulos Gerados',
            'segurada' => 'Segurada',
            'Banco_id' => 'Banco',
            'analise_solicitada' => 'Analise Solicitada',
            'dialogo_salvo' => 'Dialogo Salvo',
            'ModeloContrato_id' => 'Modelo Contrato',
            'enviando' => 'Enviando',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('numero_do_contrato', $this->numero_do_contrato, true);
        $criteria->compare('Analise_de_Credito_id', $this->Analise_de_Credito_id);
        $criteria->compare('Financeira_id', $this->Financeira_id);
        $criteria->compare('Cotacao_id', $this->Cotacao_id);
        $criteria->compare('Tabela_id', $this->Tabela_id);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('valor', $this->valor);
        $criteria->compare('valor_texto', $this->valor_texto, true);
        $criteria->compare('qtd_parcelas', $this->qtd_parcelas);
        $criteria->compare('carencia', $this->carencia);
        $criteria->compare('valor_parcela', $this->valor_parcela);
        $criteria->compare('valor_parcela_texto', $this->valor_parcela_texto, true);
        $criteria->compare('valor_entrada', $this->valor_entrada);
        $criteria->compare('valor_final', $this->valor_final);
        $criteria->compare('Status_Proposta_id', $this->Status_Proposta_id);
        $criteria->compare('titulos_gerados', $this->titulos_gerados);
        $criteria->compare('segurada', $this->segurada);
        $criteria->compare('Banco_id', $this->Banco_id);
        $criteria->compare('analise_solicitada', $this->analise_solicitada);
        $criteria->compare('dialogo_salvo', $this->dialogo_salvo);
        $criteria->compare('ModeloContrato_id', $this->ModeloContrato_id);
        $criteria->compare('enviando', $this->enviando);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function hasOmniConfig() {
        return PropostaOmniConfig::model()->find('habilitado AND Proposta = ' . $this->id);
    }

    public function getDialogo() {

        $dialogo = Dialogo::model()->find('Entidade_Relacionamento = "Proposta" ' . ' AND Entidade_Relacionamento_id = ' . $this->id . ' AND habilitado');

        if ($dialogo != NULL)
            return $dialogo;
        else
            return NULL;
    }

    public function getFator() {
        $condition = 'habilitado AND Tabela_Cotacao_id = ' . $this->Tabela_id . ' AND carencia = ' . $this->carencia . ' AND parcela = ' . $this->qtd_parcelas;

        return Fator::model()->find($condition);
    }

    public function getComentariosOmni($propostaId, $draw) {
        $mensagens = [];

        $util = new Util;

        $proposta = Proposta::model()->findByPk($propostaId);

        $pOmniConfig = $proposta->hasOmniConfig();

        if ($proposta->hasOmniConfig() == NULL) {
            foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id . ' order by id desc ') as $msg) {
                $mensagens[] = [
                    'conteudo' => $msg->conteudo,
                    'dataHora' => $util->bd_date_to_view($msg->data_criacao),
                ];
            }
        } else {
            if ($proposta->hasOmniConfig()->numPropLoj == NULL) {
                $xmlString = '<Envelope>
                   <Body>
                      <CM_PROPOSTA>
                         <proposta xmlns:tem="http://tempuri.org/">
                            <identificacao>
                                <chave>' . $pOmniConfig->usuarioHost . '</chave>
                                <senha>' . $this->getSenhaXML() . '</senha>
                            </identificacao>
                            <dados>
                               <operacao>Consultar</operacao>
                               <idProposta>' . $proposta->codigo . '</idProposta>
                            </dados>
                         </proposta>
                      </CM_PROPOSTA>
                   </Body>
                </Envelope>';

                $apiUrl = $this->getLinkXML();

                $objProposta = new SoapVar($xmlString, XSD_ANYXML);

                if ($proposta->Status_Proposta_id == 9) {

                    $soapCliente = new SoapClient($apiUrl, ["soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS]);

                    $result = $soapCliente->CM_PROPOSTA($objProposta, null);

                    $xmlObj = simplexml_load_string($result->propostaResponse);

                    foreach ($xmlObj->comentarios->comentario as $com) {
                        $mensagens[] = [
                            'conteudo' => substr((string) $com->comentario, 25),
                            'dataHora' => $util->bd_date_to_view((string) $com->dataHora),
                        ];
                    }
                } else if ($proposta->Status_Proposta_id == 3) {
                    if (!$proposta->dialogo_salvo) {
                        $soapCliente = new SoapClient($apiUrl, ["soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS]);

                        $result = $soapCliente->CM_PROPOSTA($objProposta, null);

                        $xmlObj = simplexml_load_string($result->propostaResponse);

                        foreach ($xmlObj->comentarios->comentario as $com) {
                            $Mensagem['conteudo'] = (string) $com->comentario;

                            Mensagem::model()->novo($Mensagem, 422, $proposta->getDialogo()->id);

                            $mensagens[] = [
                                'conteudo' => substr((string) $com->comentario, 25),
                                'dataHora' => $util->bd_date_to_view((string) $com->dataHora),
                            ];
                        }

                        foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                            $mensagens[] = [
                                'conteudo' => substr($msg->conteudo, 25),
                                'dataHora' => $util->bd_date_to_view($msg->data_criacao),
                            ];
                        }

                        $proposta->dialogo_salvo = 1;
                        $proposta->update();
                    } else {
                        foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                            $mensagens[] = [
                                'conteudo' => substr($msg->conteudo, 25),
                                'dataHora' => $util->bd_date_to_view($msg->data_criacao),
                            ];
                        }
                    }
                } else {
                    $mensagens[] = [
                        'conteudo' => 'Sem atualizações',
                        'dataHora' => date('d/m/Y'),
                    ];
                }
            } else {
                if ($proposta->Status_Proposta_id == 9) {
                    $headers = array(
                        'Content-Type:text/xml;charset=UTF-8',
                        'Connection: close'
                    );

                    $xml = "<soapenv:Envelope
                    xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:ws='http://ws.host.banco.venda.jretail/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ws:consultarProposta>
                            <xmlIn>
                                <host>
                                    <autenticacao>
                                        <codUsr>hstcredshow</codUsr>
                                        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
                                    </autenticacao>
                                    <proposta>
                                        <codLoja>51130000</codLoja>
                                        <numPropLoj>" . $proposta->propostaOmniConfig->numPropLoj . "</numPropLoj>
                                    </proposta>
                                </host>
                            </xmlIn>
                        </ws:consultarProposta>
                    </soapenv:Body>
                </soapenv:Envelope>";

                    $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL?wsdl');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

                    $retorno = curl_exec($ch);

                    $objXML = simplexml_load_string($retorno);
                    $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
                    $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

                    $resultado = $objXML->xpath("//ws:consultarPropostaResponse");

                    $resp = $resultado[0]->xpath('xmlOut');

                    $cdata = simplexml_load_string($resp[0]);

                    $response = $cdata->xpath('proposta/pendencias/pendencia/desc')[0];

                    $Mensagem['conteudo'] = (string) $response;

                    //Mensagem::model()->novo($Mensagem, 422, $proposta->getDialogo()->id);

                    $mensagens[] = [
                        'conteudo' => (string) $response,
                        'dataHora' => date('d/m/Y'),
                    ];
                    if ($proposta->dialogo_salvo) {
                        foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                            $mensagens[] = [
                                'conteudo' => $msg->conteudo,
                                'dataHora' => $util->bd_date_to_view($msg->data_criacao),
                            ];
                        }
                    }
                } else {
                    $mensagens[] = [
                        'conteudo' => 'Sem atualizações',
                        'dataHora' => date('d/m/Y'),
                    ];
                }
            }
        }


        return [
            "draw" => $draw,
            "recordsTotal" => count($mensagens),
            "recordsFiltered" => count($mensagens),
            "data" => $mensagens
        ];
    }

    public function renderBotaoStatus() {

        $itemDoBordero = NULL;

        if ($this->Status_Proposta_id == 2) {
            $itemDoBordero = ItemDoBordero::model()->find('Proposta_id = ' . $this->id . ' AND t.habilitado');
            $itemRecebido = ItemRecebimento::model()->find('Proposta_id = ' . $this->id . ' AND t.habilitado');
            
            if ($itemDoBordero != NULL) {
                $loteHasBordero = LoteHasBordero::model()->find('Bordero_id = ' . $itemDoBordero->bordero->id . ' AND t.habilitado');

                if ($loteHasBordero != NULL) {
                    $statusLote = LotePagamentoHasStatusLotePagamento::model()->find('LotePagamento_id = ' . $loteHasBordero->Lote_id . ' AND StatusLotePagamento_id = 2 AND t.habilitado');

                    if ($statusLote != NULL) {
                        return "<button type='button' class='btn label label-success'>PGTO EFETUADO</button>";
                    } else {
                        return "<button type='button' class='btn label label-success'>PGTO AUTORIZADO</button>";
                    }
                } else {
                    return "<button type='button' class='btn label label-success'>PGTO AUTORIZADO</button>";
                }
            } else {
                if ($itemRecebido != NULL) {
                    return "<button type='button' class='btn label label-success'>CTRTO RECEBIDO</button>";
                }else{
                    return "<button type='button' class='btn label label-success'>APROVADA</button>";
                }
            }
        } else if ($this->Status_Proposta_id == 7) {
            return "<button type='button' class='btn label label-warning'>PENDENTE</button>";
        } else if ($this->Status_Proposta_id == 8) {
            return "<button type='button' class='btn label label-warning'>CANCELADO</button>";
        } else if ($this->Status_Proposta_id == 3) {
            return "<button type='button' class='btn label label-danger'>NEGADO</button>";
        } else {
            return "<button type='button' class='btn label label-danger'>INDEFINIDO</button>";
        }
    }

}
