<?php

class NucleoFiliais extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return NucleoFiliais the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'NucleoFiliais';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('data_cadastro, GrupoFiliais_id, nome', 'required'),
            array('habilitado, GrupoFiliais_id', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 200),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, data_cadastro, habilitado, GrupoFiliais_id, nome', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'filials' => array(self::HAS_MANY, 'Filial', 'NucleoFiliais_id'),
            'lotePagamentos' => array(self::HAS_MANY, 'LotePagamento', 'NucleoFiliais_id'),
            'grupoFiliais' => array(self::BELONGS_TO, 'GrupoFiliais', 'GrupoFiliais_id'),
            'nucleoFiliaisHasDadosBancarioses' => array(self::HAS_MANY, 'NucleoFiliaisHasDadosBancarios', 'NucleoFiliais_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'data_cadastro' => 'Data Cadastro',
            'habilitado' => 'Habilitado',
            'GrupoFiliais_id' => 'Grupo Filiais',
            'nome' => 'Nome',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('GrupoFiliais_id', $this->GrupoFiliais_id);
        $criteria->compare('nome', $this->nome, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function findAllAdmFiliais($idGrupo = null)
    {

        $nucleos = [];

        $sql    = "SELECT   DISTINCT    NF.id               AS idNucleo                                                     "
                . "FROM                 NucleoFiliais       AS NF                                                           "
                . "INNER    JOIN        Filial              AS F        ON   F.habilitado AND  F.NucleoFiliais_id   = NF.id "
                . "INNER    JOIN        AdminHasParceiro    AS AP       ON  AP.habilitado AND AP.Parceiro           =  F.id "
                . "WHERE                                                    NF.habilitado AND AP.Administrador      =       " . Yii::app()->session["usuario"]->id;
        
        if(isset($idGrupo))
        {
            
            $idGrupoStr = join(",", $idGrupo);
            
            $sql .= " AND NF.GrupoFiliais_id IN ($idGrupoStr) ";
            
        }

        $dados = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($dados as $d)
        {

            $nucleos[] = NucleoFiliais::model()->findByPk($d["idNucleo"]);
        }

        return $nucleos;
    }

    public function getIdNucleoBeta(){

        $query  = " SELECT NDB.NucleoFiliais_id AS 'NucleoId' FROM beta.NucleoFiliais AS N ";
        $query .= " INNER JOIN beta.NucleoFiliais_has_Dados_Bancarios AS NDB ON NDB.NucleoFiliais_id = N.id ";
        $query .= " INNER JOIN beta.Dados_Bancarios AS DB ON DB.id = NDB.Dados_Bancarios_id ";
        $query .= " WHERE DB.agencia = '".$this->nucleoFiliaisHasDadosBancarioses->dadosBancarios->agencia."' AND DB.numero = '".$this->nucleoFiliaisHasDadosBancarioses->dadosBancarios->numero."' ";

        $r 		= Yii::app()->db->createCommand($query)->queryRow();

        if( isset($r['NucleoId']) && $r['NucleoId'] != null )
        {
                return $r['NucleoId'];
        }
        else
        {
                return null;
        }
        
    }

}
