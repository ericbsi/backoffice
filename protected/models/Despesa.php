<?php

/**
 * This is the model class for table "Despesa".
 *
 * The followings are the available columns in table 'Despesa':
 * @property integer $id
 * @property string $descricao
 * @property string $dataDespesa
 * @property integer $rateio
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $Fornecedor_id
 * @property integer $Titulo_id
 * @property integer $Usuario_criacao
 * @property integer $Usuario_Baixa
 * @property integer $Nota_Fiscal_id
 *
 * The followings are the available model relations:
 * @property Fornecedor $fornecedor
 * @property NotaFiscal $notaFiscal
 * @property Titulo $titulo
 * @property Usuario $usuarioCriacao
 * @property Usuario $usuarioBaixa
 */
class Despesa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Despesa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Despesa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, dataDespesa, habilitado, data_cadastro, Fornecedor_id, Usuario_criacao', 'required'),
			array('rateio, habilitado, Fornecedor_id, Titulo_id, Usuario_criacao, Usuario_Baixa, Nota_Fiscal_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descricao, dataDespesa, rateio, habilitado, data_cadastro, Fornecedor_id, Titulo_id, Usuario_criacao, Usuario_Baixa, Nota_Fiscal_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
			'notaFiscal' => array(self::BELONGS_TO, 'NotaFiscal', 'Nota_Fiscal_id'),
			'titulo' => array(self::BELONGS_TO, 'Titulo', 'Titulo_id'),
			'usuarioCriacao' => array(self::BELONGS_TO, 'Usuario', 'Usuario_criacao'),
			'usuarioBaixa' => array(self::BELONGS_TO, 'Usuario', 'Usuario_Baixa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'dataDespesa' => 'Data Despesa',
			'rateio' => 'Rateio',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'Fornecedor_id' => 'Fornecedor',
			'Titulo_id' => 'Titulo',
			'Usuario_criacao' => 'Usuario Criacao',
			'Usuario_Baixa' => 'Usuario Baixa',
			'Nota_Fiscal_id' => 'Nota Fiscal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('dataDespesa',$this->dataDespesa,true);
		$criteria->compare('rateio',$this->rateio);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('Fornecedor_id',$this->Fornecedor_id);
		$criteria->compare('Titulo_id',$this->Titulo_id);
		$criteria->compare('Usuario_criacao',$this->Usuario_criacao);
		$criteria->compare('Usuario_Baixa',$this->Usuario_Baixa);
		$criteria->compare('Nota_Fiscal_id',$this->Nota_Fiscal_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}