<?php

/**
 * This is the model class for table "AdminHasParceiro".
 *
 * The followings are the available columns in table 'AdminHasParceiro':
 * @property integer $id
 * @property integer $Parceiro
 * @property integer $Administrador
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Filial $parceiro
 * @property Usuario $administrador
 */
class AdminHasParceiro extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdminHasParceiro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AdminHasParceiro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Parceiro, Administrador', 'required'),
			array('Parceiro, Administrador, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Parceiro, Administrador, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parceiro' => array(self::BELONGS_TO, 'Filial', 'Parceiro'),
			'administrador' => array(self::BELONGS_TO, 'Usuario', 'Administrador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Parceiro' => 'Parceiro',
			'Administrador' => 'Administrador',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Parceiro',$this->Parceiro);
		$criteria->compare('Administrador',$this->Administrador);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}