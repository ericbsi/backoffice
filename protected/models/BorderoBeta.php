<?php

class BorderoBeta extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getDbConnection()
	{
		return Yii::app()->beta;
	}


	public function tableName()
	{
		return 'Bordero';
	}

	public function rules()
	{
		return array(
			array('dataCriacao, destinatario, criadoPor, Status', 'required'),
			array('destinatario, criadoPor, Status, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, dataCriacao, destinatario, criadoPor, Status, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function getValor()
	{
		$valorTotal = 0;

		foreach(  $this->itemDoBorderos as $item )
		{
			$valorTotal += $item->proposta->valorRepasse();
		}

		return $valorTotal;
	}

	public function relations()
	{
		return array(
			'destinatarioObj' 				=> array(self::BELONGS_TO, 	'FilialBeta',					'destinatario'),
			'status' 						=> array(self::BELONGS_TO, 	'StatusDoBordero', 				'Status'),
			'criadoPor0' 					=> array(self::BELONGS_TO, 	'Usuario', 						'criadoPor'),
			'itemDoBorderos' 				=> array(self::HAS_MANY, 	'ItemDoBorderoBeta', 			'Bordero'),
			'loteHasBorderos' 				=> array(self::HAS_MANY, 	'LoteHasBorderoBeta', 			'Bordero_id'),
			'recebimentoDeDocumentacaos' 	=> array(self::HAS_MANY, 	'RecebimentoDeDocumentacao', 	'Bordero'),
		);
	}

	public function getPropostas( $draw, $start, $length, $borderoId )
	{
		$data 					= [];
		$bordero 				= BorderoBeta::model()->findByPk($borderoId);

		$total 					= count($bordero->itemDoBorderos);

		foreach( $bordero->itemDoBorderos as $item )
		{
			$data[]				= [
				'codigo'		=> $item->proposta->codigo,
				'valor'			=> "R$ ".number_format($item->proposta->valorRepasse(), 2, ',', '.'),
				'cliente' 		=> strtoupper($item->proposta->analiseDeCredito->cliente->pessoa->nome),
				'cpf'			=> $item->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
				'filial'		=> strtoupper($item->proposta->analiseDeCredito->filial->getConcat()),
			];
		}

		return [
			'draw'				=> $draw,
			'recordsTotal'		=> $total,
			'recordsFiltered'	=> $length,
			'data' 				=> $data
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dataCriacao' => 'Data Criacao',
			'destinatario' => 'Destinatario',
			'criadoPor' => 'Criado Por',
			'Status' => 'Status',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dataCriacao',$this->dataCriacao,true);
		$criteria->compare('destinatario',$this->destinatario);
		$criteria->compare('criadoPor',$this->criadoPor);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}