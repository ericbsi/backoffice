<?php


class LotePagamentoBeta extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	public function getDadosBancariosPagos($idLote) {

        $lote 				= LotePagamentoBeta::model()->find('habilitado AND id = ' . $idLote);
        $url          = '';
        $retorno 			= array('dados' => [], 'susseco' => true);

        $dadosPagamento 	= DadosPagamentoBeta::model()->findByPk($lote->DadosPagamento_id);

        if ($dadosPagamento !== null) {
            
            $anexo 			= AnexoBeta::model()->find("habilitado AND entidade_relacionamento = 'DadosPagamento' AND id_entidade_relacionamento = $dadosPagamento->id");
            
            if($anexo == null)
            {
              $linkComprovante = "";
            }
            else
            {
              if($anexo->url != null){
                $linkComprovante = $anexo->url;
              }else{
                $linkComprovante = $anexo->relative_path;
              }
            }

            $retorno['dados']['empresa'] = 'Banco: ' . $dadosPagamento->dadosBancariosDebito->banco->codigo . '-' .
                    $dadosPagamento->dadosBancariosDebito->banco->nome . ' ';
            $retorno['dados']['empresa'] .= 'Ag.: ' . $dadosPagamento->dadosBancariosDebito->agencia . ' ';
            $retorno['dados']['empresa'] .= 'CC: ' . $dadosPagamento->dadosBancariosDebito->numero;

            $retorno['dados']['parceiro'] = 'Banco: ' . $dadosPagamento->dadosBancariosCredito->banco->codigo . '-' .
                    $dadosPagamento->dadosBancariosCredito->banco->nome . ' ';
            $retorno['dados']['parceiro'] .= 'Ag.: ' . $dadosPagamento->dadosBancariosCredito->agencia . ' ';
            $retorno['dados']['parceiro'] .= 'CC: ' . $dadosPagamento->dadosBancariosCredito->numero;

            $dateTime = new DateTime($dadosPagamento->dataCompensacao);
            $dataCompensacao = $dateTime->format('d/m/Y H:i:s');

            $retorno['dados']['comprovante'] = $dadosPagamento->comprovante;
            $retorno['dados']['linkComprovante'] = $linkComprovante;
            $retorno['dados']['dataPagamento'] = $dataCompensacao;
            $retorno['dados']['valorPago'] = 'R$ ' . number_format($dadosPagamento->valorPago, 2, ',', '.');
        }

        return $retorno;
   }

	public function tableName()
	{
		return 'LotePagamento';
	}

	public function rules()
	{
		return array(
			array('data_cadastro, Usuario_id, NucleoFiliais_id', 'required'),
			array('habilitado, Usuario_id, NucleoFiliais_id, DadosPagamento_id', 'numerical', 'integerOnly'=>true),
			array('id, data_cadastro, habilitado, Usuario_id, NucleoFiliais_id, DadosPagamento_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'dadosPagamento' 						=> array(self::BELONGS_TO, 	'DadosPagamento', 							'DadosPagamento_id'),
			'nucleoFiliais' 						=> array(self::BELONGS_TO, 	'NucleoFiliaisBeta', 						'NucleoFiliais_id'),
			'usuario' 								=> array(self::BELONGS_TO, 	'Usuario', 									'Usuario_id'),
			'lotePagamentoHasStatusLotePagamentos' 	=> array(self::HAS_MANY, 	'LotePagamentoHasStatusLotePagamentoBeta', 	'LotePagamento_id'),
			'loteHasBorderos' 						=> array(self::HAS_MANY, 	'LoteHasBorderoBeta',						'Lote_id'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' 					=> 'ID',
			'data_cadastro' 		=> 'Data Cadastro',
			'habilitado' 			=> 'Habilitado',
			'Usuario_id' 			=> 'Usuario',
			'NucleoFiliais_id' 		=> 'Nucleo Filiais',
			'DadosPagamento_id' 	=> 'Dados Pagamento',
		);
	}

	public function getValor()
   	{
      $valorTotal     	= 0;

      foreach( $this->loteHasBorderos as $lhb )
      {
        foreach( $lhb->bordero->itemDoBorderos as $item )
        {
          $valorTotal 	+= $item->proposta->valorRepasse();
        }
      }
      
      return $valorTotal;
   	}

  public function getBorderos($draw, $start, $length, $loteId, $ambiente)
  {
      $data = [];

      $criteria = new CDbCriteria;
      $criteria->addInCondition('t.Lote_id', [$loteId], ' AND ');
      $criteria->addInCondition('t.habilitado', [1], ' AND ');

      $totalRegistros         = count( LoteHasBorderoBeta::model()->findAll( $criteria ) );

      $criteria->offset       = $start;
      $criteria->limit        = $length;

      foreach( LoteHasBorderoBeta::model()->findAll( $criteria ) as $lhb )
      {
        $data[]               = [
          'btn-more'          => '<button style="padding:3px 8px!important;" type="button" class="btopen btn-xs btn-success btn-rounded"><i class="fa fa-plus"></i></button>',
          'codigo'            => str_pad($lhb->bordero->id, 10, '0', STR_PAD_LEFT),
          'valor'             => "R$ ".number_format($lhb->bordero->getValor(), 2, ',', '.'),
          'borderoId'         => $lhb->bordero->id,
          'filial'            => mb_strtoupper($lhb->bordero->destinatarioObj->getConcat()),
          'ambiente'          => $ambiente,
        ];  
      }

      return [
          "draw"              => $draw,
          "recordsTotal"      => $length,
          "recordsFiltered"   => $totalRegistros,
          "data"              => $data,
      ];
  }

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);
		$criteria->compare('DadosPagamento_id',$this->DadosPagamento_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}