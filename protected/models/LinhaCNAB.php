<?php

/**
 * This is the model class for table "LinhaCNAB".
 *
 * The followings are the available columns in table 'LinhaCNAB':
 * @property integer $id
 * @property integer $Parcela_id
 * @property integer $ArquivoExtportado_id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property string $caracteres
 *
 * The followings are the available model relations:
 * @property ArquivoCNAB $arquivoExtportado
 * @property Parcela $parcela
 */
class LinhaCNAB extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LinhaCNAB';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ArquivoExtportado_id, data_cadastro, habilitado, caracteres', 'required'),
			array('Parcela_id, ArquivoExtportado_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Parcela_id, ArquivoExtportado_id, data_cadastro, habilitado, caracteres', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'arquivoExtportado' => array(self::BELONGS_TO, 'ArquivoCNAB', 'ArquivoExtportado_id'),
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Parcela_id' => 'Parcela',
			'ArquivoExtportado_id' => 'Arquivo Extportado',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'caracteres' => 'Caracteres',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('ArquivoExtportado_id',$this->ArquivoExtportado_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('caracteres',$this->caracteres,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LinhaCNAB the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
