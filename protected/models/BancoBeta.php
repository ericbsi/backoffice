<?php


class BancoBeta extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	public function tableName()
	{
		return 'Banco';
	}

	public function rules()
	{
		return array(
			array('nome', 'length', 'max'=>45),
			array('codigo', 'length', 'max'=>100),
			array('id, nome, codigo', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'cnabDetalhes' => array(self::HAS_MANY, 'CnabDetalhe', 'Banco_id'),
			'cnabHeaders' => array(self::HAS_MANY, 'CnabHeader', 'Banco_id'),
			'cnabTrailers' => array(self::HAS_MANY, 'CnabTrailer', 'Banco_id'),
			'dadosBancarioses' => array(self::HAS_MANY, 'DadosBancariosBeta', 'Banco_id'),
			'propostas' => array(self::HAS_MANY, 'PropostaBeta', 'Banco_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'codigo' => 'Codigo',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('codigo',$this->codigo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}