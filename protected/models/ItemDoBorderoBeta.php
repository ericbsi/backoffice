<?php

class ItemDoBorderoBeta extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}
	public function tableName()
	{
		return 'ItemDoBordero';
	}

	public function rules()
	{
		return array(
			array('Bordero, Status, Proposta_id', 'required'),
			array('Bordero, Status, Proposta_id, habilitado, Parcela_id', 'numerical', 'integerOnly'=>true),
			array('id, Bordero, Status, Proposta_id, habilitado, Parcela_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
			'bordero' => array(self::BELONGS_TO, 'BorderoBeta', 'Bordero'),
			'proposta' => array(self::BELONGS_TO, 'PropostaBeta', 'Proposta_id'),
			'status' => array(self::BELONGS_TO, 'StatusItemDoBordero', 'Status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Bordero' => 'Bordero',
			'Status' => 'Status',
			'Proposta_id' => 'Proposta',
			'habilitado' => 'Habilitado',
			'Parcela_id' => 'Parcela',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Bordero',$this->Bordero);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Parcela_id',$this->Parcela_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}