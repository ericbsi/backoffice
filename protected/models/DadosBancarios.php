<?php

/**
 * This is the model class for table "Dados_Bancarios".
 *
 * The followings are the available columns in table 'Dados_Bancarios':
 * @property integer $id
 * @property string $agencia
 * @property string $operacao
 * @property string $numero
 * @property string $data_abertura
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $Tipo_Conta_Bancaria_id
 * @property integer $Banco_id
 * @property string $cgc
 *
 * The followings are the available model relations:
 * @property DadosPagamento[] $dadosPagamentos
 * @property DadosPagamento[] $dadosPagamentos1
 * @property Banco $banco
 * @property TipoContaBancaria $tipoContaBancaria
 * @property EmpresaHasDadosBancarios[] $empresaHasDadosBancarioses
 * @property FinanceiraHasDadosBancarios[] $financeiraHasDadosBancarioses
 * @property NucleoFiliaisHasDadosBancarios[] $nucleoFiliaisHasDadosBancarioses
 */
class DadosBancarios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DadosBancarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Dados_Bancarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, Tipo_Conta_Bancaria_id, Banco_id', 'numerical', 'integerOnly'=>true),
			array('agencia, operacao, numero, data_abertura', 'length', 'max'=>45),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('cgc', 'length', 'max'=>20),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, agencia, operacao, numero, data_abertura, habilitado, data_cadastro, data_cadastro_br, Tipo_Conta_Bancaria_id, Banco_id, cgc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dadosPagamentos' => array(self::HAS_MANY, 'DadosPagamento', 'Dados_Bancarios_Debito'),
			'dadosPagamentos1' => array(self::HAS_MANY, 'DadosPagamento', 'Dados_Bancarios_Credito'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
			'tipoContaBancaria' => array(self::BELONGS_TO, 'TipoContaBancaria', 'Tipo_Conta_Bancaria_id'),
			'empresaHasDadosBancarioses' => array(self::HAS_MANY, 'EmpresaHasDadosBancarios', 'Dados_Bancarios_id'),
			'financeiraHasDadosBancarioses' => array(self::HAS_MANY, 'FinanceiraHasDadosBancarios', 'Dados_Bancarios_id'),
			'nucleoFiliaisHasDadosBancarioses' => array(self::HAS_MANY, 'NucleoFiliaisHasDadosBancarios', 'Dados_Bancarios_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agencia' => 'Agencia',
			'operacao' => 'Operacao',
			'numero' => 'Numero',
			'data_abertura' => 'Data Abertura',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'Tipo_Conta_Bancaria_id' => 'Tipo Conta Bancaria',
			'Banco_id' => 'Banco',
			'cgc' => 'Cgc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agencia',$this->agencia,true);
		$criteria->compare('operacao',$this->operacao,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('data_abertura',$this->data_abertura,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('Tipo_Conta_Bancaria_id',$this->Tipo_Conta_Bancaria_id);
		$criteria->compare('Banco_id',$this->Banco_id);
		$criteria->compare('cgc',$this->cgc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}