<?php

/**
 * This is the model class for table "Analise_de_Credito".
 *
 * The followings are the available columns in table 'Analise_de_Credito':
 * @property integer $id
 * @property string $codigo
 * @property integer $Cliente_id
 * @property integer $Filial_id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property string $numero_do_pedido
 * @property string $descricao_do_bem
 * @property string $mercadoria
 * @property string $vendedor
 * @property integer $Vendedor_id
 * @property string $telefone_loja
 * @property string $promotor
 * @property integer $Procedencia_compra_id
 * @property integer $mercadoria_retirada_na_hora
 * @property integer $celular_pre_pago
 * @property string $observacao
 * @property string $alerta
 * @property double $valor
 * @property double $entrada
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property Cliente $cliente
 * @property ProcedenciaCompra $procedenciaCompra
 * @property Usuario $usuario
 * @property Filial $filial
 * @property ItemDaAnalise[] $itemDaAnalises
 */
class AnaliseDeCredito extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnaliseDeCredito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Analise_de_Credito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, Cliente_id, Filial_id, Procedencia_compra_id, Usuario_id', 'required'),
			array('Cliente_id, Filial_id, habilitado, Vendedor_id, Procedencia_compra_id, mercadoria_retirada_na_hora, celular_pre_pago, Usuario_id', 'numerical', 'integerOnly'=>true),
			array('valor, entrada', 'numerical'),
			array('codigo', 'length', 'max'=>240),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('numero_do_pedido, vendedor, telefone_loja, promotor, alerta', 'length', 'max'=>45),
			array('mercadoria', 'length', 'max'=>200),
			array('data_cadastro, descricao_do_bem, observacao', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, codigo, Cliente_id, Filial_id, habilitado, data_cadastro, data_cadastro_br, numero_do_pedido, descricao_do_bem, mercadoria, vendedor, Vendedor_id, telefone_loja, promotor, Procedencia_compra_id, mercadoria_retirada_na_hora, celular_pre_pago, observacao, alerta, valor, entrada, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
			'procedenciaCompra' => array(self::BELONGS_TO, 'ProcedenciaCompra', 'Procedencia_compra_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'itemDaAnalises' => array(self::HAS_MANY, 'ItemDaAnalise', 'Analise_de_Credito_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'Cliente_id' => 'Cliente',
			'Filial_id' => 'Filial',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'numero_do_pedido' => 'Numero Do Pedido',
			'descricao_do_bem' => 'Descricao Do Bem',
			'mercadoria' => 'Mercadoria',
			'vendedor' => 'Vendedor',
			'Vendedor_id' => 'Vendedor',
			'telefone_loja' => 'Telefone Loja',
			'promotor' => 'Promotor',
			'Procedencia_compra_id' => 'Procedencia Compra',
			'mercadoria_retirada_na_hora' => 'Mercadoria Retirada Na Hora',
			'celular_pre_pago' => 'Celular Pre Pago',
			'observacao' => 'Observacao',
			'alerta' => 'Alerta',
			'valor' => 'Valor',
			'entrada' => 'Entrada',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('numero_do_pedido',$this->numero_do_pedido,true);
		$criteria->compare('descricao_do_bem',$this->descricao_do_bem,true);
		$criteria->compare('mercadoria',$this->mercadoria,true);
		$criteria->compare('vendedor',$this->vendedor,true);
		$criteria->compare('Vendedor_id',$this->Vendedor_id);
		$criteria->compare('telefone_loja',$this->telefone_loja,true);
		$criteria->compare('promotor',$this->promotor,true);
		$criteria->compare('Procedencia_compra_id',$this->Procedencia_compra_id);
		$criteria->compare('mercadoria_retirada_na_hora',$this->mercadoria_retirada_na_hora);
		$criteria->compare('celular_pre_pago',$this->celular_pre_pago);
		$criteria->compare('observacao',$this->observacao,true);
		$criteria->compare('alerta',$this->alerta,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('entrada',$this->entrada);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}