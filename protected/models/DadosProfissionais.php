<?php

/**
 * This is the model class for table "Dados_Profissionais".
 *
 * The followings are the available columns in table 'Dados_Profissionais':
 * @property integer $id
 * @property string $empresa
 * @property string $data_admissao
 * @property integer $Pessoa_id
 * @property integer $Endereco_id
 * @property integer $Contato_id
 * @property integer $Tipo_Ocupacao_id
 * @property integer $Classe_Profissional_id
 * @property double $renda_liquida
 * @property integer $aposentado
 * @property integer $pensionista
 * @property string $tipo_de_comprovante
 * @property string $profissao
 * @property string $mes_ano_renda
 * @property integer $tempo_de_servico_meses
 * @property integer $tempo_de_servico_anos
 * @property string $numero_do_beneficio
 * @property string $orgao_pagador
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property string $cnpj_cpf
 * @property integer $principal
 *
 * The followings are the available model relations:
 * @property ClasseProfissional $classeProfissional
 * @property Contato $contato
 * @property Endereco $endereco
 * @property Pessoa $pessoa
 * @property TipoOcupacao $tipoOcupacao
 */
class DadosProfissionais extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Dados_Profissionais';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Pessoa_id, Endereco_id, Contato_id, Tipo_Ocupacao_id, Classe_Profissional_id, aposentado, pensionista, tempo_de_servico_meses, tempo_de_servico_anos, habilitado, principal', 'numerical', 'integerOnly'=>true),
			array('renda_liquida', 'numerical'),
			array('empresa, profissao, numero_do_beneficio, cnpj_cpf', 'length', 'max'=>100),
			array('tipo_de_comprovante', 'length', 'max'=>45),
			array('mes_ano_renda', 'length', 'max'=>10),
			array('orgao_pagador', 'length', 'max'=>150),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_admissao, data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, empresa, data_admissao, Pessoa_id, Endereco_id, Contato_id, Tipo_Ocupacao_id, Classe_Profissional_id, renda_liquida, aposentado, pensionista, tipo_de_comprovante, profissao, mes_ano_renda, tempo_de_servico_meses, tempo_de_servico_anos, numero_do_beneficio, orgao_pagador, habilitado, data_cadastro, data_cadastro_br, cnpj_cpf, principal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classeProfissional' => array(self::BELONGS_TO, 'ClasseProfissional', 'Classe_Profissional_id'),
			'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
			'endereco' => array(self::BELONGS_TO, 'Endereco', 'Endereco_id'),
			'pessoa' => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id'),
			'tipoOcupacao' => array(self::BELONGS_TO, 'TipoOcupacao', 'Tipo_Ocupacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'empresa' => 'Empresa',
			'data_admissao' => 'Data Admissao',
			'Pessoa_id' => 'Pessoa',
			'Endereco_id' => 'Endereco',
			'Contato_id' => 'Contato',
			'Tipo_Ocupacao_id' => 'Tipo Ocupacao',
			'Classe_Profissional_id' => 'Classe Profissional',
			'renda_liquida' => 'Renda Liquida',
			'aposentado' => 'Aposentado',
			'pensionista' => 'Pensionista',
			'tipo_de_comprovante' => 'Tipo De Comprovante',
			'profissao' => 'Profissao',
			'mes_ano_renda' => 'Mes Ano Renda',
			'tempo_de_servico_meses' => 'Tempo De Servico Meses',
			'tempo_de_servico_anos' => 'Tempo De Servico Anos',
			'numero_do_beneficio' => 'Numero Do Beneficio',
			'orgao_pagador' => 'Orgao Pagador',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'cnpj_cpf' => 'Cnpj Cpf',
			'principal' => 'Principal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('empresa',$this->empresa,true);
		$criteria->compare('data_admissao',$this->data_admissao,true);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('Endereco_id',$this->Endereco_id);
		$criteria->compare('Contato_id',$this->Contato_id);
		$criteria->compare('Tipo_Ocupacao_id',$this->Tipo_Ocupacao_id);
		$criteria->compare('Classe_Profissional_id',$this->Classe_Profissional_id);
		$criteria->compare('renda_liquida',$this->renda_liquida);
		$criteria->compare('aposentado',$this->aposentado);
		$criteria->compare('pensionista',$this->pensionista);
		$criteria->compare('tipo_de_comprovante',$this->tipo_de_comprovante,true);
		$criteria->compare('profissao',$this->profissao,true);
		$criteria->compare('mes_ano_renda',$this->mes_ano_renda,true);
		$criteria->compare('tempo_de_servico_meses',$this->tempo_de_servico_meses);
		$criteria->compare('tempo_de_servico_anos',$this->tempo_de_servico_anos);
		$criteria->compare('numero_do_beneficio',$this->numero_do_beneficio,true);
		$criteria->compare('orgao_pagador',$this->orgao_pagador,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('cnpj_cpf',$this->cnpj_cpf,true);
		$criteria->compare('principal',$this->principal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DadosProfissionais the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
