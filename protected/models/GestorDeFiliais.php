<?php

class GestorDeFiliais {

    public function producao($start, $length, $draw, $statusProposta, $de, $ate, $filial, $modalidade, $codigo_filter = null, $op){
        $queryExport        = "";
        $filiaisId          = [];
        $filiaisBetaId      = [];
        $util               = new Util;
        $operacao           = "";

        $innerStatusPgtonr  = " ";
        $innerStatusPgtobt  = " ";

        $wherePgtoStatus    = " ";
        $wherePgtoStatusbta = " ";
        $whereModalidade    = "";
        $whereModalidadebta = "";
        $whereCodigo        = "";
        $whereCodigobta     = "";

        $innerOperacao      = "";
        $innerOperacaobta   = "";

        $selectTotais       = "SELECT ROUND(SUM(valor),2) as totalInicial, ROUND(SUM(valor_entrada),2) as totalEntrada, ROUND(SUM(valor_repasse),2) as totalRepasse FROM( ";
        $selectLinhas       = "SELECT FilialLocal, data_cadastro, id, Codigo, nome, cpf, valor, valor_entrada, status, cssStatus, ambiente, qtdParcelas FROM( ";

        $queryLinhas        = "";
        $queryTotais        = "";

        if(isset($codigo_filter) && trim($codigo_filter) != '' && $codigo_filter != null){

            $whereCodigo    = " AND P.codigo LIKE '%".$codigo_filter."%' ";
            $whereCodigobta = " AND bP.codigo LIKE '%".$codigo_filter."%' ";
        }

        $dados              = [];
        $count              = 0;

        if( isset( $op ) && $op != '0' ){
            $innerOperacao      = " AND TC.ModalidadeId = " . $op ." ";
            $innerOperacaobta   = " AND bTC.ModalidadeId = " . $op ." ";
        }

        if( isset( $modalidade ) && $modalidade != '0' ){

            if( $modalidade == '1' ){
                $whereModalidade    = " AND P.Financeira_id IN(11,10) ";
                $whereModalidadebta = " AND bP.Financeira_id IN(11,10) ";
            }
            else if($modalidade == '2'){
                $whereModalidade    = " AND P.Financeira_id IN(5,6) ";
                $whereModalidadebta = " AND bP.Financeira_id IN(5,6) ";
            }
        }

        if (empty($filial) || $filial === 0) {
            foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro) {
                $filiaisId[] = $hasParceiro->parceiro->id;
                $filialBeta = FilialBeta::model()->find("cnpj =  '" . $hasParceiro->parceiro->cnpj . "'");

                if ($filialBeta != NULL) {
                    $filiaisBetaId[] = $filialBeta->id;
                }
            }
        } else {
            for ($i = 0; $i < sizeof($filial); $i++) {
                $filialNR = Filial::model()->findByPk($filial[$i]);
                $filialBeta = FilialBeta::model()->find("cnpj =  '" . $filialNR->cnpj . "'");

                $filiaisId[] = $filialNR->id;

                if ($filialBeta != NULL) {
                    $filiaisBetaId[] = $filialBeta->id;
                }
            }
        }

        $filiaisId = join(',', $filiaisId);
        $filiaisBetaId = join(',', $filiaisBetaId);

        /* Se o status for aprovado, a proposta não pode fazer parte de um item do bordero, pois, se fizer, ela é pgto aut. */
        if ($statusProposta === '2') {
            $innerStatusPgtonr = " LEFT JOIN nordeste2.ItemDoBordero AS IB ON IB.Proposta_id = P.id ";
            $wherePgtoStatus = " AND IB.id IS NULL ";

            $innerStatusPgtobt = " LEFT JOIN beta.ItemDoBordero AS bIB ON bIB.Proposta_id = bP.id ";
            $wherePgtoStatusbta = " AND bIB.id IS NULL ";
        }

        /* Propostas foram autorizadas para pagamento */
        else if ($statusProposta === '100') {
            $innerStatusPgtonr .= " INNER JOIN nordeste2.ItemDoBordero                            AS IB       ON IB.Proposta_id           = P.id              AND IB.habilitado   ";
            $innerStatusPgtonr .= " INNER JOIN nordeste2.Bordero                                  AS Br       ON Br.id                    = IB.Bordero        AND Br.habilitado   ";
            $innerStatusPgtonr .= " LEFT  JOIN nordeste2.Lote_has_Bordero                         AS LHB      ON LHB.Bordero_id           = Br.id             AND LHB.habilitado  ";
            $innerStatusPgtonr .= " LEFT JOIN nordeste2.LotePagamento                             AS LP       ON LP.id                    = LHB.Lote_id       AND LP.habilitado   ";
            $innerStatusPgtonr .= " LEFT JOIN nordeste2.LotePagamento_has_StatusLotePagamento     AS LPHS     ON LPHS.LotePagamento_id    = LP.id             AND LPHS.habilitado AND LPHS.StatusLotePagamento_id = 2 ";

            $wherePgtoStatus = "  AND (LHB.id IS NULL OR LPHS.id IS NULL )";

            $innerStatusPgtobt .= " INNER JOIN beta.ItemDoBordero                                 AS bIB       ON bIB.Proposta_id           = bP.id           AND bIB.habilitado   ";
            $innerStatusPgtobt .= " INNER JOIN beta.Bordero                                       AS bBr       ON bBr.id                    = bIB.Bordero     AND bBr.habilitado   ";
            $innerStatusPgtobt .= " LEFT  JOIN beta.Lote_has_Bordero                              AS bLHB      ON bLHB.Bordero_id           = bBr.id          AND bLHB.habilitado  ";
            $innerStatusPgtobt .= " LEFT JOIN beta.LotePagamento                                  AS bLP       ON bLP.id                    = bLHB.Lote_id    AND bLP.habilitado   ";
            $innerStatusPgtobt .= " LEFT JOIN beta.LotePagamento_has_StatusLotePagamento          AS bLPHS     ON bLPHS.LotePagamento_id    = bLP.id          AND bLPHS.habilitado AND bLPHS.StatusLotePagamento_id = 2 ";

            $wherePgtoStatusbta = "  AND (bLHB.id IS NULL OR bLPHS.id IS NULL ) ";
        }

        /* Propostas já foram pagas */
        else if ($statusProposta === '101') {
            $innerStatusPgtonr .= " INNER JOIN nordeste2.ItemDoBordero                            AS IB       ON IB.Proposta_id           = P.id              AND IB.habilitado   ";
            $innerStatusPgtonr .= " INNER JOIN nordeste2.Bordero                                  AS Br       ON Br.id                    = IB.Bordero        AND Br.habilitado   ";
            $innerStatusPgtonr .= " INNER JOIN nordeste2.Lote_has_Bordero                         AS LHB      ON LHB.Bordero_id           = Br.id             AND LHB.habilitado  ";
            $innerStatusPgtonr .= " INNER JOIN nordeste2.LotePagamento                            AS LP       ON LP.id                    = LHB.Lote_id       AND LP.habilitado   ";
            $innerStatusPgtonr .= " INNER JOIN nordeste2.LotePagamento_has_StatusLotePagamento    AS LPHS     ON LPHS.LotePagamento_id    = LP.id             AND LPHS.habilitado ";

            $wherePgtoStatus = "  AND LPHS.StatusLotePagamento_id = 2 ";

            $innerStatusPgtobt .= " INNER JOIN beta.ItemDoBordero                                 AS bIB      ON bIB.Proposta_id           = bP.id            AND bIB.habilitado   ";
            $innerStatusPgtobt .= " INNER JOIN beta.Bordero                                       AS bBr      ON bBr.id                    = bIB.Bordero      AND bBr.habilitado   ";
            $innerStatusPgtobt .= " INNER JOIN beta.Lote_has_Bordero                              AS bLHB     ON bLHB.Bordero_id           = bBr.id           AND bLHB.habilitado  ";
            $innerStatusPgtobt .= " INNER JOIN beta.LotePagamento                                 AS bLP      ON bLP.id                    = bLHB.Lote_id     AND bLP.habilitado   ";
            $innerStatusPgtobt .= " INNER JOIN beta.LotePagamento_has_StatusLotePagamento         AS bLPHS    ON bLPHS.LotePagamento_id    = bLP.id           AND bLPHS.habilitado ";

            $wherePgtoStatusbta = "  AND bLPHS.StatusLotePagamento_id = 2 ";
        }

        else if ( $statusProposta === '200' ){

            //RecebimentoDeDocumentacao_has_Proposta
            $innerStatusPgtonr .= " INNER JOIN nordeste2.RecebimentoDeDocumentacao_has_Proposta AS RDP ON RDP.Proposta_id = P.id AND RDP.habilitado ";
        }

        //$query = " SELECT FilialLocal, data_cadastro, id, Codigo, nome, cpf, valor, valor_entrada, status, cssStatus, ambiente FROM( ";
        $query  = " SELECT P.data_cadastro, P.id, P.codigo, Pe.nome, CPF.numero as cpf, P.valor, ";
        $query .= " P.valor_entrada, SP.status, SP.cssClass as 'cssStatus', '1' AS ambiente, ";
        $query .= " CONCAT(F.nome_fantasia, ' - ', FE.cidade) as 'FilialLocal', ";
        $query .= " CASE WHEN TC.ModalidadeId = 1 THEN P.valor - P.valor_entrada ";
        $query .= " ELSE ((P.valor - P.valor_entrada) - ((P.valor - P.valor_entrada) * (TC.taxa / 100))) ";
        $query .= " END AS valor_repasse, P.qtd_parcelas AS qtdParcelas ";
        $query .= " FROM nordeste2.Proposta as P ";
        $query .= " INNER JOIN nordeste2.Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id ";
        $query .= $innerStatusPgtonr;
        $query .= " INNER JOIN nordeste2.Filial as F ON F.id = AC.Filial_id ";
        $query .= " INNER JOIN nordeste2.Filial_has_Endereco as FHE ON FHE.Filial_id = AC.Filial_id ";
        $query .= " INNER JOIN nordeste2.Endereco as FE ON FHE.Endereco_id = FE.id ";
        $query .= " INNER JOIN nordeste2.Cliente as C ON C.id = AC.Cliente_id ";
        $query .= " INNER JOIN nordeste2.Pessoa as Pe ON Pe.id = C.Pessoa_id ";
        $query .= " INNER JOIN nordeste2.Pessoa_has_Documento as PeHD ON Pe.id = PeHD.Pessoa_id ";
        $query .= " INNER JOIN nordeste2.Documento as CPF ON CPF.id = PeHD.Documento_id AND CPF.Tipo_documento_id = 1";
        $query .= " INNER JOIN nordeste2.Status_Proposta as SP ON SP.id = P.Status_Proposta_id ";
        $query .= " INNER JOIN nordeste2.Tabela_Cotacao AS TC ON TC.id = P.Tabela_id ";
        $query .= $innerOperacao;
        $query .= " WHERE P.habilitado AND AC.Filial_id IN($filiaisId)  " . $wherePgtoStatus . $whereModalidade . $whereCodigo;
        $query .= " AND P.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";

        if ($statusProposta !== '0') {
            if ($statusProposta === '2') {
                $query .= " AND P.Status_Proposta_id IN(2) ";
            } else if ($statusProposta !== '100' && $statusProposta !== '101') {
                $query .= " AND P.Status_Proposta_id = " . $statusProposta . " ";
            }
        } else if ($statusProposta === '0') {
            $query .= " AND P.Status_Proposta_id IN(2, 7) ";
        }

        $query .= " UNION ";
        $query .= " SELECT bP.data_cadastro, bP.id,  bP.codigo, bPe.nome, bCPF.numero as cpf, bP.valor, ";
        $query .= " bP.valor_entrada, bSP.status, bSP.cssClass as 'cssStatus','2' AS ambiente, ";
        $query .= " CONCAT(bF.nome_fantasia, ' - ', bFE.cidade) as 'FilialLocal', ";
        $query .= " CASE WHEN bTC.ModalidadeId = 1 THEN bP.valor - bP.valor_entrada ";
        $query .= " ELSE ((bP.valor - bP.valor_entrada) - ((bP.valor - bP.valor_entrada) * (bTC.taxa / 100))) ";
        $query .= " END AS valor_repasse, bP.qtd_parcelas AS qtdParcelas ";
        $query .= " FROM beta.Proposta as bP ";
        $query .= " INNER JOIN beta.Analise_de_Credito as bAC ON bAC.id = bP.Analise_de_Credito_id ";
        $query .= $innerStatusPgtobt;
        $query .= " INNER JOIN beta.Filial as bF ON bF.id = bAC.Filial_id ";
        $query .= " INNER JOIN beta.Filial_has_Endereco as bFHE ON bFHE.Filial_id = bAC.Filial_id ";
        $query .= " INNER JOIN beta.Endereco as bFE ON bFHE.Endereco_id = bFE.id ";
        $query .= " INNER JOIN beta.Cliente as bC ON bC.id = bAC.Cliente_id ";
        $query .= " INNER JOIN beta.Pessoa as bPe ON bPe.id = bC.Pessoa_id ";
        $query .= " INNER JOIN beta.Pessoa_has_Documento as bPeHD ON bPe.id = bPeHD.Pessoa_id ";
        $query .= " INNER JOIN beta.Documento as bCPF ON bCPF.id = bPeHD.Documento_id AND bCPF.Tipo_documento_id = 1";
        $query .= " INNER JOIN beta.Status_Proposta as bSP ON bSP.id = bP.Status_Proposta_id ";
        $query .= " INNER JOIN beta.Tabela_Cotacao AS bTC ON bTC.id = bP.Tabela_id ";
        $query .= $innerOperacaobta;
        $query .= " WHERE bP.habilitado AND bAC.Filial_id IN($filiaisBetaId) " . $wherePgtoStatusbta . $whereModalidadebta . $whereCodigobta;
        $query .= " AND bP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";

        if ($statusProposta !== '0') {
            if ($statusProposta === '2') {
                $query .= " AND bP.Status_Proposta_id IN(2) ";
            } else if ($statusProposta !== '100' && $statusProposta !== '101') {
                $query .= " AND bP.Status_Proposta_id = " . $statusProposta . " ";
            }
        } else if ($statusProposta === '0') {
            $query .= " AND bP.Status_Proposta_id IN(2, 7) ";
        }

        $query .= " ) AS TEMP ORDER BY data_cadastro DESC ";

        //Montando querys
        $queryLinhas  = $selectLinhas;
        $queryLinhas .= $query;

        $queryTotais  = $selectTotais;
        $queryTotais .= $query;

        $totais  = $qparc = Yii::app()->db->createCommand($queryTotais)->queryRow();

        $qparc = Yii::app()->db->createCommand($queryLinhas)->queryAll();
        $count = count($qparc);

        /*$h = fopen('log/logIndicativos.txt', 'w');
        fwrite($h, $queryTotais);
        fclose($h);*/

        $queryExport  = $queryLinhas;

        $queryLinhas .= " LIMIT $start, $length ";

        foreach (Yii::app()->db->createCommand($queryLinhas)->queryAll() as $r) {
            $obProposta = null;

            if ($r['ambiente'] == '1') {
                $obProposta = Proposta::model()->findByPk($r['id']);
            } else if ($r['ambiente'] == '2') {
                $obProposta = PropostaBeta::model()->findByPk($r['id']);
            }

            if( $obProposta->Financeira_id  == 10 ){
                $operacao                   = "CSC";
                if ($obProposta->tabelaCotacao->ModalidadeId == 1) {
                    $operacao = "CSC COM JUROS";
                }
                if ($obProposta->tabelaCotacao->ModalidadeId == 2) {
                    $operacao = "CSC SEM JUROS";
                }
            }
            else if( $obProposta->Financeira_id  == 11 ){
                if ($obProposta->tabelaCotacao->ModalidadeId == 1) {
                    $operacao = "CDC COM JUROS";
                }
                if ($obProposta->tabelaCotacao->ModalidadeId == 2) {
                    $operacao = "CDC SEM JUROS";
                }
            }
            else{
                if ($obProposta->tabelaCotacao->ModalidadeId == 1) {
                    $operacao = "CREDSHOW COM JUROS";
                }
                if ($obProposta->tabelaCotacao->ModalidadeId == 2) {
                    $operacao = "CREDSHOW SEM JUROS";
                }
            }

            $dados[] = [
                'codigo' => $r['Codigo'],
                'cliente' => mb_strtoupper(substr($r['nome'], 0, 12)),
                'cpf' => $r['cpf'],
                'filial' => $r['FilialLocal'],
                'v_inicial' => number_format($r['valor'], 2, ',', '.'),
                'v_entrada' => number_format($r['valor_entrada'], 2, ',', '.'),
                'v_financiado' => number_format(( $r['valor'] - $r['valor_entrada']), 2, ',', '.'),
                'data_cadastro' => $util->bd_date_to_view(substr($r['data_cadastro'], 0, 10)),
                'status' => $obProposta->renderBotaoStatus(),
                'v_repasse' => number_format($obProposta->valorRepasse(), 2,',',''),
                'parcelas' => $obProposta->qtd_parcelas,
                'vendedor' => mb_strtoupper($obProposta->analiseDeCredito->vendedor),
                'carencia' => $obProposta->carencia,
                'operacao' => $operacao,
            ];
        }

        return [
            'draw'              => $draw,
            'data'              => $dados,
            'recordsTotal'      => $length,
            'recordsFiltered'   => $count,
            'queryExport'       => $queryExport,
            'totalInicial'      => number_format($totais['totalInicial'], 2, ',','.'),
            'totalEntrada'      => number_format($totais['totalEntrada'], 2, ',','.'),
            'totalFinanciado'   => number_format($totais['totalInicial']-$totais['totalEntrada'], 2, ',','.'),
            'totalRepasse'      => number_format($totais['totalRepasse'], 2, ',','.'),
        ];
    }

    public function indicativosProducao($start, $length, $draw, $de, $ate, $filial) {

        ini_set('memory_limit', '-1');

        $filiaisId = [];
        $filiaisBetaId = [];
        $util = new Util;

        $dados = [];
        $count = 0;
        $totalGeralQtd = 0;
        $totalGeralValor = 0;

        $linhaAprovado = [
            'status' => '<button type="button" class="btn btn-block btn-sm btn-success">Aprovado</button>',
            'quantidade' => 0,
            'valor' => 0,
            'p_analisada' => 0,
            'p_total_analisado' => 0,
            'mais_detalhes' => '<button style="padding:1px 7px!important;" data-bind-show="#wrapper-grid-aprovadas" data-bind-hide="#indicativos-wrapper" class="btn btn-icon waves-effect waves-light btn-success btn-move-panel"> <i class="fa  fa-plus"></i> </button>',
        ];

        $linhaNegado = [
            'status' => '<button type="button" class="btn btn-block btn-sm btn-danger">Negado</button>',
            'quantidade' => 0,
            'valor' => 0,
            'p_analisada' => 0,
            'p_total_analisado' => 0,
            'mais_detalhes' => '<button style="padding:1px 7px!important;" data-bind-show="#wrapper-grid-negadas" data-bind-hide="#indicativos-wrapper" class="btn btn-icon waves-effect waves-light btn-success btn-move-panel"> <i class="fa  fa-plus"></i> </button>',
        ];

        $linhaCancelado = [
            'status' => '<button type="button" class="btn btn-block btn-sm btn-warning">Cancelado</button>',
            'quantidade' => 0,
            'valor' => 0,
            'p_analisada' => 0,
            'p_total_analisado' => 0,
            'mais_detalhes' => '<button style="padding:1px 7px!important;" data-bind-show="#wrapper-grid-canceladas" data-bind-hide="#indicativos-wrapper" class="btn btn-icon waves-effect waves-light btn-success btn-move-panel"> <i class="fa  fa-plus"></i> </button>',
        ];

        if (empty($filial) || $filial === 0) {
            foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro) {
                $filiaisId[] = $hasParceiro->parceiro->id;
                $filialBeta = FilialBeta::model()->find("cnpj =  '" . $hasParceiro->parceiro->cnpj . "'");

                if ($filialBeta != NULL) {
                    $filiaisBetaId[] = $filialBeta->id;
                }
            }
        } else {
            for ($i = 0; $i < sizeof($filial); $i++) {
                $filialNR = Filial::model()->findByPk($filial[$i]);
                $filialBeta = FilialBeta::model()->find("cnpj =  '" . $filialNR->cnpj . "'");

                $filiaisId[] = $filialNR->id;

                if ($filialBeta != NULL) {
                    $filiaisBetaId[] = $filialBeta->id;
                }
            }
        }

        $filiaisId = join(',', $filiaisId);
        $filiaisBetaId = join(',', $filiaisBetaId);

        $query = " SELECT data_cadastro, id, Codigo, nome, cpf, valor, valor_entrada, status, cssStatus, statusId, ambiente FROM( ";
        $query .= " SELECT P.data_cadastro, P.id, P.codigo, Pe.nome, CPF.numero as cpf, P.valor, P.valor_entrada, SP.status, SP.cssClass as 'cssStatus', P.Status_Proposta_id as 'statusId', '1' AS ambiente FROM nordeste2.Proposta as P ";
        $query .= " INNER JOIN nordeste2.Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id ";
        $query .= " INNER JOIN nordeste2.Cliente as C ON C.id = AC.Cliente_id ";
        $query .= " INNER JOIN nordeste2.Pessoa as Pe ON Pe.id = C.Pessoa_id ";
        $query .= " INNER JOIN nordeste2.Pessoa_has_Documento as PeHD ON Pe.id = PeHD.Pessoa_id ";
        $query .= " INNER JOIN nordeste2.Documento as CPF ON CPF.id = PeHD.Documento_id AND CPF.Tipo_documento_id = 1";
        $query .= " INNER JOIN nordeste2.Status_Proposta as SP ON SP.id = P.Status_Proposta_id ";
        $query .= " WHERE P.habilitado AND AC.Filial_id IN($filiaisId) AND P.Status_Proposta_id IN(2,3,7,8) ";
        $query .= " AND P.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";

        $query .= " UNION ";
        $query .= " SELECT bP.data_cadastro, bP.id,  bP.codigo, bPe.nome, bCPF.numero as cpf, bP.valor, bP.valor_entrada, bSP.status, bSP.cssClass as 'cssStatus', bP.Status_Proposta_id as 'statusId', '2' AS ambiente FROM beta.Proposta as bP ";
        $query .= " INNER JOIN beta.Analise_de_Credito as bAC ON bAC.id = bP.Analise_de_Credito_id ";
        $query .= " INNER JOIN beta.Cliente as bC ON bC.id = bAC.Cliente_id ";
        $query .= " INNER JOIN beta.Pessoa as bPe ON bPe.id = bC.Pessoa_id ";
        $query .= " INNER JOIN beta.Pessoa_has_Documento as bPeHD ON bPe.id = bPeHD.Pessoa_id ";
        $query .= " INNER JOIN beta.Documento as bCPF ON bCPF.id = bPeHD.Documento_id AND bCPF.Tipo_documento_id = 1";
        $query .= " INNER JOIN beta.Status_Proposta as bSP ON bSP.id = bP.Status_Proposta_id ";
        $query .= " WHERE bP.habilitado AND bAC.Filial_id IN($filiaisBetaId) AND bP.Status_Proposta_id IN(2,3,7,8) ";
        $query .= " AND bP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";

        $query .= " ) AS TEMP ORDER BY data_cadastro DESC ";

        $count = count(Yii::app()->db->createCommand($query)->queryAll());

        foreach (Yii::app()->db->createCommand($query)->queryAll() as $r) {
            /* Aprovadas */
            if ($r['statusId'] === '2' || $r['statusId'] === '7') {
                $linhaAprovado['quantidade'] += 1;
                $linhaAprovado['valor'] += ($r['valor'] - $r['valor_entrada']);
            }

            /* Negadas */ else if ($r['statusId'] === '3') {
                $linhaNegado['quantidade'] += 1;
                $linhaNegado['valor'] += ($r['valor'] - $r['valor_entrada']);
            }

            /* Canceladas */ else if ($r['statusId'] === '8') {
                $linhaCancelado['quantidade'] += 1;
                $linhaCancelado['valor'] += ($r['valor'] - $r['valor_entrada']);
            }

            $totalGeralQtd += 1;
            $totalGeralValor += ($r['valor'] - $r['valor_entrada']);
        }

        /* Porcentagens */

        //Em quantidade
        $p_analisada_ap = ( $totalGeralQtd > 0 ? ($linhaAprovado['quantidade'] * 100) / $totalGeralQtd : 0 );
        $p_analisada_ng = ( $totalGeralQtd > 0 ? ($linhaNegado['quantidade'] * 100) / $totalGeralQtd : 0 );
        $p_analisada_cl = ( $totalGeralQtd > 0 ? ($linhaCancelado['quantidade'] * 100) / $totalGeralQtd : 0 );

        //Em valor
        $p_total_analisado_ap = ( $totalGeralValor > 0 ? ($linhaAprovado['valor'] * 100) / $totalGeralValor : 0 );
        $p_total_analisado_ng = ( $totalGeralValor > 0 ? ($linhaNegado['valor'] * 100) / $totalGeralValor : 0 );
        $p_total_analisado_cl = ( $totalGeralValor > 0 ? ($linhaCancelado['valor'] * 100) / $totalGeralValor : 0 );

        //Em quantidade
        $linhaAprovado['p_analisada'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_analisada_ap . '%;">' . number_format($p_analisada_ap, 2) . '%</div></div>';
        $linhaNegado['p_analisada'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-danger wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_analisada_ng . '%;">' . number_format($p_analisada_ng, 2) . '%</div></div>';
        $linhaCancelado['p_analisada'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-warning wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_analisada_cl . '%;">' . number_format($p_analisada_cl, 2) . '%</div></div>';


        //Em valor
        $linhaAprovado['p_total_analisado'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_total_analisado_ap . '%;">' . number_format($p_total_analisado_ap, 2) . '%</div></div>';
        $linhaNegado['p_total_analisado'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-danger wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_total_analisado_ng . '%;">' . number_format($p_total_analisado_ng, 2) . '%</div></div>';
        $linhaCancelado['p_total_analisado'] = '<div class="progress progress-lg m-b-5"><div class="progress-bar progress-bar-warning wow animated progress-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p_total_analisado_cl . '%;">' . number_format($p_total_analisado_cl, 2) . '%</div></div>';

        /* FIM Porcentagens */


        $linhaAprovado['valor'] = 'R$ ' . number_format($linhaAprovado['valor'], 2, ',', '.');
        $linhaNegado['valor'] = 'R$ ' . number_format($linhaNegado['valor'], 2, ',', '.');
        $linhaCancelado['valor'] = 'R$ ' . number_format($linhaCancelado['valor'], 2, ',', '.');

        $dados[] = $linhaAprovado;
        $dados[] = $linhaNegado;
        $dados[] = $linhaCancelado;

        return [
            'draw' => $draw,
            'data' => $dados,
            'recordsTotal' => $length,
            'recordsFiltered' => 3,
        ];
    }

    public function indicativosVendas() {
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        /* Pegando primeiro dia do mês atual */
        $Dia = date('Y-m-01');
        $meses = [
            ['01', 'Janeiro', 0],
            ['02', 'Fevereiro', 0],
            ['03', 'Março', 0],
            ['04', 'Abril', 0],
            ['05', 'Maio', 0],
            ['06', 'Junho', 0],
            ['07', 'Julho', 0],
            ['08', 'Agosto', 0],
            ['09', 'Setembro', 0],
            ['10', 'Outubro', 0],
            ['11', 'Novembro', 0],
            ['12', 'Dezembro', 0]
        ];

        $totalMes = 0;
        $nDeVendasMes = 0;
        $valorMedioVendasMes = 0;
        $totalVendasDia = 0;
        $totaisBeta = NULL;
        $filiaisId = [];
        $filiaisBetaId = [];
        $aprovadasCompDiaADia = [];
        $negadasCompDiaADia = [];
        $dias = [];
        $mesesReport = [];
        $producaoMesAMesAp = [];
        $producaoMesAMesNeg = [];
        $producaoMesAMesSeg = [];
        $deAte = [];
        $aprovadasValorCompDiaADia = [];
        $i = 1;

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Percorrendo todas as filiais ativadas no cadastro do administrador.
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro) {
            $totaisBeta = [];

            /*
             * Adicionando os ids das filiais no array
             */
            $filiaisId[] = $hasParceiro->parceiro->id;

            /*
             * Pegando a mesma filial no outro banco de dados, através do CNPJ
             * Objeto FilialBeta, que se conecta ao BD Beta
             */
            $filialBeta = FilialBeta::model()->find("cnpj =  '" . $hasParceiro->parceiro->cnpj . "'");

            /*
             * Se a filial possuir cadastro no outro banco de dados, vamos carregar os indicativos dela
             */
            if ($filialBeta != NULL) {
                /* Armazenando os ids das filiais beta */
                $filiaisBetaId[] = $filialBeta->id;

                $totaisBeta = $filialBeta->actionIndicativosVendas();

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                //Totais Filial Beta
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                $totalMes += $totaisBeta['totalMes'];
                $nDeVendasMes += $totaisBeta['nDeVendasMes'];
                $totalVendasDia += $totaisBeta['totalVendasDia'];
            }
        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //Adicionando o nome janeiro ao array primeiro
        //O While abaixo adiciona os outros meses, até o mes atual
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        $mesesReport['meses'][] = $meses[0][1];
        $mesesReport['n'][] = $meses[0][0];

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // Equanto o mes da posição atual for diferente do mês atual,
        // continue adicionando
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        while ($meses[$i][0] < date('m')) {
            $mesesReport['meses'][] = $meses[$i][1];
            $mesesReport['n'][] = $meses[$i][0];
            $i++;
        }

        $mesesReport['meses'][] = $meses[11][1];
        $mesesReport['n'][] = $meses[11][0];

        /* Produção Mes a mes */
        for ($x = 0; $x < sizeof($mesesReport['n']); $x++) {
            $countAp = 0;
            $totalSegAp = 0;
            $countNeg = 0;
            $criteriaMesAMes = new CDbCriteria;
            $de = date('Y-' . $mesesReport['n'][$x] . '-01') . ' 00:00:00';
            $ate = date('Y-' . $mesesReport['n'][$x] . '-t') . ' 23:59:59';

            /* Fazendo levantamento de todos os dias do mês atual */
            if ($mesesReport['n'][$x] === date('m')) {
                while ($Dia <= date('Y-m-d')) {
                    $dias[] = substr($Dia, 8, 2); //2016-06-03
                    $countAprovadas = 0;
                    $valorAprovadas = 0;
                    $countNegadas = 0;

                    /* PESQUISANDO NA BASE NORDESTE - DIA A DIA DO MES */
                    $criteriaAprovadasNegadas = new CDbCriteria;
                    $criteriaAprovadasNegadas->with = [ 'analiseDeCredito' => ['alias' => 'ac']];
                    $criteriaAprovadasNegadas->addInCondition('ac.Filial_id', $filiaisId, ' AND ');
                    $criteriaAprovadasNegadas->addInCondition('t.Status_Proposta_id', [2, 3], ' AND ');
                    $criteriaAprovadasNegadas->addInCondition('t.habilitado', [1], ' AND ');
                    $criteriaAprovadasNegadas->addBetweenCondition('t.data_cadastro', $Dia . ' 00:00:00', $Dia . ' 23:59:59', 'AND');

                    foreach (Proposta::model()->findAll($criteriaAprovadasNegadas) as $p) {
                        if ($p->Status_Proposta_id == 2) {
                            $countAprovadas++;
                            $valorAprovadas += ($p->valor - $p->valor_entrada);
                        } else {
                            $countNegadas++;
                        }
                    }
                    /* FIM PESQUISANDO NA BASE NORDESTE - DIA A DIA DO MES */

                    /* PESQUISANDO NA BASE BETA - DIA A DIA DO MES */
                    $cAprovadasNegadasBeta = new CDbCriteria;
                    $cAprovadasNegadasBeta->with = [ 'analiseDeCredito' => ['alias' => 'ac']];
                    $cAprovadasNegadasBeta->addInCondition('ac.Filial_id', $filiaisBetaId, ' AND ');
                    $cAprovadasNegadasBeta->addInCondition('t.Status_Proposta_id', [2, 3], ' AND ');
                    $cAprovadasNegadasBeta->addInCondition('t.habilitado', [1], ' AND ');
                    $cAprovadasNegadasBeta->addBetweenCondition('t.data_cadastro', $Dia . ' 00:00:00', $Dia . ' 23:59:59', 'AND');

                    foreach (PropostaBeta::model()->findAll($cAprovadasNegadasBeta) as $pBeta) {
                        if ($pBeta->Status_Proposta_id == 2) {
                            $countAprovadas++;
                            $valorAprovadas += ($pBeta->valor - $pBeta->valor_entrada);
                        } else {
                            $countNegadas++;
                        }
                    }
                    /* FIM PESQUISANDO NA BASE BETA - DIA A DIA DO MES */

                    $aprovadasCompDiaADia[] = $countAprovadas;
                    $aprovadasValorCompDiaADia[] = $valorAprovadas;
                    $negadasCompDiaADia[] = $countNegadas;

                    //Incrementando o dia
                    $Dia = date('Y-m-d', strtotime("+1 day", strtotime($Dia)));
                }
            }

            /* Mes a mes base Nordeste */
            $criteriaMesAMes->with = ['analiseDeCredito' => ['alias' => 'ac']];
            $criteriaMesAMes->addInCondition('ac.Filial_id', $filiaisId, ' AND ');
            $criteriaMesAMes->addInCondition('t.Status_Proposta_id', [2, 3, 7], ' AND ');
            $criteriaMesAMes->addInCondition('t.habilitado', [1], ' AND ');
            $criteriaMesAMes->addBetweenCondition('t.data_cadastro', $de, $ate, ' AND ');

            foreach (Proposta::model()->findAll($criteriaMesAMes) as $pMesAMes) {
                if (($pMesAMes->Status_Proposta_id == 2 || $pMesAMes->Status_Proposta_id == 7) && $pMesAMes->titulos_gerados) {
                    if ($pMesAMes->segurada) {
                        $totalSegAp += $pMesAMes->calcularValorDoSeguro();
                    }

                    $countAp += ( $pMesAMes->valor - $pMesAMes->valor_entrada );
                } else {
                    $countNeg += ( $pMesAMes->valor - $pMesAMes->valor_entrada );
                }
            }
            /* FIM Mes a mes base Nordeste */
            $cMesAMesBeta = new CDbCriteria;
            $cMesAMesBeta->with = ['analiseDeCredito' => ['alias' => 'ac']];
            $cMesAMesBeta->addInCondition('ac.Filial_id', $filiaisBetaId, ' AND ');
            $cMesAMesBeta->addInCondition('t.Status_Proposta_id', [2, 3, 7], ' AND ');
            $cMesAMesBeta->addInCondition('t.habilitado', [1], ' AND ');
            $cMesAMesBeta->addBetweenCondition('t.data_cadastro', $de, $ate, ' AND ');

            foreach (PropostaBeta::model()->findAll($cMesAMesBeta) as $pMesAMesBeta) {
                if (($pMesAMesBeta->Status_Proposta_id == 2 || $pMesAMesBeta->Status_Proposta_id == 7) && $pMesAMesBeta->titulos_gerados) {
                    if ($pMesAMesBeta->segurada) {
                        $totalSegAp += $pMesAMesBeta->calcularValorDoSeguro();
                    }

                    $countAp += ( $pMesAMesBeta->valor - $pMesAMesBeta->valor_entrada );
                } else {
                    $countNeg += ( $pMesAMesBeta->valor - $pMesAMesBeta->valor_entrada );
                }
            }

            //$producaoMesAMesSeg[]            = $totalSegAp;
            $producaoMesAMesAp[] = $countAp;
            $producaoMesAMesNeg[] = $countNeg;
        }
        /* FIM Produção Mes a mes */

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        //	* Este while vai nos ajudar a percorrer todos os dias
        //	* do mês atual, desde o primeiro dia, até o dia atual
        //	* para fazermos um comparativo entre aprovadas e negadas, dia-a-dia
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /* Montando critérios de pesquisa */
        $criteria = new CDbCriteria;

        /*
         * Inner join analise de credito
         * pois ela possui o id da filial
         */

        $criteria->with = ['analiseDeCredito' => ['alias' => 'ac']];
        $criteria->addInCondition('ac.Filial_id', $filiaisId, ' AND ');

        /* Propostas aprovadas e habilitadas, com titulos gerados */
        $criteria->addInCondition('t.Status_Proposta_id', [2, 7], ' AND ');
        $criteria->addInCondition('t.habilitado', [1], ' AND ');

        /* Período inicial, primeiro dia do mês atual, até final do mês */
        $criteria->addBetweenCondition('t.data_cadastro', date('Y-m-01') . ' 00:00:00', date("Y-m-t", strtotime(date('Y-m-d'))) . ' 23:59:59', 'AND');

        $propostas = Proposta::model()->findAll($criteria);

        foreach ($propostas as $proposta) {
            $totalMes += ($proposta->valor - $proposta->valor_entrada);

            if ($proposta->data_cadastro >= date('Y-m-d 00:00:00') && $proposta->data_cadastro <= date('Y-m-d 23:59:59')) {
                $totalVendasDia += ($proposta->valor - $proposta->valor_entrada);
            }
        }

        $nDeVendasMes += count($propostas);
        $valorMedioVendasMes = ( $nDeVendasMes > 0 ? ($totalMes / $nDeVendasMes) : 0 );

        /* retorna array configurado com os indicativos */
        return [
            'totalMes' => number_format($totalMes, 2, ',', '.'),
            'nDeVendasMes' => $nDeVendasMes,
            'valorMedioVendasMes' => number_format($valorMedioVendasMes, 2, ',', '.'),
            'totalVendasDia' => number_format($totalVendasDia, 2, ',', '.'),
            'countAprovadas' => $aprovadasCompDiaADia,
            'countNegadas' => $negadasCompDiaADia,
            'dias' => $dias,
            'meses' => $mesesReport['meses'],
            'producaoMesAMesAp' => $producaoMesAMesAp,
            'producaoMesAMesNeg' => $producaoMesAMesNeg,
            'producaoMesAMesSeg' => $producaoMesAMesSeg,
            'filiaisBeta' => $filiaisBetaId,
            'filiaisNr' => $filiaisId,
            'aprovadasValorCompDiaADia' => $aprovadasValorCompDiaADia
        ];
    }

    public function producaoSeguro( $start, $length, $draw, $de, $ate, $filial ){

        $filiaisId          = [];
        $dados              = [];
        $count              = 0;
        $query              = "";
        $queryTotais        = "";

        $util               = new Util;

        if ( empty( $filial ) || $filial === 0 ) {

            foreach (Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro) {
                $filiaisId[] = $hasParceiro->parceiro->id;
            }
        }

        else {

            for ( $i = 0; $i < sizeof($filial); $i++ ){
                $filialNR = Filial::model()->findByPk($filial[$i]);
                $filiaisId[] = $filialNR->id;
            }
        }

        $filiaisId               = join(',', $filiaisId);

        $query                   = " SELECT * FROM( ";
        $query                  .= " SELECT 'n' as ambiente, nP.id as 'PropostaId', nP.codigo ";
        $query                  .= " FROM nordeste2.Proposta AS nP ";
        $query                  .= " INNER JOIN nordeste2.Analise_de_Credito AS nAC ON nAC.id = nP.Analise_de_Credito_id ";
        $query                  .= " WHERE nAC.Filial_id IN($filiaisId) ";
        $query                  .= " AND nP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";
        $query                  .= " AND nP.segurada AND nP.habilitado AND nAC.habilitado AND nP.Status_Proposta_id IN(2,7) ";

        $query                  .= " UNION ";
        $query                  .= " SELECT 'b' as ambiente, bP.id as 'PropostaId', bP.codigo ";
        $query                  .= " FROM beta.Proposta AS bP ";
        $query                  .= " INNER JOIN beta.Analise_de_Credito AS bAC ON bAC.id = bP.Analise_de_Credito_id ";
        $query                  .= " WHERE bAC.Filial_id IN($filiaisId) ";
        $query                  .= " AND bP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";
        $query                  .= " AND bP.segurada AND bP.habilitado AND bAC.habilitado AND bP.Status_Proposta_id IN(2,7) ";
        $query                  .= " ) AS QRY ";

        $count                   = count( Yii::app()->db->createCommand($query)->queryAll() );
        $query                  .= " LIMIT $start, $length ";

        $queryTotais             = " SELECT SUM(valor) as valor, SUM(siv) as siv, SUM(civ) as civ, SUM(tfinciv) as tfinciv, SUM(tfinsiv) as tfinsiv FROM(  ";
        $queryTotais             .= " SELECT ";
        $queryTotais             .= " SUM(CASE WHEN V.id IS NULL THEN 0 ELSE IV.preco END) AS 'civ', ";
        $queryTotais             .= " SUM(CASE WHEN V.id IS NULL THEN (((P.valor-P.valor_entrada)/100)*5) WHEN IV.id IS NULL THEN (((P.valor-P.valor_entrada)/100)*5) ELSE 0 END) AS 'siv', ";
        $queryTotais             .= " SUM(CASE WHEN V.id IS NULL THEN 0 ELSE ((P.valor-P.valor_entrada)-IV.preco) END) AS 'tfinciv', ";
        $queryTotais             .= " SUM(CASE WHEN V.id IS NULL THEN ((P.valor-P.valor_entrada)-(((P.valor-P.valor_entrada)/100)*5)) ELSE 0 END) AS 'tfinsiv', ";
        $queryTotais             .= " SUM(P.valor-P.valor_entrada) AS 'valor' ";
        $queryTotais             .= " FROM nordeste2.Proposta AS P ";
        $queryTotais             .= " INNER JOIN nordeste2.Analise_de_Credito AS AC ON AC.id = P.Analise_de_Credito_id ";
        $queryTotais             .= " LEFT JOIN nordeste2.Venda AS V ON V.Proposta_id = P.id ";
        $queryTotais             .= " LEFT JOIN nordeste2.Item_da_Venda AS IV ON IV.Venda_id = V.id AND IV.Item_do_Estoque_id = 1 ";
        $queryTotais             .= " WHERE ";
        $queryTotais            .= "  P.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";
        $queryTotais             .= " AND P.segurada AND P.habilitado AND AC.habilitado AND P.Status_Proposta_id IN(2,7) ";
        $queryTotais             .= " AND AC.Filial_id IN($filiaisId) ";
        $queryTotais             .= " UNION ";
        $queryTotais             .= " SELECT ";
        $queryTotais             .= " SUM(CASE WHEN bV.id IS NULL THEN 0 ELSE bIV.preco END) AS 'civ', ";
        $queryTotais             .= " SUM(CASE WHEN bV.id IS NULL THEN (((bP.valor-bP.valor_entrada)/100)*5) WHEN bIV.id IS NULL THEN (((bP.valor-bP.valor_entrada)/100)*5) ELSE 0 END) AS 'siv', ";
        $queryTotais             .= " SUM(CASE WHEN bV.id IS NULL THEN 0 ELSE ((bP.valor-bP.valor_entrada)-bIV.preco) END) AS 'tfinciv', ";
        $queryTotais             .= " SUM(CASE WHEN bV.id IS NULL THEN ((bP.valor-bP.valor_entrada)-(((bP.valor-bP.valor_entrada)/100)*10)) ELSE 0 END) AS 'tfinsiv', ";
        $queryTotais             .= " SUM(bP.valor-bP.valor_entrada) AS 'valor' ";
        $queryTotais             .= " FROM beta.Proposta AS bP ";
        $queryTotais             .= " INNER JOIN beta.Analise_de_Credito AS bAC ON bAC.id = bP.Analise_de_Credito_id ";
        $queryTotais             .= " LEFT JOIN beta.Venda AS bV ON bV.Proposta_id = bP.id ";
        $queryTotais             .= " LEFT JOIN beta.Item_da_Venda AS bIV ON bIV.Venda_id = bV.id AND bIV.Item_do_Estoque_id = 1 ";
        $queryTotais             .= " WHERE ";
        $queryTotais            .= "  bP.data_cadastro BETWEEN '" . $util->view_date_to_bd($de) . " 00:00:00' AND '" . $util->view_date_to_bd($ate) . " 23:59:59' ";
        $queryTotais             .= " AND bP.segurada AND bP.habilitado AND bAC.habilitado AND bP.Status_Proposta_id IN(2,7) ";
        $queryTotais             .= " AND bAC.Filial_id IN($filiaisId) ";
        $queryTotais             .= " ) as totais ";

        $totais                 =   Yii::app()->db->createCommand($queryTotais)->queryRow();

        foreach ( Yii::app()->db->createCommand($query)->queryAll() as $r ){

            if( $r['ambiente'] == 'n' ){
                $proposta           = Proposta::model()->findByPk($r['PropostaId']);
            }
            else{
                $proposta           = PropostaBeta::model()->findByPk($r['PropostaId']);
            }

            $dados[]            = [
                "codigo"        => $proposta->codigo,
                "filial"        => mb_strtoupper($proposta->analiseDeCredito->filial->getConcat()           ),
                "data_cadastro" => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)           ),
                "crediarista"   => mb_strtoupper($proposta->analiseDeCredito->usuario->nome_utilizador      ),
                "v_inicial"     => "R$ ".number_format($proposta->valor - $proposta->valor_entrada, 2, ',', ''    ),
                "v_seguro"      => "R$ ".number_format($proposta->calcularValorDoSeguro(), 2, ',', ''             ),
                "v_financiado"  => "R$ ".number_format((($proposta->valor - $proposta->valor_entrada)+  $proposta->calcularValorDoSeguro()), 2, ',', ''    ),
            ];
        }

        return[
            'draw'              => $draw,
            'data'              => $dados,
            'recordsTotal'      => sizeof($dados),
            'recordsFiltered'   => $count,
            'totalF'            => "R$ " . number_format($totais['civ']+$totais['siv']+$totais['valor'], 2, ',', '.'),
            'totalS'            => "R$ " . number_format($totais['civ']+$totais['siv'], 2, ',', '.'),
            'totalSoli'         => "R$ " . number_format($totais['valor'], 2, ',', '.'),
        ];
    }
}
