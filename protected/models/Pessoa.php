<?php

class Pessoa extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'Pessoa';
    }

    public function rules() {
        return array(
                array('Contato_id, nome, Estado_Civil_id', 'required'),
                array('Contato_id, habilitado, naturalidade_cidade, Estado_Civil_id', 'numerical', 'integerOnly' => true),
                array('nome', 'length', 'max' => 150),
                array('razao_social', 'length', 'max' => 200),
                array('data_cadastro_br', 'length', 'max' => 50),
                array('sexo', 'length', 'max' => 5),
                array('naturalidade', 'length', 'max' => 4),
                array('nacionalidade', 'length', 'max' => 100),
                array('estado_civil', 'length', 'max' => 45),
                array('nascimento, data_cadastro', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('id, Contato_id, nome, razao_social, nascimento, habilitado, data_cadastro, data_cadastro_br, sexo, naturalidade, naturalidade_cidade, nacionalidade, estado_civil, Estado_Civil_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'clientes' => array(self::HAS_MANY, 'Cliente', 'Pessoa_id'),
                'dadosProfissionaises' => array(self::HAS_MANY, 'DadosProfissionais', 'Pessoa_id'),
                'familiars' => array(self::HAS_MANY, 'Familiar', 'Pessoa_id'),
                'fornecedors' => array(self::HAS_MANY, 'Fornecedor', 'Pessoa_id'),
                'naturalidadeCidade' => array(self::BELONGS_TO, 'Cidade', 'naturalidade_cidade'),
                'usuarios' => array(self::HAS_MANY, 'Usuario', 'Pessoa_id'),
                'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id', 'condition' => 'contato.habilitado'),
        );
    }

    public function getRG() {

        $pessoaHasDocumento = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id);

        if (count($pessoaHasDocumento) > 0) {
            foreach ($pessoaHasDocumento as $phd) {

                $doc = Documento::model()->findByPk($phd->Documento_id);

                if ($doc != NULL) {
                    if ($doc->tipoDocumento->id == 2) {
                        return $doc;
                    }
                }
            }
        } else {
            return NULL;
        }
    }

    public function getCPF() {

        $pessoaHasDocumento = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id);

        if (count($pessoaHasDocumento) > 0) {
            foreach ($pessoaHasDocumento as $phd) {

                $doc = Documento::model()->findByPk($phd->Documento_id);

                if ($doc != NULL) {
                    if ($doc->tipoDocumento->id == 1)
                        return $doc;
                }
            }
        }

        return null;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'id' => 'ID',
                'Contato_id' => 'Contato',
                'nome' => 'Nome',
                'razao_social' => 'Razao Social',
                'nascimento' => 'Nascimento',
                'habilitado' => 'Habilitado',
                'data_cadastro' => 'Data Cadastro',
                'data_cadastro_br' => 'Data Cadastro Br',
                'sexo' => 'Sexo',
                'naturalidade' => 'Naturalidade',
                'naturalidade_cidade' => 'Naturalidade Cidade',
                'nacionalidade' => 'Nacionalidade',
                'estado_civil' => 'Estado Civil',
                'Estado_Civil_id' => 'Estado Civil',
        );
    }

    public function novo($Pessoa) {
        $nPessoa = new Pessoa;
        $nPessoa->attributes = $Pessoa;
        $nPessoa->save();

        return $nPessoa;
    }

    public function getEndereco() {

        $pessoaHasEndereco = PessoaHasEndereco::model()->find('Pessoa_id = ' . $this->id);
        if($pessoaHasEndereco != null){
            return Endereco::model()->findByPk($pessoaHasEndereco->Endereco_id);
        }else{
            return null;
        }
    }

}
