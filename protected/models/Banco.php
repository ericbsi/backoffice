<?php

/**
 * This is the model class for table "Banco".
 *
 * The followings are the available columns in table 'Banco':
 * @property integer $id
 * @property string $nome
 * @property string $codigo
 *
 * The followings are the available model relations:
 * @property CnabDetalhe[] $cnabDetalhes
 * @property CnabHeader[] $cnabHeaders
 * @property CnabTrailer[] $cnabTrailers
 * @property DadosBancarios[] $dadosBancarioses
 * @property Proposta[] $propostas
 * @property TipoCNAB[] $tipoCNABs
 */
class Banco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Banco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Banco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome', 'length', 'max'=>45),
			array('codigo', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome, codigo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cnabDetalhes' => array(self::HAS_MANY, 'CnabDetalhe', 'Banco_id'),
			'cnabHeaders' => array(self::HAS_MANY, 'CnabHeader', 'Banco_id'),
			'cnabTrailers' => array(self::HAS_MANY, 'CnabTrailer', 'Banco_id'),
			'dadosBancarioses' => array(self::HAS_MANY, 'DadosBancarios', 'Banco_id'),
			'propostas' => array(self::HAS_MANY, 'Proposta', 'Banco_id'),
			'tipoCNABs' => array(self::HAS_MANY, 'TipoCNAB', 'Banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'codigo' => 'Codigo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('codigo',$this->codigo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}