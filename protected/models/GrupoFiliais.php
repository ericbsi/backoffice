<?php

/**
 * This is the model class for table "GrupoFiliais".
 *
 * The followings are the available columns in table 'GrupoFiliais':
 * @property integer $id
 * @property string $nome_fantasia
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property NucleoFiliais[] $nucleoFiliaises
 */
class GrupoFiliais extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GrupoFiliais the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'GrupoFiliais';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_fantasia, data_cadastro', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('nome_fantasia', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome_fantasia, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nucleoFiliaises' => array(self::HAS_MANY, 'NucleoFiliais', 'GrupoFiliais_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_fantasia' => 'Nome Fantasia',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome_fantasia',$this->nome_fantasia,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function findAllAdmFiliais()
        {
            
            $grupos = [];
            
            $sql    = "SELECT   DISTINCT    GF.id               AS idGrupo                                                      "
                    . "FROM                 GrupoFiliais        AS GF                                                           "
                    . "INNER    JOIN        NucleoFiliais       AS NF       ON  NF.habilitado AND NF.GrupoFiliais_id    = GF.id "
                    . "INNER    JOIN        Filial              AS F        ON   F.habilitado AND  F.NucleoFiliais_id   = NF.id "
                    . "INNER    JOIN        AdminHasParceiro    AS AP       ON  AP.habilitado AND AP.Parceiro           =  F.id "
                    . "WHERE                                                    GF.habilitado AND AP.Administrador      =       " . Yii::app()->session["usuario"]->id;
            
            $dados = Yii::app()->db->createCommand($sql)->queryAll();
            
            foreach ($dados as $d)
            {
                
                $grupos[] = GrupoFiliais::model()->findByPk($d["idGrupo"]);
                
            }
            
            return $grupos;
            
        }
        
}