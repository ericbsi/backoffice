<?php


class NivelRoleHasRoleHasFuncao extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'Nivel_Role_has_Role_has_Funcao';
	}


	public function rules()
	{

		return array(
			array('Nivel_Role_id, Role_has_Funcao_id, habilitado', 'required'),
			array('Nivel_Role_id, Role_has_Funcao_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, Nivel_Role_id, Role_has_Funcao_id, habilitado', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'nivelRole' => array(self::BELONGS_TO, 'NivelRole', 'Nivel_Role_id'),
			'roleHasFuncao' => array(self::BELONGS_TO, 'RoleHasFuncao', 'Role_has_Funcao_id'),
			'nivelRoleHasRoleHasFuncaoHasStyleConfigs' => array(self::HAS_MANY, 'NivelRoleHasRoleHasFuncaoHasStyleConfig', 'Nivel_Role_has_Role_has_Funcao_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Nivel_Role_id' => 'Nivel Role',
			'Role_has_Funcao_id' => 'Role Has Funcao',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($limit)
    {
        $criteria = new CDbCriteria;

        /* Campos texto, usando LIKE */
        $criteria->addSearchCondition('controller', $this->controller);
        $criteria->addSearchCondition('nome', $this->nome);
        $criteria->addSearchCondition('template', $this->template);
        $criteria->addSearchCondition('descricao', $this->descricao);

        $criteria->compare('habilitado', $this->habilitado);
        $criteria->offset = $limit['start'];
        $criteria->limit = $limit['limit'];

        return RoleHasFuncao::model()->findAll($criteria);
    }


	public function dataTable($draw, $limit, $nivelId)
    {
        $nivelRole 						= NivelRole::model()->findByPk($nivelId);
        $rhf                    		= new RoleHasFuncao;
        $rhf->Role_id 					= $nivelRole->role->id;
        $funcoesCollection      		= [];
        $recordsTotal           		= count(RoleHasFuncao::model()->findAll('Role_id = ' . $nivelRole->role->id . ' AND habilitado'));

        $i                      		= 1;
        
        foreach ($rhf->search($limit) as $funcao)
        {   
            $checked             		= "";
            $checkedExibir       		= "";
            $hab                    	= 1;
            $eHab                		= 1;

            $nivelRoleHasRoleHasFuncao 	= NivelRoleHasRoleHasFuncao::model()->find('habilitado AND Nivel_Role_id = ' . $nivelRole->id . ' AND Role_has_Funcao_id = ' . $funcao->id);
           	
            if( $nivelRoleHasRoleHasFuncao !== null )
            {
                if( $nivelRoleHasRoleHasFuncao->show_menu )
                {
                	$checkedExibir = "checked";
                    $eHab          = 0;
                }

                $checked    = "checked";
                $hab        = 0;
            }
            
            $funcoesCollection[] =  [
            	'checkFilial'   => '<div class="checkbox checkbox-primary"><input data-hab="'.$hab.'" data-role-has-funcao-id="'.$funcao->id.'" data-nivel-id="'.$nivelRole->id.'" class="input_check" '.$checked.' id="checkbox'.$i.'" type="checkbox"><label for="checkbox'.$i.'"></label></div>',
                'exibirMenu'    => '<div class="checkbox checkbox-primary"><input data-hab="'.$eHab.'" data-role-has-funcao-id="'.$funcao->id.'" data-nivel-id="'.$nivelRole->id.'" class="input_check_exibir" '.$checkedExibir.' id="checkbox_'.$i.'" type="checkbox"><label for="checkbox_'.$i.'"></label></div>',
                'label'         => mb_strtoupper($funcao->funcao->label),
                'descricao'     => mb_strtoupper($funcao->funcao->descricao),
            ];

            $i++;
        }

        return ['collection'    =>  $funcoesCollection, 'total'  => $recordsTotal];
    }

    public function nivelRoleRoleHasFuncao( $nivel_id, $funcao_id, $hab )
    {
        $nrhrhf                                 = NivelRoleHasRoleHasFuncao::model()->find('Nivel_Role_id = ' . $nivel_id . ' AND Role_has_Funcao_id = ' . $funcao_id);

        $result                                 = [];

        if( $nrhrhf                             === NULL )
        {
            $nrhrhf                             = new NivelRoleHasRoleHasFuncao;
            $nrhrhf->Nivel_Role_id              = $nivel_id;
            $nrhrhf->Role_has_Funcao_id         = $funcao_id;
            $nrhrhf->show_menu                  = 0;
            $nrhrhf->habilitado                 = 1;
            $nrhrhf->save();

            /*
            if( ! )
            {
                ob_start();
                var_dump($nrhrhf->getErrors());
                $result[] = ob_get_clean();
            }
            */
        }
        else
        {
            $nrhrhf->habilitado                 = $hab;
            $nrhrhf->update();
        }

        return $result;
    }
}