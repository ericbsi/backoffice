<?php

class NivelRole extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NivelRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Nivel_Role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Role_id, nome, descricao', 'required'),
			array('Role_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Role_id, nome, descricao, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function persist( $NivelRole, $Nivel_Role_id )
	{
		$retorno['hasErrors']       		= false;
        $nr                         		= new NivelRole;
    		
        $role 								= Role::model()->findByPk( $NivelRole['Role_id'] );
        
        /* Se houver um id, trata-se de uma atualização nos dados */
        if ($Nivel_Role_id 					!== '0')
        {
            $nr = NivelRole::model()->findByPk($Nivel_Role_id);
        }

        /* Se não, trata-se de um novo registro */ 
        else
        {
            $nr->Status_Nivel_Role_id    	= 1;
        }

        $nr->attributes          			= $NivelRole;

        /* Se houver erro ao salvar/atualizar, configura o retorno de acordo */
        if (!$nr->save())
        {
            $retorno['hasErrors'] 			= true;
            $retorno['errors'] 				= $nr->getErrors();
        }

        $retorno['countRoleNiveis']			= count($role->getNiveisDeAcesso(1,[1,2]));

        return $retorno;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'role'					        => array(self::BELONGS_TO, 	'Role', 				        'Role_id', 'on' 				=> 'role.habilitado = 1'			        ),
			'nivelRoleConfigs' 		        => array(self::HAS_ONE, 	'NivelRoleConfig', 		        'Nivel_Role_id'	 													        ),
            'nivelRoleHasFuncaos'           => array(self::HAS_MANY,    'NivelRoleHasFuncao',           'Nivel_Role_id', 'condition'    => 'nivelRoleHasFuncaos.habilitado'         ),
            'nivelRoleHasRoleHasFuncaos'    => array(self::HAS_MANY,    'NivelRoleHasRoleHasFuncao',    'Nivel_Role_id', 'condition'    => 'nivelRoleHasRoleHasFuncaos.habilitado'  ),
            'usuarioRoles'                  => array(self::HAS_MANY,    'UsuarioRole',                  'Nivel_Role_id', 'on'           => 'usuarioRoles.habilitado = 1'            ),
            'status'                        => array(self::BELONGS_TO,  'StatusNivelRole',              'Status_Nivel_Role_id'                                                      ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'             => 'ID',
			'Role_id'        => 'Role',
            'nome'           => 'Nome',
			'descricao'      => 'Descrição',
			'habilitado'     => 'Habilitado',
		);
	}

	
	/**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model Funcao]
     * @author Eric
     * @version 1.0
    */
    public function search($limit)
    {
        $criteria = new CDbCriteria;

        /* Campos texto, usando LIKE 
        $criteria->addSearchCondition('controller', $this->controller);
        $criteria->addSearchCondition('nome', $this->nome);
        $criteria->addSearchCondition('template', $this->template);
        $criteria->addSearchCondition('descricao', $this->descricao);
        */
        $criteria->addSearchCondition('nome',           $this->nome             );
        $criteria->addSearchCondition('descricao',      $this->descricao        );
        $criteria->addInCondition('Role_id', 	[$this->Role_id 	],	'AND'   );
        $criteria->addInCondition('habilitado', [$this->habilitado 	], 	'AND'   );
        $criteria->offset 	= $limit['start'];
        $criteria->limit 	= $limit['limit'];

        return NivelRole::model()->findAll($criteria);
    }

    /**
     * Monta a estrutura necessária para apresentação em uma jQuery DataTable
     * @param INT $draw - variável padrão do plugin dataTables
     * @param ARRAY $limit 
     * @return [json result]
     * @author Eric
     * @version 1.0
    */
    public function dataTable($draw, $limit, $Role_id)
    {
        $niv                    = new NivelRole;
        $niv->Role_id           = $Role_id;
        $niveisCollection      	= [];
        $recordsTotal           = count(NivelRole::model()->findAll( 'Role_id = ' . $Role_id ));

        foreach ($niv->search($limit) as $nivel)
        {
            $niveisCollection[] =  [
                'nome'         	=> mb_strtoupper($nivel->nome           ),
                'descricao'     => mb_strtoupper($nivel->descricao      ),
                'status'        => mb_strtoupper($nivel->status->nome   ),
                'idNivel'     	=> $nivel->id,
                'btn_editar'    => '<a data-form-wrapper="#portlet-form" data-source="/nivelRole/loadFormEdit/" data-entity-id="' . $nivel->id . '" data-form-load="#form-add-novo-nivel" data-collapse-div="#bg-default" class="btn btn-credshow-orange waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-edit"></i></a>',
            ];
        }

        return ['collection'    =>  $niveisCollection, 'total'  => $recordsTotal];
    }

    /**
     * Retorna um array com as informações necessárias para carregar um formulário de edição
     * de um obejto 
     * @param INT $id
     * @return [arr result]
     * @author Eric
     * @version 1.0
     */
    public function loadFormEdit($id)
    {
        $nr                                 = NivelRole::model()->findByPk($id);

        return                                 [
            'fields'                        => [
                'id'                        => [
                    'value'                 => $nr->id,
                    'type'                  => 'text',
                    'input_bind'            => 'nivelrole_id',
                ],
                'nome'                      => [
                    'value'                 => mb_strtoupper($nr->nome),
                    'type'                  => 'text',
                    'input_bind'            => 'nivelrole_nome',
                ],
                'descricao'                 => [
                    'value'                 => mb_strtoupper($nr->descricao),
                    'type'                  => 'text',
                    'input_bind'            => 'nivelrole_descricao',
                ],
                'status'                    => [
                    'value'                 => mb_strtoupper($nr->status->nome),
                    'type'                  => 'text',
                    'input_bind'            => 'nivelrole_status',
                ],
            ]
        ];
    }

}