<?php

/**
 * This is the model class for table "Pessoa_has_Documento".
 *
 * The followings are the available columns in table 'Pessoa_has_Documento':
 * @property integer $id
 * @property integer $Documento_id
 * @property integer $Pessoa_id
 * @property string $data_cadastro
 * @property string $data_cadastro_br
 * @property integer $habilitado
 */
class PessoaHasDocumento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PessoaHasDocumento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Pessoa_has_Documento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Documento_id, Pessoa_id', 'required'),
			array('Documento_id, Pessoa_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro_br', 'length', 'max'=>50),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Documento_id, Pessoa_id, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Documento_id' => 'Documento',
			'Pessoa_id' => 'Pessoa',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Cadastro Br',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Documento_id',$this->Documento_id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}