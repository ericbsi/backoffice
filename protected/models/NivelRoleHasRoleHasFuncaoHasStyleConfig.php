<?php

/**
 * This is the model class for table "Nivel_Role_has_Role_has_Funcao_has_Style_Config".
 *
 * The followings are the available columns in table 'Nivel_Role_has_Role_has_Funcao_has_Style_Config':
 * @property integer $id
 * @property integer $Nivel_Role_has_Role_has_Funcao_id
 * @property integer $Style_Config_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property NivelRoleHasRoleHasFuncao $nivelRoleHasRoleHasFuncao
 * @property StyleConfig $styleConfig
 */
class NivelRoleHasRoleHasFuncaoHasStyleConfig extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NivelRoleHasRoleHasFuncaoHasStyleConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Nivel_Role_has_Role_has_Funcao_has_Style_Config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nivel_Role_has_Role_has_Funcao_id, Style_Config_id, habilitado', 'required'),
			array('Nivel_Role_has_Role_has_Funcao_id, Style_Config_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Nivel_Role_has_Role_has_Funcao_id, Style_Config_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nivelRoleHasRoleHasFuncao' => array(self::BELONGS_TO, 'NivelRoleHasRoleHasFuncao', 'Nivel_Role_has_Role_has_Funcao_id'),
			'styleConfig' => array(self::BELONGS_TO, 'StyleConfig', 'Style_Config_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Nivel_Role_has_Role_has_Funcao_id' => 'Nivel Role Has Role Has Funcao',
			'Style_Config_id' => 'Style Config',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Nivel_Role_has_Role_has_Funcao_id',$this->Nivel_Role_has_Role_has_Funcao_id);
		$criteria->compare('Style_Config_id',$this->Style_Config_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}