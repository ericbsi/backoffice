<?php

/**
 * This is the model class for table "Style_Config".
 *
 * The followings are the available columns in table 'Style_Config':
 * @property integer $id
 * @property string $class
 * @property string $font_color
 * @property string $icon_color
 * @property string $img_url
 * @property integer $habilitado
 * @property string $css_snnipet
 *
 * The followings are the available model relations:
 * @property NivelRoleHasFuncaoHasStyleConfig[] $nivelRoleHasFuncaoHasStyleConfigs
 */
class StyleConfig extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StyleConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Style_Config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('class, font_color, icon_color, img_url', 'length', 'max'=>100),
			array('css_snnipet', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, class, font_color, icon_color, img_url, habilitado, css_snnipet', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nivelRoleHasFuncaoHasStyleConfigs' => array(self::HAS_MANY, 'NivelRoleHasFuncaoHasStyleConfig', 'Style_Config_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'class' => 'Class',
			'font_color' => 'Font Color',
			'icon_color' => 'Icon Color',
			'img_url' => 'Img Url',
			'habilitado' => 'Habilitado',
			'css_snnipet' => 'Css Snnipet',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('font_color',$this->font_color,true);
		$criteria->compare('icon_color',$this->icon_color,true);
		$criteria->compare('img_url',$this->img_url,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('css_snnipet',$this->css_snnipet,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}