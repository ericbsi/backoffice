<?php

class Usuario extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'Usuario';
    }

    public function rules()
    {

        return array(
            /*array('', 'required'),*/
            array('habilitado, tipo_id, primeira_senha', 'numerical', 'integerOnly' => true),
            array('limite_analise', 'numerical'),
            array('username, tipo', 'length', 'max' => 45),
            array('nome_utilizador', 'length', 'max' => 200),
            array('role', 'length', 'max' => 99),
            array('password', 'length', 'max' => 235),
            array('data_cadastro_br', 'length', 'max' => 50),
            array('data_cadastro, data_ultima_acao', 'safe'),
            array('id, username, nome_utilizador, role, password, data_cadastro, data_cadastro_br, habilitado, tipo, tipo_id, limite_analise, primeira_senha, data_ultima_acao', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'adminHasParceiros'                     => array(self::HAS_MANY,    'AdminHasParceiro', 'Administrador', 'condition' => 'adminHasParceiros.habilitado'),
            'analiseDeCreditos'                     => array(self::HAS_MANY,    'AnaliseDeCredito', 'Usuario_id'),
            'analistaHasPropostaHasStatusPropostas' => array(self::HAS_MANY,    'AnalistaHasPropostaHasStatusProposta', 'Analista_id'),
            'arquivoCnabs'                          => array(self::HAS_MANY,    'ArquivoCnab', 'Usuario_id'),
            'borderos'                              => array(self::HAS_MANY,    'Bordero', 'criadoPor'),
            'cobrancas'                             => array(self::HAS_MANY,    'Cobranca', 'Usuario_id'),
            'conciliacaos'                          => array(self::HAS_MANY,    'Conciliacao', 'Usuario_id'),
            'despesas'                              => array(self::HAS_MANY,    'Despesa', 'Usuario_criacao'),
            'despesas1'                             => array(self::HAS_MANY,    'Despesa', 'Usuario_Baixa'),
            'dialogos'                              => array(self::HAS_MANY,    'Dialogo', 'criado_por'),
            'empresaHasUsuarios'                    => array(self::HAS_MANY,    'EmpresaHasUsuario', 'Usuario_id'),
            'estornoBaixaHasStatusEstornos'         => array(self::HAS_MANY,    'EstornoBaixaHasStatusEstorno', 'Usuario_id'),
            'fichamentoHasStatusFichamentos'        => array(self::HAS_MANY,    'FichamentoHasStatusFichamento', 'Usuario_id'),
            'filialHasUsuarios'                     => array(self::HAS_MANY,    'FilialHasUsuario', 'Usuario_id'),
            'grupoDeAnalistasHasUsuarios'           => array(self::HAS_MANY,    'GrupoDeAnalistasHasUsuario', 'Usuario_id'),
            'lotePagamentos'                        => array(self::HAS_MANY,    'LotePagamento',                    'Usuario_id'),
            'lotePagamentoHasStatusLotePagamentos'  => array(self::HAS_MANY,    'LotePagamentoHasStatusLotePagamento', 'Usuario_id'),
            'mensagems'                             => array(self::HAS_MANY,    'Mensagem',                     'Usuario_id'),
            'negacaoDeCreditos'                     => array(self::HAS_MANY,    'NegacaoDeCredito',             'Analista'),
            'reanalises'                            => array(self::HAS_MANY,    'Reanalise',                    'Analista'),
            'recebimentoDeDocumentacaos'            => array(self::HAS_MANY,    'RecebimentoDeDocumentacao',    'criadoPor'),
            'registroDePendenciases'                => array(self::HAS_MANY,    'RegistroDePendencias',         'criadoPor'),
            'resgatePontoses'                       => array(self::HAS_MANY,    'ResgatePontos',                'Usuario_id'),            
            'sequencias'                            => array(self::HAS_MANY,    'Sequencia',                    'Usuario_id'),
            'solicitacaoDeCancelamentos'            => array(self::HAS_MANY,    'SolicitacaoDeCancelamento',    'Solicitante'),
            'solicitacaoDeCancelamentos1'           => array(self::HAS_MANY,    'SolicitacaoDeCancelamento',    'AceitePor'),
            'usuarioLeuMensagems'                   => array(self::HAS_MANY,    'UsuarioLeuMensagem',           'Usuario_id'),
            /* novos relacionamentos */
            'mainRole'                              => array(self::HAS_ONE,     'UsuarioRole',  'Usuario_id',   'condition' => 'mainRole.principal = 1 AND mainRole.habilitado = 1'),
            'roles'                                 => array(self::HAS_MANY,    'UsuarioRole',  'Usuario_id',   'condition' => 'roles.principal = 0 AND roles.habilitado = 1'),
            'pessoa'                                => array(self::BELONGS_TO,  'Pessoa',       'Pessoa_id',    'condition' => 'pessoa.habilitado'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'nome_utilizador' => 'Nome Utilizador',
            'role' => 'Role',
            'password' => 'Password',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data Cadastro Br',
            'habilitado' => 'Habilitado',
            'tipo' => 'Tipo',
            'tipo_id' => 'Tipo',
            'limite_analise' => 'Limite Analise',
            'primeira_senha' => 'Primeira Senha',
            'data_ultima_acao' => 'Data Ultima Acao',
        );
    }

    public function autorizado()
    {
        $retorno            = FALSE;

        $funcao             = Funcao::model()->find("controller = '" . Yii::app()->controller->id . "' AND nome = '" . Yii::app()->controller->action->id . "' AND habilitado");

        if($funcao !== NULL)
        {
        
            if(isset($this->mainRole->role->id))
            {
                
                /*Verifica se a Main role do usuario, possui acesso a esta função*/
                $roleHF             = RoleHasFuncao::model()->find("habilitado AND Funcao_id = " . $funcao->id . " AND Role_id = " . $this->mainRole->role->id);

                if ($roleHF !== NULL )
                {
                    /*Se possuir, verifica se o nível possui acesso*/
                    //if (NivelRoleHasFuncao::model()->find("Funcao_id = " . $funcao->id . " AND Nivel_Role_id = " . $this->mainRole->Nivel_Role_id . " AND habilitado ") != NULL)
                    if (NivelRoleHasRoleHasFuncao::model()->find("Role_has_Funcao_id = " . $roleHF->id . " AND Nivel_Role_id = " . $this->mainRole->Nivel_Role_id . " AND habilitado ") != NULL)
                    {
                        $retorno    = TRUE;
                    }
                    
                }

                /* Se, após verificar na role principal, o retorno ainda for negativo, verificar nas outras roles*/
                if (!$retorno)
                {
                    
                    foreach ($this->roles as $role)
                    {
                    $funcao         = Funcao::model()->find("controller = '" . Yii::app()->controller->id . "' AND nome = '" . Yii::app()->controller->action->id . "' AND habilitado");

                    $roleHFs        = RoleHasFuncao::model()->find("habilitado AND Funcao_id = " . $funcao->id . " AND Role_id = " . $role->role->id);

                    if($roleHFs != NULL)
                    {
                        if (NivelRoleHasRoleHasFuncao::model()->find("Role_has_Funcao_id = " . $roleHFs->id . " AND Nivel_Role_id = " . $role->Nivel_Role_id . " AND habilitado ") != NULL)
                        {
                            $retorno    = TRUE;
                            break;
                        }
                        else
                        {
                            $retorno    = FALSE;
                        }
                    }
                    else
                    {
                        $retorno    = FALSE;
                    }

                    /*
                    if (NivelRoleHasRoleHasFuncao::model()->find("Role_has_Funcao_id = " . $roleHF->id . " AND Nivel_Role_id = " . $role->Nivel_Role_id . " AND habilitado ") != NULL)
                    {
                        $retorno    = TRUE;
                    }
                    */
                } 

                }
            
            }
        
        }
        
        return $retorno;
    }

    /**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model Funcao]
     * @author Eric
     * @version 1.0
     */
    public function search($limit)
    {
        $criteria                                   = new CDbCriteria;

        /* Campos texto, usando LIKE */
        $criteria->addSearchCondition('nome_utilizador',    $this->nome_utilizador, ' AND ');
        $criteria->addSearchCondition('username',           $this->username,        ' AND ');
        $criteria->addInCondition('habilitado',             [0,1],                  ' AND ');
        $criteria->offset                                   = $limit['start'];
        $criteria->limit                                    = $limit['limit'];

        return Usuario::model()->findAll($criteria);
    }

    public function persist( $Usuario, $Contato, $Documento, $perfil_principal, $perfis_secundarios = null, $UsuarioId )
    {
        
        $contato                                                    = new Contato;
        $retorno                                                    = ['comErro'=>false];

        $transaction                                                = Yii::app()->db->beginTransaction();

        if( $contato->save() )
        {
            $email                                                  = new Email;
            $email->email                                           = $Contato['email'];
            $email->data_cadastro                                   = date('Y-m-d H:i:s');
            
            if( $email->save() )
            {
                $che                                                = new ContatoHasEmail;
                $che->Contato_id                                    = $contato->id;
                $che->Email_id                                      = $email->id;
                
                if( $che->save() )
                {
                    $pessoa                                         = new Pessoa;
                    $pessoa->nome                                   = $Usuario['nome_utilizador'];
                    $pessoa->Contato_id                             = $contato->id;
                    $pessoa->Estado_Civil_id                        = 1;

                    if( $pessoa->save() )
                    {
                        $usuario                                    = new Usuario;
                        $usuario->nome_utilizador                   = $Usuario['nome_utilizador'];
                        $usuario->username                          = $Usuario['username'];
                        $usuario->password                          = Yii::app()->hasher->hashPassword( $Usuario['password'] );
                        $usuario->Pessoa_id                         = $pessoa->id;
                        $usuario->primeira_senha                    = 1;

                        if( $usuario->save() )
                        {
                            /*Perfil principal*/
                            $nr                                     = NivelRole::model()->findByPk( $perfil_principal );

                            if( $nr !== null )
                            {
                                $ur                                 = new UsuarioRole;
                                $ur->Usuario_id                     = $usuario->id;
                                $ur->Role_id                        = $nr->role->id;
                                $ur->habilitado                     = 1;
                                $ur->principal                      = 1;
                                $ur->Nivel_Role_id                  = $nr->id;
                                $ur->data_cadastro                  = date('Y-m-d H:i:s');

                                if( !$ur->save() )
                                {
                                    $retorno['comErro']             = true;
                                }
                            }

                            /*Perfis secundários*/
                            if( $perfis_secundarios                 !== null )
                            {
                                for ( $i = 0; $i < sizeof($perfis_secundarios); $i++ )
                                {   
                                    $nrs                            = NivelRole::model()->findByPk( $perfis_secundarios[$i] );

                                    if( ($nrs !== null) && ($nr->id !== $nrs->id ) )
                                    {
                                        $urs                        = new UsuarioRole;
                                        $urs->Usuario_id            = $usuario->id;
                                        $urs->Role_id               = $nrs->role->id;
                                        $urs->habilitado            = 1;
                                        $urs->principal             = 0;
                                        $urs->Nivel_Role_id         = $nrs->id;
                                        $urs->data_cadastro         = date('Y-m-d H:i:s');

                                        if( !$urs->save() )
                                        {   
                                            $retorno['comErro']     = true;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            $retorno['comErro']                     = true;
                        }

                    }
                    else
                    {
                        $retorno['comErro']                         = true;
                    }
                }
                else
                {
                    $retorno['comErro']                             = true;
                }
            }
            else
            {
                $retorno['comErro']                                 = true;
            }
        }
        else
        {
            $retorno['comErro']                                     = true;
        }
            
        if( $retorno['comErro'] === true )
        {
            $transaction->rollBack();
        }
        else
        {
            $transaction->commit();
        }

        return $retorno['comErro'];
    }

    public function checkUserNameAvailability($username) {

        $usuario = Usuario::model()->find('username = ' . "'$username'");

        if ($usuario != NULL)
            echo "false";
        else
            echo "true";
    }

    public function dataTable($draw, $limit, $nome_filter, $username_filter)
    {
        $usu                                = new Usuario;
        $usuariosCollection                 = [];
        $recordsTotal                       = count(Usuario::model()->findAll());
        $roles                              = '';

        if(isset($nome_filter) && trim($nome_filter) != '' && $nome_filter != null)
        {
            $usu->nome_utilizador   = $nome_filter;
        }

        if(isset($username_filter) && trim($username_filter) != '' && $username_filter != null)
        {
            $usu->username          = $username_filter; 
        }
        
        foreach ($usu->search($limit) as $usuario)
        {
            $btn_hab_des_css_class           = " btn-success";
            $btn_hab_des_incon               = " glyphicon-ok";
            $user_hab_des                    = 0;

            if( !$usuario->habilitado )
            {
                $btn_hab_des_css_class       = " btn-danger";
                $btn_hab_des_incon           = " glyphicon-ban-circle";
                $user_hab_des                = 1;
            }

            /*Hack para forçar a criação de uma pessoa para cada usuário, caso ainda não tenha*/
            if( $usuario->Pessoa_id         === NULL )
            {   
                $c                          = [];
                $contato                    = Contato::model()->novo($c);

                $pessoa['nome']             = $usuario->nome_utilizador;
                $pessoa['Contato_id']       = $contato->id;
                $pessoa['Estado_Civil_id']  = 1;

                $Pessoa                     = Pessoa::model()->novo( $pessoa );

                $usuario->Pessoa_id         = $Pessoa->id;
                $usuario->update();

            }
            /*FIM*/


            $usuariosCollection[]           =  [
                'nome'                      => mb_strtoupper($usuario->nome_utilizador),
                'username'                  => mb_strtoupper($usuario->username),
                'email'                     => ($usuario->pessoa->contato->getEmail() !== NULL) ? mb_strtoupper($usuario->pessoa->contato->getEmail()->email) : "Sem E-mail",
                //'btn_config'                => '<form method="post" action="/user/configurar/"><input type="hidden" name="RoleId" value="'.$usuario->id.'"><button type="submit" class="btn btn-credshow-blue-one waves-effect waves-light btn-sm btn-load-form"><i class="glyphicon glyphicon-cog"></i></button></form>',
                'btn_hab_des'               => '<button data-user-id="'.$usuario->id.'" data-user-hab-des="'.$user_hab_des.'" type="submit" class="btn btn-hab-des waves-effect waves-light btn-sm '.$btn_hab_des_css_class.'"><i class="glyphicon '.$btn_hab_des_incon.'"></i></button>',
                'idUsuario'                 => $usuario->id,
            ];

            $roles                          = '';
        }

        return ['collection'                =>  $usuariosCollection, 'total'  => $recordsTotal];
    }

}
