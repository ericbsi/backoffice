<?php

/**
 * This is the model class for table "NucleoFiliais_has_Dados_Bancarios".
 *
 * The followings are the available columns in table 'NucleoFiliais_has_Dados_Bancarios':
 * @property integer $NucleoFiliais_id
 * @property integer $Dados_Bancarios_id
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property DadosBancarios $dadosBancarios
 * @property NucleoFiliais $nucleoFiliais
 */
class NucleoFiliaisHasDadosBancariosBeta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NucleoFiliaisHasDadosBancariosBeta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->beta;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'NucleoFiliais_has_Dados_Bancarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NucleoFiliais_id, Dados_Bancarios_id', 'required'),
			array('NucleoFiliais_id, Dados_Bancarios_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('NucleoFiliais_id, Dados_Bancarios_id, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dadosBancarios' => array(self::BELONGS_TO, 'DadosBancariosBeta', 'Dados_Bancarios_id'),
			'nucleoFiliais' => array(self::BELONGS_TO, 'NucleoFiliaisBeta', 'NucleoFiliais_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NucleoFiliais_id' => 'Nucleo Filiais',
			'Dados_Bancarios_id' => 'Dados Bancarios',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NucleoFiliais_id',$this->NucleoFiliais_id);
		$criteria->compare('Dados_Bancarios_id',$this->Dados_Bancarios_id);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}