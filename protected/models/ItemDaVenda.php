<?php

/**
 * This is the model class for table "Item_da_Venda".
 *
 * The followings are the available columns in table 'Item_da_Venda':
 * @property integer $id
 * @property integer $Item_do_Estoque_id
 * @property integer $Venda_id
 * @property integer $seq
 * @property integer $quantidade
 * @property double $desconto
 * @property double $preco
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ItemDoEstoque $itemDoEstoque
 * @property Venda $venda
 */
class ItemDaVenda extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemDaVenda the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_da_Venda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Item_do_Estoque_id, Venda_id, seq, quantidade, desconto, preco', 'required'),
			array('Item_do_Estoque_id, Venda_id, seq, quantidade, habilitado', 'numerical', 'integerOnly'=>true),
			array('desconto, preco', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Item_do_Estoque_id, Venda_id, seq, quantidade, desconto, preco, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemDoEstoque' => array(self::BELONGS_TO, 'ItemDoEstoque', 'Item_do_Estoque_id'),
			'venda' => array(self::BELONGS_TO, 'Venda', 'Venda_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Item_do_Estoque_id' => 'Item Do Estoque',
			'Venda_id' => 'Venda',
			'seq' => 'Seq',
			'quantidade' => 'Quantidade',
			'desconto' => 'Desconto',
			'preco' => 'Preco',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Item_do_Estoque_id',$this->Item_do_Estoque_id);
		$criteria->compare('Venda_id',$this->Venda_id);
		$criteria->compare('seq',$this->seq);
		$criteria->compare('quantidade',$this->quantidade);
		$criteria->compare('desconto',$this->desconto);
		$criteria->compare('preco',$this->preco);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}