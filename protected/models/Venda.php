<?php

/**
 * This is the model class for table "Venda".
 *
 * The followings are the available columns in table 'Venda':
 * @property integer $id
 * @property string $codigo
 * @property string $data_cadastro
 * @property integer $Cliente_id
 * @property integer $habilitado
 * @property integer $NotaFiscal_id
 * @property integer $Proposta_id
 *
 * The followings are the available model relations:
 * @property ItemDaVenda[] $itemDaVendas
 * @property NotaFiscal $notaFiscal
 * @property Proposta $proposta
 * @property VendaHasFormaDePagamentoHasCondicaoDePagamento[] $vendaHasFormaDePagamentoHasCondicaoDePagamentos
 * @property VendaHasNotaFiscal[] $vendaHasNotaFiscals
 */
class Venda extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Venda the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Venda';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro', 'required'),
			array('Cliente_id, habilitado, NotaFiscal_id, Proposta_id', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, codigo, data_cadastro, Cliente_id, habilitado, NotaFiscal_id, Proposta_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemDaVendas' => array(self::HAS_MANY, 'ItemDaVenda', 'Venda_id'),
			'notaFiscal' => array(self::BELONGS_TO, 'NotaFiscal', 'NotaFiscal_id'),
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
			'vendaHasFormaDePagamentoHasCondicaoDePagamentos' => array(self::HAS_MANY, 'VendaHasFormaDePagamentoHasCondicaoDePagamento', 'Venda_id'),
			'vendaHasNotaFiscals' => array(self::HAS_MANY, 'VendaHasNotaFiscal', 'Venda_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'data_cadastro' => 'Data Cadastro',
			'Cliente_id' => 'Cliente',
			'habilitado' => 'Habilitado',
			'NotaFiscal_id' => 'Nota Fiscal',
			'Proposta_id' => 'Proposta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('NotaFiscal_id',$this->NotaFiscal_id);
		$criteria->compare('Proposta_id',$this->Proposta_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}