<?php

class UsuarioRoleController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function filters()
  	{
	    return array(
	      'accessControl', // perform access control for CRUD operations
	      'postOnly + delete' // we only allow deletion via POST request
	    );
  	}

  	public function accessRules()
  	{
	   	return 
	   	[
	   		[
	   			'allow',
	   			'actions' 		=> ['changeUsuarioRole'],
	   			'users' 		=> ['@'],
	   			//'expression'=> 'Yii::app()->session["usuario"]->autorizado()'
	   		],

	   		[
	   			'deny',
	   			'users'	 	=> ['*']
	   		]
	   	];
  	}

	//sobrescreve a função do Controller, classe pai
	public function init(){

	}

	/**
     * Seta um objeto e suas propriedades, e busca registros com as características
     * @param ARRAY $limit 
     * @return [Model Funcao]
     * @author Eric
     * @version 1.0
    */
	public function actionChangeUsuarioRole()
	{
		/*
		* relacionamento de Main ou Secondary
		* 's' => secundário 'p' => principal
		*/
		$tipo 								= $_POST['tipo'];
		$nivelRole 							= NivelRole::model()->find( 'id = ' . $_POST['nivelId'] .' AND habilitado' );
		
		/*Se for p, 'principal', informamos que principal é verdadeiro (1), se não, é falso (0)*/
		$principal 							= ($tipo === 'p') ? 1 : 0; 
		$UsuarioRole 						= [];
		
		/*
			*Não preciso inserir o habilitado nesta busca,
			*pois iremos setar esta propriedade dependendo do post
		*/
		$relacao 							= UsuarioRole::model()->find( 't.Usuario_id = ' . $_POST['usuarioId'] .' AND t.Nivel_Role_id = ' . $_POST['nivelId'] );
		
		/*------------------------------------------------------*/
		/*
		* Se já existir uma relação, seja ela Main ou Secondary, 
		* Apenas setamos 0 ou 1, de acordo com o click do checkbox
		*/
		/*------------------------------------------------------*/
		if( $relacao !== NULL )
		{
			$relacao->habilitado 			= $_POST['hab'];
			$relacao->principal 			= $principal;
			$relacao->update();
		}
		
		/*------------------------------------------------------*/
		/*
		* A relação ainda não existe, portanto, se for habilitar (1)
		* criamos uma nova entrada e persistimos no banco de dados.
		*/
		/*------------------------------------------------------*/
		else
		{
			if( $_POST['hab'] === '1' )
			{
				/*
				* Crio o array que configura um novo objeto,
				* pois nas duas situações, main ou secondary, podemos ter
				* a criação de um novo objeto UsuarioRole
				*/
				$UsuarioRole['Usuario_id' 	]	= $_POST['usuarioId'];
				$UsuarioRole['habilitado' 	] 	= $_POST['hab'];
				$UsuarioRole['Role_id' 		] 	= $nivelRole->role->id;
				$UsuarioRole['Nivel_Role_id'] 	= $nivelRole->id;
				$UsuarioRole['data_cadastro'] 	= date('Y-m-d H:i:s');
				$UsuarioRole['principal'	]	= $principal;
				$relacao 						= UsuarioRole::model()->novo($UsuarioRole);
			}
		}


		/*------------------------------------------------------*/
		/* 
		* Se o tipo for p 'principal', e a ação for de habilitar,
		* caso já tenha algum principal configurado e habilitado, 
		* jogamos este como secundário, pois ele só pode ter um principal
		*/
		/*------------------------------------------------------*/
		if( $tipo === 'p' && $_POST['hab'] === '1')
		{
			$mainRoles 	= UsuarioRole::model()->findAll( 'principal AND t.habilitado AND t.Usuario_id = ' . $_POST['usuarioId'] . ' AND t.id <> ' . $relacao->id );

			foreach( $mainRoles as $mr )
			{
				$mr->principal 	= 0;
				$mr->update();
			}
		}
	}
}