<?php

class BackofficeController extends Controller
{	
	public $layout = '//layouts/ubold';

	public function init()
	{

	}

	public function actions(){

		return [
				
			'captcha'		=>[
				'class'		=>'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			],
			
			'page'			=>[
				'class'		=>'CViewAction',
			],
		];
	}

	public function actionIndex(){

		$this->render('/backoffice/index');

	}
}