<?php

class FidcController extends Controller
{

    public $layout = '';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            /*
              [
              'allow',
              'actions' => ['index', 'gridRecebimentos'],
              'users' => ['@'],
              'expression' => 'Yii::app()->session["usuario"]->autorizado()'
              ],
             */
            [
                'allow',
                'actions' => ['index', 'gridRecebimentos'],
                'users' => ['*'],
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionIndex()
    {
        $this->layout = '//layouts/ubold';
        $this->render('index');
    }

    public function actionGridRecebimentos()
    {

        $util = new Util;

        $dados = [];
        

        //$query  = " SELECT LPAD(Pa.id, 25, '0') AS 'IdParcela', F.cnpj AS 'CNPJ', F.nome_fantasia AS 'Cedente', RG.label AS 'NossoNumero', ";
        $query = " SELECT LPAD(Pa.id, 25, '0') AS 'IdParcela', F.cnpj AS 'CNPJ', F.nome_fantasia AS 'Cedente', SUBSTRING(CONCAT(NF.documento,'/', LPAD(Pa.seq,2,'0')),3) AS 'NossoNumero', ";
        $query .= " Pe.nome AS 'Sacado', Pa.valor AS 'valor_nominal', Pa.valor_atual AS 'valor_atual', Ba.data_da_baixa AS data_baixa, ";
        $query .= " Pa.vencimento as 'vencimento', D.numero AS CPF, "
                . " CASE WHEN Ba.id IS NULL THEN '1' ELSE '2' END AS BAIXA ";
        $query .= " FROM        nordeste2.Parcela                AS Pa ";
        $query .= " INNER JOIN  nordeste2.Titulo                 AS T    ON   T.id =  Pa.Titulo_id               AND   T.habilitado ";
        $query .= " INNER JOIN  nordeste2.Proposta               AS P    ON   P.id =   T.Proposta_id             AND   P.habilitado ";
        $query .= " INNER JOIN  nordeste2.Venda                  AS V    ON   P.id =   V.Proposta_id             AND   V.habilitado ";
        $query .= " INNER JOIN  nordeste2.Venda_has_Nota_Fiscal  AS Vnf  ON   V.id = Vnf.Venda_id                AND Vnf.habilitado ";
        $query .= " INNER JOIN  nordeste2.Nota_Fiscal            AS NF   ON  NF.id = Vnf.Nota_Fiscal_id          AND  NF.habilitado ";
        $query .= " INNER JOIN  nordeste2.Analise_de_Credito     AS AC   ON  AC.id =   P.Analise_de_Credito_id   AND  AC.habilitado ";
        $query .= " INNER JOIN  nordeste2.Cliente                AS Cli  ON Cli.id =  AC.Cliente_id              AND Cli.habilitado ";
        $query .= " INNER JOIN  nordeste2.Pessoa                 AS Pe   ON  Pe.id = Cli.Pessoa_id               AND  Pe.habilitado ";
        $query .= " INNER JOIN  nordeste2.Pessoa_has_Documento   AS PhD  ON  Pe.id = PhD.Pessoa_id               AND PhD.habilitado ";
        $query .= " INNER JOIN  nordeste2.Documento              AS D    ON   D.id = PhD.Documento_id            AND   D.habilitado AND D.Tipo_documento_id = 1 ";
        $query .= " INNER JOIN  nordeste2.Filial                 AS F    ON   F.id =  AC.Filial_id                                  ";
        $query .= " INNER JOIN  nordeste2.Ranges_gerados         AS RG   ON  Pa.id =  RG.Parcela_id                                 ";
        $query .= " LEFT  JOIN  nordeste2.Baixa                  AS Ba   ON  Pa.id =  Ba.Parcela_id              AND  Ba.baixado    ";
        $query .= " WHERE Pa.habilitado AND Pa.vencimento BETWEEN '" . $util->view_date_to_bd($_POST['data_de']) . "' AND '" . $util->view_date_to_bd($_POST['data_ate']) . "' "
                . " AND P.Financeira_id IN (10,11) AND P.Status_Proposta_id IN(2,7) AND  T.NaturezaTitulo_id = 1";

        /* Pagos */
        if ($_POST['situacao'] === '1')
        {
            $query .= " AND Ba.id IS NOT NULL ";
        }

        /* Em aberto */ else if ($_POST['situacao'] === '2')
        {
            $query .= " AND Ba.id IS NULL ";
        }

        $query .= " ORDER BY Pa.vencimento ASC ";

        $queryExport = $query;

        $total = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= " LIMIT " . $_POST['start'] . ", " . $_POST['length'];

        foreach (Yii::app()->db->createCommand($query)->queryAll() as $r)
        {
            $situacao = 'A VENCER';

            if ($r["BAIXA"] == "2")
            {
                $situacao = 'PAGO';
            } else if ($r['vencimento'] < date('Y-m-d'))
            {
                $situacao = 'VENCIDO';
            }
            
            if($r['data_baixa'] == null){
                $data_baixa = '';
            }else{
                $data_baixa = $util->bd_date_to_view($r['data_baixa']);
            }

            $dados[] = [
                "cpf" => $r['CPF'],
                "sacado" => mb_strtoupper($r['Sacado']),
                "nosso_numero" => $r['IdParcela'],
                "n_documento" => $r['NossoNumero'],
                "tipo_recebivel" => 'DUPLICATA',
                "valor_nominal" => 'R$ ' . number_format($r['valor_nominal'], 2, ',', '.'),
                "valor_atual" => 'R$ ' . number_format($r['valor_atual'], 2, ',', '.'),
                "data_vencimento" => $util->bd_date_to_view($r['vencimento']),
                "data_baixado" =>  $data_baixa,
                "situacao_recebivel" => $situacao,
                
            ];
        }

        echo json_encode([
            'draw' => $_POST['draw'],
            'recordsFiltered' => $total,
            'recordsTotal' => 10,
            'data' => $dados,
            'queryXlsExport' => $queryExport
        ]);
    }

}
