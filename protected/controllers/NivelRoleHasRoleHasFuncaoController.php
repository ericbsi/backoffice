<?php

class NivelRoleHasRoleHasFuncaoController extends Controller
{

	public $layout = '';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' 	=> ['dataTable', 'nivelRoleRoleHasFuncao', 'exibirMenu'],
                'users' 	=> ['@']
                //'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionDataTable()
    {
        /*
            Se esta variável for preenchida via POST OU GET, e for diferente de '0', será montada uma grid com checkboxes 
            que associam uma função a um nível de acesso
        */

        $funcoesCollection      = NivelRoleHasRoleHasFuncao::model()->dataTable($_POST['draw'], [ 'start' => $_POST['start'], 'limit' => $_POST['length']], $_POST['nivelId']);

        echo json_encode([
            'draw'              => $_POST['draw'                            ],
            'data'              => $funcoesCollection['collection'          ],
            'recordsTotal'      => count($funcoesCollection['collection'    ]),
            'recordsFiltered'   => $funcoesCollection['total'               ]
        ]);
    }
    
    public function actionNivelRoleRoleHasFuncao()
    {
        echo json_encode( NivelRoleHasRoleHasFuncao::model()->nivelRoleRoleHasFuncao( $_POST['nivelId'], $_POST['funcaoId'], $_POST['hab'] ) );
    }


    public function actionExibirMenu()
    {
        $nrhrhf                   = NivelRoleHasRoleHasFuncao::model()->find( 'Role_has_Funcao_id = ' . $_POST['funcaoId'] . ' AND Nivel_Role_id = ' . $_POST['nivelId'] );

        if( $nrhrhf               !== NULL )
        {
            $nrhrhf->show_menu    = $_POST['hab'];
            $nrhrhf->update();

            if( NivelRoleHasRoleHasFuncaoHasStyleConfig::model()->find('habilitado AND Nivel_Role_has_Role_has_Funcao_id = ' . $nrhrhf->id) == NULL )
            {
                $styleCfg                                       = new NivelRoleHasRoleHasFuncaoHasStyleConfig;
                $styleCfg->Nivel_Role_has_Role_has_Funcao_id    = $nrhrhf->id;
                $styleCfg->Style_Config_id                      = 4;
                $styleCfg->habilitado                           = 1;
                $styleCfg->save();
            }

        }
    }    
}