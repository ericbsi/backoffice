<?php

class BorderoController extends Controller{

	public $layout = '';

	public function actionIndex()
	{
		echo "string";
	}

	public function actionInit()
	{

	}

	public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete' // we only allow deletion via POST request
    );
  }

  public function accessRules()
  {
      return [
            /*
            [
                'allow',
                'actions' => [],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            */
            [
                'allow',
                'actions'   => ['propostas'],
                'users'     => ['@']
            ],
            [
                'deny',
                'users'     => ['*']
            ]
      ];
  }

  public function actionPropostas()
  {
    if( $_POST['ambiente'] == 'n' )
    {
  	 echo json_encode( Bordero::model()->getPropostas( $_POST['draw'], $_POST['start'], $_POST['length'], $_POST['borderoId'] ) );
    }
    else
    {
     echo json_encode( BorderoBeta::model()->getPropostas( $_POST['draw'], $_POST['start'], $_POST['length'], $_POST['borderoId'] ) );
    }

  }
}