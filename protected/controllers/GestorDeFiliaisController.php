<?php

class GestorDeFiliaisController extends Controller {

    public $layout = '';

    public function actionIndex() {
        $this->layout = '//layouts/ubold';
        $this->render('index');
    }

    public function filters() {
        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return [
            [
                'allow',
                'actions' => ['indicadoresIniciais', 'index', 'producao', 'producaoSeguro'],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'allow',
                'actions' => ['gridProducao', 'indicativosProducao', 'listarFiliaisFiltro', 'listarNucleosFiltro','listarFiliais', 'listarFiliaisNucleo', 'listarNucleosGrupos', 'gridSeguro'],
                'users' => ['@']
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init() {
        
    }

    public function actionProducao() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $this->layout = '//layouts/ubold';


        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');
        

        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
        
        $cs->registerScriptFile('/assets/ubold/js/gestorDeFiliais/fn-gestor-producaov2.js', CClientScript::POS_END);

        $this->render('producao');
    }

    public function actionGridSeguro(){

        $g = new GestorDeFiliais;

        echo json_encode( $g->producaoSeguro( $_POST['start'], $_POST['length'], $_POST['draw'], $_POST['de'], $_POST['ate'], $_POST['filial'] ) );
    }

    public function actionProducaoSeguro(){
        
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $this->layout   = '//layouts/ubold';

        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');
        

        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
        
        $cs->registerScriptFile('/assets/ubold/js/gestorDeFiliais/fn-gestor-producao-seguro.js', CClientScript::POS_END);

        $this->render('producaoSeguro');
    }

    public function actionGridProducao() {
        $g = new GestorDeFiliais;

        if( !isset($_POST['codigo_filter']) ){
            $_POST['codigo_filter'] = null;
        }

        echo json_encode($g->producao($_POST['start'], $_POST['length'], $_POST['draw'], $_POST['statusProposta'], $_POST['de'], $_POST['ate'], $_POST['filial'], $_POST['modalidade'], $_POST['codigo_filter'], $_POST['operacao']));
    }

    public function actionIndicativosProducao() {
        $g = new GestorDeFiliais;

        echo json_encode($g->indicativosProducao($_POST['start'], $_POST['length'], $_POST['draw'], $_POST['de'], $_POST['ate'], $_POST['filial']));
    }

    public function actionIndicadoresIniciais() {
        $g = new GestorDeFiliais;

        echo json_encode($g->indicativosVendas());
    }

    public function actionListarFiliaisNucleo() 
    {

        $retorno = [];

        if ($_POST['nucleoId'] === '0')
        {
            echo json_encode([
                ['id' => 0, 'text' => 'Selecione uma filial']
            ]);
        }

        else
        {
            $_POST['nucleoId']  = join(',', $_POST['nucleoId']);

            $filiais            = Filial::model()->findAll('NucleoFiliais_id IN('.$_POST['nucleoId'].')');

            foreach ($filiais   as $filial)
            {
                $retorno[]      = [
                    'id'        => $filial->id,
                    'text'      => $filial->getConcat()
                ];
            }

            echo json_encode($retorno);
        }
    }

    public function actionListarNucleosGrupos() 
    {

        $retorno    = []    ;
        $gruposId   = null  ;

        if (isset($_POST['variavel']) && !empty($_POST['variavel']))
        {
            
            $gruposId   = $_POST['variavel'];
            
        }
            
        $nucleos = NucleoFiliais::model()->findAllAdmFiliais($gruposId);

        foreach ( $nucleos as $nucleo )
        {

            $retorno[]  = 
            [
                'id'    => $nucleo->id    ,
                'text'  => $nucleo->nome  
            ];

        }

        if (empty($retorno))
        {
            
            $retorno = 
            [
                [
                    'id'    => 0                    , 
                    'text'  => 'Nada selecionado'
                ]
            ];
            
        }
        echo json_encode($retorno);
    }
    
    public function actionListarFiliais(){
        $retorno    = [];
        
        $variavel   = [];
        
        if (isset($_POST['variavel']) && !empty($_POST['variavel']))
        {
            $variavel = $_POST['variavel'];
        }
        else if(isset($_POST['nucleoId']) && !empty($_POST['nucleoId']))
        {
            $variavel = $_POST['nucleoId'];
        }
        
        for ( $i = 0; $i < sizeof($variavel); $i++ )
        { 
            $nucleo = NucleoFiliais::model()->findByPk($variavel[$i]);
            
            if($nucleo !== null)
            {
                $filiais = Filial::model()->findAll('habilitado AND NucleoFiliais_id =' . $nucleo->id);

                foreach ($filiais as $filial) 
                {
                        $retorno[] = [
                            'id' => $filial->id,
                            'text' => $filial->getConcat()
                        ];
                    
                }
                
            }
        }
        
        if(count($retorno) > 0)
        {
            echo json_encode($retorno);
        }
        else
        {
            
            foreach ( Filial::model()->findAll('habilitado') as $filial)
            {
                $retorno[]  = 
                [
                    'id'    => $filial->id,
                    'text'  => $filial->getConcat()
                ];
            }
            
            echo json_encode($retorno);
        }
    }
    
    public function actionListarFiliaisFiltro() 
    {

        $retorno    = [];
        
        $variavel   = [];
        
        if (isset($_POST['variavel']) && !empty($_POST['variavel']))
        {
            $variavel = $_POST['variavel'];
        }
        else if(isset($_POST['nucleoId']) && !empty($_POST['nucleoId']))
        {
            $variavel = $_POST['nucleoId'];
        }
        
        for ( $i = 0; $i < sizeof($variavel); $i++ )
        { 
            $nucleo = NucleoFiliais::model()->findByPk($variavel[$i]);
            
            if($nucleo !== null)
            {
                $filiais = Filial::model()->findAll('habilitado AND NucleoFiliais_id =' . $nucleo->id);

                foreach ($filiais as $filial) 
                {
                    if (AdminHasParceiro::model()->find('habilitado AND Parceiro = ' . $filial->id . ' AND Administrador = ' . Yii::app()->session['usuario']->id) !== NULL)
                    {
                        $retorno[] = [
                            'id' => $filial->id,
                            'text' => $filial->getConcat()
                        ];
                    }
                }
                
            }
        }

        if(count($retorno) > 0)
        {
            echo json_encode($retorno);
        }
        else
        {
            
            foreach ( Yii::app()->session['usuario']->adminHasParceiros as $hasParceiro )
            {
                $retorno[]  = 
                [
                    'id'    => $hasParceiro->parceiro->id,
                    'text'  => $hasParceiro->parceiro->getConcat()
                ];
            }
            
            echo json_encode($retorno);
        }
    }

    public function actionListarNucleosFiltro() 
    {
        $retorno    = [];
        
        $variavel   = [];
        
        if(isset($_POST['grupoId']) && !empty($_POST['grupoId']))
        {
            $variavel = $_POST['grupoId'];
        }
        
        for ( $i = 0; $i < sizeof($variavel); $i++ )
        { 
            $grupo = GrupoFiliais::model()->findByPk($variavel[$i]);
            
            if($grupo !== null)
            {
                $nucleos = NucleoFiliais::model()->findAll('habilitado AND GrupoFiliais_id =' . $grupo->id);

                foreach ($nucleos as $nucleo) 
                {
                    $retorno[]  = [
                        'id'    => $nucleo->id,
                        'text'  => mb_strtoupper($nucleo->nome)
                    ];
                }
            }
        }

        echo json_encode($retorno);
    }
}