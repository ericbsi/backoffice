<?php

class AdminHasParceiroController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' 		=> ['index', 'adminHasParceiro'],
                'users' 		=> ['@'],
                'expression' 	=> 'Yii::app()->session["usuario"]->autorizado()'
            ],
            /*[
                'allow',
                'actions'     => [],
                'users'       => ['@'],
            ],
            */
            [
                'deny',
                'users' 		  => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionAdminHasParceiro()
    {
    	$AdminHasParceiro 								= AdminHasParceiro::model()->find('Administrador = ' .intval($_POST['usuario']). ' AND Parceiro = ' . intval($_POST['filial']));

    	/*	
    		Não existe registro com esta combinação, e a opção é ativar
			Então cria-se um novo registro    		
    	*/
    	if( $AdminHasParceiro === NULL && $_POST['hab'] === '1' )
    	{
    		$AdminHasParceiro 							= new AdminHasParceiro;	
    		$AdminHasParceiro->Parceiro 				= intval($_POST['filial']);
    		$AdminHasParceiro->Administrador 			= intval($_POST['usuario']);
    		$AdminHasParceiro->habilitado	 			= 1;
    		$AdminHasParceiro->save();
    	}
    	/*Já existe registro, apenas setamos hab[0 || 1]*/
    	else
    	{
    		$AdminHasParceiro->habilitado 				= intval($_POST['hab']);
    		$AdminHasParceiro->update();
    	}
    }
}