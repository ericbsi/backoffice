<?php

class PropostaController extends Controller
{
	public $layout = '';

	public function filters()
  {
	  return array(
	    'accessControl', // perform access control for CRUD operations
	    'postOnly + delete' // we only allow deletion via POST request
	  );
  }

  public function accessRules()
  {
  	 return 
  	 [
  	   [
  	   	'allow',
  	   	'actions' 		=> ['index', 'vitaAdmin', 'gridVitaPropostasAdmin', 'getDialogo'],
  	   	'users' 		=> ['@'],
  	     /*'expression'	=> 'Yii::app()->session["usuario"]->autorizado()'*/
  	   ],

  	   [
  	   	'deny',
  	   	'users'	 		=> ['*']
  	   ]
     ];
  }

  public function init()
  {

	}

  public function actionGetDialogo()
  {
    echo json_encode( Proposta::model()->getComentariosOmni( $_POST['propostaId'], $_POST['draw'] ) );
  }

	public function actionIndex()
	{
		$this->render('index');
	}

  public function actionVitaAdmin()
  {
  		$baseUrl                    = Yii::app()->baseUrl;
        $cs                         = Yii::app()->getClientScript();

        $this->layout               = '//layouts/ubold';

        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');

        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js',                        CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js',                         CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js',                       CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js',                        CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js',                    CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js',                     CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js',                      CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',   CClientScript::POS_END);
        
        $cs->registerScriptFile('/assets/ubold/js/proposta/vita/fn-vita-admin2.js',                          		    CClientScript::POS_END);

        $this->render('vitaAdmin');
  }

  public function actionGridVitaPropostasAdmin()
  {
      $de             = $_POST['de'             ];
      $ate            = $_POST['ate'            ];
      $statusProposta = $_POST['statusProposta' ];
      $cpf            = $_POST['cpf'            ];
      $usuario        = $_POST['usuario'        ];
      $length         = $_POST['length'         ];
      $start          = $_POST['start'          ];
      $count          = 0;
      $teste          = [];

      $util           = new Util;
      $dados          = [];

      $query           = " SELECT P.data_cadastro, P.id as 'idprop', P.codigo as 'Codigo', Pe.nome as 'nome', CPF.numero as cpf, P.valor as 'valor', P.valor_entrada as 'valor_entrada', SP.status, SP.cssClass as 'cssStatus', '1' AS ambiente, ";
      $query          .= " U.username as 'usuario', F.nome_fantasia as 'Empresa' ";
      $query          .= " FROM nordeste2.Proposta as P ";
      $query          .= " INNER JOIN nordeste2.Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id ";
      $query          .= " INNER JOIN nordeste2.Usuario as U ON AC.Usuario_id = U.id ";
      $query          .= " INNER JOIN nordeste2.Filial_has_Usuario as FHU ON FHU.Usuario_id = AC.Usuario_id AND FHU.Filial_id = 241 ";
      $query          .= " INNER JOIN nordeste2.Filial as F  ON FHU.Filial_id = F.id ";
      $query          .= " INNER JOIN nordeste2.Cliente as C ON C.id = AC.Cliente_id ";
      $query          .= " INNER JOIN nordeste2.Pessoa as Pe ON Pe.id = C.Pessoa_id ";
      $query          .= " INNER JOIN nordeste2.Pessoa_has_Documento as PeHD ON Pe.id = PeHD.Pessoa_id ";
      $query          .= " INNER JOIN nordeste2.Documento as CPF ON CPF.id = PeHD.Documento_id AND CPF.Tipo_documento_id = 1";
      $query          .= " INNER JOIN nordeste2.Status_Proposta as SP ON SP.id = P.Status_Proposta_id ";
      $query          .= " WHERE P.Financeira_id = 7 AND  P.habilitado ";
      $query          .= " AND P.data_cadastro BETWEEN '".$util->view_date_to_bd($de)." 00:00:00' AND '".$util->view_date_to_bd($ate)." 23:59:59' ";

      if( $statusProposta !== '0' )
      {
        $query        .= " AND P.Status_Proposta_id = ".$statusProposta ." ";
      }
    
      if( !empty( $usuario ) && trim($usuario) !== '' && $usuario !== NULL )
      {
        $query        .= " AND U.username LIKE '%".$usuario ."%' ";
      }

      if( !empty( $cpf ) && trim($cpf) !== '' && $cpf !== NULL )
      {
        $query        .= " AND CPF.numero LIKE '%".$cpf ."%' ";
      }

      $count          = count( Yii::app()->db->createCommand($query)->queryAll() );

      $query          .= " LIMIT $start, $length ";

      foreach( Yii::app()->db->createCommand($query)->queryAll() as $r )
      {
        $proposta           = Proposta::model()->findByPk( $r['idprop'] );

        $teste[]            = $proposta->getComentariosOmni($proposta->id, 1);

        $dados[]            = [
          'propostaId'      => $proposta->id,
          'codigo'          => $r['Codigo'],
          'cliente'         => mb_strtoupper($r['nome']),
          'cpf'             => $r['cpf'],
          'v_inicial'       => "R$ ".number_format($r['valor'], 2, ',', '.'),
          'v_entrada'       => mb_strtoupper($r['usuario']),
          'v_financiado'    => mb_strtoupper($r['Empresa']),
          'data_cadastro'   => $util->bd_date_to_view(substr($r['data_cadastro'], 0,10)),
          'status'          => "<button type='button' class='".$r['cssStatus']."'>".mb_strtoupper($r['status'])."</button>"
        ];
      }

      echo json_encode([
        'draw'                => $_POST['draw'],
        'data'                => $dados,
        'recordsTotal'        => $length,
        'recordsFiltered'     => $count,
        'teste'               => $teste
      ]);
  }
}