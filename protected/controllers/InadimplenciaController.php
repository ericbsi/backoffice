<?php

class InadimplenciaController extends Controller {
  
    public $layout = '';
    
    public function actionIndex() {
        $this->layout = '//layouts/ubold';
        $this->render('listarInadimplencia');
    }
    
    public function filters() {
        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete' // we only allow deletion via POST request
        );
    }
    
    
    
}

?>

