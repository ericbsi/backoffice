<?php

class RecebimentoController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return
                [
                    [
                        'allow',
                        'actions' => ['gerarLotes','devolver','devContrato','selecionadas', 'bipagem', 'propostasBipagem', 'getPreLotes', 'gerarArquivoLecca', 'lotesCNABS', 'arquivoDetalhado', 'preLoteDetalhado', 'borderoDetalhado', 'importarDP', 'lotesDP'],
                        'users' => ['@'],
                        'expression' => 'Yii::app()->session["usuario"]->autorizado()'
                    ],
                    [
                        'deny',
                        'users' => ['*']
                    ]
        ];
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionBipagem() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/ubold';

        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/css/login/font-awesome.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        $cs->registerCssFile('/assets/ubold/css/multipleselect.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');

        //$cs->registerScriptFile('/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js');
        $cs->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/js/formalizacao/fn-formalizacao-de-propostas-v11.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);

        $this->render('/recebimento/bipagem');
    }

    public function actionDevContrato() {
        $ident = $_POST['ident'];
        $proposta = null;
        if ($ident != '') {
            $proposta = Proposta::model()->find("habilitado and codigo = '" . $ident . "' or id = " . $ident);

            $sql = "SELECT PL.data_cadastro, LC.id as lcid, IB.id as ibid FROM
                            ArquivoCNAB                                 AS AC
                            INNER JOIN LinhaCNAB 			AS LC 	ON LC.habilitado 						AND LC.ArquivoExtportado_id = AC.id
                            INNER JOIN Parcela 				AS PA 	ON PA.id 			= LC.Parcela_id 	AND PA.habilitado
                            INNER JOIN Titulo 				AS T 	ON T.id 			= PA.Titulo_id 		AND T.NaturezaTitulo_id 	= 2 		AND T.habilitado
                            INNER JOIN Proposta 			AS P 	ON P.id 			= T.Proposta_id 	AND P.habilitado
                            INNER JOIN ItemDoBordero 		AS IB 	ON IB.Proposta_id 	= P.id 				AND IB.habilitado
                            INNER JOIN PreLote_has_Bordero 	AS PHB 	ON PHB.Bordero_id 	= IB.Bordero 		AND PHB.habilitado
                            INNER JOIN PreLote 				AS PL 	ON PL.id 			= PHB.PreLote_id 	AND PL.habilitado

                    WHERE
                            AC.TipoCNAB_id 	= 2 	AND
                            AC.habilitado               AND
                            P.id = " . $proposta->id;

            $dados = Yii::app()->db->createCommand($sql)->queryAll();
        }

        $rows = [];
        $util = new Util;
        if ($proposta != null && count($dados) > 0) {
            $row = array(
                'data_pl' => $util->bd_date_to_view(substr($dados[0]['data_cadastro'], 0, 10)),
                'codigo' => $proposta->codigo,
                'parceiro' => $proposta->analiseDeCredito->filial->getConcat(),
                'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                'financiado' => "R$ " . number_format(($proposta->valor - $proposta->valor_entrada), 2, ',', '.'),
                'repasse' => "R$ " . number_format($proposta->valorRepasse(), 2, ',', '.'),
                'devolver' => '<button id="dev_cotrato" class="btn btn-danger btn-xs">DEVOLVER</button>'
                . '<input type="hidden" id="idlc" value="'.$dados[0]['lcid'].'">'
                . '<input type="hidden" id="idib" value="'.$dados[0]['ibid'].'">'
            );

            $rows[] = $row;
        }
        echo json_encode(
                array(
                    'data' => $rows
                )
        );
    }

    public function actionDevolver(){
        $idlc = $_POST['idlc'];
        $idib = $_POST['idib'];

        $transaction = Yii::app()->db->beginTransaction();

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Nao foi possivel devolver a proposta!",
            "tipo" => "error"
        );
        $certo = false;

        $linhaCNAB = LinhaCNAB::model()->findByPk($idlc);
        if($linhaCNAB != null){
            $linhaCNAB->habilitado = 0;
            if($linhaCNAB->update()){
                $itemBordero = ItemDoBordero::model()->findByPk($idib);
                if($itemBordero != null){
                    $itemBordero->habilitado = 0;
                    if($itemBordero->update()){
                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Procedimento realizado com sucesso!",
                            "tipo" => "success"
                        );
                        $certo = true;
                    }else{
                        $certo = false;
                    }
                }else{
                    $certo = false;
                }
            }else{
                $certo = false;
            }
        }else{
            $certo = false;
        }

        $certo ? $transaction->commit() : $transaction->rollBack();

        echo json_encode($arrReturn);
    }

    public function actionGerarLotes(){
        $id_arq = $_POST['id_arq'];
        $transaction = Yii::app()->db->beginTransaction();
        $sql = "SELECT

        DISTINCT PL.id

        FROM ArquivoCNAB AS AC
        INNER JOIN LinhaCNAB                                    AS LC ON LC.ArquivoExtportado_id = AC.id    AND LC.habilitado
        INNER JOIN Parcela 					AS Pa 	ON Pa.id = LC.Parcela_id            AND Pa.habilitado
        INNER JOIN Titulo 					AS T 	ON T.id = Pa.Titulo_id              AND T.habilitado
        INNER JOIN Proposta                                     AS P 	ON P.id = T.Proposta_id             AND T.habilitado
        INNER JOIN ItemDoBordero                                AS IB 	ON IB.Proposta_id = P.id            AND IB.habilitado
        INNER JOIN Bordero 					AS B 	ON B.id = IB.Bordero                AND B.Status = 6 AND B.habilitado
        INNER JOIN PreLote_has_Bordero                          AS PB	ON PB.Bordero_id = B.id             AND PB.habilitado
        INNER JOIN PreLote 					AS PL 	ON PL.id = PB.PreLote_id            AND PL.habilitado
        WHERE AC.TipoCNAB_id = 2 AND AC.id = " . $id_arq;

        $preLotes = Yii::app()->db->createCommand($sql)->queryAll();

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Lotes gerados com sucesso!",
            "tipo" => "success"
        );
        $certo = true;

        foreach ($preLotes as $pl) {
            $preLote = PreLote::model()->findByPk($pl['id']);
            $lote = new LotePagamento;
            $lote->data_cadastro = date('Y-m-d H:i:s');
            $lote->habilitado = 1;
            $lote->Usuario_id = Yii::app()->session['usuario']->id;
            $lote->NucleoFiliais_id = $preLote->NucleoFiliais_id;

            $preLote->habilitado = 0;
            $preLote->StatusPreLote_id = 1;
            if (!$lote->save() || !$preLote->update()) {
                $certo = false;
                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Não foi possível salvar determinado lote",
                    "tipo" => "error"
                );
                break;
            } else {
                $lhs = new LotePagamentoHasStatusLotePagamento;
                $lhs->StatusLotePagamento_id = 1;
                $lhs->LotePagamento_id = $lote->id;
                $lhs->data_cadastro = date('Y-m-d H:i:s');
                $lhs->Usuario_id = $lote->Usuario_id;
                $lhs->habilitado = 1;
                if (!$lhs->save()) {
                    $certo = false;
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível criar o status do lote",
                        "tipo" => "error"
                    );
                    break;
                }
            }

            $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $preLote->id);
            foreach ($phb as $b) {

                $lhb = new LoteHasBordero;
                $lhb->habilitado = 1;
                $lhb->Bordero_id = $b->Bordero_id;
                $lhb->Lote_id = $lote->id;
                $b->habilitado = 0;
                if (!$lhb->save() || !$b->update()) {
                    $certo = false;
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível salvar determinado lhb",
                        "tipo" => "error"
                    );
                    break;
                }

                $bord = Bordero::model()->findByPk($b->Bordero_id);
                $bord->Status = 2;
                if (!$bord->update()) {
                    $certo = false;
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível atualizar o status de algum bordero",
                        "tipo" => "error"
                    );
                    break;
                }
            }
        }

        if($certo){
            $arquivo = ArquivoCNAB::model()->findByPk($id_arq);
            if($arquivo != null){
                $arquivo->habilitado = 0;
                if($arquivo->update()){
                    $linhas = LinhaCNAB::model()->findAll('ArquivoExtportado_id = ' . $id_arq);
                    if(count($linhas) > 0){
                        foreach ($linhas as $l) {
                            $l->habilitado = 0;
                            if (!$l->update()) {
                                $certo = false;
                                $arrReturn = array(
                                    "hasError" => true,
                                    "msg" => "Não foi possível desabilitar as linhas CNAB",
                                    "tipo" => "error"
                                );
                                break;
                            }
                        }
                    }else{
                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Linhas nao encontradas",
                            "tipo" => "error"
                        );
                        $certo = false;
                    }
                }else{
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível desabilitar as arquivo CNAB",
                        "tipo" => "error"
                    );
                    $certo = false;
                }
            }else{
                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Arquivo nao localizado",
                    "tipo" => "error"
                );
                $certo = false;
            }
        }

        $certo ? $transaction->commit() : $transaction->rollBack();

        echo json_encode($arrReturn);
    }

    public function actionSelecionadas() {

        $qtdProp = 0;
        $totalRep = 0;
        $totalFin = 0;
        $qtdNcl = [];
        if (isset($_POST['selecionadas'])) {
            $selecionadas = $_POST['selecionadas'];
            foreach ($selecionadas as $id) {
                $proposta = Proposta::model()->findByPk($id);
                $qtdProp ++;

                $totalRep += $proposta->valorRepasse();
                $totalFin += ($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro());
                if (!in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, $qtdNcl)) {
                    $qtdNcl[] = $proposta->analiseDeCredito->filial->NucleoFiliais_id;
                }
            }
        }
        echo json_encode(
                array(
                    'qtd_props' => $qtdProp,
                    'totalRep' => number_format($totalRep, 2, ',', '.'),
                    'totalFin' => number_format($totalFin, 2, ',', '.'),
                    'qtdNcl' => count($qtdNcl)
                )
        );
    }

    public function actionPropostasBipagem() {
        $filiais = $_POST['filiais'];
        $dt_ini = $_POST['dt_ini'];
        $dt_fin = $_POST['dt_fin'];
        $contrato = $_POST['contrato'];
        $selects = explode(",", $_POST['selects']);
        $totalFin = 0;
        $totalRep = 0;

        if (isset($_POST['fisico']) && $_POST['fisico'] != '' && $_POST['fisico'] == "2") {
            $fisco = "INNER JOIN ItemRecebimento    AS IR   ON IR.habilitado    AND IR.Proposta_id = P.id
                      LEFT  JOIN Baixa              AS B    ON B.baixado        AND B.Parcela_id = Pa.id ";
            $cf = "B.id is null AND ";
        } else {
            $fisco = "LEFT JOIN ItemRecebimento    AS IR   ON IR.habilitado    AND IR.Proposta_id = P.id
                      LEFT  JOIN Baixa              AS B    ON B.baixado        AND B.Parcela_id = Pa.id ";
            $cf = "IR.id is null AND B.id is null AND ";
        }

        if (isset($_POST['valor_max']) && $_POST['valor_max'] != '') {
            $valor_max = str_replace(',', '.', str_replace('.', '', $_POST['valor_max']));
        }
        $sql = "SELECT P.id FROM Proposta AS P
                INNER JOIN Analise_de_Credito   AS AC   ON AC.habilitado    AND P.Analise_de_Credito_id = AC.id";

        if (isset($filiais) && $filiais != '') {
            $sql .= " AND AC.Filial_id IN ( " . join(",", $filiais) . ") ";
        }

        $sql .= " INNER JOIN Filial               AS F    ON F.habilitado     AND F.id = AC.Filial_id
                INNER JOIN Titulo               AS T    ON T.habilitado     AND T.NaturezaTitulo_id = 2     AND T.Proposta_id = P.id
                INNER JOIN Parcela              AS Pa   ON Pa.Titulo_id = T.id
                LEFT  JOIN LinhaCNAB            AS LC   ON LC.Parcela_id = Pa.id AND LC.habilitado
                LEFT  JOIN ItemDoBordero        AS IB   ON IB.habilitado    AND IB.Proposta_id = P.id

                " . $fisco . "

                WHERE " . $cf . " P.Status_Proposta_id IN (2,7) AND LC.id is null AND P.Financeira_id = 11 AND P.habilitado AND P.titulos_gerados = 1 AND IB.id is null";

        if (isset($dt_ini) && $dt_ini != '') {
            $sql .= " AND P.data_cadastro >= '" . $dt_ini . " 00:00:00' ";
        }
        if (isset($dt_fin) && $dt_fin != '') {
            $sql .= " AND P.data_cadastro <= '" . $dt_fin . " 23:59:59' ";
        }
        if (isset($contrato) && $contrato != '') {
            $sql .= " AND P.codigo = '" . $contrato . "'";
        }

        $sql .= " GROUP BY P.id ORDER BY P.data_cadastro ASC";

        $propostas = Yii::app()->db->createCommand($sql)->queryAll();

        $rows = [];
        $util = new Util;
        $qtd_props = 0;
        foreach ($propostas as $p) {

            if (isset($valor_max) && $valor_max != '') {
                $proposta = Proposta::model()->findByPk($p['id']);
                in_array($proposta->id, $selects) ? $checked = 'checked' : $checked = '';
                if (($totalRep + $proposta->valorRepasse()) < $valor_max) {
                    $totalFin += ($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro());
                    $totalRep += $proposta->valorRepasse();
                    $qtd_props++;
                    $row = array(
                        'modalidade' => $proposta->tabelaCotacao->ModalidadeId == 2 ? "Retenção" : "Juros",
                        'codigo' => $proposta->codigo,
                        'filial' => $proposta->analiseDeCredito->filial->getConcat(),
                        'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                        'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                        'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                        'financiado' => "R$ " . number_format(($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro()), 2, ',', '.'),
                        'parcelas' => $proposta->qtd_parcelas . "x R$ " . number_format($proposta->valor_parcela, 2, ',', '.'),
                        'repasse' => "R$ " . number_format($proposta->valorRepasse(), 2, ',', '.'),
                        'marcar' => '<input ' . $checked . ' value="' . $proposta->id . '" class="pfin" name="propostas" type="checkbox" data-plugin="switchery" data-color="#f05050" data-size="small" data-switchery="true" style="display: none;">'
                    );

                    $rows[] = $row;
                }
            } else {
                $proposta = Proposta::model()->findByPk($p['id']);
                in_array($proposta->id, $selects) ? $checked = 'checked' : $checked = '';
                $totalFin += ($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro());
                $totalRep += $proposta->valorRepasse();
                $qtd_props++;
                $row = array(
                    'modalidade' => $proposta->tabelaCotacao->ModalidadeId == 2 ? "Retenção" : "Juros",
                    'codigo' => $proposta->codigo,
                    'filial' => $proposta->analiseDeCredito->filial->getConcat(),
                    'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                    'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                    'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                    'financiado' => "R$ " . number_format(($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro()), 2, ',', '.'),
                    'parcelas' => $proposta->qtd_parcelas . "x R$ " . number_format($proposta->valor_parcela, 2, ',', '.'),
                    'repasse' => "R$ " . number_format($proposta->valorRepasse(), 2, ',', '.'),
                    'marcar' => '<input ' . $checked . ' value="' . $proposta->id . '" class="pfin" name="propostas" type="checkbox" data-plugin="switchery" data-color="#f05050" data-size="small" data-switchery="true" style="display: none;">'
                );

                $rows[] = $row;
            }
        }

        echo json_encode(
                array(
                    'data' => $rows,
                    'qtd' => $qtd_props . " Propostas",
                    'totalFin' => 'R$ ' . number_format($totalFin, 2, ',', '.'),
                    'totalRep' => 'R$ ' . number_format($totalRep, 2, ',', '.'),
                    'sql' => $sql
                )
        );
    }

    public function actionGerarArquivoLecca() {

        $retorno = [
            "hasErrors" => false,
            "title" => "Sucesso",
            "msg" => "CNAB gerado com sucesso",
            "type" => "success",
            "nomeArquivo" => "nada",
            "url" => "nada",
            "cnab" => "",
            "sucesso" => ""
        ];

        $hasErrors = false;
        $dados = [];
        $recordsFiltered = 0;
        $recordsTotal = 0;
        $erro = "";

        $marcadas = [];

        $sql = "SELECT  DISTINCT P.id                AS idProposta
                FROM         Proposta                AS P
                INNER JOIN   Analise_de_Credito      AS AC   ON   AC.habilitado  AND  AC.id =    P.Analise_de_Credito_id
                INNER JOIN   Filial                  AS F    ON    F.habilitado  AND   F.id =   AC.Filial_id
                INNER JOIN   Filial_has_Endereco     AS FhE  ON  FhE.habilitado  AND   F.id =  FhE.Filial_id
                INNER JOIN   Endereco                AS E    ON    E.habilitado  AND   E.id =  FhE.Endereco_id
                INNER JOIN   NucleoFiliais           AS NuF  ON  NuF.habilitado  AND NuF.id =    F.NucleoFiliais_id
                INNER JOIN   Cliente                 AS C    ON    C.habilitado  AND   C.id =   AC.Cliente_id
                INNER JOIN   Pessoa                  AS Pe   ON   Pe.habilitado  AND  Pe.id =    C.Pessoa_id
                INNER JOIN   Pessoa_has_Documento    AS PHD  ON   PHD.habilitado AND  Pe.id =    PHD.Pessoa_id
                INNER JOIN   Documento               AS DOC  ON   DOC.habilitado AND  DOC.id =    PHD.Documento_id AND DOC.Tipo_documento_id = 1
                INNER JOIN   Tabela_Cotacao          AS TC   ON   TC.id =    P.Tabela_id
                INNER JOIN   ModalidadeTabela        AS M    ON                       M.id =   TC.ModalidadeId
                INNER JOIN   Bordero				 AS B	 ON B.habilitado AND B.Status = 6
                INNER  JOIN   ItemDoBordero           AS IB   ON   IB.habilitado  AND IB.Proposta_id = P.id
                WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 11 ";

        $sql .= " AND NOT EXISTS    (
                                        SELECT *
                                        FROM        LinhaCNAB   AS LC
                                        INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                        INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                        WHERE LC.habilitado AND T.NaturezaTitulo_id = 2 AND P.id = T.Proposta_id
                                    ) ";

        if (isset($_POST["propostasMarcadas"])) {

            $pm = join(",", $_POST["propostasMarcadas"]);

            $sql .= " AND P.id in ($pm) ";
        }

        $sql .= "ORDER BY P.id DESC                ";

        try {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r) {
                $marcadas[] = $r["idProposta"];
            }

            if (!empty($marcadas)) {

                $linhas = "";

                $retLinhasLecca = $this->exportarLinhasLecca($marcadas);

                $cnab = $retLinhasLecca["linhas"];
                $arquivoCNAB = $retLinhasLecca["arquivoCNAB"];
                $erro = $retLinhasLecca["erro"];
                $sucesso = $retLinhasLecca["sucesso"];
                $passos = $retLinhasLecca["passos"];
                $idsPropostas = $retLinhasLecca['$vdIdsPropostas'];

                if (empty($erro)) {

                    $pasta = "remessaFDICLecca";

                    if (!is_dir($pasta)) {
                        mkdir("remessaFDICLecca", 0700);
                    }

                    $cont = 1;

                    $arquivo = "CDCS" . date("dmY") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";

                    while (file_exists($pasta . "/" . $arquivo)) {
                        $cont++;
                        $arquivo = "CDCS" . date("dmY") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";
                    }

                    $file = fopen($pasta . "/" . $arquivo, 'w');

                    fwrite($file, $cnab);
                    fclose($file);

                    /* Envio para o servidor de arquivos da credshow */
                    $s3 = new S3Client;
                    $rUpS3 = $s3->putLecca($pasta . "/" . $arquivo, $arquivo);

                    if ($rUpS3['@metadata']['statusCode'] == '200') {
                        $retorno["url"] = $rUpS3['@metadata']['effectiveUri'];
                        unlink($pasta . "/" . $arquivo);
                    }

                    //$retorno["url"]         = "/" . $pasta . "/" . $arquivo;
                    $retorno["nomeArquivo"] = $arquivo;
                    $retorno["sql"] = $sql;

                    $arquivoCNAB->urlArquivo = $retorno["url"];

                    if (!$arquivoCNAB->update()) {

                        ob_start();
                        var_dump($arquivo->getErrors());
                        $erroArquivoCNAB = ob_get_clean();

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Erro",
                            "msg" => $erroArquivoCNAB,
                            "type" => "error",
                            "cnab" => $cnab,
                            "sql" => $sql,
                        ];
                    } else {

                        ob_start();
                        var_dump($arquivoCNAB);
                        $varDumpArquivoCNAB = ob_get_clean();

                        $retorno["sucesso"] = $sucesso . $varDumpArquivoCNAB;
                        $retorno["passos"] = $passos;
                        $retorno["idsPropostas"] = $idsPropostas;
                    }
                } else {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => $erro,
                        "type" => "error",
                        "sql" => $sql,
                    ];
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Erro ao gerar arquivo",
                    "msg" => "Nenhuma proposta marcada",
                    "type" => "error",
                    "nomeArquivo" => "",
                    "url" => "",
                    "sql" => $sql
                ];
            }
        } catch (Exception $ex) {
            $retorno = [
                "hasErrors" => true,
                "title" => "Erro ao gerar arquivo",
                "msg" => $ex->getMessage(),
                "type" => "error",
                "nomeArquivo" => "nada",
                "url" => "nada",
                "cnab" => ""
            ];
        }

        echo json_encode($retorno);
    }

    public function exportarLinhasLecca($idsPropostas) {


        $content = "";
        $erro = "";
        $sucesso = "";

        function tirarAcentos($string) {
            return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(Ç)/", "/(ç)/"), explode(" ", "a A e E i I o O u U n N C c"), $string);
        }

        $transaction = Yii::app()->db->beginTransaction();

        $arquivoCNAB = new ArquivoCNAB;
        $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
        $arquivoCNAB->habilitado = 1;
        $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
        $arquivoCNAB->TipoCNAB_id = 2;
        $arquivoCNAB->urlArquivo = "/";

        if ($arquivoCNAB->save()) {

            $valorTotal = 0;

            $passos = 0;

            ob_start();
            var_dump($idsPropostas);
            $vdIdsPropostas = ob_get_clean();

            foreach ($idsPropostas as $p) {

                $linha = "";

                $proposta = Proposta::model()->find("habilitado AND id = $p");

                $passos++;

                if ($proposta !== null) {

                    $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                    if ($parametrosLecca != null) {
                        $codigoRede = $parametrosLecca->cod_lojista;
                        $digitoCodigoRede = "0";
                    }

                    if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                        $codigoRede = "96020";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 99) {
                        $codigoRede = "96022";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 59) {
                        $codigoRede = "96024";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75) {
                        $codigoRede = "96023";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [63, 122])) {
                        $codigoRede = "96025";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [41, 121])) {
                        $codigoRede = "96026";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [67])) {
                        $codigoRede = "96030";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 87) {
                        $codigoRede = "96033";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 43) {
                        $codigoRede = "96034";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 120) {
                        $codigoRede = "96035";
                        $digitoCodigoRede = "0";
                    }

                    $valorEntrada = number_format($proposta->valor_entrada, 2, '', '');
                    $valorParcela = number_format($proposta->valor_parcela, 2, '', '');

                    if (isset($proposta->analiseDeCredito->cliente->pessoa->nome)) {
                        $nomeCliente = $proposta->analiseDeCredito->cliente->pessoa->nome;
                    } else {
                        $nomeCliente = 'DADOS DE NOME NÃO ENCONTRADO';
                    }


                    $valorIOF = 0;

                    $valor = 0;

                    if ($proposta->tabelaCotacao->ModalidadeId == 2) {

                        $valor = $proposta->qtd_parcelas * $proposta->valor_parcela;

                        $valorTotal += $valor;

                        $valorCompra = number_format(($valor), 2, '', '');

                        $taxa = str_pad("0", 10, "0", STR_PAD_LEFT);

                        $fator = $proposta->getFator();
                        if (isset($fator)) {
                            $valorRepasse = $valor - ($valor * ($fator->porcentagem_retencao / 100));
                        } else {
                            $valorRepasse = 0;
                        }


                        if ($fator !== null) {
                            $valorIOF = ($valorRepasse * $fator->fatorIOF);
                        }

                        $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                        if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                            $codTabela = "00000602";

                            if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                                $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                            }
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [59, 99])) {

                            if ($proposta->carencia == 15) {
                                $codTabela = "00000607";
                            } else if ($proposta->carencia == 30) {
                                $codTabela = "00000608";
                            } else if ($proposta->carencia == 45) {
                                $codTabela = "00000609";
                            }

                            if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                                $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                            }
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 67) {
                            if ($proposta->carencia == 15) {
                                $codTabela = "00000613";
                            } else if ($proposta->carencia == 30) {
                                $codTabela = "00000614";
                            } else if ($proposta->carencia == 45) {
                                $codTabela = "00000539";
                            }

                            if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                                $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                            }
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 43) {

                            if ($proposta->carencia == 30) {
                                $codTabela = "00000450";
                            }

                            if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                                $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                            }
                        }
                        
                        if ($parametrosLecca != null) {
                            if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                                $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                            } else if ($proposta->carencia == 15) {
                                $codTabela = str_pad($parametrosLecca->cod_tab15, 8, "0", STR_PAD_LEFT);
                            } else if ($proposta->carencia == 30) {
                                $codTabela = str_pad($parametrosLecca->cod_tab30, 8, "0", STR_PAD_LEFT);
                            } else if ($proposta->carencia == 45) {
                                $codTabela = str_pad($parametrosLecca->cod_tab45, 8, "0", STR_PAD_LEFT);
                            }
                        }
                        
                    } else {

                        $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                        //TAC + FINANCIADO + SEGURO
                        //if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40 && $proposta->tabelaCotacao->ModalidadeId == 1) {
                            //$valor = (($proposta->valor - $proposta->valor_entrada)*0.05) + ($proposta->valor - $proposta->valor_entrada) + $proposta->calcularValorDoSeguro();
                        //    $valor = ($proposta->valor - $proposta->valor_entrada) + $proposta->calcularValorDoSeguro();
                        //} else {
                        //    $valor = $proposta->valor - $proposta->valor_entrada;
                        //}

                        $valor = $proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro();

                        $valorTotal += $valor;

                        $valorCompra = number_format(($valor), 2, '', '');

                        $taxa = str_pad(number_format($proposta->tabelaCotacao->taxa, 4, '', ''), 10, "0", STR_PAD_LEFT);

                        $fator = $proposta->getFator();

                        if ($fator !== null) {
                            $valorIOF = (($proposta->valor - $proposta->valor_entrada + $proposta->calcularValorDoSeguro()) * $fator->fatorIOF);
                        }

                        if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                            $codTabela = "00000601";
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75) {
                            $codTabela = "00000610";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [41, 121])) {
                            $codTabela = "00000611";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [63, 122])) {
                            $codTabela = "00000612";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [67])) {
                            $codTabela = "00000615";
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 120) {
                            $codTabela = "00000449";
                        }
                        
                        if ($parametrosLecca != null) {
                            $codTabela = str_pad($parametrosLecca->cod_tabJuros, 8, "0", STR_PAD_LEFT);
                        }

                        if (strpos($proposta->tabelaCotacao->descricao, 'PROMO') > -1) {
                            $codTabela = str_pad($parametrosLecca->cod_tabPromo, 8, "0", STR_PAD_LEFT);
                        }
                        
                    }

                    $valorIOFstr = str_pad(number_format($valorIOF, 2, '', ''), 15, "0", STR_PAD_LEFT);

                    $cadastro = Cadastro::model()->find('habilitado AND Cliente_id = ' . $proposta->analiseDeCredito->cliente->id);
                    if ($cadastro != null) {
                        if (isset($cadastro->nome_do_pai)) {
                            $nomePai = str_pad($cadastro->nome_do_pai, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $nomePai = str_pad("JOAO CARLOS LUIZ", 35, " ", STR_PAD_RIGHT);
                        }
                        if (isset($cadastro->nome_da_mae)) {
                            $nomeMae = str_pad($cadastro->nome_da_mae, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $nomeMae = str_pad("MARIA DO CARMO LUZIA", 35, " ", STR_PAD_RIGHT);
                        }
                    } else {
                        $nomeMae = str_repeat("JOAO MIGUEL GONÇALVES", 35);
                        $nomePai = str_repeat("MARIA JOAQUINA MARCELO", 35);
                    }

                    $dadosProfissionais = DadosProfissionais::model()->find('habilitado AND Pessoa_id = ' . $proposta->analiseDeCredito->cliente->pessoa->id . ' AND principal');
                    if ($dadosProfissionais != null) {
                        if (isset($dadosProfissionais->profissao)) {
                            $profissao = str_pad($profissao = $dadosProfissionais->profissao, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $profissao = str_pad("OUTROS", 35, " ", STR_PAD_RIGHT);
                        }
                        if (isset($dadosProfissionais->renda_liquida)) {
                            $salario = number_format($dadosProfissionais->renda_liquida, 2, "", "");
                        } else {
                            $salario = number_format(1000, 2, "", "");
                        }
                        if (isset($dadosProfissionais->empresa)) {
                            $razaoSocial = $dadosProfissionais->empresa;
                        } else {
                            $razaoSocial = "OUTROS";
                        }
                        $logradouro = $dadosProfissionais->endereco->logradouro;
                        $numero = $dadosProfissionais->endereco->numero;
                        $complemento = $dadosProfissionais->endereco->complemento;
                        $bairro = $dadosProfissionais->endereco->bairro;
                        $cidade = $dadosProfissionais->endereco->cidade;
                        $uf_dp = $dadosProfissionais->endereco->uf;
                        $cep_dp = $dadosProfissionais->endereco->cep;
                    } else {
                        $profissao = str_pad("OUTROS", 35, " ", STR_PAD_RIGHT);
                        $salario = number_format(1000, 2, "", "");
                        $razaoSocial = "OUTROS";

                        $logradouro = "DADO INEXISTENTE";
                        $numero = "DADO INEXISTENTE";
                        $complemento = "DADO INEXISTENTE";
                        $bairro = "DADO INEXISTENTE";
                        $cidade = "DADO INEXISTENTE";
                        $uf_dp = "RN";
                        $cep_dp = "0";
                    }

                    $endereco = $proposta->analiseDeCredito->cliente->pessoa->getEndereco();
                    if ($endereco != null) {
                        $logradouro_cliente = $endereco->logradouro;
                        $numero_cliente = $endereco->numero;
                        $complemento_cliente = $endereco->complemento;
                        $uf_cliente = $endereco->uf;
                        $bairro_cliente = $endereco->bairro;
                        $cidade_cliente = $endereco->cidade;
                        $enderecoCEP = trim($endereco->cep);
                    } else {
                        $logradouro_cliente = "DADO INEXISTENTE";
                        $numero_cliente = "DADO INEXISTENTE";
                        $complemento_cliente = "DADO INEXISTENTE";
                        $uf_cliente = "RN";
                        $bairro_cliente = "DADO INEXISTENTE";
                        $cidade_cliente = "DADO INEXISTENTE";
                        $enderecoCEP = "0";
                    }

                    $nomeCliente = strtoupper(strtr(utf8_decode($nomeCliente), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"));

                    $cht = ContatoHasTelefone::model()->find('habilitado AND Contato_id = ' . $proposta->analiseDeCredito->cliente->pessoa->Contato_id);
                    if ($cht != null) {
                        $telefone = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->numero;
                        $tddd = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->discagem_direta_distancia;
                    } else {
                        $telefone = "0";
                        $tddd = "0";
                    }

                    $nucleo = $proposta->analiseDeCredito->filial->nucleoFiliais;
                    $nHDB = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $nucleo->id);
                    if ($nHDB != null) {
                        $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nHDB->Dados_Bancarios_id);

                        $nome_agencia = $dadosBancarios->banco->nome;

                        $posicaoA = strpos($dadosBancarios->agencia, '-');

                        if ($posicaoA != 0 || $posicaoA === true) {
                            $agencia = substr($dadosBancarios->agencia, 0, $posicaoA);
                            $digitoAgencia = substr($dadosBancarios->agencia, -($posicaoA - 3));
                        } else {
                            $agencia = $dadosBancarios->agencia;
                            $digitoAgencia = "-";
                        }

                        $posicaoB = strpos($dadosBancarios->numero, '-');
                        if ($posicaoB != 0 || $posicaoB === true) {
                            $conta = substr($dadosBancarios->numero, 0, $posicaoB);
                            $digitoConta = substr($dadosBancarios->numero, -($posicaoB - 2));
                        } else {
                            $conta = $dadosBancarios->numero;
                            $digitoConta = "-";
                        }

                        if ($dadosBancarios->Tipo_Conta_Bancaria_id == 1) {
                            $flagConta = '001';
                        } else {
                            $flagConta = '002';
                        }
                    } else {
                        $dadosBancarios = null;
                        $nome_agencia = "-";
                        $digitoAgencia = '-';
                        $digitoConta = '-';
                        $conta = "-";
                        $agencia = '-';
                        $flagConta = '-';
                    }

                    $cnpj = $proposta->analiseDeCredito->filial->cnpj;

                    $cnpj = str_replace('.', '', $cnpj);
                    $cnpj = str_replace('-', '', $cnpj);
                    $cnpj = str_replace('/', '', $cnpj);
                    $cnpj = str_replace(' ', '', $cnpj);

                    if ($proposta->getDialogo() != null) {
                        if (Mensagem::model()->find('Dialogo_id = ' . $proposta->getDialogo()->id) != null) {
                            $msg = Mensagem::model()->find('Dialogo_id = ' . $proposta->getDialogo()->id)->conteudo;
                        } else {
                            $msg = "-";
                        }
                    } else {
                        $msg = "-";
                    }

                    $data_operacao = '';
                    $primeiro_vencimento = '';

                    if (date('Y-m-d', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro))) <= date('Y-m-d')) {
                        $data_operacao = date('Ymd', strtotime("+15days", strtotime($proposta->data_cadastro)));
                        $primeiro_vencimento = date('Ymd', strtotime("+" . ($proposta->carencia + 15) . "days", strtotime($proposta->data_cadastro)));
                    } else {
                        $data_operacao = date('Ymd', strtotime($proposta->data_cadastro));
                        $primeiro_vencimento = date('Ymd', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro)));
                    }

                    /* 0001-0004 */$linha .= "CDCS";
                    /* 0005-0012 */$linha .= $data_operacao;
                    /* 0013-0016 */$linha .= "0005";
                    /* 0017-0021 */$linha .= $codigoRede;
                    /* 0022-0022 */$linha .= $digitoCodigoRede;
                    /* 0023-0027 */$linha .= "00001";
                    /* 0028-0028 */$linha .= "0";
                    /* 0029-0063 */$linha .= str_pad("CREDSHOW S/A", 35, " ", STR_PAD_RIGHT);
                    /* 0064-0075 */$linha .= str_pad($proposta->id, 12, "0", STR_PAD_LEFT);
                    /* 0075-0083 */$linha .= $codTabela;
                    /* 0084-0093 */$linha .= $taxa;
                    /* 0094-0101 */$linha .= $primeiro_vencimento;
                    /* 0102-0116 */$linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT);
                    /* 0117-0131 */$linha .= str_pad($valorEntrada, 15, "0", STR_PAD_LEFT);
                    /* 0132-0146 */$linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT);
                    /* 0147-0161 */$linha .= "000000000000000";
                    /* 0162-0164 */$linha .= str_pad($proposta->qtd_parcelas, 3, "0", STR_PAD_LEFT);
                    /* 0165-0179 */$linha .= str_pad($valorParcela, 15, "0", STR_PAD_LEFT);
                    /* 0180-0194 */$linha .= $valorIOFstr;
                    /* 0195-0195 */$linha .= "D";
                    /* 0196-0196 */$linha .= "I";
                    /* 0197-0197 */$linha .= str_pad(substr($digitoAgencia, 0, 1), 1, " ", STR_PAD_RIGHT);
                    /* 0198-0199 */$linha .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT);
                    /* 0200-0234 */$linha .= str_pad(substr(tirarAcentos($nomeCliente), 0, 35), 35, " ", STR_PAD_RIGHT);
                    /* 0235-0237 */$linha .= "001";
                    /* 0238-0240 */$linha .= $dadosBancarios->banco->codigo;
                    /* 0241-0244 */$linha .= str_pad($agencia, 4, "0", STR_PAD_LEFT);
                    /* 0245-0252 */$linha .= str_pad($conta, 8, "0", STR_PAD_LEFT);
                    /* 0253-0266 */$linha .= $cnpj;
                    /* 0267-0267 */$linha .= "N";
                    /* 0268-0282 */$linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT);
                    /* 0283-0342 */$linha .= str_pad(substr(tirarAcentos($nomeCliente), 0, 60), 60, " ", STR_PAD_RIGHT);
                    /* 0343-0347 */$linha .= "00001";
                    /* 0348-0348 */$linha .= "4";
                    /* 0349-0349 */$linha .= $proposta->analiseDeCredito->cliente->pessoa->sexo;
                    /* 0350-0365 */$linha .= str_pad($proposta->analiseDeCredito->cliente->pessoa->getRG()->numero, 16, " ", STR_PAD_RIGHT);
                    /* 0366-0367 */$linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor;
                    /* 0368-0369 */$linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor;
                    /* 0370-0404 */$linha .= str_pad("BRASILEIRA", 35, " ", STR_PAD_RIGHT);
                    /* 0405-0439 */$linha .= str_pad(substr(tirarAcentos($nomePai), 0, 35), 35, STR_PAD_LEFT);
                    /* 0440-0474 */$linha .= str_pad(substr(tirarAcentos($nomeMae), 0, 35), 35, STR_PAD_LEFT);
                    /* 0475-0479 */$linha .= str_pad(substr($proposta->analiseDeCredito->cliente->pessoa->getRG()->orgao_emissor, 0, 5), 5, " ", STR_PAD_RIGHT);
                    /* 0480-0481 */$linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor;
                    /* 0482-0516 */$linha .= substr(tirarAcentos($profissao), 0, 35);
                    /* 0517-0551 */$linha .= substr(tirarAcentos($profissao), 0, 35);
                    /* 0552-0562 */$linha .= $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                    /* 0563-0570 */$linha .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->nascimento));
                    /* 0571-0578 */$linha .= "00000001";
                    /* 0579-0590 */$linha .= str_pad($salario, 12, "0", STR_PAD_LEFT);
                    /* 0591-0598 */$linha .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->getRG()->data_emissao));
                    /* 0599-3598 */$linha .= str_pad(substr(tirarAcentos(utf8_decode(preg_replace('/\s/', ' ', $msg))), 0, 3000), 3000, "-", STR_PAD_RIGHT);
                    /* 3599-3600 */$linha .= "1";
                    /* 3601-3660 */$linha .= str_pad(substr(tirarAcentos(($logradouro_cliente)), 0, 60), 60, " ", STR_PAD_RIGHT);
                    /* 3661-3665 */$linha .= str_pad(substr($numero_cliente, 0, 5), 5, "0", STR_PAD_LEFT);
                    /* 3666-3695 */$linha .= str_pad(substr(tirarAcentos(($complemento_cliente)), 0, 30), 30, " ", STR_PAD_RIGHT);
                    /* 3696-3697 */$linha .= $uf_cliente;
                    /* 3698-3727 */$linha .= str_pad(substr(tirarAcentos(($bairro_cliente)), 0, 30), 30, " ", STR_PAD_RIGHT);
                    /* 3728-3767 */$linha .= str_pad(substr(tirarAcentos(($cidade_cliente)), 0, 40), 40, " ", STR_PAD_RIGHT);
                    /* 3768-3772 */$linha .= "00001";
                    /* 3773-3780 */$linha .= $enderecoCEP;
                    /* 3781-3784 */$linha .= "0000";
                    /* 3785-3796 */$linha .= "000000000000";
                    /* 3797-3808 */$linha .= str_pad(substr($telefone, 0, 12), 12, "0", STR_PAD_LEFT);
                    /* 3809-3858 */$linha .= str_pad("SEMEMAIL", 50, " ", STR_PAD_LEFT);
                    /* 3859-3862 */$linha .= str_pad($tddd, 4, "0", STR_PAD_LEFT);
                    /* 3863-3892 */$linha .= str_pad(substr(tirarAcentos($razaoSocial), 0, 30), 30, " ", STR_PAD_RIGHT);
                    /* 3893-3900 */$linha .= "20130201";
                    /* 3901-3914 */$linha .= "00000000000000";
                    /* 3915-3974 */$linha .= str_pad(substr(tirarAcentos(($logradouro)), 0, 60), 60, " ", STR_PAD_RIGHT);
                    /* 3975-3979 */$linha .= str_pad(substr($numero, 0, 5), 5, " ", STR_PAD_RIGHT);
                    /* 3980-4009 */$linha .= str_pad(substr(tirarAcentos(($complemento)), 0, 30), 30, " ", STR_PAD_RIGHT);
                    /* 4010-4039 */$linha .= str_pad(substr(tirarAcentos(($bairro)), 0, 30), 30, " ", STR_PAD_RIGHT);
                    /* 4040-4079 */$linha .= str_pad(substr(tirarAcentos(($cidade)), 0, 40), 40, " ", STR_PAD_RIGHT);
                    /* 4080-4081 */$linha .= str_pad($uf_dp, 2, " ", STR_PAD_RIGHT);
                    /* 4082-4094 */$linha .= str_pad($salario, 12, "0", STR_PAD_LEFT);
                    /* 4095-4102 */$linha .= str_pad(substr(str_replace("-", "", $cep_dp), 0, 8), 8, "0", STR_PAD_LEFT);
                    /* 4103-4103 */$linha .= substr($digitoAgencia, 0, 1);
                    /* 4104-4105 */$linha .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT);
                    /* 4106-4125 */$linha .= str_pad(substr(tirarAcentos($nome_agencia), 0, 20), 20, " ", STR_PAD_RIGHT);
                    /* 4126-4126 */$linha .= "1";
                    /* 4127-4161 */$linha .= str_pad(substr(tirarAcentos($nucleo->nome), 0, 35), 35, " ", STR_PAD_RIGHT);
                    /* 4161-4173 */$linha .= "000000000000";
                    /* 4174-4176 */$linha .= "001";
                    /* 4177-4179 */$linha .= $dadosBancarios->banco->codigo;
                    /* 4180-4182 */$linha .= str_pad($agencia, 4, "0", STR_PAD_LEFT);
                    /* 4183-4190 */$linha .= str_pad($conta, 8, "0", STR_PAD_LEFT);
                    /* 4191-4193 */$linha .= str_pad($flagConta, 3, "0", STR_PAD_RIGHT);
                    /* 4194-4196 */$linha .= "001";
                    /* 4197-4204 */$linha .= $primeiro_vencimento;
                    /* 4205-4205 */$linha .= "0";

                    $linha .= chr(13) . chr(10);

                    $content .= $linha;

                    $titulo = Titulo::model()->find("habilitado AND Proposta_id = $proposta->id AND NaturezaTitulo_id = 2");

                    if ($titulo !== null) {

                        $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id");

                        if ($parcela !== null) {

                            $linhaCNAB = new LinhaCNAB;
                            $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                            $linhaCNAB->habilitado = 1;
                            $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                            $linhaCNAB->Parcela_id = $parcela->id;
                            $linhaCNAB->caracteres = utf8_encode($linha);

                            if (!$linhaCNAB->save()) {

                                ob_start();
                                var_dump($linhaCNAB->getErrors());
                                $erro = ob_get_clean();

                                break;
                            }
                        } else {

                            $erro = "Parcela da proposta $proposta->codigo não encontrada.";

                            break;
                        }
                    } else {

                        $erro = "Título da proposta $proposta->codigo não encontrada.";

                        break;
                    }
                } else {

                    $erro = "Proposta de id $p não encontrada.";

                    break;
                }
            }

            if (empty($content)) {

                $erro = "Nenhuma linha gerada.";

                $transaction->rollBack();
            } else if (empty($erro)) {
                $sucesso = "Deu certo.";
                $transaction->commit();
            }
        } else {

            ob_start();
            var_dump($arquivoCNAB->getErrors());
            $erro = ob_get_clean();

            $transaction->rollBack();
        }


        return ["linhas" => $content, "arquivoCNAB" => $arquivoCNAB, "erro" => $erro, "sucesso" => $sucesso, "passos" => $passos, '$vdIdsPropostas' => $vdIdsPropostas];
    }

    public function actionLotesCNABS() {
        $propostas = $_POST['marcadas'];
        if ($_POST['imagem'] == 1) {
            $porImagem = 1;
        } else {
            $porImagem = 0;
        }
        $transaction = Yii::app()->db->beginTransaction();
        $certo = false;

        $retorno = array(
            "msg" => "Os Pré-Lotes foram gerados com sucesso",
            "tipo" => "success"
        );

        foreach ($propostas as $p) {
            $proposta = Proposta::model()->findByPk($p);
            $bordero = Bordero::model()->find('Status = 5 AND habilitado AND destinatario = ' . $proposta->analiseDeCredito->filial->id);
            $preLote = PreLote::model()->find('habilitado AND StatusPreLote_id = 1 AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->nucleoFiliais->id);

            if ($bordero == null) {
                $bordero = new Bordero;
                $bordero->dataCriacao = date('Y-m-d H:i:s');
                $bordero->destinatario = $proposta->analiseDeCredito->filial->id;
                $bordero->criadoPor = Yii::app()->session['usuario']->id;
                $bordero->Status = 5;
                $bordero->habilitado = 1;

                if ($preLote == null) {
                    $preLote = new PreLote;
                    $preLote->NucleoFiliais_id = $proposta->analiseDeCredito->filial->nucleoFiliais->id;
                    $preLote->data_cadastro = date('Y-m-d H:i:s');
                    $preLote->habilitado = 1;
                    $preLote->Usuario_id = Yii::app()->session['usuario']->id;
                    $preLote->StatusPreLote_id = 1;
                    if (!$preLote->save()) {
                        ob_start();
                        var_dump($preLote->getErrors());
                        $r1 = ob_get_clean();
                        $certo = false;
                        $retorno = array(
                            "msg" => "Erro ao salvar PL " . $r1,
                            "tipo" => "error"
                        );
                        break;
                    } else {
                        $certo = true;
                    }
                }

                if ($bordero->save()) {
                    $certo = true;
                    $ib = ItemDoBordero::model()->find('habilitado AND pagoImagem = ' . $porImagem . ' AND Proposta_id = ' . $proposta->id);

                    if ($ib == null) {
                        $ib = new ItemDoBordero;
                        $ib->Bordero = $bordero->id;
                        $ib->Proposta_id = $proposta->id;
                        $ib->Status = 2;
                        $ib->pagoImagem = $porImagem;
                    } else {
                        $ib->Bordero = $bordero->id;
                        $ib->Proposta_id = $proposta->id;
                        $ib->Status = 2;
                        $ib->pagoImagem = $porImagem;
                    }

                    $titulo = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 2 AND Proposta_id = ' . $proposta->id);
                    $parcela = Parcela::model()->find('habilitado AND Titulo_id = ' . $titulo->id);
                    $ib->Parcela_id = $parcela->id;

                    if (!$ib->save()) {
                        ob_start();
                        var_dump($ib->getErrors());
                        $r1 = ob_get_clean();
                        $certo = false;
                        $retorno = array(
                            "msg" => "Erro ao salvar item do bordero " . $r1,
                            "tipo" => "error"
                        );
                        break;
                    } else {
                        $certo = true;
                        $preLoteHB = new PreLoteHasBordero;
                        $preLoteHB->Bordero_id = $bordero->id;
                        $preLoteHB->PreLote_id = $preLote->id;
                        $preLoteHB->habilitado = 1;
                        if (!$preLoteHB->save()) {
                            ob_start();
                            var_dump($preLoteHB->getErrors());
                            $r1 = ob_get_clean();
                            $certo = false;
                            $retorno = array(
                                "msg" => "Erro ao salvar PHB " . $r1,
                                "tipo" => "error"
                            );
                            break;
                        }
                    }
                } else {
                    ob_start();
                    var_dump($bordero->getErrors());
                    $r1 = ob_get_clean();

                    ob_start();
                    var_dump($preLote->getErrors());
                    $r2 = ob_get_clean();

                    $certo = false;
                    $retorno = array(
                        "msg" => "Erro ao salvar borderô " . $r1 . $r2,
                        "tipo" => "error"
                    );
                    break;
                }
            } else {
                if ($preLote == null) {
                    $preLote = new PreLote;
                    $preLote->NucleoFiliais_id = $proposta->analiseDeCredito->filial->nucleoFiliais->id;
                    $preLote->data_cadastro = date('Y-m-d H:i:s');
                    $preLote->habilitado = 1;
                    $preLote->Usuario_id = Yii::app()->session['usuario']->id;
                    $preLote->StatusPreLote_id = 1;
                    if (!$preLote->save()) {
                        ob_start();
                        var_dump($preLote->getErrors());
                        $r1 = ob_get_clean();
                        $certo = false;
                        $retorno = array(
                            "msg" => "Erro ao salvar PL " . $r1,
                            "tipo" => "error"
                        );
                        break;
                    } else {
                        $certo = true;
                    }
                }

                $ib = ItemDoBordero::model()->find('habilitado AND pagoImagem = ' . $porImagem . ' AND Proposta_id = ' . $proposta->id);

                if ($ib == null) {
                    $ib = new ItemDoBordero;
                    $ib->Bordero = $bordero->id;
                    $ib->Proposta_id = $proposta->id;
                    $ib->Status = 2;
                    $ib->pagoImagem = $porImagem;
                } else {
                    $ib->Bordero = $bordero->id;
                    $ib->Proposta_id = $proposta->id;
                    $ib->Status = 2;
                    $ib->pagoImagem = $porImagem;
                }

                if (!$ib->save()) {
                    ob_start();
                    var_dump($ib->getErrors());
                    $r1 = ob_get_clean();
                    $certo = false;
                    $retorno = array(
                        "msg" => "Erro ao salvar item do bordero " . $r1,
                        "tipo" => "error"
                    );
                    break;
                }
            }
        }

        $preLotesUpdates = PreLote::model()->findAll('habilitado AND StatusPreLote_id = 1');
        foreach ($preLotesUpdates as $plu) {
            $plu->StatusPreLote_id = 2;
            if (!$plu->update()) {
                ob_start();
                var_dump($plu->getErrors());
                $r1 = ob_get_clean();
                $certo = false;
                $retorno = array(
                    "msg" => "Erro ao finalizar pré lote " . $r1,
                    "tipo" => "error"
                );
                break;
            } else {
                $phb = PreLoteHasBordero::model()->findAll('PreLote_id = ' . $plu->id);
                foreach ($phb as $b) {
                    $bord = Bordero::model()->findByPk($b->Bordero_id);
                    $bord->Status = 6;
                    if (!$bord->update()) {
                        ob_start();
                        var_dump($bord->getErrors());
                        $r1 = ob_get_clean();
                        $certo = false;
                        $retorno = array(
                            "msg" => "Erro ao finalizar borderos em pre lotes " . $r1,
                            "tipo" => "error"
                        );
                        break;
                    }
                }
            }
        }

        if ($certo) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        echo json_encode(array('data' => $retorno));
    }

    public function actionGetPreLotes() {
        $rows = [];
        $util = new Util;
        $sql = "SELECT

            AC.data_cadastro,
            AC.urlArquivo,
            AC.Usuario_id,
            AC.id,
            COUNT(DISTINCT B.id) as qtdb,
            COUNT(P.id) as qtdp,
            SUM((P.valor - P.valor_entrada)) as fin,
            SUM((P.valor - P.valor_entrada) - ( ((P.valor - P.valor_entrada)/100) * (Fa.porcentagem_retencao))) AS rep

            FROM ArquivoCNAB AS AC
            INNER JOIN LinhaCNAB AS LC ON LC.ArquivoExtportado_id = AC.id AND LC.habilitado
            INNER JOIN Parcela 					AS Pa ON Pa.id = LC.Parcela_id AND Pa.habilitado
            INNER JOIN Titulo 					AS T 	ON T.id = Pa.Titulo_id 	AND T.habilitado
            INNER JOIN Proposta 				AS P 	ON P.id = T.Proposta_id AND T.habilitado
            INNER JOIN ItemDoBordero                            AS IB 	ON IB.Proposta_id = P.id AND IB.habilitado
            INNER JOIN Bordero 					AS B 	ON B.id = IB.Bordero 	AND B.Status = 6 AND B.habilitado
            INNER JOIN Tabela_Cotacao          	AS TC   ON TC.id = P.Tabela_id
            INNER JOIN ModalidadeTabela        	AS M    ON M.id =   TC.ModalidadeId
            INNER JOIN Fator                  	AS Fa   ON Fa.habilitado  		AND TC.id = Fa.Tabela_Cotacao_id AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas
            WHERE AC.TipoCNAB_id = 2
            GROUP BY AC.id";

        $arquivos = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($arquivos as $arq) {
            if ($arq['id'] != null) {
                
                $fin = 0;
                $rep = 0;

                $sql2 = "SELECT

                        DISTINCT PL.id

                        FROM ArquivoCNAB AS AC
                        INNER JOIN LinhaCNAB                                    AS LC ON LC.ArquivoExtportado_id = AC.id    AND LC.habilitado
                        INNER JOIN Parcela 					AS Pa 	ON Pa.id = LC.Parcela_id            AND Pa.habilitado
                        INNER JOIN Titulo 					AS T 	ON T.id = Pa.Titulo_id              AND T.habilitado
                        INNER JOIN Proposta                                     AS P 	ON P.id = T.Proposta_id             AND T.habilitado
                        INNER JOIN ItemDoBordero                                AS IB 	ON IB.Proposta_id = P.id            AND IB.habilitado
                        INNER JOIN Bordero 					AS B 	ON B.id = IB.Bordero                AND B.Status = 6 AND B.habilitado
                        INNER JOIN PreLote_has_Bordero                          AS PB	ON PB.Bordero_id = B.id             AND PB.habilitado
                        INNER JOIN PreLote 					AS PL 	ON PL.id = PB.PreLote_id            AND PL.habilitado
                        WHERE AC.TipoCNAB_id = 2 AND AC.id = " . $arq['id'];

                $preLotes = Yii::app()->db->createCommand($sql2)->queryAll();

                foreach ($preLotes as $plid) {
                    $pl = PreLote::model()->findByPk($plid['id']);

                    $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $pl->id);
                    $borderos = [];
                    $propostas = [];
                    foreach ($phb as $b) {
                        $borderos[] = $b->Bordero_id;
                        $bord = Bordero::model()->findByPk($b->Bordero_id);
                        if ($bord->habilitado == 1) {
                            foreach ($bord->itemDoBorderos as $prop) {
                                if ($prop->habilitado == 1) {
                                    $propostas[] = $prop;
                                    $fin += $prop->proposta->valor - $prop->proposta->valor_entrada + $prop->proposta->calcularValorDoSeguro();
                                    $rep += $prop->proposta->valorRepasse();
                                }
                            }
                        }
                    }
                }

                $detalhar = '<button value="' . $arq['id'] . '" class="btn btn-xs btn-success" style="border-radius: 100px"><i class="fa fa-plus"></i> Detalhes</button>';
                $import_export = "<a style='border-radius: 10px' class='btn btn-xs btn-info' href='" . $arq['urlArquivo'] . "' download>"
                        . " <i class='fa fa-download'></i>"
                        . "</a>";
                $consolidar = '<button value="' . $arq['id'] . '" class="btn btn-xs btn-danger" style="border-radius: 100px"><i class="fa fa-check"></i> Gerar Lotes</button>';
                $row = array(
                    'deta' => $detalhar,
                    'data' => $util->bd_date_to_view(substr($arq['data_cadastro'], 0, 10)),
                    'qtdb' => $arq['qtdb'] . " Borderôs",
                    'qtdp' => $arq['qtdp'] . " Propostas",
                    'user' => Usuario::model()->findByPk($arq['Usuario_id'])->nome_utilizador,
                    'fina' => "R$ " . number_format($fin, 2, ',', '.'),
                    'repa' => "R$ " . number_format($rep, 2, ',', '.'),
                    'stat' => "AG. IMPORTAÇÃO",
                    'ipep' => $import_export,
                    'cons' => $consolidar
                );
                $rows[] = $row;
            }
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionArquivoDetalhado() {
        $id_arq = $_POST['id_arq'];

        $sql = "SELECT

        DISTINCT PL.id

        FROM ArquivoCNAB AS AC
        INNER JOIN LinhaCNAB                                    AS LC ON LC.ArquivoExtportado_id = AC.id    AND LC.habilitado
        INNER JOIN Parcela 					AS Pa 	ON Pa.id = LC.Parcela_id            AND Pa.habilitado
        INNER JOIN Titulo 					AS T 	ON T.id = Pa.Titulo_id              AND T.habilitado
        INNER JOIN Proposta                                     AS P 	ON P.id = T.Proposta_id             AND T.habilitado
        INNER JOIN ItemDoBordero                                AS IB 	ON IB.Proposta_id = P.id            AND IB.habilitado
        INNER JOIN Bordero 					AS B 	ON B.id = IB.Bordero                AND B.Status = 6 AND B.habilitado
        INNER JOIN PreLote_has_Bordero                          AS PB	ON PB.Bordero_id = B.id             AND PB.habilitado
        INNER JOIN PreLote 					AS PL 	ON PL.id = PB.PreLote_id            AND PL.habilitado
        WHERE AC.TipoCNAB_id = 2 AND AC.id = " . $id_arq;

        $preLotes = Yii::app()->db->createCommand($sql)->queryAll();
        $rows = [];
        $util = new Util;


        foreach ($preLotes as $plid) {
            $pl = PreLote::model()->findByPk($plid['id']);

            $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $pl->id);
            $borderos = [];
            $propostas = [];
            $fin = 0;
            $rep = 0;
            foreach ($phb as $b) {
                $borderos[] = $b->Bordero_id;
                $bord = Bordero::model()->findByPk($b->Bordero_id);
                if ($bord->habilitado == 1) {
                    foreach ($bord->itemDoBorderos as $prop) {
                        if ($prop->habilitado == 1) {
                            $propostas[] = $prop;
                            $fin += $prop->proposta->valor - $prop->proposta->valor_entrada + $prop->proposta->calcularValorDoSeguro();
                            $rep += $prop->proposta->valorRepasse();
                        }
                    }
                }
            }

            $detalhar = '<button value="' . $pl->id . '" class="btn btn-xs btn-success" style="border-radius: 100px"><i class="fa fa-plus"></i> Detalhes</button>';

            $row = array(
                'nucl' => strtoupper($pl->nucleoFiliais->nome),
                'data' => $util->bd_date_to_view(substr($pl->data_cadastro, 0, 10)),
                'qtdb' => count($borderos) . " Borderôs",
                'qtdp' => count($propostas) . " Propostas",
                'user' => $pl->usuario->nome_utilizador,
                'fina' => "R$ " . number_format($fin, 2, ',', '.'),
                'repa' => "R$ " . number_format($rep, 2, ',', '.'),
                'stat' => $pl->statusPreLote->nome,
                'deta' => $detalhar
            );

            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionPreLoteDetalhado() {
        $id = $_POST['id_pl'];
        $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $id);

        $borderos = [];
        $propostas = [];
        $fin = 0;
        $rep = 0;
        $rows = [];

        foreach ($phb as $b) {
            $borderos = [];
            $propostas = [];
            $fin = 0;
            $rep = 0;
            $borderos[] = $b->Bordero_id;
            $bord = Bordero::model()->findByPk($b->Bordero_id);
            foreach ($bord->itemDoBorderos as $prop) {
                $propostas[] = $prop;
                $fin += $prop->proposta->valor - $prop->proposta->valor_entrada + $prop->proposta->calcularValorDoSeguro();
                $rep += $prop->proposta->valorRepasse();
            }

            $row = array(
                'deta' => '<button value="' . $bord->id . '" class="btn btn-xs btn-success" style="border-radius: 100px"><i class="fa fa-plus"></i> Detalhes</button>',
                'fili' => $bord->destinatarioObj->getConcat(),
                'qtdb' => count($borderos) . " Borderôs",
                'qtdp' => count($propostas) . " Propostas",
                'fina' => "R$ " . number_format($fin, 2, ',', '.'),
                'repa' => "R$ " . number_format($rep, 2, ',', '.')
            );

            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionBorderoDetalhado() {
        $id = $_POST['id_bord'];
        $rows = [];
        $util = new Util;
        $bord = Bordero::model()->findByPk($id);
        foreach ($bord->itemDoBorderos as $prop) {
            $row = array(
                'data' => $util->bd_date_to_view(substr($prop->proposta->data_cadastro, 0, 10)),
                'codi' => $prop->proposta->codigo,
                'clie' => strtoupper($prop->proposta->analiseDeCredito->cliente->pessoa->nome),
                'inic' => "R$ " . number_format($prop->proposta->valor, 2, ',', '.'),
                'entr' => "R$ " . number_format($prop->proposta->valor_entrada, 2, ',', '.'),
                'care' => $prop->proposta->carencia,
                'fina' => "R$ " . number_format(($prop->proposta->valor - $prop->proposta->valor_entrada + $prop->proposta->calcularValorDoSeguro()), 2, ',', '.'),
                'repa' => "R$ " . number_format($prop->proposta->valorRepasse(), 2, '.', ','),
                'parc' => $prop->proposta->qtd_parcelas . "x R$ " . number_format($prop->proposta->valor_parcela, 2, ',', '.')
            );
            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionImportarDP() {

        $certo = true;
        $arrReturn = array(
            "hasError" => true,
            "msg" => "Escolha um arquivo",
            "tipo" => "error"
        );

        try {

            if (isset($_FILES['anexo_importacao'])) {

                $aleatorio = date('YmdHis');
                $ext = substr($_FILES['anexo_importacao']['name'], strrpos($_FILES['anexo_importacao']['name'], '.'));

                if (move_uploaded_file($_FILES['anexo_importacao']['tmp_name'], '/var/www/backoffice/uploads/' . $aleatorio . '_DePara' . $ext)) {

                    $transaction = Yii::app()->db->beginTransaction();

                    $arquivoCNAB = new ArquivoCNAB;
                    $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
                    $arquivoCNAB->habilitado = 1;
                    $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
                    $arquivoCNAB->TipoCNAB_id = 4;
                    $arquivoCNAB->urlArquivo = "/uploads/" . $aleatorio . "_DePara" . $ext;

                    if ($arquivoCNAB->save()) {

                        $arquivo = fopen('/var/www/backoffice/uploads/' . $aleatorio . '_DePara' . $ext, 'r');
                        $i = 0;

                        while (!feof($arquivo)) {

                            $i++;
                            $linha = fgets($arquivo);

                            if (substr($linha, 0, 1) == "1" && $linha != '') {

                                $idProposta = (int) substr($linha, 1, 12);

                                $proposta = Proposta::model()->find("habilitado AND id = $idProposta");

                                if ($proposta !== null) {

                                    $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = 1 AND Proposta_id = $proposta->id");

                                    if ($proposta->Status_Proposta_id == 8 || $titulo !== null || ($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {

                                        $seqParcela = (int) substr($linha, 41, 3);

                                        if ($proposta->Status_Proposta_id == 8 || ($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                            $parcela = 0;
                                        } else {
                                            if ($titulo !== null) {
                                                $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela");
                                                if ($parcela !== null) {
                                                    $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela")->id;
                                                } else {
                                                    $certo = false;

                                                    $arrReturn = array(
                                                        "hasError" => true,
                                                        "msg" => "Parcelas a pagar da Proposta de código $proposta->codigo não encontrada"
                                                        . " SeqParcela: $seqParcela",
                                                        "tipo" => "error"
                                                    );

                                                    break;
                                                }
                                                $parcela2 = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela");
                                            } else {
                                                $certo = false;

                                                $arrReturn = array(
                                                    "hasError" => true,
                                                    "msg" => "Título a pagar da Proposta de código $proposta->codigo não encontrada"
                                                    . " SeqParcela: $seqParcela",
                                                    "tipo" => "error"
                                                );

                                                break;
                                            }
                                        }

                                        if ($parcela !== null) {

                                            if ($parcela2 != null) {
                                                $parcela2->ref_lecca = intval(substr($linha, 13, 12)) . substr($linha, 41, 3);

                                                $parcela2->update();
                                            }

                                            if (($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                                $linhaCNAB = null;
                                            } else {
                                                $linhaCNAB = LinhaCNAB::model()->find("habilitado AND Parcela_id = $parcela");
                                            }

                                            if ($linhaCNAB == null || ($linhaCNAB->arquivoExtportado->TipoCNAB_id !== 4)) {

                                                $linhaCNAB = new LinhaCNAB;
                                                $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                                                $linhaCNAB->habilitado = 1;
                                                $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                                                $linhaCNAB->Parcela_id = $parcela;
                                                $linhaCNAB->caracteres = $linha;

                                                if (!$linhaCNAB->save()) {

                                                    ob_start();
                                                    var_dump($linhaCNAB->getErrors());
                                                    $r1 = ob_get_clean();

                                                    $arrReturn = array(
                                                        "hasError" => false,
                                                        "msg" => $r1 . '(' . $i . ')' . "linhas: " . $linha,
                                                        "tipo" => "error"
                                                    );

                                                    $certo = false;

                                                    break;
                                                }
                                            } else {
                                                if (!($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                                    $arrReturn = array(
                                                        "hasError" => false,
                                                        "msg" => "Proposta $proposta->codigo já importada. ID Parcela: $parcela->id"
                                                        . " SeqParcela: $seqParcela " . ' substr($linha, 41,3): ' . substr($linha, 41, 3)
                                                        . " i: $i",
                                                        "tipo" => "error"
                                                    );

                                                    $certo = false;

                                                    break;
                                                }
                                            }
                                        } else {

                                            $certo = false;

                                            $arrReturn = array(
                                                "hasError" => true,
                                                "msg" => "Não foi possível realizar a importação. Parcela do Título a pagar da Proposta de $proposta->codigo não encontrada"
                                                . " SeqParcela: $seqParcela",
                                                "tipo" => "error"
                                            );

                                            break;
                                        }
                                    } else {

                                        $certo = false;

                                        $arrReturn = array(
                                            "hasError" => true,
                                            "msg" => "Não foi possível realizar a importação. Título a pagar da Proposta de $proposta->codigo não encontrado",
                                            "tipo" => "error"
                                        );

                                        break;
                                    }
                                } else {

                                    $certo = false;

                                    $arrReturn = array(
                                        "hasError" => true,
                                        "msg" => "Não foi possível realizar a importação. Proposta de id $idProposta não encontrada",
                                        "tipo" => "error"
                                    );

                                    break;
                                }
                            }
                        }

                        if ($certo) {
                            $arrReturn = array(
                                "hasError" => false,
                                "msg" => "Importação realizada com sucesso",
                                "tipo" => "success"
                            );
                        }
                    } else {

                        $certo = false;

                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Não foi possível realizar a importação",
                            "tipo" => "error"
                        );
                    }

                    if ($certo) {

                        $s3 = new S3Client;
                        $rS3Up = $s3->putLecca("uploads/" . $aleatorio . "_DePara" . $ext, $aleatorio . "_DePara" . $ext);

                        if ($rS3Up['@metadata']['statusCode'] == '200') {
                            unlink("uploads/" . $aleatorio . "_DePara" . $ext);
                            $arquivoCNAB->urlArquivo = $rS3Up['@metadata']['effectiveUri'];
                            $arquivoCNAB->update();
                        }

                        $transaction->commit();
                    } else {

                        $transaction->rollBack();
                    }
                } else {

                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível mover o arquivo",
                        "tipo" => "error"
                    );
                }
            } else {

                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Nenhum arquivo enviado",
                    "tipo" => "error"
                );
            }
        } catch (Exception $ex) {

            $arrReturn = array(
                "hasError" => true,
                "msg" => $ex->getMessage(),
                "tipo" => "error"
            );
        }

        echo json_encode($arrReturn);
    }

    public function actionLotesDP() {
        $max = Yii::app()->db
                ->createCommand("SELECT MAX(id) AS 'ultimoImportado' FROM ArquivoCNAB WHERE habilitado AND TipoCNAB_id = 4")
                ->queryRow();

        $propostas = [];

        $transaction = Yii::app()->db->beginTransaction();

        $certo = true;

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Lotes gerados com sucesso",
            "tipo" => "success"
        );

        $linhas = LinhaCNAB::model()->findAll('habilitado AND ArquivoExtportado_id = ' . $max['ultimoImportado']);
        $propostasDP = [];
        foreach ($linhas as $l) {
            $idProposta = (int) substr($l->caracteres, 1, 12);
            if (!in_array($idProposta, $propostasDP)) {
                $propostasDP[] = (string) $idProposta;
            }
        }

        $preLotes = PreLote::model()->findAll('habilitado AND StatusPreLote_id = 2');
        foreach ($preLotes as $pl) {
            $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $pl->id);
            $selecionar = false;

            foreach ($phb as $pb) {
                $bord = Bordero::model()->findByPk($pb->Bordero_id);
                foreach ($bord->itemDoBorderos as $bor) {
                    if (in_array($bor->proposta->id, $propostasDP) && $bor->habilitado == 1) {
                        $selecionar = true;
                    }
                }
            }

            if ($selecionar) {
                $loteExistente = LotePagamento::model()->find(
                        "data_cadastro > '" . date('Y-m-d H:i:s', strtotime('-1minutes', strtotime(date('Y-m-d H:i:s')))) .
                        "' AND habilitado AND NucleoFiliais_id = " . $pl->NucleoFiliais_id);

                if ($loteExistente == null) {
                    $lote = new LotePagamento;
                    $lote->data_cadastro = date('Y-m-d H:i:s');
                    $lote->habilitado = 1;
                    $lote->Usuario_id = $pl->Usuario_id;
                    $lote->NucleoFiliais_id = $pl->NucleoFiliais_id;

                    $pl->habilitado = 0;
                    $pl->StatusPreLote_id = 1;
                    if (!$lote->save() || !$pl->update()) {
                        $certo = false;
                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Não foi possível salvar determinado lote",
                            "tipo" => "error"
                        );
                        break;
                    } else {
                        $lhs = new LotePagamentoHasStatusLotePagamento;
                        $lhs->StatusLotePagamento_id = 1;
                        $lhs->LotePagamento_id = $lote->id;
                        $lhs->data_cadastro = date('Y-m-d H:i:s');
                        $lhs->Usuario_id = $lote->Usuario_id;
                        $lhs->habilitado = 1;
                        if (!$lhs->save()) {
                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Não foi possível o status do lote",
                                "tipo" => "error"
                            );
                            break;
                        }
                    }

                    $phb = PreLoteHasBordero::model()->findAll('habilitado AND PreLote_id = ' . $pl->id);
                    foreach ($phb as $b) {

                        $lhb = new LoteHasBordero;
                        $lhb->habilitado = 1;
                        $lhb->Bordero_id = $b->Bordero_id;
                        $lhb->Lote_id = $lote->id;
                        $b->habilitado = 0;
                        if (!$lhb->save() || !$b->update()) {
                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Não foi possível salvar determinado lhb",
                                "tipo" => "error"
                            );
                            break;
                        }

                        $bord = Bordero::model()->findByPk($b->Bordero_id);
                        $bord->Status = 2;
                        if (!$bord->update()) {
                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Não foi possível atualizar o status de algum bordero",
                                "tipo" => "error"
                            );
                            break;
                        }

                        foreach ($bord->itemDoBorderos as $prop) {
                            $propostas[] = $prop->proposta->id;
                        }
                    }
                } else {
                    $phb = PreLoteHasBordero::model()->findAll('PreLote_id = ' . $pl->id);
                    foreach ($phb as $b) {

                        $lhb = new LoteHasBordero;
                        $lhb->habilitado = 1;
                        $lhb->Bordero_id = $b->Bordero_id;
                        $lhb->Lote_id = $loteExistente->id;
                        $b->habilitado = 0;
                        $pl->habilitado = 0;
                        $pl->StatusPreLote_id = 1;

                        if (!$lhb->save() || !$b->update() || !$pl->update()) {
                            ob_start();
                            var_dump($lhb->getErrors());
                            $r1 = ob_get_clean();

                            ob_start();
                            var_dump($b->getErrors());
                            $r2 = ob_get_clean();

                            ob_start();
                            var_dump($pl->getErrors());
                            $r3 = ob_get_clean();


                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Não foi possível salvar determinado lhb (else)" . $r1 . $r2 . $r3,
                                "tipo" => "error"
                            );
                            break;
                        }

                        $bord = Bordero::model()->findByPk($b->Bordero_id);
                        $bord->Status = 2;
                        if (!$bord->update()) {
                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Não foi possível atualizar o status de algum bordero",
                                "tipo" => "error"
                            );
                            break;
                        }

                        foreach ($bord->itemDoBorderos as $prop) {
                            $propostas[] = $prop->proposta->id;
                        }
                    }
                }

                if (!$certo) {
                    break;
                }
            }
        }

        if ($certo) {
            $linhas = LinhaCNAB::model()->findAll('habilitado AND ArquivoExtportado_id = ' . $max['ultimoImportado']);

            foreach ($linhas as $l) {
                $idProposta = (int) substr($l->caracteres, 1, 12);
                $pr = Proposta::model()->findByPk($idProposta);
                if (!in_array($pr->id, $propostas)) {
                    $certo = false;
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Proposta não encontrada nos PréLotes: " . $pr->codigo,
                        "tipo" => "error"
                    );
                    break;
                }
            }

            if ($certo) {
                foreach ($propostas as $p) {
                    if (!in_array($p, $propostasDP)) {
                        $proposta = Proposta::model()->findByPk($p);
                        if ($proposta != null) {
                            $titulo = Titulo::model()->find('NaturezaTitulo_id = 2 AND Proposta_id = ' . $proposta->id);
                            if ($titulo != null) {
                                $parcela = Parcela::model()->find('Titulo_id = ' . $titulo->id);
                                if ($parcela != null) {
                                    $linhaCNAB = LinhaCNAB::model()->findAll('Parcela_id = ' . $parcela->id);
                                    foreach ($linhaCNAB as $lc) {
                                        $lc->habilitado = 0;
                                        if (!$lc->update()) {
                                            $certo = false;
                                            $arrReturn = array(
                                                "hasError" => true,
                                                "msg" => "Não foi possível desabilitar a linhaCNAB: " . $lc->id,
                                                "tipo" => "error"
                                            );
                                            break;
                                        }
                                    }
                                } else {
                                    $certo = false;
                                    $arrReturn = array(
                                        "hasError" => true,
                                        "msg" => "Parcela não localizada: " . $p,
                                        "tipo" => "error"
                                    );
                                    break;
                                }

                                $itens = ItemDoBordero::model()->findAll('Proposta_id = ' . $proposta->id);
                                if (count($itens) > 0) {
                                    foreach ($itens as $i) {
                                        $i->habilitado = 0;
                                        if (!$i->update()) {
                                            $certo = false;
                                            $arrReturn = array(
                                                "hasError" => true,
                                                "msg" => "Item do Bordero não desabilitado: " . $i->id,
                                                "tipo" => "error"
                                            );
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $certo = false;
                                $arrReturn = array(
                                    "hasError" => true,
                                    "msg" => "Titulo não localizado: " . $p,
                                    "tipo" => "error"
                                );
                                break;
                            }
                        } else {
                            $certo = false;
                            $arrReturn = array(
                                "hasError" => true,
                                "msg" => "Proposta não localizada: " . $p,
                                "tipo" => "error"
                            );
                            break;
                        }
                    }

                    $titulo = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 2 AND Proposta_id = ' . $p);
                    if ($titulo != null) {
                        $parcela = Parcela::model()->find('habilitado AND Titulo_id = ' . $titulo->id);
                        if ($parcela != null) {
                            $linhaCNAB = LinhaCNAB::model()->findAll('habilitado AND Parcela_id = ' . $parcela->id);
                            foreach ($linhaCNAB as $lc) {
                                $arquivo = $lc->arquivoExtportado;
                                if ($arquivo->habilitado == 1) {
                                    foreach ($arquivo->linhaCNABs as $lns) {

                                        if ($lns->habilitado == 1) {
                                            $titulo_sl = Titulo::model()->findByPk($lns->parcela->Titulo_id);
                                            $prop_sl = Proposta::model()->findByPk($titulo_sl->Proposta_id);
                                            $item_sl = ItemDoBordero::model()->findAll("Proposta_id = " . $prop_sl->id);
                                            foreach ($item_sl as $pib2) {
                                                $loteHasB = LoteHasBordero::model()->find('Bordero_id = ' . $pib2->Bordero);
                                                if ($loteHasB == null) {
                                                    $bord_sl = Bordero::model()->findByPk($pib2->Bordero);

                                                    $bord_sl->habilitado = 0;
                                                    $pib2->habilitado = 0;

                                                    if (!$bord_sl->update() || !$pib2->update()) {
                                                        $certo = false;
                                                        $arrReturn = array(
                                                            "hasError" => true,
                                                            "msg" => "Não foi possível desabilitar bordero/item 2",
                                                            "tipo" => "error"
                                                        );
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        $lns->habilitado = 0;
                                        if (!$lns->update()) {
                                            $certo = false;
                                            $arrReturn = array(
                                                "hasError" => true,
                                                "msg" => "Não foi possível desabilitar linha CNAB 2",
                                                "tipo" => "error"
                                            );
                                            break;
                                        }
                                    }
                                }
                                $arquivo->habilitado = 0;
                                if (!$arquivo->update()) {
                                    $certo = false;
                                    $arrReturn = array(
                                        "hasError" => true,
                                        "msg" => "Não foi possível desabilitar o arquivo 2",
                                        "tipo" => "error"
                                    );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($certo) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        echo json_encode($arrReturn);
    }

}
