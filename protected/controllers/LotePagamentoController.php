<?php

class LotePagamentoController extends Controller{

	public $layout = '';

	public function actionIndex()
	{
	 $this->render('index');
	}

  public function filters()
  {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
  }

  public function accessRules()
  {
        return [
            /*
            [
                'allow',
                'actions' => [],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            */
            [
                'allow',
                'actions'   => ['borderos'],
                'users'     => ['@']
            ],
            [
                'deny',
                'users'     => ['*']
            ]
        ];
  }

  //sobrescreve a função do Controller, classe pai
  public function init()
  {

  }

  public function actionBorderos()
  {
    if( $_POST['ambiente'] == 'n' )
    {
  	 echo json_encode( LotePagamento::model()->getBorderos($_POST['draw'], $_POST['start'], $_POST['length'], $_POST['loteId'], $_POST['ambiente'] ) );
    }
    else
    {
     echo json_encode( LotePagamentoBeta::model()->getBorderos($_POST['draw'], $_POST['start'], $_POST['length'], $_POST['loteId'], $_POST['ambiente'] ) ); 
    }
  }
}