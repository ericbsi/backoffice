<?php

class FinanceiroController extends Controller {

    public $layout = '';

    public function actionIndex() {
        $this->render('index');
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return [
            /*
              [
              'allow',
              'actions' => [],
              'users' => ['@'],
              'expression' => 'Yii::app()->session["usuario"]->autorizado()'
              ],
             */
            [
                'allow',
                'actions' => ['arquivosGerados','valoresAReceber', 'gridLotesAReceber', 'liquidacoesFdic', 'exportarLinhasCNABLecca'],
                'users' => ['@']
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    public function actionArquivosGerados(){

        $start = $_POST['start'];

        $sql = "SELECT AC.*, COUNT(LC.ArquivoExtportado_id) AS qtd 
                FROM        ArquivoCNAB AS  AC
                INNER JOIN  LinhaCNAB   AS  LC  ON LC.ArquivoExtportado_id = AC.id AND LC.habilitado
                WHERE AC.TipoCNAB_id = 3 AND AC.habilitado
                GROUP BY AC.id order by id desc ";

        $total = count(Yii::app()->db->createCommand($sql)->queryAll());

        $sql .= " limit $start, 10 ";

        $arquivos = Yii::app()->db->createCommand($sql)->queryAll();

        $rows = [];

        $util = new Util;

        foreach ($arquivos as $arq)
        {

            $row = array(
                'idArquivo'     => $arq['id'],
                'dt_geracao'    => $util->bd_date_to_view(substr($arq['data_cadastro'], 0, 10)),
                'qtd_titulos'   => $arq['qtd'],
                'usuario'       => Usuario::model()->findByPk($arq['Usuario_id'])->nome_utilizador,
                'download'      => ''
                . '<a href="' . $arq['urlArquivo'] . '" download>Baixar'
                . '     <i class="clip-download">'
                . '     </i>'
                . '</a>'
            );

            $rows[]             = $row;
        }

        echo json_encode(
            array(
                'data'              => $rows,
                "draw"              => $_POST['draw'],
                "recordsTotal"      => count($rows),
                "recordsFiltered"   => $total
            )
        );
    }

    //sobrescreve a função do Controller, classe pai
    public function init() {
        
    }

    public function actionValoresAReceber() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');
        /* FIM CSS */

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);

        $cs->registerScriptFile('../assets/ubold/js/gestorDeFiliais/fn-contas-a-receber-gestor.js', CClientScript::POS_END);

        $this->layout = '//layouts/ubold';

        $this->render('/gestorDeFiliais/valoresAReceber');
    }

    public function actionGridLotesAReceber() {
        echo json_encode(Titulo::model()->listarLotesPagamento($_POST['draw'], $_POST['start'], $_POST['length'], $_POST['situacao'], $_POST['de'], $_POST['ate'], $_POST['nucleo']));
    }

    public function actionLiquidacoesFdic(){

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $this->layout = '//layouts/ubold';

        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');
        
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
        
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);

        $cs->registerScriptFile('/assets/ubold/js/financeiro/fn-financeiro-baixasfdic.js', CClientScript::POS_END);

        $this->render('liquidacoesFdic');
    }

    public function actionExportarLinhasCNABLecca() {

        $util                   = new Util;

        $de                     = $util->view_date_to_bd($_POST['de']);
        $ate                    = $util->view_date_to_bd($_POST['te']);

        $retorno2               = [
            "hasErrors"         => false,
            "title"             => "Sucesso",
            "msg"               => "CNAB gerado com sucesso",
            "type"              => "success",
            "nomeArquivo"       => "nada",
            "url"               => "nada",
            "cnab"              => ""
        ];

        $erro                   = "";
        $linhas                 = "";
        $retorno                = "";
        $cont                   = 1;

        try {

            $transaction = Yii::app()->db->beginTransaction();

            $arquivoCNAB = new ArquivoCNAB;
            $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
            $arquivoCNAB->habilitado = 1;
            $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
            $arquivoCNAB->TipoCNAB_id = 3;
            $arquivoCNAB->urlArquivo = "/";

            $sql     = "SELECT P.id as idProposta, P.Financeira_id as fin, Pa.seq as seq, Pa.id AS idParcela, ";  
            $sql    .= " B.valor_pago AS valor_pago, Pa.ref_lecca AS NU_DOCUMENTO, B.data_da_baixa AS dataBaixa ";
            $sql    .= "FROM Parcela AS Pa ";

            $sql    .= "INNER JOIN Baixa            AS B    ON   B.baixado    AND Pa.id =   B.Parcela_id ";
            $sql    .= "INNER JOIN Titulo           AS T    ON   T.habilitado AND  T.id =  Pa.Titulo_id            AND  T.NaturezaTitulo_id    =   1 ";
            $sql    .= "INNER JOIN Proposta         AS P    ON   P.habilitado AND  P.id =   T.Proposta_id          AND  P.Financeira_id        IN (11) ";
            $sql    .= "WHERE Pa.habilitado AND (Pa.ref_lecca is not null) AND B.data_da_baixa BETWEEN '".$de." 00:00:00' AND '".$ate." 23:59:59' ";
            $sql    .= "GROUP BY B.Parcela_id ";

            $parcelasBaixadas = Yii::app()->db->createCommand($sql)->queryAll();

            if (count($parcelasBaixadas) > 0) {

                if ($arquivoCNAB->save()) {

                    foreach ($parcelasBaixadas as $pb) {

                        if ($pb['fin'] == 11) {


                            $consultoria = str_pad($pb ["NU_DOCUMENTO"], 18, "0", STR_PAD_LEFT);
                            $consultoria = str_pad($consultoria, 25, " ", STR_PAD_RIGHT);

                            $documento = str_pad($pb["NU_DOCUMENTO"], 11, "0", STR_PAD_LEFT);
                            $titulo = str_pad(substr($pb["NU_DOCUMENTO"], 0, -3), 10, "0", STR_PAD_LEFT);
                        } else {
                            $consultoria = str_pad($pb["idParcela"], 25, " ", STR_PAD_RIGHT);

                            $proposta = Proposta::model()->findByPk($pb['idProposta']);
                            $venda = $proposta->venda;
                            $notaF = null;
                            foreach ($venda->notasFiscais as $nf) {
                                $notaF = $nf->notaFiscal;
                            }

                            $documento = str_pad(($notaF->documento . $pb['seq']), 11, "0", STR_PAD_RIGHT);
                            $titulo = $titulo = str_pad(substr($pb["idParcela"], 0, -3), 10, "0", STR_PAD_LEFT);
                        }

                        $parcela = Parcela::model()->findByPk($pb['idParcela']);

                        $valorParcelaRepasse = ($parcela->titulo->proposta->valorRepasse() / $parcela->titulo->proposta->qtd_parcelas);
                        $valorParcela = $parcela->valor;

                        $cont++;

                        $linha = "";

                        $valorFormatadoRepasse = number_format($valorParcelaRepasse, 2, '', '');
                        $valorFormatado = number_format($valorParcela, 2, '', '');
                        $valorPago = number_format($pb['valor_pago'], 2, '', '');

                        $cpf = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                        $nomeCliente = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome;
                        $endereco = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getEndereco();

                        $cedente = "CREDSHOW FUNDO DE INVESTIMENTOS EM DIREITOS CR23273905000130";


                        if ($endereco == null) {

                            str_repeat(" ", 40);
                        } else {
                            $enderecoCompleto = trim($endereco->logradouro) . " ";
                            $enderecoCompleto .= trim($endereco->numero) . " ";
                            $enderecoCompleto .= trim($endereco->bairro) . " ";
                            $enderecoCompleto .= trim($endereco->cidade) . " ";
                            $enderecoCompleto .= trim($endereco->uf) . " ";
                            $enderecoCompleto .= trim($endereco->complemento) . " ";

                            $enderecoCEP = trim($endereco->cep);

                            $enderecoCompleto = strtoupper(substr(strtr(utf8_decode(trim($enderecoCompleto)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                            $nomeCliente = strtoupper(substr(strtr(utf8_decode(trim($nomeCliente)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                        }

                        $dateTime = new DateTime($pb["dataBaixa"]);
                        $dataBaixa = $dateTime->format("dmy");

                        $linha .= "1"; //de:   1 ate:   1
                        $linha .= str_repeat(" ", 19); //de:   2 ate:  20
                        $linha .= "02"; //de:  21 ate:  22 
                        $linha .= "00"; //de:  23 ate:  24 
                        $linha .= str_repeat("0", 04); //de:  25 ate:  28 
                        $linha .= "00"; //de:  29 ate:  30 
                        $linha .= "0199"; //de:  31 ate:  34 
                        $linha .= "  "; //de:  35 ate:  36
                        $linha .= "0"; //de:  37 ate:  37
                        $linha .= $consultoria; //de:  38 ate:  62 /** nosso numero?*/ verificar isso depois
                        $linha .= str_repeat("0", 03); //de:  63 ate:  65
                        $linha .= str_repeat("0", 05); //de:  66 ate:  70
                        $linha .= $documento; //de:  71 ate:  81
                        $linha .= str_repeat(" ", 01); //de:  82 ate:  82
                        $linha .= str_pad($valorPago, 10, "0", STR_PAD_LEFT); //de:  83 ate:  92
                        $linha .= str_repeat(" ", 01); //de:  93 ate:  93
                        $linha .= str_repeat(" ", 01); //de:  94 ate:  94
                        //$linha .= str_repeat("0", 06); //de:  95 ate: 100
                        $linha .= $dataBaixa; //de:  95 ate: 100
                        $linha .= str_repeat(" ", 04); //de: 101 ate: 104
                        $linha .= str_repeat(" ", 01); //de: 105 ate: 105
                        $linha .= str_repeat(" ", 01); //de: 106 ate: 106
                        $linha .= str_repeat(" ", 02); //de: 107 ate: 108
                        //                    $linha .= str_pad($parcela->seq         ,  2, "0", STR_PAD_LEFT)          ; //de: 109 ate: 110
                        $linha .= "02"; //de: 109 ate: 110
                        //$linha .= "          "; //de: 111 ate: 120
                        $linha .= $titulo; //de: 111 ate: 120
                        $linha .= date("dmy", strtotime($parcela->vencimento)); //de: 121 ate: 126
                        $linha .= str_pad($valorFormatado, 13, "0", STR_PAD_LEFT); //de: 127 ate: 139
                        $linha .= str_repeat("0", 03); //de: 140 ate: 142
                        $linha .= str_repeat("0", 05); //de: 143 ate: 147
                        $linha .= "01"; //de: 148 ate: 149
                        $linha .= " "; //de: 150 ate: 150
                        $linha .= date("dmy", strtotime($parcela->titulo->proposta->data_cadastro)); //de: 151 ate: 156
                        $linha .= "00"; //de: 157 ate: 158
                        $linha .= "0"; //de: 159 ate: 159
                        $linha .= "02"; //de: 160 ate: 161
                        $linha .= str_repeat("0", 12); //de: 162 ate: 173
                        $linha .= str_repeat("0", 19); //de: 174 ate: 192
                        $linha .= str_pad($valorFormatadoRepasse, 13, "0", STR_PAD_LEFT); //de: 193 ate: 205
                        $linha .= str_repeat("0", 13); //de: 206 ate: 218
                        $linha .= "01"; //de: 219 ate: 220
                        $linha .= str_pad($cpf, 14, " ", STR_PAD_LEFT); //de: 221 ate: 234
                        $linha .= str_pad($nomeCliente, 40, " ", STR_PAD_RIGHT); //de: 235 ate: 274
                        $linha .= str_pad($enderecoCompleto, 40, " ", STR_PAD_RIGHT); //de: 275 ate: 314
                        $linha .= "         "; //de: 315 ate: 323
                        $linha .= "   "; //de: 324 ate: 326
                        $linha .= str_pad($enderecoCEP, 8, "0", STR_PAD_LEFT); //de: 327 ate: 334
                        $linha .= $cedente; //de: 335 ate: 394
                        $linha .= str_pad(" ", 44, " ", STR_PAD_LEFT);
                        //de: 395 ate: 438
                        $linha .= str_pad($cont, 06, "0", STR_PAD_LEFT); //de: 439 ate: 444

                        $linha .= chr(13) . chr(10);

                        $linhas .= $linha;

                        $linhaCNAB = new LinhaCNAB;
                        $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                        $linhaCNAB->habilitado = 1;
                        $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                        $linhaCNAB->Parcela_id = $parcela->id;
                        $linhaCNAB->caracteres = utf8_encode($linha);

                        if (!$linhaCNAB->save()) {

                            ob_start();
                            var_dump($linhaCNAB->getErrors());
                            $erro = ob_get_clean();

                            $transaction->rollBack();

                            $linhas = "";

                            break;
                        }
                    }

                    if (!empty($linhas)) {

                        $cont++;

                        $qtdLinhas = str_pad($cont, 6, "0", STR_PAD_LEFT);

                        $dataRemessa = date("dmy");

                        $retorno = "01REMESSA01COBRANCA       00000000000000001291CREDSHOW FUNDO DE INVESTIMENTO611PAULISTA S.A.  $dataRemessa        MX0000001                                                                                                                                                                                                                                                                                                                                 000001";

                        $retorno .= chr(13) . chr(10);

                        $retorno .= $linhas;

                        $retorno .= "9                                                                                                                                                                                                                                                                                                                                                                                                                                                     $qtdLinhas";
                    }
                } else {

                    ob_start();
                    var_dump($arquivoCNAB->getErrors());
                    $erro = ob_get_clean();

                    $transaction->rollBack();
                }

                //return ["linhas" => $retorno, "arquivoCNAB" => $arquivoCNAB, "erro" => $erro];

                $cnab = $retorno;

                if (!empty($cnab)) {

                    $pasta = "liquidacoesLECCA";

                    if (!is_dir($pasta)) {
                        mkdir("liquidacoesLECCA", 0700);
                    }

                    $cont = 1;

                    $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";

                    while (file_exists($pasta . "/" . $arquivo)) {

                        $cont++;

                        $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";
                    }

                    $file = fopen($pasta . "/" . $arquivo, 'w');
                    fwrite($file, $cnab);
                    fclose($file);

                    $retorno2["url"] = "/" . $pasta . "/" . $arquivo;
                    $retorno2["nomeArquivo"] = $arquivo;
                    $retorno2["sql"] = $sql;

                    $arquivoCNAB->urlArquivo = $retorno2["url"];

                    if (!$arquivoCNAB->update()) {

                        ob_start();
                        var_dump($arquivoCNAB->getErrors());
                        $erro = ob_get_clean();

                        $transaction->rollBack();
                    } else {
                        $transaction->commit();
                    }
                } else {

                    $erroMsg = "";

                    if (isset($erro) && !empty($erro)) {
                        $erroMsg = $erro;
                    }

                    $retorno2 = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => $erroMsg,
                        "type" => "error",
                        "cnab" => $cnab,
                        "sql" => $sql,
                    ];
                }
            } else {

                $retorno2 = [
                    "hasErrors" => true,
                    "title" => "Erro",
                    "msg" => "Não foram registradas baixas desde a geração do ultimo arquivo",
                    "type" => "error",
                    "post" => $_POST,
                    "sql" => $sql,
                ];
            }
        } catch (Exception $ex) {

            $retorno2 = [
                "hasErrors" => true,
                "title" => "Erro",
                "msg" => $ex->getMessage(),
                "type" => "error",
                "post" => $_POST,
                "sql" => $sql,
            ];
        }

        echo json_encode($retorno2);
    }
}