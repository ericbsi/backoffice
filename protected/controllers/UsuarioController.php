<?php

class UsuarioController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
            array('allow',
                'actions'       => array(
                                            'getAdministradores', 
                                            'cadastrarAdmin', 
                                            'administradores', 
                                            'getUsuarios', 
                                            'getFiliaisAdmin', 
                                            'changePassword', 
                                            'index', 
                                            'view', 
                                            'checkUsername', 
                                            'create', 
                                            'update', 
                                            'admin', 
                                            'delete',
                                            'mudarSenha',
                                            'mudarUtilizador',
                                            'mudarUsername',
                                            'listarFiliaisSelect', 
                                            'mudarFilialSelect',
                                            'marcarTodasFiliaisAdmin',
                                            'marcarFilialAdmin',
                                            'salvarAdmin'
                                        ),
                'users'         => array('@'),
                /*'expression'    =>  'Yii::app()->session["usuario"]->role == "empresa_admin" || Yii::app()->session["usuario"]->role == "super"'*/
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    
    public function actionMarcarTodasFiliaisAdmin()
    {
        $retorno = Usuario::model()->marcarTodasFiliaisAdmin($_POST['idUsuario'],$_POST['classeCheck']);
        
        echo json_encode($retorno);
    }
    
    public function actionMarcarFilialAdmin()
    {
        $retorno = Usuario::model()->marcarFilialAdmin($_POST['idUsuario'],$_POST['idFilial'],$_POST['classeCheck']);
        
        echo json_encode($retorno);
    }

    public function actionCadastrarAdmin() {

        echo json_encode(Usuario::model()->novoAdminEmpresas($_POST['Admin'], $_POST['Parceiros']));
    }

    public function actionSalvarAdmin() {

        $retorno = Usuario::model()->salvarAdmin    (
                                                        $_POST['nomeUtilizador' ], 
                                                        $_POST['username'       ], 
                                                        $_POST['senha'          ], 
                                                        $_POST['parceiros'      ]
                                                    );
        
        echo json_encode($retorno);
    }

    public function actionAdministradores() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-administradores-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/usuario/fn-gerenciarAdministradores.js', CClientScript::POS_END);

        $this->render('/usuario/administradores');
    }

    public function actionGetAdministradores() {

        echo json_encode(Usuario::model()->getAdmins($_POST['draw'], $_POST['start']));
    }

    public function actionCheckUsername() {

        $username = $_POST['username'];

        $return = Usuario::model()->checkUserNameAvailability($username);
    }

    public function actionView($id) {

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /* Se o usuario for analista de credito, não criar filialhasusuario */

    public function actionCreate() {

        //Yii::app()->session['usuario']->updateVistoPorUltimo();

        $model = new Usuario;
        $empresaHasUsuario = new EmpresaHasUsuario;
        $filialHasUsuario = new FilialHasUsuario;

        if (!empty($_POST['Usuario'])) {

            $model->attributes = $_POST['Usuario'];
            $model->password = Yii::app()->hasher->hashPassword($model->password);
            $role = Role::model()->find("papel = " . "'$model->tipo'");

            $model->data_cadastro = date('Y-m-d H:i:s');
            $model->data_cadastro_br = date('d/m/Y H:i:s');

            $model->tipo_id = $role->id;
            $model->role = $role->papel;

            if ($model->save()) {

                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Cadastro de usuario', 'usuario', $model->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " cadastrou o usuário " . $model->username, "127.0.0.1", null, Yii::app()->session->getSessionId());

                if ($model->tipo == 'empresa_admin') {
                    $empresaHasUsuario->attributes = $_POST['EmpresaHasUsuario'];
                } else if ($model->tipo == 'crediarista' || $model->tipo == 'vendedor' || $model->tipo == 'caixa') {

                    if ($model->tipo == 'crediarista') {
                        $dirCaptchas = 'images/captchas/captchas_' . $model->id;

                        if (!is_dir($dirCaptchas)) {
                            $oldUmask = umask(0);
                            mkdir($dirCaptchas, 0777, true);
                            umask($oldUmask);
                        }
                    }

                    $filialHasUsuario->attributes = $_POST['FilialHasUsuario'];
                    $filialHasUsuario->data_cadastro = date('Y-m-d H:i:s');
                    $filialHasUsuario->data_cadastro_br = date('d/m/Y H:i:s');
                    $filialHasUsuario->Usuario_id = $model->id;
                    $filialHasUsuario->save();

                    $filial = Filial::model()->findByPk($filialHasUsuario->Filial_id);
                    $empresaHasUsuario->Empresa_id = $filial->empresa->id;
                } else if ($model->tipo == 'analista_de_credito') {

                    $empHasUsuario = EmpresaHasUsuario::model()->find('Usuario_id =' . Yii::app()->session['usuario']->id);
                    $empresa = Empresa::model()->findByPk($empHasUsuario->Empresa_id);
                    $empresaHasUsuario->Empresa_id = $empresa->id;

                    $grupoDeAnalistasHasUsuario = new GrupoDeAnalistasHasUsuario;
                    $grupoDeAnalistasHasUsuario->data_cadastro = date('Y-m-d H:i:s');
                    $grupoDeAnalistasHasUsuario->data_cadastro_br = date('d/m/Y H:i:s');
                    $grupoDeAnalistasHasUsuario->Usuario_id = $model->id;
                    $grupoDeAnalistasHasUsuario->Grupo_de_Analistas_id = $_POST['Grupo_de_Analistas'];
                    $grupoDeAnalistasHasUsuario->save();
                    
                } else {
                    $empHasUsuario = EmpresaHasUsuario::model()->find('Usuario_id =' . Yii::app()->session['usuario']->id);
                    $empresa = Empresa::model()->findByPk($empHasUsuario->Empresa_id);
                    $empresaHasUsuario->Empresa_id = $empresa->id;
                }

                $empresaHasUsuario->Usuario_id = $model->id;
                $empresaHasUsuario->data_cadastro = date('Y-m-d H:i:s');
                $empresaHasUsuario->data_cadastro_br = date('d/m/Y H:i:s');
                $empresaHasUsuario->save();

                $role = Role::model()->find("papel ='" . $model->tipo . "'");

                $usuarioRole = new UsuarioRole;
                $usuarioRole->Usuario_id = $model->id;
                $usuarioRole->Role_id = $role->id; //
                $usuarioRole->data_cadastro = date('Y-m-d H:i:s');
                $usuarioRole->data_cadastro_br = date('d/m/Y H:i:s');
                $usuarioRole->save();

                $this->redirect($this->createUrl('admin'));
            }
        }

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/validate-form-usuario.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-form-usuario.js', CClientScript::POS_END);

        $cs->registerScript('FormElements', 'FormValidator.init();');
        $cs->registerScript('FormElements', 'FormElements.init();');

        $this->render('create', array(
            'model' => $model,
            'empresaHasUsuario' => $empresaHasUsuario,
            'filialHasUsuario' => $filialHasUsuario,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        $filialHasUsuario = FilialHasUsuario::model()->find("Usuario_id = " . $model->id);


        if (isset($_POST['Usuario'])) {

            $model->attributes = $_POST['Usuario'];

            if ($model->save()) {

                $this->redirect($this->createUrl('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'filialHasUsuario' => $filialHasUsuario,
        ));
    }

    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Usuario');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionGetUsuarios() {

        $tipoUserLogado = $_POST['tipoUserLogado'];

        if ($tipoUserLogado == 1) {

            echo json_encode(Usuario::model()->listEmpresasAdmins($_POST['draw']));
        } else if ($tipoUserLogado == 3) {

            echo json_encode(Yii::app()->session['usuario']->listUsuariosEmpresa($_POST['draw'], $_POST['start'], $_POST['columns']));
        }
    }

    public function actionGetFiliaisAdmin() {

        $retorno = Usuario::model()->listFiliaisAdmin   (
                                                            $_POST['idUsuario'  ]   ,
                                                            $_POST['draw'       ]   , 
                                                            $_POST['start'      ]   , 
                                                            $_POST['search'     ]
                                                        );
        
        echo json_encode($retorno);
            
    }

    public function actionAdmin() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-usuarios-grid.js', CClientScript::POS_END);

        $this->render('dashboard');
    }

    public function loadModel($id) {

        $model = Usuario::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function actionChangePassword() {

        //Yii::app()->session['usuario']->updateVistoPorUltimo();

        $retorno = array('empty' => true);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js');
        $cs->registerScriptFile($baseUrl . '/js/validate-form-change-pass.js');
        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js');
        $cs->registerScript('FormWizard', 'FormValidator.init();');

        if (isset($_POST['old_password']) && isset($_POST['new_password']) && isset($_POST['password_again'])) {

            $senha_antiga = $_POST['old_password'];
            $nova_senha = $_POST['new_password'];

            $retorno = Yii::app()->session['usuario']->changePassword($senha_antiga, $nova_senha);
        }

        $this->render('changePass', array(
            'retorno' => $retorno
        ));
    }
    
    public function actionListarFiliaisSelect()
    {
       
       $idUsuario = $_POST['idUsuario'];
       
       $retorno = Usuario::model()->listarFiliaisSelect($idUsuario);
       
       echo json_encode($retorno);
       
    }

    ///////////////////////////////////////////////
    // funcao para alterar a senha. ///////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 13-05-2015//
    ///////////////////////////////////////////////
    public function actionMudarSenha() {
        
        $arrayRetorno = array   (
                                        'hasErrors'     => 0                                ,
                                        'msg'           => 'Senha alterada com Sucesso.'    ,
                                        'pntfyClass'    => 'success'
                                    );
        if(isset($_POST) && isset($_POST['nomeUsuario']) && isset($_POST['senha']))
        {
            $nomeUsuario    = $_POST['nomeUsuario'  ]   ; //resgate o nome do usuario enviado pelo post
            $senha          = $_POST['senha'        ]   ; //resgate a senha enviado pelo post

            $usuario = Usuario::model()->find("habilitado and username = '" . $nomeUsuario . "'  "); //busque o registro no banco pelo id
            
            if ($usuario !== null)
            {
                $usuario->password          = Yii::app()->hasher->hashPassword($senha)  ;   //altere a senha
                $usuario->primeira_senha    = 1                                         ;   //mude para primeira senha para forçar o usuario a trocar de senha

                //salve os logs
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Alteração de senha de usuário', 'usuario', $usuario->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou a senha.", "127.0.0.1", null, Yii::app()->session->getSessionId());

                if(!$usuario->update())
                {

                    ob_start();
                    var_dump($usuario->getErrors());
                    $result = ob_get_clean();

                    $arrayRetorno = array   (
                                                'hasErrors'     => 1                                    ,
                                                'msg'           => 'Erro ao alterar senha. ' . $result  ,
                                                'pntfyClass'    => 'error'
                                            );
                }
                else
                {

                    $usuarioS1   = Yii::app()->dbnr->createCommand()->update('Usuario',['password' => $usuario->password, 'primeira_senha' => 1],'username=:username',[':username'=>$usuario->username]);

                }
            }
            else
            {
                
                $arrayRetorno = array   (
                                            'hasErrors'     => 1                                    ,
                                            'msg'           => 'Usuário não encontrado. ', // . $result  ,
                                            'pntfyClass'    => 'error'
                                        );
            }
            
        }
        
        echo json_encode($arrayRetorno);
        
    }

    ///////////////////////////////////////////////
    // funcao para alterar nomes de usuario. //////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 09-10-2015//
    ///////////////////////////////////////////////
    public function actionMudarUtilizador() 
    {
        
        $arrayRetorno = array   (
                                        'hasErrors'     => 0                                ,
                                        'msg'           => 'Username alterado com Sucesso.' ,
                                        'pntfyClass'    => 'success'
                                    );
        
        if(isset($_POST) && isset($_POST['nomeUsuario']) && isset($_POST['utilizador']))
        {
            $nomeUsuario    = $_POST['nomeUsuario'  ]   ; //resgate o nome do usuario enviado pelo post
            $utilizador     = $_POST['utilizador'   ]   ; //resgate a senha enviado pelo post

            $usuario = Usuario::model()->find("habilitado and username = '" . $nomeUsuario . "'  "); //busque o registro no banco pelo id
            
            if ($usuario !== null)
            {
                $usuario->nome_utilizador   = $utilizador   ;   //altere o nome_utilizador

                //salve os logs
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Alteração de nome de usuário', 'usuario', $usuario->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o nome do usuário.", "127.0.0.1", null, Yii::app()->session->getSessionId());

                if(!$usuario->update())
                {

                    ob_start();
                    var_dump($usuario->getErrors());
                    $result = ob_get_clean();

                    $arrayRetorno = array   (
                                                'hasErrors'     => 1                                        ,
                                                'msg'           => 'Erro ao alterar o usuário. ' . $result  ,
                                                'pntfyClass'    => 'error'
                                            );
                }
            }
            else
            {
                
                $arrayRetorno = array   (
                                            'hasErrors'     => 1                                    ,
                                            'msg'           => 'Usuário não encontrado. ', // . $result  ,
                                            'pntfyClass'    => 'error'
                                        );
            }
            
        }
        
        echo json_encode($arrayRetorno);
        
    }

    ///////////////////////////////////////////////
    // funcao para alterar nomes de usuario. //////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 09-10-2015//
    ///////////////////////////////////////////////
    public function actionMudarUsername() 
    {
        
        $arrayRetorno = array   (
                                        'hasErrors'     => 0                                ,
                                        'msg'           => 'Username alterado com Sucesso.' ,
                                        'pntfyClass'    => 'success'
                                    );
        
        if(isset($_POST) && isset($_POST['nomeUsuario']) && isset($_POST['username']))
        {
            $nomeUsuario    = $_POST['nomeUsuario']   ; //resgate o nome do usuario enviado pelo post
            $username     = $_POST['username'   ]   ; //resgate a senha enviado pelo post

            $usuario = Usuario::model()->find("habilitado and username = '" . $nomeUsuario . "'  "); //busque o registro no banco pelo id
            
            if ($usuario !== null)
            {
                $usuario->username   = $username   ;   //altere o nome_utilizador

                //salve os logs
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Alteração de username', 'usuario', $usuario->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o username.", "127.0.0.1", null, Yii::app()->session->getSessionId());

                if(!$usuario->update())
                {

                    ob_start();
                    var_dump($usuario->getErrors());
                    $result = ob_get_clean();

                    $arrayRetorno = array   (
                                                'hasErrors'     => 1                                        ,
                                                'msg'           => 'Erro ao alterar o usuário. ' . $result  ,
                                                'pntfyClass'    => 'error'
                                            );
                }
            }
            else
            {
                
                $arrayRetorno = array   (
                                            'hasErrors'     => 1                                    ,
                                            'msg'           => 'Usuário não encontrado. ', // . $result  ,
                                            'pntfyClass'    => 'error'
                                        );
            }
            
        }
        
        echo json_encode($arrayRetorno);
        
    }

   ///////////////////////////////////////////////
   // funcao para alterar a filial do usuario. ///
   ///////////////////////////////////////////////
   // Autor : André Willams // Data : 05-08-2015//
   ///////////////////////////////////////////////
   public function actionMudarFilialSelect()
   {

      $arrayRetorno = array(
          'hasErrors' => 0,
          'msg' => 'Filial alterado com Sucesso.',
          'pntfyClass' => 'success'
      );

      $idFilial   = $_POST['idFilial' ]; //resgate o id do filial enviado pelo post
      $idUsuario  = $_POST['idUsuario']; //resgate o id do usuario pelo post

      $filialHasUsuario = FilialHasUsuario::model()->find('Usuario_id = ' . $idUsuario);

      $filialHasUsuario->Filial_id = $idFilial;

      if (!$filialHasUsuario->update())
      {

         ob_start();
         var_dump($filialHasUsuario->getErrors());
         $filialHasUsuario = ob_get_clean();

         $arrayRetorno['hasErrors'] = 1;
         $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
         $arrayRetorno['pntfyClass'] = 'error';
      } else
      {
         //salve os logs
         $sigacLog = new SigacLog;
         $sigacLog->saveLog('Alteração de Filial do Usuário', 'Filial Has Usuário', $filialHasUsuario->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou a filial.", "127.0.0.1", null, Yii::app()->session->getSessionId());
      }

      echo json_encode($arrayRetorno);
   }

    protected function performAjaxValidation($model) {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuario-form') {

            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
