<?php

class MigracaoController extends Controller {

    public $layout = '//layouts/column2';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),

            array ('allow',
                'actions'       => array('rodar'),
                'users'         => array('@'),
                'expression'    => 'Yii::app()->session["usuario"]->role == "admin"'
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Migracao;

        if (isset($_POST['Migracao'])) {
            $model->attributes = $_POST['Migracao'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Migracao'])) {
            $model->attributes = $_POST['Migracao'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex(){
        $dataProvider = new CActiveDataProvider('Migracao');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionRodar(){

        //set_time_limit(27000);

        $ip                         = "187.12.141.11";
        $porta                      = "1433";
        $db                         = "GMM";
        $usuario                    = "sa";
        $senha                      = "Grup0M@r3SA";
        //$top                        = 30000;
        //$top      = 10000;

        $util                       = new Util;
        $conexao                    = new PDO("dblib:host=" . $ip . ":" . $porta . ";dbname=" . $db . ";charset=utf8", $usuario, $senha);

        $migracao                   = new Migracao;
        $migracao->habilitado       = 1;

        $migracao->origem           = 1;
        $migracao->destino          = 19;
        $migracao->usuarioId        = Yii::app()->session['usuario']->id;
        $migracao->data             = date('Y-m-d H:m:s');
        $migracao->data_cadastro    = date('Y-m-d H:m:s');
        
//        $recno                      = ItensBuscados::model()->getAllRegOk( $migracao->origem, $migracao->destino );

        //$sql = "SELECT TOP " . $top . " A1_COD, A1_NOME, A1_CGC, A1_PESSOA, A1_DDD, A1_TEL, ";
        $sql = "SELECT A1_COD, A1_NOME, A1_CGC, A1_PESSOA, A1_DDD, A1_TEL, ";
        $sql .= "A1_PFISICA, A1_DTNASC, A1_TPESSOA, A1_PAIS, ";
        $sql .= "A1_MUN, A1_EST, A1_YMAE, A1_YSEXO, A1_YCIVIL, ";
        $sql .= "A1_YLOGRA, A1_NR, A1_BAIRRO, A1_CEP, A1_COMPLEM, A1_YATIPO = CASE WHEN A1_YATIPO = '' THEN '2' ELSE A1_YATIPO END, ";
        $sql .= "SA1.R_E_C_N_O_ AS 'SA1.RECNO' ";
        $sql .= "FROM SA1010 AS SA1 ";
        $sql .= "WHERE   SA1.D_E_L_E_T_ = '' AND A1_TIPO = 'F' AND A1_PESSOA = 'F' AND ";
        $sql .= "A1_MSBLQL <> '1'    AND LEN(A1_COD) > 3 "; //SA1.R_E_C_N_O_ NOT IN (" . $recno . ") ";
        
        $sth = $conexao->prepare($sql);
        $sth->execute();
        $dados = $sth->fetchAll();

        //var_dump($migracao);
        //echo '<br><br>';

        if ( $migracao->save() ) {

            foreach ($dados as $d) {

                $itemBuscado = new ItensBuscados;
                $itemBuscado->Migracao_id = $migracao->id;
                $itemBuscado->tabela = "SA1";
                $itemBuscado->registro = $d['SA1.RECNO'];
                $itemBuscado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                $itemBuscado->data_cadastro = date('Y-m-d H:m:s');
                $itemBuscado->habilitado = 1;

                if ($util->validarCPF(trim($d['A1_CGC']))) {

                    if (!Documento::model()->cadastrado(trim($d['A1_CGC']))) {
                        $cliente = new Cliente;
                        $pessoa  = new Pessoa;
                        $contato = new Contato;
                        $endereco = new Endereco;


                        $cliente->habilitado    = 1;
                        $pessoa->habilitado     = 1;
                        $contato->habilitado    = 1; 
                        $endereco->habilitado   = 1;

                        $pessoa->nome = $d['A1_NOME'];
                        $pessoa->nascimento = date('Y-m-d', strtotime($d['A1_DTNASC']));
                        $pessoa->data_cadastro = date('Y-m-d H:m:s');
                        $pessoa->sexo = $d['A1_YSEXO'];

                        if (Empty($d['A1_YCIVIL'])) {
                            $pessoa->Estado_Civil_id = $d['A1_YCIVIL'];
                        } else {
                            $pessoa->Estado_Civil_id = 1;
                        }
                        
                        $trans = Yii::app()->db->beginTransaction();

                        if (!$contato->save()) {
                            echo 'eita <br>';
                        }

                        $pessoa->Contato_id = $contato->id;
                        

                        if ($pessoa->save()) {

                            $itemMigrado = new ItensMigrados;
                            $itemMigrado->Migracao_id = $migracao->id;
                            $itemMigrado->tabela = "Pessoa";
                            $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                            $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                            $itemMigrado->habilitado = 1;
                            $itemMigrado->registro = $pessoa->id;

                            if (!$itemMigrado->save()) {
                                var_dump($itemMigrado->getErrors());
                            }

                            $cpf                    = new Documento;

                            $cpf->Tipo_documento_id = 1;
                            $cpf->numero = $d['A1_CGC'];
                            $cpf->data_cadastro = date('Y-m-d H:m:s');
                            $cpf->habilitado = 1;

                            if ($cpf->save()) {

                                $itemMigrado = new ItensMigrados;
                                $itemMigrado->Migracao_id = $migracao->id;
                                $itemMigrado->tabela = "Documento";
                                $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                $itemMigrado->habilitado = 1;
                                $itemMigrado->registro = $cpf->id;

                                if (!$itemMigrado->save()) {
                                    var_dump($itemMigrado->getErrors());
                                }

                                $pHasCPF = new PessoaHasDocumento;

                                $pHasCPF->Documento_id = $cpf->id;
                                $pHasCPF->Pessoa_id = $pessoa->id;
                                $pHasCPF->habilitado = 1;
                                $pHasCPF->data_cadastro = date('Y-m-d H:m:s');

                                if ($pHasCPF->save()) {

                                    $itemMigrado = new ItensMigrados;
                                    $itemMigrado->Migracao_id = $migracao->id;
                                    $itemMigrado->tabela = "PessoaHasDocumento";
                                    $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                    $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                    $itemMigrado->habilitado = 1;
                                    $itemMigrado->registro = $pHasCPF->id;

                                    if (!$itemMigrado->save()) {
                                        var_dump($itemMigrado->getErrors());
                                    }

                                    $endereco->logradouro = $d['A1_YLOGRA'];
                                    $endereco->numero = $d['A1_NR'];
                                    $endereco->bairro = $d['A1_BAIRRO'];
                                    $endereco->complemento = $d['A1_COMPLEM'];
                                    $endereco->cep = $d['A1_CEP'];
                                    $endereco->cidade = $d['A1_MUN'];
                                    $endereco->uf = $d['A1_EST'];
                                    $endereco->Tipo_Endereco_id = $d['A1_YATIPO'];

                                    if ($endereco->save()) {

                                        $itemMigrado = new ItensMigrados;
                                        $itemMigrado->Migracao_id = $migracao->id;
                                        $itemMigrado->tabela = "Endereco";
                                        $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                        $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                        $itemMigrado->habilitado = 1;
                                        $itemMigrado->registro = $endereco->id;

                                        if (!$itemMigrado->save()) {
                                            var_dump($itemMigrado->getErrors());
                                        }

                                        $pHasE = new PessoaHasEndereco;

                                        $pHasE->Pessoa_id = $pessoa->id;
                                        $pHasE->Endereco_id = $endereco->id;

                                        $pHasE->data_cadastro = date('Y-m-d H:m:s');

                                        if ($pHasE->save()) {

                                            $cliente->Pessoa_id = $pessoa->id;

                                            $itemMigrado = new ItensMigrados;
                                            $itemMigrado->Migracao_id = $migracao->id;
                                            $itemMigrado->tabela = "PessoaHasEndereco";
                                            $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                            $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                            $itemMigrado->habilitado = 1;
                                            $itemMigrado->registro = $pHasE->id;

                                            if (!$itemMigrado->save()) {
                                                var_dump($itemMigrado->getErrors());
                                            }

                                            if ($cliente->save()) {

                                                $itemMigrado = new ItensMigrados;
                                                $itemMigrado->Migracao_id = $migracao->id;
                                                $itemMigrado->tabela = "Cliente";
                                                $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                                $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                                $itemMigrado->habilitado = 1;
                                                $itemMigrado->registro = $cliente->id;

                                                if (!$itemMigrado->save()) {
                                                    var_dump($itemMigrado->getErrors());
                                                }
                                                
                                                $cadastro                           = new Cadastro;
                                                $cadastro->Cliente_id               = $cliente->id;
                                                $cadastro->data_cadastro	        = date('Y-m-d H:m:s');
                                                $cadastro->titular_do_cpf	        = 1;
                                                $cadastro->habilitado		        = 1;
                                                $cadastro->conjugue_compoe_renda    = 0;
                                                $cadastro->Empresa_id		        = 5;
                                                $cadastro->numero_de_dependentes    = 0;
                                                $cadastro->atualizar                = 1;
                                                
                                                if($cadastro->save())
                                                {

                                                    $itemMigrado = new ItensMigrados;
                                                    $itemMigrado->Migracao_id = $migracao->id;
                                                    $itemMigrado->tabela = "Cadastro";
                                                    $itemMigrado->obs = trim($d['A1_COD']) . ' ' . trim($d['A1_NOME']) . ' ' . $d['A1_CGC'];
                                                    $itemMigrado->data_cadastro = date('Y-m-d H:m:s');
                                                    $itemMigrado->habilitado = 1;
                                                    $itemMigrado->registro = $cadastro->id;

                                                    if ($itemMigrado->save()) {
                                                        $trans->commit();
                                                        $itemBuscado->sucesso = 1;
                                                    }else{
                                                        var_dump($itemMigrado->getErrors());
                                                        $trans->rollBack();
                                                    }
                                                }else{
                                                    $itemBuscado->sucesso = 0;
                                                    $itemBuscado->obs .= 'Cadastro: ' . var_dump($cadastro->getErrors());
                                                    $trans->rollBack();
                                                }
                                            } else {
                                                $itemBuscado->sucesso = 0;
                                                $itemBuscado->obs .= 'Cliente: ' . var_dump($cliente->getErrors());
                                                $trans->rollBack();
                                            }
                                        } else {
                                            $itemBuscado->sucesso = 0;
                                            $itemBuscado->obs .= 'PessoaHasEndereco: ' . var_dump($pHasE->getErrors());
                                            $trans->rollBack();
                                        }
                                    } else {
                                        $itemBuscado->sucesso = 0;
                                        $itemBuscado->obs .= 'Endereco: ' . var_dump($endereco->getErrors());
                                        $trans->rollBack();
                                    }
                                } else {
                                    $itemBuscado->sucesso = 0;
                                    $itemBuscado->obs .= 'PessoaHasCPF ' . var_dump($pHasCPF->getErrors());
                                    $trans->rollBack();
                                }
                            } else {
                                $itemBuscado->sucesso = 0;
                                $itemBuscado->obs .= 'CPF ' . var_dump($cpf->getErrors());
                                $trans->rollBack();
                            }
                            
                        } else {
                            $itemBuscado->sucesso = 0;
                            $itemBuscado->obs .= ' Pessoa:  ' . var_dump($pessoa->getErrors());
                            $trans->rollBack();
                        }
                        
                    } else{
                        continue;
                        /*echo 'CGC já cadastrado<br><br>';
                        $itemBuscado->sucesso  = 0;
                        $itemBuscado->obs     .= 'CGC inválido';*/
                    }
                } else {
                    echo 'CGC inválido<br><br>';

                    $itemBuscado->sucesso = 0;
                    $itemBuscado->obs .= 'CGC inválido';
                }

                if (!$itemBuscado->save()) {
                    var_dump($itemBuscado->getErrors());
                }
            
            }
            
        } else {
            var_dump($migracao->getErrors());
        }
    }

    public function actionAdmin() {
        $model = new Migracao('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Migracao']))
            $model->attributes = $_GET['Migracao'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Migracao::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'migracao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
