<?php

class PerfilController extends Controller{
	
	public $layout = '';

	public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete' // we only allow deletion via POST request
    );
  }

  public function accessRules()
  {
   	return 
   	[
   		[
   			'allow',
   			'actions' 	=> ['admin','loadFormEdit','dataTable', 'persist', 'configurar'],
   			'users' 		=> ['@'],
   			'expression'=> 'Yii::app()->session["usuario"]->autorizado()'
   		],

   		[
   			'deny',
   			'users'	 	=> ['*']
   		]
   	];
  }

	//sobrescreve a função do Controller, classe pai
	public function init(){

	}

  /**
    * Inclui e editar informações sobre as funções
    * @param POST['Funcao'].
    * @return JSON
    * @author Eric
    * @version 1.0
  */
	public function actionAdmin()
	{
      $baseUrl = Yii::app()->baseUrl;
      $cs = Yii::app()->getClientScript();
      /* CSS */
      $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
      $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
      $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
      $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
      /* FIM CSS */

      $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

      $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

      /* dataTables */
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
      $cs->registerScriptFile('/js/jquery.blockUI.js', CClientScript::POS_END);
      $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
      /* FIM dataTables */
      
      $cs->registerScriptFile('../assets/ubold/js/perfil/fn-perfis-admin.js', CClientScript::POS_END);

		  $this->layout = '//layouts/ubold';
		  $this->render('admin');
	}

	public function actionIndex(){

  }

  /**
    * Configura as principais informações de uma entidade [Perfis]
    * @param POST['PerfilId'].
    * @return html
    * @author Eric
    * @version 1.0
  */
  public function actionConfigurar()
  {
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    /* CSS */
    $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
    /* FIM CSS */

    $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

    $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

    /* dataTables */
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/custombox/dist/custombox.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/custombox/dist/legacy.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/jquery.blockUI.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
    /* FIM dataTables */

    $cs->registerScriptFile('/assets/ubold/js/perfil/fn-niveis-acessos.js', CClientScript::POS_END);
      
    $this->layout = '//layouts/ubold';
      
    $this->render('configurar', ['perfil' => Role::model()->findByPk( $_POST['RoleId'] )] );
  }

  /**
    * Inclui e editar informações sobre as funções
    * @param POST['Funcao'].
    * @return JSON
    * @author Eric
    * @version 1.0
  */
  public function actionPersist()
  {
      $retorno = Role::model()->persist($_POST['Role'], $_POST['RoleId']);

      if ($retorno['hasErrors'] === true)
      {
            foreach ($retorno['errors'] as $erro)
            {
                $retorno['msgConfig'][] = [
                    'tipo' => 'error',
                    'titulo' => 'Não foi possível realizar a operação: ',
                    'mensagem' => 'Motivo : ' . ($erro[0]),
                    'posicao' => 'top left'
                ];
            }
      }
      else
      {
            $retorno['msgConfig'][] = [
                'tipo' => 'success',
                'titulo' => 'Operação realizada com sucesso: ',
                'mensagem' => 'Mensagem : Perfil cadastrado, já disponível para consulta.',
                'posicao' => 'top left'
            ];
      }
      echo json_encode($retorno);
  }

  /**
    * Inclui e editar informações sobre as funções
    * @param POST['Funcao'].
    * @return JSON
    * @author Eric
    * @version 1.0
  */
  public function actionDataTable()
  {
      $rolesCollection     = Role::model()->dataTable($_GET['draw'], [ 'start' => $_GET['start'], 'limit' => $_GET['length']]);

      echo json_encode([
         'draw'            => $_GET['draw'                        ],
         'data'            => $rolesCollection['collection'       ],
         'recordsTotal'    => count($rolesCollection['collection' ]),
         'recordsFiltered' => $rolesCollection['total'            ]
      ]);
  }

  /**
    * Inclui e editar informações sobre as funções
    * @param POST['Funcao'].
    * @return JSON
    * @author Eric
    * @version 1.0
  */
  public function actionLoadFormEdit()
  {
    echo json_encode(Role::model()->loadFormEdit($_POST['id']));
  }

}