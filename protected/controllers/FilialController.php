<?php

class FilialController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}


	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            
            [
                'allow',
                'actions' 		=> ['index', 'dataTableAdminFiliais'],
                'users' 		=> ['@'],
                'expression' 	=> 'Yii::app()->session["usuario"]->autorizado()'
            ],
            
            [
                'deny',
                'users' 		  => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionDataTableAdminFiliais()
    {
    	$rows 				= [];

        /*Criterias*/
        $criteria           = new CDbCriteria;
        $criteria->offset   = $_POST['start'];
        $criteria->limit    = 10;

        $criteriaFilter     = new CDbCriteria;
        $criteria->addInCondition('habilitado', [1],        'AND');
        $criteriaFilter->addInCondition('habilitado', [1],  'AND');

        $zero               = '';

        if( $_POST['NucleoId'] !== '0')
        {
            $criteria->addInCondition('NucleoFiliais_id', [ intval($_POST['NucleoId']) ],       'AND');
            $criteriaFilter->addInCondition('NucleoFiliais_id', [ intval($_POST['NucleoId']) ], 'AND');
        }
        
        $total                  = count( Filial::model()->findAll($criteriaFilter) );

    	foreach( Filial::model()->findAll( $criteria ) as $filial )
    	{
            $checked            = "";
            $hab                = 1;

            if( AdminHasParceiro::model()->find('Parceiro = ' .$filial->id. ' AND Administrador = '.$_POST['User_Id'].' AND habilitado' ) )
            {
                $checked        = " checked ";
                $hab            = 0;
            }

    		$rows[]			    = [
                'checkAtivar'   => '<div class="checkbox checkbox-primary"><input data-hab="'.$hab.'" data-filial-id="'.$filial->id.'" data-user-id="'.$_POST['User_Id'].'" class="input_check" '.$checked.' type="checkbox"><label></label></div>',
    			'filial' 	    => mb_strtoupper($filial->getConcat()),
    			'cnpj' 		    => $filial->cnpj,
    		];
    	}

    	echo json_encode([
	       'draw'            => $_POST['draw'],
	       'data'            => $rows,
	       'recordsTotal'    => 10,
	       'recordsFiltered' => $total,
           'zero'            => $zero
      	]);
      	
    }    
}