<?php

class TabelaCotacaoController extends Controller
{
  public function filters(){
    return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete' // we only allow deletion via POST request
    );
  }

  public function accessRules(){
    return 
    [
      [
        'allow',
        'actions'   => ['admin', 'persist', 'listar', 'getFatores', 'atualizarFatores', 'parceiros', 'addRemParceiro', 'selectAllFiliais', 'ativarDesativar'],
        'users'     => ['@'],
        'expression'=> 'Yii::app()->session["usuario"]->autorizado()'
      ],

      [
        'deny',
        'users'   => ['*']
      ]
    ];
  }

  //sobrescreve a função do Controller, classe pai
  public function init(){

  }

  public function actionAddRemParceiro(){
  
    $retorno  = ['hasErrors' => false];

    $t        = Yii::app()->db->beginTransaction();
    
    $fhtc     = FilialHasTabelaCotacao::model()->find("t.Tabela_Cotacao_id = " . $_POST['tId'] . " AND t.Filial_id = " . $_POST['fId'] );

    /*Existe, vamos ativar ou desativar*/
    if( $fhtc != NULL ){
      $fhtc->habilitado = $_POST['hab'];

      if( !$fhtc->update() ){
        $retorno['hasErrors'] = true;
      }

    }
    else{
      $fhtc                     = new FilialHasTabelaCotacao;
      $fhtc->Tabela_Cotacao_id  = $_POST['tId'];
      $fhtc->Filial_id          = $_POST['fId'];
      $fhtc->habilitado         = 1;

      if( !$fhtc->save() ){
        $retorno['hasErrors']   = true;
      }
    }

    if( !$retorno['hasErrors'] ){
      $t->commit();
    }
    else{
      $t->rollBack();
    }

    echo json_encode($retorno);

  }

  public function actionSelectAllFiliais(){

    $retorno      = ['hasErrors' => false];
    $nucleoId     = $_POST['nucleoId'];
    $isCheck      = $_POST['isCheck'];
    $tabela       = TabelaCotacao::model()->findByPk($_POST['tabela']);

    $criteria     = new CDbCriteria;
    $criteria->addInCondition('t.habilitado', [1], ' AND ');

    if( $nucleoId != '0' ){
      $criteria->addInCondition('t.NucleoFiliais_id', [$nucleoId], ' AND ');
    }

    $t            = Yii::app()->db->beginTransaction();
    
    foreach( Filial::model()->findAll($criteria) as $filial ){

      /*tenta encontrar se há ocorrencia*/
      $fhtc                         = FilialHasTabelaCotacao::model()->find( 't.Tabela_Cotacao_id = ' .$tabela->id.' AND t.Filial_id = ' .$filial->id );

      /*Se for ação de marcar*/
      if( $isCheck  == 1 ){
        /*caso exista*/
        if( $fhtc                     !== NULL ){

          /*se estiver desabilitado habilita*/
          if(!$fhtc->habilitado){

            $fhtc->habilitado         = 1;

            if(!$fhtc->update()){

              $retorno['hasErrors']   = true;
            }
          }
        }
        else{
          $nFhtc                      = new FilialHasTabelaCotacao;
          $nFhtc->Tabela_Cotacao_id   = $tabela->id;
          $nFhtc->Filial_id           = $filial->id;
          $nFhtc->habilitado          = 1;
            
          if( !$nFhtc->save() ){
            $retorno['hasErrors']     = true;
          }
        }
      }

      /*Se for ação de desmarcar*/
      else{
        if( $fhtc                     != NULL ){

          $fhtc->habilitado           = 0;
          if(!$fhtc->update()){
            $retorno['hasErrors']     = true;
          }
        }
      }
    }

    if( !$retorno['hasErrors'] ){
      $t->commit();
    }
    else{
      $t->rollBack();
    }

    echo json_encode($retorno);
  }

  public function actionParceiros(){

    $habilitar            = 1;
    $Checked              = '';
    $criteria             = new CDbCriteria;
    $criteria->addInCondition('t.habilitado', [1], ' AND ');
  
    if( isset( $_POST['idNucleo'] ) && $_POST['idNucleo'] != '0' ){
      $criteria->addInCondition('t.NucleoFiliais_id', [ $_POST['idNucleo'] ], ' AND ');
    }

    $dados                = [];

    $total                = count( Filial::model()->findAll($criteria) );

    $criteria->offset     = $_POST['start'];
    $criteria->limit      = 10;

    foreach( Filial::model()->findAll($criteria) as $filial ){

      $habilitado         = 1;
      $Checked            = '';

      if( FilialHasTabelaCotacao::model()->find("t.Tabela_Cotacao_id = " . $_POST['idTabela'] . " AND t.Filial_id = " . $filial->id . " AND t.habilitado ") != NULL ){
        $habilitar        = 0;
        $Checked          = ' checked ';
      }

      $dados[]            = [
        'checkPrincipal'  => '<div class="checkbox checkbox-primary"><input data-tabelaid="'.$_POST['idTabela'].'" data-filialid="' . $filial->id . '" class="input_check" data-type="p" data-hab="' . $habilitar . '" ' . $Checked . ' type="checkbox"><label></label></div>',
        'filial'          => mb_strtoupper($filial->getConcat())
      ];
    }

    echo json_encode([
      'draw'              => $_POST['draw'],
      'data'              => $dados,
      'recordsTotal'      => count($dados),
      'recordsFiltered'   => $total,
    ]);
  }

  public function actionAtualizarFatores(){

    $row                = 0;
    $colunas            = [];
    $fatores            = [];
    $retorno            = ['hasErrors' => false];
    
    $t                  = Yii::app()->db->beginTransaction();

    $tabela             = TabelaCotacao::model()->findByPk( $_POST['tabelaId'] );

    if( isset($_FILES['anexo_importacao']) ){
      if ( ( $handle    = fopen($_FILES['anexo_importacao']['tmp_name'], "r" ) ) !== FALSE ){
        while ( ( $data = fgetcsv($handle, 1000, ";") ) !== FALSE ){
          $row++;
          
          if( $row      == 1 ){
            $colunas    = $data;
          }
          else{
            $fatores[]  = $data;
          }
        }
      }
    }
    
    for ($c = 0; $c < count($colunas); $c++){
      
      $carencia                             = intval($colunas[$c]);
      $seq_parc                             = 0;
    
      for ( $i = 0; $i < count($fatores); $i++ )
      {
        if( $fatores[$i][$c]                  != "" && !empty($fatores[$i][$c]) )
        {
          $seq_parc = $i+2;

          $fatorExistente                     = Fator::model()->find('Tabela_Cotacao_id = ' . $tabela->id . ' AND carencia = ' . $carencia . ' AND parcela = ' . $seq_parc . ' AND habilitado');

          /*IOF*/
          if( $_POST['tipo'] == '2' ){
            
            if( $fatorExistente                 != NULL ){
              
              $fatorExistente->fatorIOF         = doubleval(str_replace([','], ['.'], $fatores[$i][$c]));
              
              if( !$fatorExistente->update() ){
                $retorno['hasErrors']           = true;
                $retorno['msgConfig'][]         = [
                  'tipo'                        => 'error',
                  'titulo'                      => 'Não foi possível realizar a operação: ',
                  'mensagem'                    => 'Motivo : Não foi possível desativar os fatores existentes',
                  'posicao'                     => 'top right'
                ];
              }
            }

          }

          /*NOVOS FATORES*/
          else{

            //Tenta encontrar o fator para poder desabilitar
            if( $fatorExistente                 != NULL ){
              $fatorExistente->habilitado       = 0;
              
              if(!$fatorExistente->update()){
                $retorno['hasErrors']           = true;
                $retorno['msgConfig'][]         = [
                  'tipo'                        => 'error',
                  'titulo'                      => 'Não foi possível realizar a operação: ',
                  'mensagem'                    => 'Motivo : Não foi possível desativar os fatores existentes',
                  'posicao'                     => 'top right'
                ];
              }
            }
            
            $novoFator                          = new Fator;
            $novoFator->carencia                = $carencia;
            $novoFator->parcela                 = $seq_parc;
            $novoFator->habilitado              = 1;
            
            if( $tabela->ModalidadeId == 2 )
            {
              $novoFator->fator                 = 0;
              $novoFator->porcentagem_retencao  = doubleval(str_replace([','], ['.'], $fatores[$i][$c]));
            }
            else{
              $novoFator->fator                 = doubleval(str_replace([','], ['.'], $fatores[$i][$c]));
              $novoFator->porcentagem_retencao  = 0;
            }

            $novoFator->Tabela_Cotacao_id       = $tabela->id;
            $novoFator->fatorIOF                = 0;
            
            if( !$novoFator->save() ){
              $retorno['hasErrors']             = true;
              $retorno['msgConfig'][]           = [
                'tipo'                        => 'error',
                'titulo'                      => 'Não foi possível realizar a operação: ',
                'mensagem'                    => 'Motivo : Não foi possível salvar o novo fator',
                'posicao'                     => 'top right'
              ];
            }
          }
        }
      }
    }

    if( !$retorno['hasErrors'] ){
      $retorno['msgConfig'][]           = [
        'tipo'                          => 'success',
        'titulo'                        => 'Operação realizada com sucesso: ',
        'mensagem'                      => 'Os fatores da tabela foram atualizados',
        'posicao'                       => 'top right'
      ];
      $t->commit();
    }
    else{
      $t->rollBack();
    }

    echo json_encode($retorno);
  }

  public function actionGetFatores(){

    $dados                = [];
    $total                = 0;
    $criteria             = new CDbCriteria;
    $criteria->addInCondition('t.Tabela_Cotacao_id' , [$_POST['tabelaId'] ], ' AND ');
    $criteria->addInCondition('t.habilitado'        , [1                  ], ' AND ');

    $total                = count( Fator::model()->findAll($criteria) );

    $criteria->limit      = $total;
    $criteria->offset     = 0;
    $criteria->order      = 't.carencia ASC, t.parcela ASC';

    foreach( Fator::model()->findAll($criteria) as  $fator )
    {
      $dados[]              = [
        "carencia"          => $fator->carencia,
        "fator"             => $fator->fator,
        "fatorIof"          => $fator->fatorIOF,
        "retencao"          => number_format($fator->porcentagem_retencao,2,',','.') . '%',
        "seq_parcela"       => $fator->parcela.'°'
      ];
    }

    echo json_encode([
      'draw'              => 1,
      'data'              => $dados,
      'recordsTotal'      => count($dados),
      'recordsFiltered'   => $total,
    ]);
  }

  public function actionAtivarDesativar(){
    $tabela             = TabelaCotacao::model()->findByPk($_POST['tabela']);
    $tabela->habilitado = $_POST['acao'];
    $tabela->update();
  }

  public function actionListar()
  {
    $dados                        = [];
    $criteria                     = new CDbCriteria;    
    $criteria->addInCondition('t.habilitado', [$_POST['situacao']], ' AND ');

    $btn_ativar_desativar         = "";

    $total                        = count( TabelaCotacao::model()->findAll($criteria) );

    $criteria->offset             = $_POST['start'];
    $criteria->limit              = 10;
    $criteria->order              = 't.id desc';

    foreach( TabelaCotacao::model()->findAll( $criteria ) as $p )
    { 
      $btn_ativar_desativar       = "";

      if( $p->habilitado ){
        $btn_ativar_desativar     = '<a data-hab="0" data-tabela="'.$p->id.'" style="padding:1px 7px!important;" class="btn btn-danger waves-effect waves-light btn-hab-des"> <span class="btn-label"><i class="fa fa-times"></i></span>Desativar </a>';
      }
      else{
        $btn_ativar_desativar     = '<a data-hab="1" data-tabela="'.$p->id.'" style="padding:1px 7px!important;" class="btn btn-success waves-effect waves-light btn-hab-des"> <span class="btn-label"><i class="fa fa-check"></i></span>Ativar </a>';
      }

      $dados[]                    = [ 
        'idTabela'                => $p->id,
        'btn_load_parceiros'      => '<button style="padding:3px 8px!important;" type="button" class="details-control btopen btn-xs btn-success btn-rounded"><i class="fa fa-plus"></i></button>',
        'descricao'               => mb_strtoupper($p->descricao),
        'modalidade'              => mb_strtoupper($p->modalidade->descricao),
        'btn_load_config'         => '<a data-tabela="'.$p->id.'" data-politica-nome="'.mb_strtoupper($p->descricao).'" style="padding:1px 7px!important;" data-bind-show="#wrapper-tabela-fatores" data-bind-hide="#wrapper-politicas-grid" class="btn btn-info waves-effect waves-light btn-move-panel btn-load-tabela-fatores"> <span class="btn-label"><i class="fa fa-wrench"></i></span>Configurar </a>',
        'btn_ativar_desativar'    => $btn_ativar_desativar
      ];
    }

    echo json_encode([
      'draw'                      => $_POST['draw'],
      'data'                      => $dados,
      'recordsTotal'              => count($dados),
      'recordsFiltered'           => $total,
    ]);
  }

  public function actionAdmin()
  {
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $this->layout = '//layouts/ubold';

    $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
    $cs->registerCssFile('/css/login/font-awesome.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
    $cs->registerCssFile('/assets/ubold/css/multipleselect.css');
    $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');

    
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js');
    $cs->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

    $cs->registerScriptFile('/assets/ubold/js/tabelaCotacao/fn-tabela-cotacao-admin.js', CClientScript::POS_END);

    $this->render('admin');
  }
  
  public function actionPersist(){

    $retorno                      = ['hasErrors' => false];

    $t                            = Yii::app()->db->beginTransaction();

    if( isset( $_POST['TabelaCotacao'] ) ){

      $novaTabela                 = new TabelaCotacao;
      $novaTabela->attributes     = $_POST['TabelaCotacao'];
      $novaTabela->habilitado     = 1;
      $novaTabela->data_cadastro  = date('Y-m-d H:i:s');

      if( $novaTabela->save() ){

        if( isset($_POST['Carencias']) ){

          for ( $i = 0; $i < count( $_POST['Carencias'] ); $i++ ){
            $tabelaHasCarencia                    = new TabelaCotacaoHasCarencia;
            $tabelaHasCarencia->TabelaCotacaoId   = $novaTabela->id;
            $tabelaHasCarencia->CarenciaId        = $_POST['Carencias'][$i];
            $tabelaHasCarencia->habilitado        = 1;

            if( !$tabelaHasCarencia->save() ){
              $retorno['hasErrors']               = true;
              $retorno['msgConfig'][]             = [
                'tipo'                            => 'error',
                'titulo'                          => 'Não foi possível realizar a operação: ',
                'mensagem'                        => 'Motivo : Não foi possível cadastrar a carência selecionada',
                'posicao'                         => 'top right'
              ];
            }
          }
        }
      }
      else{
        $retorno['hasErrors']                     = true;
        $retorno['msgConfig'][]                   = [
          'tipo'                                  => 'error',
          'titulo'                                => 'Não foi possível realizar a operação: ',
          'mensagem'                              => 'Motivo : Não foi possível cadastrar a tabela',
          'posicao'                               => 'top right'
        ];
      }
    }

    if( $retorno['hasErrors'] ){
      $t->rollBack();
    }
    else{
      $retorno['msgConfig'][]                     = [
        'tipo'                                    => 'success',
        'titulo'                                  => 'Operação realizada com sucesso: ',
        'mensagem'                                => "A nova table '" . mb_strtoupper($novaTabela->descricao) . "', já está disponível.",
        'posicao'                                 => 'top right'
      ];
      $t->commit();
    }

    echo json_encode($retorno);

  }
}
