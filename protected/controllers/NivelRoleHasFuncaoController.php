<?php

class NivelRoleHasFuncaoController extends Controller
{
	public $layout = '';

	public function actionIndex()
	{
		$this->render('index');
	}

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['nivelFuncao', 'nivelRoleHasFuncao', 'exibirMenu'],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionNivelRoleHasFuncao()
    {
        echo json_encode($_POST);
    }

	public function actionNivelFuncao()
	{
		echo json_encode( NivelRoleHasFuncao::model()->nivelFuncao( $_POST['nivelId'], $_POST['funcaoId'], $_POST['hab'] ) );
	}

    public function actionExibirMenu()
    {
        $nrhf                   = NivelRoleHasFuncao::model()->find( 'Funcao_id = ' . $_POST['funcaoId'] . ' AND Nivel_Role_id = ' . $_POST['nivelId'] );

        if( $nrhf               !== NULL )
        {
            $nrhf->show_menu    = $_POST['hab'];
            $nrhf->update();
        }
    }
}