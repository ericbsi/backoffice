<?php

class FuncaoController extends Controller
{
    public $layout = '';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['admin', 'persist', 'loadFormEdit', 'dataTable'],
                'users' => ['@'],
                'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'allow',
                'actions' => ['teste'],
                'users' => ['*'],
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    /**
     * Retorna a tela inicial do módulo: grid de cadastros, formulários para manipulação de novas e de existentes funções
     * @param null.
     * @return view 
     * @author Eric
     * @version 1.0
     */
    public function actionAdmin()
    {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        /* FIM CSS */

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
        /* FIM dataTables */

        $cs->registerScriptFile('/assets/ubold/js/funcao/fn-funcoes-admin.js', CClientScript::POS_END);

        $this->layout = '//layouts/ubold';
        $this->render('admin');
    }

    /**
     * Inclui e editar informações sobre as funções
     * @param POST['Funcao'].
     * @return JSON
     * @author Eric
     * @version 1.0
     */
    public function actionPersist()
    {
        $retorno = Funcao::model()->persist($_POST['Funcao'], $_POST['FuncaoId']);

        if ($retorno['hasErrors'] === true)
        {
            foreach ($retorno['errors'] as $erro)
            {
                $retorno['msgConfig'][] = [
                    'tipo' => 'error',
                    'titulo' => 'Não foi possível realizar a operação: ',
                    'mensagem' => 'Motivo : ' . ($erro[0]),
                    'posicao' => 'top left'
                ];
            }
        }
        else
        {
            $retorno['msgConfig'][] = [
                'tipo' => 'success',
                'titulo' => 'Operação realizada com sucesso: ',
                'mensagem' => 'Mensagem : Função cadastrada, já disponível para consulta.',
                'posicao' => 'top left'
            ];
        }
        echo json_encode($retorno);
    }

    public function actionDataTable()
    {
        /*
            Se esta variável for preenchida via POST OU GET, e for diferente de '0', será montada uma grid com checkboxes 
            que associam uma função a um nível de acesso
        */
        $nivelId               = '0';

        if( isset( $_GET['nivelId'] ) )
        {
            $nivelId           = $_GET['nivelId'];
        }

        $funcoesCollection      = Funcao::model()->dataTable($_GET['draw'], [ 'start' => $_GET['start'], 'limit' => $_GET['length']], $nivelId);

        echo json_encode([
            'draw'              => $_GET['draw'                            ],
            'data'              => $funcoesCollection['collection'          ],
            'recordsTotal'      => count($funcoesCollection['collection'    ]),
            'recordsFiltered'   => $funcoesCollection['total'               ]
        ]);
    }

    public function actionLoadFormEdit()
    {
        echo json_encode(Funcao::model()->loadFormEdit($_POST['id']));
    }

    public function actionIndex()
    {
        
    }
}
