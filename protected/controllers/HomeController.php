<?php

class HomeController extends Controller
{

    public $layout = '//layouts/login';

    public function init()
    {
        
    }

    public function actions()
    {
        return [

            'captcha' => [
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ],
            'page' => [
                'class' => 'CViewAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->render('login');
    }

    public function actionError()
    {

        if ($error = Yii::app()->errorHandler->error)
        {

            if (Yii::app()->request->isAjaxRequest)
            {
                echo $error['message'];
            } else
            {
                $this->render('error', $error);
            }
        }
    }

    public function actionInicial()
    {
        
    }

    public function actionLogin()
    {

        $model      = new LoginForm;

        $baseUrl    = Yii::app()->baseUrl;
        $cs         = Yii::app()->getClientScript();

        if (isset($_POST['LoginForm']))
        {

            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login())
            {
                /*
                  O Login ocorreu com sucesso, portanto, devemos carregar as configurações iniciais
                  do Role principal do usuário que logou, como layout, view, acão inicial e configurações de menu
                 */
                
                //echo Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->index_view;

                try
                {
                    
                    if(isset(Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->template))
                    {
                        
                        $this->layout = Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->template;

                        /*CSS*/
                        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');

                        /*JS*/
                        $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

                        if( Yii::app()->session['usuario']->primeira_senha )
                        {   
                            $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js',       CClientScript::POS_END);
                            $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js',     CClientScript::POS_END);
                            $cs->registerScriptFile($baseUrl . '/js/limpaForm.js',                                                          CClientScript::POS_END);
                            $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js',     CClientScript::POS_END);
                            $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
                            $cs->registerScriptFile($baseUrl . '/assets/ubold/js/user/fn-mudar-primeira-senha.js', CClientScript::POS_END);
                        }

                        $this->render(Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->index_view);
                        
                    }
                    else
                    {

                        Yii::app()->user->logout();
                        Yii::app()->session->clear();
                        Yii::app()->session->destroy();
                        //$this->redirect(Yii::app()->homeUrl);
        
                        $this->render('login', ['loginError' => 'Usuário sem nenhuma permissão ativa.']);
                    }

                }

                catch (Exception $ex)
                {
                    echo "Erro: " . $ex->getMessage();
                }
                
            } 
            else
            {
                $this->render('login', ['loginError' => 'Não foi possível fazer login. Senha ou usuário inválidos']);
            }
        } 
        elseif (!Yii::app()->user->isGuest)
        {
            
            try
            {
                $this->layout = Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->template;
                $this->render(Yii::app()->session['usuario']->mainRole->nivelRole->nivelRoleConfigs->index_view);
            }
            catch (Exception $ex)
            {
                echo "Erro: " . $ex->getMessage();
            }
            
        }
    }

    public function actionLogout()
    {

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('logout_de_usuario', 'usuario', Yii::app()->session['usuario']->id, date('Y-m-d H:i:s'), 1, null, "Logout de usuario | Usuário: " . Yii::app()->session['usuario']->username, "127.0.0.1", null, Yii::app()->session->getSessionId());

        if (Yii::app()->session['usuario']->tipo == 'crediarista')
            Yii::app()->session['usuario']->cleanCaptchas();

        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();
        $this->redirect(Yii::app()->homeUrl);
    }
}
