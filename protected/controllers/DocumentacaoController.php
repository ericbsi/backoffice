<?php

class DocumentacaoController extends Controller {
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return
                    [
                        [
                        'allow',
                        'actions' => 
                        [
                            'index', 'getBipadas', 'validarRecebimento', 'gerarRecebimentos', 'recebidos', 
                            'novaDocumentacao', 'documentacao', 'viaParceiro', 'imprimirCapa', 'parceiroConsolidada', 
                            'documentacoes'
                        ],
                        'users' => ['@'],
                        'expression' => 'Yii::app()->session["usuario"]->autorizado()'
                    ],
                        [
                        'deny',
                        'users' => ['*']
                    ]
        ];
    }
    
    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/ubold';

        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
        $cs->registerCssFile('/css/login/font-awesome.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css');
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
        $cs->registerCssFile('/assets/ubold/css/multipleselect.css');
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');

        //$cs->registerScriptFile('/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js');
        $cs->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile('/assets/ubold/js/documentacao/fn-recebimento-documentacao-v5.js', CClientScript::POS_END);
        $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);

        $this->render('index');
    }

    public function actionGetBipadas() {
        $bipadas = $_POST['bips'];
        if($bipadas != ''){
            $propostas = Proposta::model()->findAll("habilitado and codigo IN ('" . join("','", explode(',', $bipadas)) . "') or numero_do_contrato IN ('" . join("','", explode(',', $bipadas)) . "')");
        }else{
            $propostas = [];
        }
        
        $rows = [];
        $totalFin = 0;
        $totalRep = 0;
        $qtd_props = 0;
        $util = new Util;
        foreach ($propostas as $proposta) {
            $totalFin += ($proposta->valor - $proposta->valor_entrada);
            $totalRep += $proposta->valorRepasse();
            $qtd_props++;
            $row = array(
                'modalidade' => $proposta->tabelaCotacao->ModalidadeId == 2 ? "Juros" : "Retenção",
                'codigo' => $proposta->codigo,
                'filial' => $proposta->analiseDeCredito->filial->getConcat(),
                'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                'financiado' => "R$ " . number_format(($proposta->valor - $proposta->valor_entrada), 2, ',', '.'),
                'parcelas' => $proposta->qtd_parcelas . "x R$ " . number_format($proposta->valor_parcela, 2, ',', '.'),
                'repasse' => "R$ " . number_format($proposta->valorRepasse(), 2, ',', '.'),
            );

            $rows[] = $row;
        }

        echo json_encode(
                array(
                    'data' => $rows,
                    'qtd' => $qtd_props . " Bipes",
                    'totalFin' => 'R$ ' . number_format($totalFin, 2, ',', '.'),
                    'totalRep' => 'R$ ' . number_format($totalRep, 2, ',', '.')
                )
        );
    }

    public function actionValidarRecebimento() {
        $codigo = $_POST['codigo'];

        $recibimentoInvalido = false;
        $mensagem = "";
        $proposta = Proposta::model()->find("habilitado and codigo = '" . $codigo . "'");
        if($proposta == null && isset($codigo) && $codigo != ''){
            $proposta = Proposta::model()->find("habilitado and numero_do_contrato = '" . substr($codigo, 1, -2) . "'");
        }
        if ($proposta != null) {
            $itemRecibido = ItemRecebimento::model()->find('habilitado and Proposta_id = ' . $proposta->id);
            if ($itemRecibido != null) {
                $recibimentoInvalido = true;
                $mensagem = "Contrato já recebido: " . $itemRecibido->id . " | " . $proposta->id . " | CAPA: " . str_pad($itemRecibido->recebimento->Documentacao_id, 8, "0", STR_PAD_LEFT);
            }
        } else {
            $recibimentoInvalido = true;
            $mensagem = "Contrato não encontrado no sistema";
        }

        echo json_encode(
                array(
                    'invalido' => $recibimentoInvalido,
                    'mensagem' => $mensagem
                )
        );
    }

    public function actionGerarRecebimentos() {
        $bipadas = $_POST['bips'];
        if ($bipadas != '') {
            $propostas = Proposta::model()->findAll("habilitado and codigo IN ('" . join("','", explode(',', $bipadas)) . "') or numero_do_contrato IN ('" . join("','", explode(',', $bipadas)) . "')");
        } else {
            $propostas = [];
        }

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Recebimento gerado com sucesso",
            "tipo" => "success"
        );
        $certo = true;

        $transaction = Yii::app()->db->beginTransaction();
        foreach ($propostas as $p) {
            $recebimento = Recebimento::model()->find(
                    "habilitado and " .
                    "data_cadastro > '" . date('Y-m-d H:i:s', strtotime('-10 seconds', strtotime(date('Y-m-d H:i:s')))) . "' and " .
                    "Filial_id = " . $p->analiseDeCredito->filial->id
            );
            if ($recebimento == null) {
                $recebimento = new Recebimento;
                $recebimento->Filial_id = $p->analiseDeCredito->filial->id;
                $recebimento->data_cadastro = date('Y-m-d H:i:s');
                $recebimento->habilitado = 1;
                $recebimento->Usuario_id = Yii::app()->session['usuario']->id;

                if ($recebimento->save()) {
                    $itemRecebimento = new ItemRecebimento;
                    $itemRecebimento->Proposta_id = $p->id;
                    $itemRecebimento->Recebimento_id = $recebimento->id;
                    $itemRecebimento->data_cadastro = date('Y-m-d H:i;s');
                    $itemRecebimento->habilitado = 1;

                    if (!$itemRecebimento->save()) {
                        $certo = false;

                        ob_start();
                        var_dump($itemRecebimento->getErrors());
                        $erro = ob_get_clean();

                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Item de Recebimento não salvo! " . $erro,
                            "tipo" => "error"
                        );
                        break;
                    }
                } else {
                    $certo = false;

                    ob_start();
                    var_dump($recebimento->getErrors());
                    $erro = ob_get_clean();

                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Recebimento não salvo! " . $erro,
                        "tipo" => "error"
                    );
                    break;
                }
            } else {
                $itemRecebimento = new ItemRecebimento;
                $itemRecebimento->Proposta_id = $p->id;
                $itemRecebimento->Recebimento_id = $recebimento->id;
                $itemRecebimento->data_cadastro = date('Y-m-d H:i;s');
                $itemRecebimento->habilitado = 1;

                if (!$itemRecebimento->save()) {
                    $certo = false;

                    ob_start();
                    var_dump($itemRecebimento->getErrors());
                    $erro = ob_get_clean();

                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Item de Recebimento não salvo! " . $erro,
                        "tipo" => "error"
                    );
                    break;
                }
            }
        }

        $certo ? $transaction->commit() : $transaction->rollBack();

        echo json_encode($arrReturn);
    }

    public function actionRecebidos() {

        if (isset($_POST['filiais']) && $_POST['filiais'] != '') {
            $recebimentos = Recebimento::model()->findAll(array('condition' => 'Documentacao_id is NULL and habilitado and Filial_id IN (' . join(",", $_POST['filiais']) . ')', 'order' => 'id desc'));
        } else {
            $recebimentos = Recebimento::model()->findAll(array('condition' => 'Documentacao_id is NULL and habilitado', 'order' => 'id desc'));
        }

        $rows = [];
        $util = new Util;
        foreach ($recebimentos as $r) {

            $imprimir = '<form method="post" target="_blank" action="/documentacao/viaParceiro/">'
                    . '     <input name="id_recebimento" type="hidden" value="' . $r->id . '">'
                    . '     <button type="submit" class="btn btn-primary btn-xs">'
                    . '         Via do Parceiro'
                    . '     </button>'
                    . ' </form>';

            $row = array(
                'marcar' => '<input value="' . $r->id . '" class="check_recebido" type="checkbox">',
                'codigo' => date('Ymd', strtotime($r->data_cadastro)) . str_pad($r->id, 6, "0", STR_PAD_LEFT),
                'filial' => $r->filial->getConcat(),
                'user' => strtoupper($r->usuario->nome_utilizador),
                'data' => $util->bd_date_to_view(substr($r->data_cadastro, 0, 10)) ." às ". substr($r->data_cadastro, 11, 19),
                'imprimir' => $imprimir
            );

            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionNovaDocumentacao() {
        $recebimentos = $_POST['selecionados'];

        $documentacao = new Documentacao;
        $documentacao->data_cadastro = date('Y-m-d H:i:s');
        $documentacao->habilitado = 1;
        $documentacao->Usuario_id = Yii::app()->session['usuario']->id;

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Operação realizada com sucesso!",
            "tipo" => "success"
        );
        $certo = true;

        $transaction = Yii::app()->db->beginTransaction();

        if ($documentacao->save() && count($recebimentos) > 0) {
            foreach ($recebimentos as $id) {
                $recebimento = Recebimento::model()->findByPk($id);
                if ($recebimento != null) {
                    $recebimento->Documentacao_id = $documentacao->id;
                    if (!$recebimento->update()) {
                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Não foi possível atualizar o recebimento!",
                            "tipo" => "error"
                        );
                        $certo = false;
                        break;
                    }
                } else {
                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Recebimento não encontrado!",
                        "tipo" => "error"
                    );
                    $certo = false;
                    break;
                }
            }
        } else {
            $arrReturn = array(
                "hasError" => true,
                "msg" => "Não foi possível realizar a operação!",
                "tipo" => "error"
            );
            $certo = false;
        }

        $certo ? $transaction->commit() : $transaction->rollBack();

        echo json_encode($arrReturn);
    }

    public function actionDocumentacoes() {
        $documentos = Documentacao::model()->findAll(array('condition' => 'habilitado', 'order' => 'id desc'));
        $rows = [];
        $util = new Util;
        foreach ($documentos as $doc) {

            $imprimir = '<form method="post" target="_blank" action="/documentacao/imprimirCapa/">'
                    . '     <input name="id_doc" type="hidden" value="' . $doc->id . '">'
                    . '     <button type="submit" class="btn btn-primary btn-xs">'
                    . '         Imprimir Relação'
                    . '     </button>'
                    . ' </form>';

            $row = array(
                'codigo' => str_pad($doc->id, 8, "0", STR_PAD_LEFT),
                'data' => $util->bd_date_to_view(substr($doc->data_cadastro, 0, 10)) ." às ". substr($doc->data_cadastro, 11, 19),
                'user' => $doc->usuario->nome_utilizador,
                'print' => $imprimir
            );
            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionViaParceiro() {
        $this->render('/documentacao/via_parceiro', array('processo' => Recebimento::model()->findByPk($_POST['id_recebimento'])));
    }

    public function actionImprimirCapa() {
        $sql = "SELECT 
                
                D.id AS codigo_processo,
                D.data_cadastro AS data_processo,
                P.data_cadastro AS data_cadastro, 
                P.codigo AS codigo, 
                Doc.numero AS cpf, 
                PE.nome AS nome,
                (P.valor - P.valor_entrada) AS valor_financiado,
                P.carencia,
                P.qtd_parcelas AS parcelas,
                ROUND(SUM((P.valor - P.valor_entrada) - ((P.valor - P.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS valor_repasse

                FROM

                Documentacao 					AS D
                INNER JOIN Recebimento 				AS R 	ON R.Documentacao_id = D.id
                INNER JOIN ItemRecebimento 			AS IR 	ON IR.Recebimento_id = R.id
                INNER JOIN Proposta 				AS P 	ON P.id = IR.Proposta_id
                INNER JOIN Analise_de_Credito                   AS AC 	ON AC.id = P.Analise_de_Credito_id
                INNER JOIN Cliente 				AS C 	ON C.id = AC.Cliente_id
                INNER JOIN Pessoa 				AS PE 	ON PE.id = C.Pessoa_id
                INNER JOIN Pessoa_has_Documento                 AS PD 	ON PD.Pessoa_id = PE.id
                INNER JOIN Documento 				AS Doc 	ON Doc.id = PD.Documento_id 	AND Doc.Tipo_documento_id = 1
                INNER JOIN Tabela_Cotacao 			AS TC 	ON TC.id = P.Tabela_id 			AND TC.habilitado 
                INNER JOIN Fator 				AS FA 	ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = P.carencia AND FA.parcela = P.qtd_parcelas AND FA.habilitado

                WHERE D.id = " . $_POST['id_doc'] . "

                GROUP BY P.codigo
                ORDER BY PE.nome ASC";
                
        $documentacao = Yii::app()->db->createCommand($sql)->queryAll();
        
        $this->render('/documentacao/relacao_recebimentos', array('contratos' => $documentacao));
    }
    
    public function actionParceiroConsolidada(){
        $ids = $_POST['id_rebs'];
        
        $sql = "SELECT Rec.id as 'rec', NF.id as 'nf' 

                FROM Recebimento 			AS Rec
                INNER JOIN Filial 			AS F 	ON F.id = Rec.Filial_id
                INNER JOIN NucleoFiliais                AS NF 	ON NF.id = F.NucleoFiliais_id

                WHERE Rec.id IN (" . $ids . ")
                ORDER BY NF.id, Rec.id";
        
        $recebimentos = Yii::app()->db->createCommand($sql)->queryAll();
        
        $this->render('/documentacao/parceiro_consolidada', array('recebimentos' => $recebimentos));
    }
    
}
