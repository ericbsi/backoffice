<?php

class NivelRoleController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function filters()
  {
	  return array(
	    'accessControl', // perform access control for CRUD operations
	    'postOnly + delete' // we only allow deletion via POST request
	  );
  }

  public function accessRules()
  {
   		return 
   		[
	   		[
	   			'allow',
	   			'actions' 	  => ['dataTable', 'persist', 'loadFormEdit'],
	   			'users' 	    => ['@'],
	   			'expression'  => 'Yii::app()->session["usuario"]->autorizado()'
	   		],

	   		[
	   			'deny',
	   			'users'	 	   => ['*']
	   		]
   		];
  }

  public function actionPersist()
  {
  	$retorno 							           = NivelRole::model()->persist($_POST['NivelRole'], $_POST['Nivel_Role_id']);

    if ($retorno['hasErrors'] 			=== true)
    {
        foreach ($retorno['errors'] as $erro)
        {
          $retorno['msgConfig'][] 	 = [
            'tipo' 					         => 'error',
            'titulo' 				         => 'Não foi possível realizar a operação: ',
            'mensagem' 				       => 'Motivo : ' . ($erro[0]),
            'posicao' 				       => 'top left'
          ];
        }
    }
    else
    {
      $retorno['msgConfig'][] 		= [
        'tipo' 						        => 'success',
        'titulo' 					        => 'Operação realizada com sucesso: ',
        'mensagem' 					      => 'Mensagem : Nivel cadastrado, já disponível para consulta.',
        'posicao' 				  	    => 'top left'
      ];
    }
      	
      	echo json_encode($retorno);
  }

	//sobrescreve a função do Controller, classe pai
	public function init(){

	}

  public function actionLoadFormEdit()
  {
    echo json_encode(NivelRole::model()->loadFormEdit($_POST['id']));
  }

  public function actionDataTable()
  {
    $niveisCollection     = NivelRole::model()->dataTable($_POST['draw'], [ 'start' => $_POST['start'], 'limit' => $_POST['length']], $_POST['Role_Id'] );

    echo json_encode([
      'draw'            	=> $_POST['draw'                        ],
      'data'            	=> $niveisCollection['collection'       ],
      'recordsTotal'    	=> count($niveisCollection['collection' ]),
      'recordsFiltered' 	=> $niveisCollection['total'            ]
    ]);
  }
}