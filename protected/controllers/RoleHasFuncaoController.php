<?php

class RoleHasFuncaoController extends Controller
{
	public $layout = '';

	public function actionIndex()
	{
		$this->render('index');
	}


	public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' 	=> ['dataTable', 'roleFuncao'],
                'users' 	=> ['@']
                //'expression' => 'Yii::app()->session["usuario"]->autorizado()'
            ],
            [
                'deny',
                'users' => ['*']
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionRoleFuncao()
    {
        echo json_encode( RoleHasFuncao::model()->roleFuncao( $_POST['roleId'], $_POST['funcaoId'], $_POST['hab'] ) );
    }

	public function actionDataTable()
    {
        $funcoesCollection      = RoleHasFuncao::model()->dataTable($_POST['draw'], [ 'start' => $_POST['start'], 'limit' => $_POST['length']], $_POST['perfilId']);

        echo json_encode([
            'draw'              => $_POST['draw'                            ],
            'data'              => $funcoesCollection['collection'          ],
            'recordsTotal'      => count($funcoesCollection['collection'    ]),
            'recordsFiltered'   => $funcoesCollection['total'               ]
        ]);
    }
	
}