<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AnaliseInadimplenciaController
 *
 * @author andre
 */
class AnaliseInadimplenciaController extends Controller
{

    public $layout = '';

    public function filters()
    {
        return array(
                        'accessControl'     ,   // perform access control for CRUD operations
                        'postOnly + delete'     // we only allow deletion via POST request
                    );
    }

    public function accessRules()
    {
        return  [
                    [
                        'allow'                                                                 ,
                        'actions'       =>  [
                                            ]                                                   ,
                        'users'         =>  [
                                                '@'
                                            ]                                                   ,
//                        'expression'    =>      'Yii::app()->session["usuario"]->autorizado()'
                    ],
                    [
                        'allow'                                                                 ,
                        'actions'   =>  [
                                            'bads'                  ,
                                            'index'                 ,
                                            'getResultadoSafra'     ,
                                            'getResultadoTrimestre' ,
                                            'getResultadoPNP'       ,
                                            'getSafraNucleo'        ,
                                            'getSafraFilial'        ,
                                            'teste'                 ,
                                            'getMeses'              ,
                                            'getMesesColunas'       ,
                                            'carregarBads'          ,
                                            'listarCidades'         ,
                                            'getTableSafraNucleo'   ,
                                            'getTableSafraFilial'   ,
                                            'getTableSafra'         ,
                                            'getTableAnalistas'     ,
                                            'getTableTrimestre'     ,
                                            'getTablePNP'           ,
                                            'getTableAnalistasNucleo'   
                                        ]                                                       ,
                        'users'     =>  [   
                                            '@'
                                        ]                                                       ,
                    ],
                    [
                        'deny'                                                                  ,
                        'users' =>  [
                                        '*'
                                    ]
                    ]
                ];
    }
    
    public function actionBads()
    {
        
        $baseUrl    = Yii::app()->baseUrl           ;
        $cs         = Yii::app()->getClientScript() ;
        
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css'                                       );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css'                                       );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css'                                    );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css'                                      );
        /* FIM CSS */

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifyjs/dist/notify.min.js'      , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/limpaForm.js'                                       , CClientScript::POS_END    );

        $cs->registerScriptFile($baseUrl . '/assets/ubold/plugins/notifications/notify-metro.js'    , CClientScript::POS_END    );

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js'         , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js'          , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js'        , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js'         , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js'     , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js'      , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js'       , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/jquery.blockUI.js'                                             , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/blockInterface.js'                                             , CClientScript::POS_END    );
        /* FIM dataTables */

        $cs->registerScriptFile('/js/analiseInadimplencia/bads/fn-gridBads.js'                  , CClientScript::POS_END    );

        $this->layout = '//layouts/ubold';
        
        $this->render('bads');
        
    }
    
    public function actionIndex()
    {
        
        $baseUrl    = Yii::app()->baseUrl           ;
        $cs         = Yii::app()->getClientScript() ;
        
        /* CSS */
        $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css'                                               );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css'                                               );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css'                                            );
        $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css'                                              );
        $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css'                                                                );
        $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css'                                 );
        $cs->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css'                                                   );
        /* FIM CSS */

        $cs->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js'                                                 );
        $cs->registerScriptFile('/js/jquery.validate.12.js'                                                 , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js'                         , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js'                       , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/limpaForm.js'                                                          , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/bootstrap-multiselect.js'                                              , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js'                       , CClientScript::POS_END    );

        /* dataTables */
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js'                  , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js'                , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js'             , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js'              , CClientScript::POS_END    );
        $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js'               , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/jquery.blockUI.js'                                                     , CClientScript::POS_END    );
        $cs->registerScriptFile('/js/blockInterface.js'                                                     , CClientScript::POS_END    );
        /* FIM dataTables */

        $cs->registerScriptFile('/js/analiseInadimplencia/fn-analiseInadimplencia.js'                       , CClientScript::POS_END    );

        $this->layout = '//layouts/ubold';
        
        $this->render('index');
        
    }
    
    public function actionGetResultadoPNP()
    {
        
        $erro           = "";
        $alerta         = "";
        
        $dados          = [];
        $filtroPreSub   = [];
        
        $filtroPre      = [];
        
        $filtroAgrup    = "";
        
        $arrayAuxiliar  =   [
                                "5"     ,
                                "10"    ,
                                "15"    ,
                                "20"    ,
                                "25"    ,
                                "30"    ,
                                "35"    ,
                                "40"    ,
                                "45"    ,
                                "50"    ,
                                "55"    ,
                                "60"    
                            ];
        
        $sql            = "";
        
        if(isset($_POST["filtroPreSub"]))
        {
            
            $filtroPre = $_POST["filtroPreSub"];
            
            if(true)
            {
                
                $sql = "SELECT 
                            " . $filtroPre["campoFiltro"] . " AS " . $filtroPre["aliasFiltro"] . ", " . $filtroPre["agrupamento"] . " AS agrupamento , 

                                    SUM(`Pa.valor`) AS vlrParcela,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 5   THEN `Pa.valor` ELSE 0 END) AS ate5,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 10  THEN `Pa.valor` ELSE 0 END) AS ate10,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 15  THEN `Pa.valor` ELSE 0 END) AS ate15,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 20  THEN `Pa.valor` ELSE 0 END) AS ate20,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 25  THEN `Pa.valor` ELSE 0 END) AS ate25,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 30  THEN `Pa.valor` ELSE 0 END) AS ate30,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 35  THEN `Pa.valor` ELSE 0 END) AS ate35,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 40  THEN `Pa.valor` ELSE 0 END) AS ate40,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 45  THEN `Pa.valor` ELSE 0 END) AS ate45,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 50  THEN `Pa.valor` ELSE 0 END) AS ate50,
                                                                    
                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 55  THEN `Pa.valor` ELSE 0 END) AS ate55,

                                    SUM(CASE WHEN 	DATEDIFF(`B.data_da_baixa`,LEFT(`Pa.vencimento`,10)) <= 60  THEN `Pa.valor` ELSE 0 END) AS ate60

                            FROM(

                            SELECT 
                                    P.data_cadastro     AS 'P.data_cadastro'    ,	B.data_da_baixa AS 'B.data_da_baixa'	, 
                                    Pa.vencimento 	AS 'Pa.vencimento'	,	Pa.valor 	AS 'Pa.valor'		,	
                                    P.Financeira_id	AS 'P.Financeira_id'	,	TC.ModalidadeId	AS 'TC.ModalidadeId'	,
                                    AC.Filial_id	AS 'AC.Filial_id'	,	F.id		AS 'F.id'		,
                                    NF.id               AS 'NF.id'              , 
                                    NF.nome             AS 'NF.nome'            ,       F.nome_fantasia AS 'F.nome_fantasia'    , 
                                    E.cidade            AS 'E.cidade'           ,       E.uf            AS 'E.uf'               ,
                                    Fi.classe           AS 'Fi.classe'

                            FROM            nordeste2.Proposta            AS P 
                            INNER   JOIN    nordeste2.Tabela_Cotacao      AS TC   ON  TC.habilitado   AND   P.Tabela_id               = TC.id 
                            INNER   JOIN    nordeste2.Analise_de_Credito  AS AC   ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id 
                            INNER   JOIN    nordeste2.Titulo              AS T    ON   T.habilitado   AND   T.Proposta_id             =  P.id 
                            INNER   JOIN    nordeste2.Parcela             AS Pa   ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id 
                            INNER   JOIN    nordeste2.Filial              AS F    ON   F.habilitado   AND  AC.Filial_id               =  F.id 
                            INNER   JOIN    nordeste2.Filial_has_Endereco AS FhE  ON FhE.habilitado   AND FhE.Filial_id               =  F.id 
                            INNER   JOIN    nordeste2.Endereco            AS E    ON   E.habilitado   AND FhE.Endereco_id             =  E.id 
                            INNER   JOIN    nordeste2.NucleoFiliais       AS NF   ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id 
                            LEFT    JOIN    nordeste2.Baixa               AS B    ON   B.baixado      AND   B.Parcela_id              = Pa.id  
                            LEFT    JOIN    nordeste2.PropostaOmniConfig  AS POC  ON POC.habilitado   AND POC.Proposta                =  P.id 
                            INNER   JOIN    nordeste2.Cliente             AS C    ON   C.habilitado   AND  AC.Cliente_id              =  C.id 
                            LEFT    JOIN    nordeste2.Ficha               AS Fi   ON                        C.Pessoa_id               = Fi.Pessoa_id 
                                                                                                      AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10) 
                            WHERE 
                                    P.habilitado                    AND 
                                    POC.id                  IS NULL AND 
                                    T.NaturezaTitulo_id = 1			AND
                                Pa.seq = 1 

                            /*UNION

                            SELECT 
                                    P.data_cadastro     AS 'P.data_cadastro'    ,	B.data_da_baixa AS 'B.data_da_baixa'	, 
                                    Pa.vencimento 	AS 'Pa.vencimento'	,	Pa.valor 	AS 'Pa.valor'		,	
                                    P.Financeira_id	AS 'P.Financeira_id'	,	TC.ModalidadeId	AS 'TC.ModalidadeId'	,
                                    AC.Filial_id	AS 'AC.Filial_id'	,	F.id		AS 'F.id'               ,       
                                    NF.id               AS 'NF.id'              , 
                                    NF.nome             AS 'NF.nome'            ,       F.nome_fantasia AS 'F.nome_fantasia'    , 
                                    E.cidade            AS 'E.cidade'           ,       E.uf            AS 'E.uf'               ,
                                    ' '                 AS 'Fi.classe'

                            FROM            beta.Proposta            AS P 
                            INNER   JOIN    beta.Tabela_Cotacao      AS TC   ON  TC.habilitado   AND   P.Tabela_id               = TC.id 
                            INNER   JOIN    beta.Analise_de_Credito  AS AC   ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id 
                            INNER   JOIN    beta.Titulo              AS T    ON   T.habilitado   AND   T.Proposta_id             =  P.id 
                            INNER   JOIN    beta.Parcela             AS Pa   ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id 
                            INNER   JOIN    beta.Filial              AS F    ON   F.habilitado   AND  AC.Filial_id               =  F.id 
                            INNER   JOIN    beta.Filial_has_Endereco AS FhE  ON FhE.habilitado   AND FhE.Filial_id               =  F.id 
                            INNER   JOIN    beta.Endereco            AS E    ON   E.habilitado   AND FhE.Endereco_id             =  E.id 
                            INNER   JOIN    beta.NucleoFiliais       AS NF   ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id 
                            LEFT    JOIN    beta.Baixa               AS B    ON   B.baixado      AND   B.Parcela_id              = Pa.id  
                            LEFT    JOIN    beta.PropostaOmniConfig  AS POC  ON POC.habilitado   AND POC.Proposta                =  P.id 
                            WHERE 
                                    P.habilitado                    AND 
                                    POC.id                  IS NULL AND 
                                    T.NaturezaTitulo_id = 1			AND
                                Pa.seq = 1 */
                            ) AS TEMP WHERE 1 ";
        
                $filiaisPreDefinidas = Filial::model()->findAllAdmFiliais();

                $idsFiliaisPre = [];

                foreach ($filiaisPreDefinidas as $filial)
                {
                    $idsFiliaisPre[] = $filial->id;
                }

                if (!empty($idsFiliaisPre))
                {
                    $idsFiliaisPreStr = join(",", $idsFiliaisPre);
                }
                else
                {
                    $idsFiliaisPreStr = "-1";
                }

                $sql .= " AND `F.id` IN ($idsFiliaisPreStr) ";
                
            }

            if(isset($filtroPre["mesesPartida"      ]) && !empty($filtroPre["mesesPartida"      ]))
            {

                
                if($filtroPre["nivelTable"] == "0")
                {
                    $mesesFiltro = join(",", $filtroPre["mesesPartida"]);
                }
                else
                {
                    $mesesFiltro = $filtroPre["mesesPartida"];
                }

                $sql    .= " AND CONCAT(YEAR(`P.data_cadastro`),MONTH(`P.data_cadastro`)) IN ($mesesFiltro) ";
                
                $filtroPreSub["financeiraSafra"] = $mesesFiltro;
            }

            if(isset($filtroPre["financeiraSafra"   ]) && !empty($filtroPre["financeiraSafra"   ]))
            {
                        
                /*ob_start();
                var_dump($filtroPre["financeiraSafra"]);
                $result = ob_get_clean();

                $file = fopen("testeFiltroPreSubFinanceiraSafra.txt", 'w');

                fwrite($file, $result );
                fclose($file);*/
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $financeiraSafra = join(",", $filtroPre["financeiraSafra"]);
                }
                else
                {
                    $financeiraSafra = $filtroPre["financeiraSafra"];
                }

                $sql                                .= " AND `P.Financeira_id` IN ($financeiraSafra) "    ;

                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;

            }

            if(isset($filtroPre["modalidadeSafra"   ]) && !empty($filtroPre["modalidadeSafra"   ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $modalidades = join(",", $filtroPre["modalidadeSafra"]);
                }
                else
                {
                    $modalidades = $filtroPre["modalidadeSafra"];
                }

                $sql                            .= " AND `TC.ModalidadeId` IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }      

            if(isset($filtroPre["politicaSafra"     ]) && !empty($filtroPre["politicaSafra"     ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $politicaSafra = join(",", $filtroPre["politicaSafra"]);
                }
                else
                {
                    $politicaSafra = $filtroPre["politicaSafra"];
                }

                $sql    .=  " AND EXISTS    "
                        .   "           (   "
                        .   "               SELECT * "
                        .   "               FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "               WHERE FhP.Filial_id = `AC.Filial_id` AND (`P.data_cadastro` >= FhP.dataInicio AND (FhP.dataFim IS NULL OR `P.data_cadastro` <= FhP.dataFim)) "
                        .   "                   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   "           ) ";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }    


            if(isset($filtroPre["filiaisSafra"      ]) && !empty($filtroPre["filiaisSafra"      ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $filiaisSafra = join(",", $filtroPre["filiaisSafra"]);
                }
                else
                {
                    $filiaisSafra = $filtroPre["filiaisSafra"];
                }

                $sql                            .= " AND `F.id` IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }

            if(isset($filtroPre["scoreSafra"        ]) && !empty($filtroPre["scoreSafra"        ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $scoreSafra = join(",", $filtroPre["scoreSafra"]);
                }
                else
                {
                    $scoreSafra = $filtroPre["scoreSafra"];
                }

                $sql                            .= " AND `Fi.classe` IN ($scoreSafra) "      ;

                $filtroPreSub["scoreSafra"]    = $scoreSafra                        ;

            }
            
            if(isset($filtroPre["cidadesSafra"      ]) && !empty($filtroPre["cidadesSafra"      ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $cidadesSafra = join(",", $filtroPre["cidadesSafra"]);
                }
                else
                {
                    $cidadesSafra = $filtroPre["cidadesSafra"];
                }

                $sql                            .= " AND UPPER(`E.cidade`) IN ($cidadesSafra) "       ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                    ;

            }

            if(isset($filtroPre["nivelTable"        ]) && !empty($filtroPre["nivelTable"        ]))
            {

                if($filtroPre["nivelTable"] == "0")
                {
                }
                else //if($filtroPre["nivelTable"] == "1")
                {
                    
                    $filtroAnterior  = " AND " .    $filtroPre      ["filtroPre"] . " " ;
                    
                    $sql            .=              $filtroAnterior                     ;
                }

            }

            $sql    .= "    GROUP BY agrupamento    " 
                    .  "    ORDER BY agrupamento    ";
        
            try
            {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $linha = [];

                foreach ($resultado as $r)
                {
                        
                    $campoFiltro    = "";

                    if(count($linha) > 0)
                    {
                        
                        if      ($filtroPre["nivelTable"] == "0")
                        {
                            $campoFiltro                = " `NF.id` "               ;
                            $aliasFiltro                = "idNucleo"                ;
                            $campoAgrupamento           = " `NF.id`, `NF.nome`  "   ;
                            $linha["nomeAgrupamento"]   = "Núcleo"                  ;
                            $linha["nomeTabela"     ]   = "gridPNPNucleo"           ;
                        }
                        else if ($filtroPre["nivelTable"] == "1")
                        {
                            $campoFiltro                = " `F.id` "                                                      ;
                            $aliasFiltro                = "idFilial"                                                    ;
                            $campoAgrupamento           = " `F.id`, CONCAT(`F.nome_fantasia`, ' - ', `E.cidade`, '/', `E.uf`) " ;
                            $linha["nomeAgrupamento"]   = "Filial"                                                      ;
                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"                                         ;
                        }
                        else if ($filtroPre["nivelTable"] == "2")
                        {
                            $campoFiltro                = ""                            ;
                            $aliasFiltro                = ""                            ;
                            $campoAgrupamento           = ""                            ;
//                            $filtroAgrup                = " F.id, F.nome_fantasia " ;
                            $linha["nomeAgrupamento"]   = ""                            ;
//                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"     ;
                        }
                        
                        $filtroPreSub   ["campoFiltro"      ]   = $campoFiltro                                                              ;
                        $filtroPreSub   ["aliasFiltro"      ]   = $aliasFiltro                                                              ;
                        
                        $filtroPreSub   ["nivelTable"       ]   = ((int) $filtroPre["nivelTable"]) + 1                                      ;
                        $filtroPreSub   ["filtroPre"        ]   = $filtroPre["campoFiltro"] . " = '$valorFiltro' "                          ;
                        
                        if(!empty($filtroAnterior))
                        {
                            $filtroPreSub["filtroPre"] .=  $filtroAnterior;
                        }
                        
                        $filtroPreSub   ["agrupamento"      ]   = $campoAgrupamento                                                         ;

                        $linha          ["geral"            ]   = "-"                                                                       ;
                        $linha          ["urlMontaTable"    ]   = "/analiseInadimplencia/getTablePNP"                                       ;
                        $linha          ["filtroPre"        ]   = $filtroPreSub["filtroPre"   ]                                             ;
                        $linha          ["filtroPreSub"     ]   = $filtroPreSub                                                             ;
                        $linha          ["tituloTable"      ]   = "Safra " . $linha["agrupamento"] . " por " . $linha["nomeAgrupamento"]    ;

                        $dados          [                   ]   = $linha                                                                    ;

                        $linha                                  = []                                                                        ;

                    }

                    $valorFiltro            = $r[$filtroPre["aliasFiltro"]  ];
                    $agrupamento            = $r[           "agrupamento"   ];
                    
/*                    if($filtroPre["nivel"] == "0")
                    {
                    }*/
                    
                    if ($filtroPre["nivelTable"] == "2")
                    {
                        $btnDetalhar = "";
                    }
                    else
                    {
                    
                        $btnDetalhar            = '<button '
                                            . '         style="border-radius: 20px" '
                                            . '         class="btn-success btnDetalharSafra" '
                                            . '         value="' . $r["agrupamento"] . '"> '
                                            . '     <i class="fa fa-plus"></i>'
                                            . '</button>'                                           ;
                        
                    }

                    $linha["detalhar"   ]   = $btnDetalhar                                          ;
                    
                    if($filtroPre["nivelTable"] == "0")
                    {

                        $mesExtenso             = $this->getMesExtenso( substr($r["agrupamento"],4,2))  ;
                        $linha["agrupamento"]   = $mesExtenso . "/" .   substr($r["agrupamento"],0,4)   ;
                    }
                    else
                    {
                        $linha["agrupamento"]   = strtoupper($r["agrupamento"]);
                    }

                    foreach ($arrayAuxiliar as $aux)
                    {

                        $deve = $r["vlrParcela"] - $r["ate$aux"];

                        if( $r["vlrParcela"] > 0)
                        {
                            $percentualInadimplencia    =   ($deve / $r["vlrParcela"]) * 100                                    ;
                            $percentualInadimplencia    =   number_format($percentualInadimplencia  , 2 , ","   , ".") . "%"    ;
                        }
                        else
                        {
                            $percentualInadimplencia    = "-";
                        }

                        $linha["ate$aux"]   = $percentualInadimplencia;

                    }

                }

                if(count($linha) > 0)
                {
                        
                    if      ($filtroPre["nivelTable"] == "0")
                    {
                        $campoFiltro                = " `NF.id` "                     ;
                        $aliasFiltro                = "idNucleo"                    ;
                        $campoAgrupamento           = " `NF.id`, `NF.nome`  "           ;
                        $linha["nomeAgrupamento"]   = "Núcleo"                      ;
                        $linha["nomeTabela"     ]   = "gridTrimestreNucleo"         ;
                    }
                    else if ($filtroPre["nivelTable"] == "1")
                    {
                        $campoFiltro                = " `F.id` "                      ;
                        $aliasFiltro                = "idFilial"                    ;
                        $campoAgrupamento           = " `F.id`, CONCAT(`F.nome_fantasia`, ' - ', `E.cidade`, '/', `E.uf`) "     ;
                        $linha["nomeAgrupamento"]   = "Filial"                      ;
                        $linha["nomeTabela"     ]   = "gridTrimestreFilial"         ;
                    }
                    else if ($filtroPre["nivelTable"] == "2")
                    {
                        $campoFiltro                = ""                            ;
                        $aliasFiltro                = ""                            ;
                        $campoAgrupamento           = ""                            ;
//                            $filtroAgrup                = " F.id, F.nome_fantasia " ;
                        $linha["nomeAgrupamento"]   = ""                            ;
//                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"     ;
                    }

                    $filtroPreSub   ["campoFiltro"      ]   = $campoFiltro                                                              ;
                    $filtroPreSub   ["aliasFiltro"      ]   = $aliasFiltro                                                              ;

                    $filtroPreSub   ["nivelTable"       ]   = ((int) $filtroPre["nivelTable"]) + 1                                      ;
                    $filtroPreSub   ["filtroPre"        ]   = $filtroPre["campoFiltro"] . " = '$valorFiltro' "                          ;

                    if(!empty($filtroAnterior))
                    {
                        $filtroPreSub["filtroPre"] .=  $filtroAnterior;
                    }

                    $filtroPreSub   ["agrupamento"      ]   = $campoAgrupamento                                                         ;

                    $linha          ["geral"            ]   = "-"                                                                       ;
                    $linha          ["urlMontaTable"    ]   = "/analiseInadimplencia/getTablePNP"                                       ;
                    $linha          ["filtroPre"        ]   = $filtroPreSub["filtroPre"   ]                                             ;
                    $linha          ["filtroPreSub"     ]   = $filtroPreSub                                                             ;
                    $linha          ["tituloTable"      ]   = "Safra " . $linha["agrupamento"] . " por " . $linha["nomeAgrupamento"]    ;

                    $dados          [                   ]   = $linha                                                                    ;

                }

            }
            catch (Exception $ex)
            {
                $erro = $ex->getMessage();
            }
            
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "erro"  => $erro    ,
                                    '$sql'  => $sql  
                                ]
                            );
        
    }
    
    public function actionGetResultadoTrimestre()
    {
        
        $erro           = "";
        $alerta         = "";
        
        $dados          = [];
        $filtroPreSub   = [];
        
        $filtroPre      = [];
        
        $filtroAgrup    = "";
        
        $arrayAuxiliar  =   [
                                "1"         ,
                                "123"       ,
                                "456"       ,
                                "789"       ,
                                "101112"    ,
                                "131415"    ,
                                "161718" 
                            ];
        
        $sql            = "";
        
        if(isset($_POST["filtroPreSub"]))
        {
            
            $filtroPre = $_POST["filtroPreSub"];
            
            if(true)
            {

            $sql    = " SELECT  " . $filtroPre["campoFiltro"] . " AS " . $filtroPre["aliasFiltro"] . ", " . $filtroPre["agrupamento"] . " AS agrupamento , 
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (1) 
                                                THEN    Pa.valor        
                                                ELSE    0
                                        END
                                    ) AS valorParcela1   , 
                                SUM(    CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 1                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN    Pa.valor ELSE 0 END 
                                    ) AS valorBaixa1    ,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (1,2,3) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela123 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 1                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 2                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 3                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa123  ,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (4,5,6) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela456 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 4                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 5                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 6                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa456,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (7,8,9) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela789 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 7                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 8                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 9                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa789,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (10,11,12) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela101112 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 10                                                                             AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 11                                                                             AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 12                                                                             AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa101112,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (13,14,15) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela131415 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 13                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 14                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 15                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa131415,
                                SUM(    CASE    
                                                WHEN    Pa.seq IN (16,17,18) 
                                                THEN    Pa.valor            
                                                ELSE    0
                                        END
                                    ) AS valorParcela161718 , 
                                SUM(    
                                        CASE    
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 16                                                                              AND 
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  3 month),7),'-15') 
                                                THEN    Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 17                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  2 month),7),'-15') 
                                                THEN Pa.valor 
                                                WHEN     B.id IS NOT NULL                                                                       AND 
                                                        Pa.seq = 18                                                                              AND
                                                         B.data_da_baixa <= CONCAT(LEFT(DATE_ADD(Pa.vencimento, interval  1 month),7),'-15') 
                                                THEN Pa.valor 
                                                ELSE 0 
                                        END 
                                    ) AS valorBaixa161718     
                        FROM            Proposta            AS P 
                        INNER   JOIN    Tabela_Cotacao      AS TC   ON  TC.habilitado   AND   P.Tabela_id               = TC.id 
                        INNER   JOIN    Analise_de_Credito  AS AC   ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id 
                        INNER   JOIN    Titulo              AS T    ON   T.habilitado   AND   T.Proposta_id             =  P.id 
                        INNER   JOIN    Parcela             AS Pa   ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id 
                        INNER   JOIN    Filial              AS F    ON   F.habilitado   AND  AC.Filial_id               =  F.id 
                        INNER   JOIN    Filial_has_Endereco AS FhE  ON FhE.habilitado   AND FhE.Filial_id               =  F.id 
                        INNER   JOIN    Endereco            AS E    ON   E.habilitado   AND FhE.Endereco_id             =  E.id 
                        INNER   JOIN    NucleoFiliais       AS NF   ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id 
                        LEFT    JOIN    PropostaOmniConfig  AS POC  ON POC.habilitado   AND POC.Proposta                =  P.id 
                        LEFT    JOIN    Baixa               AS B    ON   B.baixado      AND   B.Parcela_id              = Pa.id  
                        INNER   JOIN    Cliente             AS C    ON   C.habilitado   AND  AC.Cliente_id              =  C.id 
                        LEFT    JOIN    Ficha               AS Fi   ON                        C.Pessoa_id               = Fi.Pessoa_id 
                                                                                        AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10) 
                        WHERE 
                            P.habilitado                    AND 
                            POC.id                  IS NULL AND 
                            T.NaturezaTitulo_id = 1 ";
        
                $filiaisPreDefinidas = Filial::model()->findAllAdmFiliais();

                $idsFiliaisPre = [];

                foreach ($filiaisPreDefinidas as $filial)
                {
                    $idsFiliaisPre[] = $filial->id;
                }

                if (!empty($idsFiliaisPre))
                {
                    $idsFiliaisPreStr = join(",", $idsFiliaisPre);
                }
                else
                {
                    $idsFiliaisPreStr = "-1";
                }

                $sql .= " AND F.id IN ($idsFiliaisPreStr) ";
        
            }

            if(isset($filtroPre["mesesPartida"      ]) && !empty($filtroPre["mesesPartida"      ]))
            {

                
                if($filtroPre["nivelTable"] == "0")
                {
                    $mesesFiltro = join(",", $filtroPre["mesesPartida"]);
                }
                else
                {
                    $mesesFiltro = $filtroPre["mesesPartida"];
                }

                $sql    .= " AND CONCAT(YEAR(P.data_cadastro),MONTH(P.data_cadastro)) IN ($mesesFiltro) ";
                
                $filtroPreSub["mesesPartida"] = $mesesFiltro;
            }

            if(isset($filtroPre["financeiraSafra"   ]) && !empty($filtroPre["financeiraSafra"   ]))
            {
                        
                /*ob_start();
                var_dump($filtroPre["financeiraSafra"]);
                $result = ob_get_clean();

                $file = fopen("testeFiltroPreSubFinanceiraSafra.txt", 'w');

                fwrite($file, $result );
                fclose($file);*/
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $financeiraSafra = join(",", $filtroPre["financeiraSafra"]);
                }
                else
                {
                    $financeiraSafra = $filtroPre["financeiraSafra"];
                }

                $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;

                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;

            }

            if(isset($filtroPre["modalidadeSafra"   ]) && !empty($filtroPre["modalidadeSafra"   ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $modalidades = join(",", $filtroPre["modalidadeSafra"]);
                }
                else
                {
                    $modalidades = $filtroPre["modalidadeSafra"];
                }

                $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }      

            if(isset($filtroPre["politicaSafra"     ]) && !empty($filtroPre["politicaSafra"     ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $politicaSafra = join(",", $filtroPre["politicaSafra"]);
                }
                else
                {
                    $politicaSafra = $filtroPre["politicaSafra"];
                }

                $sql    .=  " AND EXISTS    "
                        .   "           (   "
                        .   "               SELECT * "
                        .   "               FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "               WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                        .   "                   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   "           ) ";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }    


            if(isset($filtroPre["filiaisSafra"      ]) && !empty($filtroPre["filiaisSafra"      ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $filiaisSafra = join(",", $filtroPre["filiaisSafra"]);
                }
                else
                {
                    $filiaisSafra = $filtroPre["filiaisSafra"];
                }

                $sql                            .= " AND F.id IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }
        
            if(isset($filtroPre["scoreSafra"        ]) && !empty($filtroPre["scoreSafra"        ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $scoreSafra = join(",", $filtroPre["scoreSafra"]);
                }
                else
                {
                    $scoreSafra = $filtroPre["scoreSafra"];
                }

                $sql                            .= " AND Fi.classe IN ($scoreSafra) "      ;

                $filtroPreSub["scoreSafra"]    = $scoreSafra                        ;

            }
            
            if(isset($filtroPre["cidadesSafra"      ]) && !empty($filtroPre["cidadesSafra"      ]))
            {
                
                if($filtroPre["nivelTable"] == "0")
                {
                    $cidadesSafra = join(",", $filtroPre["cidadesSafra"]);
                }
                else
                {
                    $cidadesSafra = $filtroPre["cidadesSafra"];
                }

                $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "       ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                    ;

            }

            if(isset($filtroPre["nivelTable"        ]) && !empty($filtroPre["nivelTable"        ]))
            {

                if($filtroPre["nivelTable"] == "0")
                {
                }
                else //if($filtroPre["nivelTable"] == "1")
                {
                    
                    $filtroAnterior  = " AND " .    $filtroPre      ["filtroPre"] . " " ;
                    
                    $sql            .=              $filtroAnterior                     ;
                }

            }

            $sql    .= "    GROUP BY agrupamento    " 
                    .  "    ORDER BY agrupamento    ";
        
            try
            {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $linha = [];

                foreach ($resultado as $r)
                {
                        
                    $campoFiltro    = "";

                    if(count($linha) > 0)
                    {
                        
                        if      ($filtroPre["nivelTable"] == "0")
                        {
                            $campoFiltro                = " NF.id "                     ;
                            $aliasFiltro                = "idNucleo"                    ;
                            $campoAgrupamento           = " NF.id, NF.nome  "           ;
                            $linha["nomeAgrupamento"]   = "Núcleo"                      ;
                            $linha["nomeTabela"     ]   = "gridTrimestreNucleo"         ;
                        }
                        else if ($filtroPre["nivelTable"] == "1")
                        {
                            $campoFiltro                = " F.id "                                                      ;
                            $aliasFiltro                = "idFilial"                                                    ;
                            $campoAgrupamento           = " F.id, CONCAT(F.nome_fantasia, ' - ', E.cidade, '/', E.uf) " ;
                            $linha["nomeAgrupamento"]   = "Filial"                                                      ;
                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"                                         ;
                        }
                        else if ($filtroPre["nivelTable"] == "2")
                        {
                            $campoFiltro                = ""                            ;
                            $aliasFiltro                = ""                            ;
                            $campoAgrupamento           = ""                            ;
//                            $filtroAgrup                = " F.id, F.nome_fantasia " ;
                            $linha["nomeAgrupamento"]   = ""                            ;
//                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"     ;
                        }
                        
                        $filtroPreSub   ["campoFiltro"      ]   = $campoFiltro                                                              ;
                        $filtroPreSub   ["aliasFiltro"      ]   = $aliasFiltro                                                              ;
                        
                        $filtroPreSub   ["nivelTable"       ]   = ((int) $filtroPre["nivelTable"]) + 1                                      ;
                        $filtroPreSub   ["filtroPre"        ]   = $filtroPre["campoFiltro"] . " = '$valorFiltro' "                          ;
                        
                        if(!empty($filtroAnterior))
                        {
                            $filtroPreSub["filtroPre"] .=  $filtroAnterior;
                        }
                        
                        $filtroPreSub   ["agrupamento"      ]   = $campoAgrupamento                                                         ;

                        $linha          ["geral"            ]   = "-"                                                                       ;
                        $linha          ["urlMontaTable"    ]   = "/analiseInadimplencia/getTableTrimestre"                                 ;
                        $linha          ["filtroPre"        ]   = $filtroPreSub["filtroPre"   ]                                             ;
                        $linha          ["filtroPreSub"     ]   = $filtroPreSub                                                             ;
                        $linha          ["tituloTable"      ]   = "Safra " . $linha["agrupamento"] . " por " . $linha["nomeAgrupamento"]    ;

                        $dados          [                   ]   = $linha                                                                    ;

                        $linha                                  = []                                                                        ;

                    }

                    $valorFiltro            = $r[$filtroPre["aliasFiltro"]  ];
                    $agrupamento            = $r[           "agrupamento"   ];
                    
/*                    if($filtroPre["nivel"] == "0")
                    {
                    }*/
                    
                    if ($filtroPre["nivelTable"] == "2")
                    {
                        $btnDetalhar = "";
                    }
                    else
                    {
                    
                        $btnDetalhar            = '<button '
                                            . '         style="border-radius: 20px" '
                                            . '         class="btn-success btnDetalharSafra" '
                                            . '         value="' . $r["agrupamento"] . '"> '
                                            . '     <i class="fa fa-plus"></i>'
                                            . '</button>'                                           ;
                        
                    }

                    $linha["detalhar"   ]   = $btnDetalhar                                          ;
                    
                    if($filtroPre["nivelTable"] == "0")
                    {

                        $mesExtenso             = $this->getMesExtenso( substr($r["agrupamento"],4,2))  ;
                        $linha["agrupamento"]   = $mesExtenso . "/" .   substr($r["agrupamento"],0,4)   ;
                    }
                    else
                    {
                        $linha["agrupamento"]   = strtoupper($r["agrupamento"]);
                    }

                    foreach ($arrayAuxiliar as $aux)
                    {

                        $deve = $r["valorParcela$aux"] - $r["valorBaixa$aux"];

                        if( $r["valorParcela$aux"] > 0)
                        {
                            $percentualInadimplencia    =   ($deve / $r["valorParcela$aux"]) * 100                              ;
                            $percentualInadimplencia    =   number_format($percentualInadimplencia  , 2 , ","   , ".") . "%"    ;
                        }
                        else
                        {
                            $percentualInadimplencia    = "-";
                        }

                        $linha[         $aux . "x"  ]   =                           $percentualInadimplencia                        ;
                        $linha["meta" . $aux . "x"  ]   = "-"                                                                       ;
                        $linha["deve" . $aux . "x"  ]   = "R$ " .   number_format(  $deve                     , 2 , ","   , ".")    ;
                        $linha["base" . $aux . "x"  ]   = "R$ " .   number_format(  $r["valorParcela$aux" ]   , 2 , ","   , ".")    ;

                    }

                }

                if(count($linha) > 0)
                {
                        
                    if      ($filtroPre["nivelTable"] == "0")
                    {
                        $campoFiltro                = " NF.id "                     ;
                        $aliasFiltro                = "idNucleo"                    ;
                        $campoAgrupamento           = " NF.id, NF.nome  "           ;
                        $linha["nomeAgrupamento"]   = "Núcleo"                      ;
                        $linha["nomeTabela"     ]   = "gridTrimestreNucleo"         ;
                    }
                    else if ($filtroPre["nivelTable"] == "1")
                    {
                        $campoFiltro                = " F.id "                      ;
                        $aliasFiltro                = "idFilial"                    ;
                        $campoAgrupamento           = " F.id, CONCAT(F.nome_fantasia, ' - ', E.cidade, '/', E.uf) "     ;
                        $linha["nomeAgrupamento"]   = "Filial"                      ;
                        $linha["nomeTabela"     ]   = "gridTrimestreFilial"         ;
                    }
                    else if ($filtroPre["nivelTable"] == "2")
                    {
                        $campoFiltro                = ""                            ;
                        $aliasFiltro                = ""                            ;
                        $campoAgrupamento           = ""                            ;
//                            $filtroAgrup                = " F.id, F.nome_fantasia " ;
                        $linha["nomeAgrupamento"]   = ""                            ;
//                            $linha["nomeTabela"     ]   = "gridTrimestreFilial"     ;
                    }

                    $filtroPreSub   ["campoFiltro"      ]   = $campoFiltro                                                              ;
                    $filtroPreSub   ["aliasFiltro"      ]   = $aliasFiltro                                                              ;

                    $filtroPreSub   ["nivelTable"       ]   = ((int) $filtroPre["nivelTable"]) + 1                                      ;
                    $filtroPreSub   ["filtroPre"        ]   = $filtroPre["campoFiltro"] . " = '$valorFiltro' "                          ;

                    if(!empty($filtroAnterior))
                    {
                        $filtroPreSub["filtroPre"] .=  $filtroAnterior;
                    }

                    $filtroPreSub   ["agrupamento"      ]   = $campoAgrupamento                                                         ;

                    $linha          ["geral"            ]   = "-"                                                                       ;
                    $linha          ["urlMontaTable"    ]   = "/analiseInadimplencia/getTableTrimestre"                                 ;
                    $linha          ["filtroPre"        ]   = $filtroPreSub["filtroPre"   ]                                             ;
                    $linha          ["filtroPreSub"     ]   = $filtroPreSub                                                             ;
                    $linha          ["tituloTable"      ]   = "Safra " . $linha["agrupamento"] . " por " . $linha["nomeAgrupamento"]    ;

                    $dados          [                   ]   = $linha                                                                    ;

                }

            }
            catch (Exception $ex)
            {
                $erro = $ex->getMessage();
            }
            
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "erro"  => $erro    ,
                                    '$sql'  => $sql  
                                ]
                            );
        
    }
    
    public function actionGetResultadoSafra()
    {
        
        $erro           = "";
        
        $dados          = [];
        
        $filtroPreSub   = [];
        
        $sql    = "SELECT   LPAD    (                                                                                                                       " 
                . "                     MONTH(  P.data_cadastro ),                                                                                          "
                . "                     2,                                                                                                                  "
                . "                     '0'                                                                                                                 "
                . "                 )                                   AS mes              ,                                                               "
                . "         YEAR    (           P.data_cadastro     )   AS ano              ,                                                               "
                . "         Pa.seq                                      AS seqParcela       ,                                                               "
                . "         SUM(           Pa.valor     )               AS valorTotal       ,                                                               "
                . "         SUM( CASE WHEN B.id IS NULL THEN 0                                                                                              "
                . "                                     ELSE Pa.valor                                                                                       "
                . "              END      )                                     AS valorPago                                                                "
                . "FROM             Proposta                                    AS P                                                                        "
                . "INNER    JOIN    Analise_de_Credito                          AS AC           ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id "
                . "INNER    JOIN    Titulo                                      AS T            ON   T.habilitado   AND   T.Proposta_id             =  P.id "
                . "INNER    JOIN    Parcela                                     AS Pa           ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id "
                . "INNER    JOIN    Tabela_Cotacao                              AS TC           ON  TC.habilitado   AND   P.Tabela_id               = TC.id "
                . "INNER    JOIN    Filial                                      AS F            ON   F.habilitado   AND  AC.Filial_id               =  F.id "
                . "INNER    JOIN    Filial_has_Endereco                         AS FhE          ON FhE.habilitado   AND FhE.Filial_id               =  F.id "
                . "INNER    JOIN    Endereco                                    AS E            ON   E.habilitado   AND FhE.Endereco_id             =  E.id "
                . "INNER    JOIN    NucleoFiliais                               AS NF           ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id ";
        
        if(isset($_POST["filtraAnalistas"]) && $_POST["filtraAnalistas"] == 1)
        {
            $sql .= " AND NF.id NOT IN (121,122,125,126,134,135) ";
        }
                
        $sql    .=  "INNER  JOIN    Analista_has_Proposta_has_Status_Proposta   AS APSP         ON APSP.habilitado  AND APSP.Proposta_id            =  P.id "
                .   "LEFT   JOIN    Baixa                                       AS B            ON   B.baixado      AND   B.Parcela_id              = Pa.id "
                .   "LEFT   JOIN    PropostaOmniConfig                          AS POC          ON POC.habilitado   AND POC.Proposta                =  P.id "
                .   "INNER  JOIN    Cliente					AS C		ON   C.habilitado   AND  AC.Cliente_id              =  C.id "
                .   "LEFT   JOIN    Ficha					AS Fi		ON                        C.Pessoa_id               = Fi.Pessoa_id "
                .   "                                                                                               AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10)  "
                .   "WHERE P.habilitado AND POC.id IS NULL AND T.NaturezaTitulo_id = 1                                                                        ";
        
        $filiaisPreDefinidas = Filial::model()->findAllAdmFiliais();
        
        $idsFiliaisPre = [];
        
        foreach ($filiaisPreDefinidas as $filial)
        {
            $idsFiliaisPre[] = $filial->id;
        }
        
        if (!empty($idsFiliaisPre))
        {
            $idsFiliaisPreStr = join(",", $idsFiliaisPre);
        }
        else
        {
            $idsFiliaisPreStr = "-1";
        }
        
        $sql .= " AND F.id IN ($idsFiliaisPreStr) ";
        
        if(isset($_POST["mesesPartida"]) && !empty($_POST["mesesPartida"]))
        {
            
            $mesesFiltro = join(",", $_POST["mesesPartida"]);
            
            $sql    .= " AND CONCAT(YEAR(P.data_cadastro),MONTH(P.data_cadastro)) IN ($mesesFiltro) ";
        }
        
        if(isset($_POST["financeiraSafra"]) && !empty($_POST["financeiraSafra"]))
        {
            
            $financeiraSafra                     = join(",", $_POST["financeiraSafra"])             ;
            
            $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;
            
            $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;
            
        }
        
        if(isset($_POST["modalidadeSafra"]) && !empty($_POST["modalidadeSafra"]))
        {
            
            $modalidades                     = join(",", $_POST["modalidadeSafra"])         ;
            
            $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;
            
            $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;
            
        }      
        
        if(isset($_POST["politicaSafra"]) && !empty($_POST["politicaSafra"]))
        {

            $politicaSafra = join(",", $_POST["politicaSafra"]);

            $sql    .=  " AND EXISTS( "
                    .   "   SELECT * "
                    .   "   FROM Filial_has_PoliticaCredito AS FhP  "
                    .   "   WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                    .   "   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                    .   " )";
            
            $filtroPreSub["politicaSafra"] = $politicaSafra;
        }    
        
        if(isset($_POST["filiaisSafra"]) && !empty($_POST["filiaisSafra"]))
        {
            
            $filiaisSafra                    = join(",", $_POST["filiaisSafra"])    ;
            
            $sql                            .= " AND F.id IN ($filiaisSafra) "      ;
            
            $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;
            
        }
        
        if(isset($_POST["scoreSafra"]) && !empty($_POST["scoreSafra"]))
        {
            
            $scoreSafra                  = join(",", $_POST["scoreSafra"])    ;
            
            $sql                        .= " AND Fi.classe IN ($scoreSafra) "      ;
            
            $filtroPreSub["scoreSafra"]  = $scoreSafra                        ;
            
        }
        
        if(isset($_POST["cidadesSafra"]) && !empty($_POST["cidadesSafra"]))
        {
            
            $cidadesSafra                    = strtoupper(join(",", $_POST["cidadesSafra"]))    ;
            
            $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "       ;
            
            $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                    ;
            
        }
        
        if(isset($_POST["filtraAnalistas"]) && $_POST["filtraAnalistas"] == 1)
        {
            
            $filtroPreSub["filtraAnalistas"]    = 1;
            
            if(isset($_POST["analistasSafra"]) && !empty($_POST["analistasSafra"]))
            {
            
                $analistasSafra                 = strtoupper(join(",", $_POST["analistasSafra"]))  ;

                $sql                            .= " AND APSP.Analista_id IN ($analistasSafra) "       ;

                $filtroPreSub["analistasSafra"]    = $analistasSafra                                    ;
                
            }
            else
            {
                $sql                            .= " AND APSP.id IS NULL "       ;
            }
            
        }
        else 
        {
            
            $filtroPreSub["filtraAnalistas"]    = 0;
        }
        
        $sql    .=  "GROUP BY CONCAT(   YEAR(P.data_cadastro) , MONTH(P.data_cadastro)  )   , Pa.seq "
                .   "ORDER BY           YEAR(P.data_cadastro) , MONTH(P.data_cadastro)      , Pa.seq ";
        
        try
        {
        
            $resultado  = Yii::app()->db->createCommand($sql)->queryAll()   ;
            $mesSafra   = ""                                                ;
            $linha      = []                                                ;

            $vlrTotSafra        = 0;
            $vlrTotSafraInad    = 0;

//            $seqParcela         = 0;
            
            foreach ($resultado as $r)
            {          

                if ($mesSafra !== $r["ano"] . $r["mes"])
                {

                    if(count($linha) > 0)
                    {

                        $perTotalInad                   = ( ($vlrTotSafraInad/$vlrTotSafra) * 100)      ;

                        $percentualInadimplenciaSafra   = number_format($perTotalInad,2,",",".") . "%"  ;

                        $linha["geral"          ]   = $percentualInadimplenciaSafra                 ;
                        
                        if ($filtroPreSub["filtraAnalistas"] == 0) 
                        {
                            $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraNucleo"   ;
                            $linha["filtroPre"      ]   = $mesSafra                                     ;
                            $linha["tituloTable"    ]   = "Safra " . $linha["safra"] . " por Núcleo"    ;
                        }
                        else
                        {
                            $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableAnalistas"     ;
                            $linha["filtroPre"      ]   = $mesSafra                                     ;
                            $linha["tituloTable"    ]   = "Safra " . $linha["safra"] . " por Analista"  ;
                        }
                        $linha["filtroPreSub"   ]   = $filtroPreSub                                 ;
                        
//                        $linha["vencimento"         ]   = $r["anoVencimento"] . $r["mesVencimento"]     ;Núcleo

                        $dados[]                        = $linha                                        ;

                        $linha                          = []                                            ;

                        $vlrTotSafra                    = 0                                             ;
                        $vlrTotSafraInad                = 0                                             ;
                    
//                        $seqParcela                     = 0                                             ;

                    }

                    $mesSafra               = $r["ano"] . $r["mes"]             ;

                    $btnDetalhar            = '<button style="border-radius: 20px" class="btn-success btnDetalharSafra" value="' . $mesSafra . '"> <i class="fa fa-plus"></i></button>' ;

                    $mesExtenso             = $this->getMesExtenso($r["mes"])   ;

                    $linha["detalhar"   ]   = $btnDetalhar                      ;
                    $linha["safra"      ]   = $mesExtenso . "/" . $r["ano"]     ;

                }
                
//                $seqParcela++;
                
                $valorPago = $r["valorPago"];
                
                if($valorPago >= $r["valorTotal"])
                {
                    $valorPago = $r["valorTotal"]; 
                }
                
                $vlrInad                    = $r["valorTotal"] - $valorPago             ;

                $percInad                   = ( ($vlrInad/$r["valorTotal"]) * 100)      ;

                $percentualInadimplencia    = number_format($percInad,2,",",".") . "%"  ;

//                $percentualInadimplencia    =   number_format($percInad         , 2 , "," , ".") . "% (Total: R$ " . 
//                                                number_format($r["valorTotal"]  , 2 , "," , ".") .     "Pago: R$ " .
//                                                number_format($valorPago        , 2 , "," , ".") . ")";

//                $linha[$seqParcela . "x"]   = $percentualInadimplencia                  ;
                $linha[$r["seqParcela"] . "x"]   = $percentualInadimplencia                  ;


                $vlrTotSafra        += $r["valorTotal"] ;
                $vlrTotSafraInad    += $vlrInad         ;

            }

            if(count($linha) > 0)
            {

                $perTotalInad                   = ( ($vlrTotSafraInad/$vlrTotSafra) * 100)      ;

                $percentualInadimplenciaSafra   = number_format($perTotalInad,2,",",".") . "%"  ;

                $linha["geral"          ]   = $percentualInadimplenciaSafra                 ;
                
/*                $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraNucleo"   ;
                $linha["filtroPre"      ]   = $mesSafra                                     ;
                $linha["filtroPreSub"   ]   = $filtroPreSub                                 ;
                $linha["tituloTable"    ]   = "Safra " . $linha["safra"] . " por Núcleo"    ;*/
                        
                if ($filtroPreSub["filtraAnalistas"] == 0) 
                {
                    $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraNucleo"   ;
                    $linha["filtroPre"      ]   = $mesSafra                                     ;
                    $linha["tituloTable"    ]   = "Safra " . $linha["safra"] . " por Núcleo"    ;
                }
                else
                {
                    $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableAnalistas"     ;
                    $linha["filtroPre"      ]   = $mesSafra                                     ;
                    $linha["tituloTable"    ]   = "Safra " . $linha["safra"] . " por Analista"  ;
                }
                $linha["filtroPreSub"   ]   = $filtroPreSub                                 ;
                        
//                $linha["vencimento"         ]   = $r["anoVencimento"] . $r["mesVencimento"]     ;
                
                $dados[]                        = $linha                                        ;

            }
            
        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "query" => $sql     ,
                                    "erro"  => $erro
                                ]
                            );
        
    }
    
    public function actionGetSafraNucleo()
    {
        
        $erro           = "";
        
        $dados          = [];
        
        $filtroPreSub   = [];
        
        $filtroPreMes   = "";
        
        $sql    = "SELECT   NF.id                                       AS idNucleo         ,                                                               "
                . "         NF.nome                                     AS nomeNucleo       ,                                                               "
                . "         Pa.seq                                      AS seqParcela       ,                                                               "
//              . "         MONTH(Pa.vencimento)                        AS mesVencimento    ,                                                               "
//              . "         YEAR(Pa.vencimento)                         AS anoVencimento    ,                                                               "
                . "         SUM(           Pa.valor     )               AS valorTotal       ,                                                               "
                . "         SUM( CASE WHEN B.id IS NULL THEN 0                                                                                              "
                . "                                     ELSE Pa.valor                                                                                       "
                . "              END      )                                     AS valorPago                                                                "
                . "FROM             Proposta                                    AS P                                                                        "
                . "INNER    JOIN    Analise_de_Credito                          AS AC           ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id "
                . "INNER    JOIN    Titulo                                      AS T            ON   T.habilitado   AND   T.Proposta_id             =  P.id "
                . "INNER    JOIN    Parcela                                     AS Pa           ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id "
                . "INNER    JOIN    Tabela_Cotacao                              AS TC           ON  TC.habilitado   AND   P.Tabela_id               = TC.id "
                . "INNER    JOIN    Filial                                      AS F            ON   F.habilitado   AND  AC.Filial_id               =  F.id "
                . "INNER    JOIN    Filial_has_Endereco                         AS FhE          ON FhE.habilitado   AND FhE.Filial_id               =  F.id "
                . "INNER    JOIN    Endereco                                    AS E            ON   E.habilitado   AND FhE.Endereco_id             =  E.id "
                . "INNER    JOIN    NucleoFiliais                               AS NF           ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id "
                . "INNER    JOIN    Analista_has_Proposta_has_Status_Proposta   AS APSP         ON APSP.habilitado  AND APSP.Proposta_id            =  P.id "
                . "LEFT     JOIN    Baixa                                       AS B            ON   B.baixado      AND   B.Parcela_id              = Pa.id "
                . "LEFT     JOIN    PropostaOmniConfig                          AS POC          ON POC.habilitado   AND POC.Proposta                =  P.id "
                . "INNER    JOIN    Cliente                                     AS C            ON   C.habilitado   AND  AC.Cliente_id              =  C.id "
                . "LEFT     JOIN    Ficha                                       AS Fi           ON                        C.Pessoa_id               = Fi.Pessoa_id "
                . "                                                                                                 AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10) "
                . "WHERE P.habilitado AND POC.id IS NULL AND T.NaturezaTitulo_id = 1                                                                ";
        
        $filiaisPreDefinidas = Filial::model()->findAllAdmFiliais();
        
        $idsFiliaisPre = [];
        
        foreach ($filiaisPreDefinidas as $filial)
        {
            $idsFiliaisPre[] = $filial->id;
        }
        
        if (!empty($idsFiliaisPre))
        {
            $idsFiliaisPreStr = join(",", $idsFiliaisPre);
        }
        else
        {
            $idsFiliaisPreStr = "-1";
        }
        
        $sql .= " AND F.id IN ($idsFiliaisPreStr) ";
                
        if(isset($_POST["filtroPre"]))
        {
            
            $filtroPreMes = substr($_POST["filtroPre"], 0, 4) . '-' . substr($_POST["filtroPre"], 4, 2);
            
            $sql .= " AND LEFT(P.data_cadastro,7) = '$filtroPreMes' ";
        }
        
        if(isset($_POST["filtroPreSub"]))
        {
                
            $filtro = $_POST["filtroPreSub"];
            
            if(isset($filtro["financeiraSafra"]) && !empty($filtro["financeiraSafra"]))
            {
                
                $financeiraSafra                     = $filtro["financeiraSafra"]                       ;

                $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;
            
                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;
            
            }
        
            if(isset($filtro["modalidadeSafra"]) && !empty($filtro["modalidadeSafra"]))
            {

//                $modalidades                     = join(",", $filtro["modalidadeSafra"])        ;
//                
                $modalidades                     = $filtro["modalidadeSafra"]                   ;

                $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }
        
            if(isset($filtro["politicaSafra"]) && !empty($filtro["politicaSafra"]))
            {

//                $politicaSafra = join(",", $filtro["politicaSafra"]);
                
                $politicaSafra = $filtro["politicaSafra"];

                $sql    .=  " AND EXISTS( "
                        .   "   SELECT * "
                        .   "   FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "   WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                        .   "   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   " )";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }
        
            if(isset($filtro["filiaisSafra"]) && !empty($filtro["filiaisSafra"]))
            {

//                $filiaisSafra                    = join(",", $filtro["filiaisSafra"])   ;
                
                $filiaisSafra                    = $filtro["filiaisSafra"]              ;

                $sql                            .= " AND F.id IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }
            
            if(isset($filtro["scoreSafra"      ]) && !empty($filtro["scoreSafra"      ]))
            {
                
                $scoreSafra = $filtro["scoreSafra"];
                
                $sql                            .= " AND Fi.classe IN ($scoreSafra) "      ;

                $filtroPreSub["scoreSafra"]    = $scoreSafra                        ;

            }

            if(isset($filtro["cidadesSafra"]) && !empty($filtro["cidadesSafra"]))
            {

                $cidadesSafra                    = strtoupper($filtro["cidadesSafra"])          ;

                $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "   ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                ;

            }
        
            if(isset($filtro["filtraAnalistas"]) && $filtro["filtraAnalistas"] == 1)
            {

                $filtroPreSub["filtraAnalistas"]    = 1;

                if(isset($filtro["analistasSafra"]) && !empty($filtro["analistasSafra"]))
                {

//                    $analistasSafra                 = strtoupper(join(",", $filtro["analistasSafra"]))  ;
                    $analistasSafra                 = $filtro["analistasSafra"]  ;

                    $sql                            .= " AND APSP.Analista_id IN ($analistasSafra) "       ;

                    $filtroPreSub["analistasSafra"]    = $analistasSafra                                    ;

                }
                else
                {
                    $sql                            .= " AND APSP.id IS NULL "       ;
                }

            }
            else
            {
                $filtroPreSub["filtraAnalistas"]    = 0;
            }
            
        }        
        
        $sql    .=  " GROUP BY NF.id    ,   NF.nome   , Pa.seq  "
                .   " ORDER BY              NF.nome   , Pa.seq  ";
        
        try
        {
        
            $resultado          = Yii::app()->db->createCommand($sql)->queryAll()   ;
            $nucleoID           = 0                                                 ;
            $linha              = []                                                ;

            $vlrTotNucleo       = 0                                                 ;
            $vlrTotNucleoInad   = 0                                                 ;
            
//          $seqParcela         = 0                                                 ;

            foreach ($resultado as $r)
            {
                
                if ($nucleoID !== $r["idNucleo"])
                {

                    if(count($linha) > 0)
                    {

                        $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)            ;

                        $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"          ;

//                      $linha["mesVencimento"  ]   = $r["mesVencimento"]                                   ;
//                      $linha["anoVencimento"  ]   = $r["anoVencimento"]                                   ;
                        $linha["geral"          ]   = $percentualInadimplenciaNucleo                        ;
                        $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraFilial"           ;
                        $linha["filtroPre"      ]   = ["nucleoID" => $nucleoID,"safra" => $filtroPreMes]    ;
                        $linha["filtroPreSub"   ]   = $filtroPreSub                                         ;
                        $linha["tituloTable"    ]   = "Safra do Núcleo " . $nomeNucleo . " por Filial"      ;

                        $dados[]                        = $linha                                                ;

                        $linha                          = []                                                    ;

                        $vlrTotNucleo                   = 0                                                     ;
                        $vlrTotNucleoInad               = 0                                                     ;
                        
//                      $seqParcela                     = 0                                                     ;

                    }

                    $nucleoID   =       $r["idNucleo"     ]     ;
                    $nomeNucleo = trim( $r["nomeNucleo"   ] )   ;

                    $btnDetalhar = "<button style='border-radius: 20px' class='btn-success btnDetalharNucleo' value=$nucleoID> <i class='fa fa-plus'></i></button>";

                    $linha["detalhar"   ]   = $btnDetalhar  ;
                    $linha["nucleoID"   ]   = $nucleoID     ;
                    $linha["nucleo"     ]   = $nomeNucleo   ;
                    
                }
                
//              $seqParcela++;
                
                $valorPago = $r["valorPago"];
                
                if($valorPago >= $r["valorTotal"])
                {
                    $valorPago = $r["valorTotal"]; 
                }

                $vlrInad                        = $r["valorTotal"] - $valorPago             ;

                $percInad                       = ( ($vlrInad/$r["valorTotal"]) * 100)      ;

                $percentualInadimplencia        = number_format($percInad,2,",",".") . "%"  ;

                $linha[$r["seqParcela"] . "x"]  = $percentualInadimplencia                  ;


                $vlrTotNucleo       += $r["valorTotal"] ;
                $vlrTotNucleoInad   += $vlrInad         ;
                
            }

            if(count($linha) > 0)
            {

                $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)            ;

                $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"          ;

//              $linha["mesVencimento"  ]   = $r["mesVencimento"]                                   ;
//              $linha["anoVencimento"  ]   = $r["anoVencimento"]                                   ;
                $linha["geral"          ]   = $percentualInadimplenciaNucleo                        ;
                $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraFilial"           ;
                $linha["filtroPre"      ]   = ["nucleoID" => $nucleoID,"safra" => $filtroPreMes]    ;
                $linha["filtroPreSub"   ]   = $filtroPreSub                                         ;
                $linha["tituloTable"    ]   = "Safra do Núcleo " . $nomeNucleo . " por Filial"      ;
                
                $dados[]                        = $linha                                                ;

            }
            
        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "query" => $sql     ,
                                    "erro"  => $erro
                                ]
                            );
        
    }
    
    public function actionGetResultadoAnalistas()
    {
        
        $erro           = "";
        
        $dados          = [];
        
        $filtroPreSub   = [];
        
        $sql    = "SELECT       U.nome_utilizador	AS usuario      ,
                                U.id                    AS idUsuario    ,
                                Pa.seq                  AS seqParcela   ,                                                                
                        SUM(    Pa.valor            )   AS valorTotal   ,                                                                
                        SUM( 
                                CASE WHEN B.id IS NULL  THEN 0
                                                        ELSE Pa.valor                                                                                             
                                                        END      
                            )                           AS valorPago                                                                
                    FROM    		Proposta                            		AS P                                                                        
                    INNER   JOIN    Analise_de_Credito                  		AS AC   ON   AC.habilitado  AND    P.Analise_de_Credito_id  = AC.id 
                    INNER   JOIN    Titulo                              		AS T    ON    T.habilitado  AND    T.Proposta_id            =  P.id 
                    INNER   JOIN    Parcela                             		AS Pa   ON   Pa.habilitado  AND   Pa.Titulo_id              =  T.id 
                    INNER   JOIN    Tabela_Cotacao                      		AS TC   ON   TC.habilitado  AND    P.Tabela_id              = TC.id 
                    INNER   JOIN    Filial                              		AS F    ON    F.habilitado  AND   AC.Filial_id              =  F.id 
                    INNER   JOIN    Filial_has_Endereco                 		AS FhE  ON  FhE.habilitado  AND  FhE.Filial_id              =  F.id 
                    INNER   JOIN    Endereco                            		AS E    ON    E.habilitado  AND  FhE.Endereco_id            =  E.id 
                    INNER   JOIN    NucleoFiliais                       		AS NF   ON   NF.habilitado  AND    F.NucleoFiliais_id       = NF.id     
                                                                                                                    AND   NF.id                             NOT IN (121,122,125,126,134,135)
                    INNER   JOIN    Analista_has_Proposta_has_Status_Proposta           AS APSP	ON APSP.habilitado  AND APSP.Proposta_id            =  P.id
                    INNER   JOIN    Usuario						AS U	ON    U.habilitado  AND APSP.Analista_id            =  U.id
                    LEFT    JOIN    Baixa                               		AS B    ON    B.baixado     AND    B.Parcela_id             = Pa.id 
                    LEFT    JOIN    PropostaOmniConfig                  		AS POC  ON  POC.habilitado  AND  POC.Proposta               =  P.id 
                    INNER   JOIN    Cliente                                             AS C    ON   C.habilitado   AND  AC.Cliente_id              =  C.id 
                    LEFT    JOIN    Ficha                                               AS Fi   ON                        C.Pessoa_id               = Fi.Pessoa_id 
                                                                                                                    AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10)  
                    WHERE P.habilitado AND POC.id IS NULL AND T.NaturezaTitulo_id = 1                                                                ";
        
        $filiaisPreDefinidas = Filial::model()->findAllAdmFiliais();
        
        $idsFiliaisPre = [];
        
        foreach ($filiaisPreDefinidas as $filial)
        {
            $idsFiliaisPre[] = $filial->id;
        }
        
        if (!empty($idsFiliaisPre))
        {
            $idsFiliaisPreStr = join(",", $idsFiliaisPre);
        }
        else
        {
            $idsFiliaisPreStr = "-1";
        }
        
        $sql .= " AND F.id IN ($idsFiliaisPreStr) ";
                
        if(isset($_POST["filtroPre"]))
        {
            
            $filtroPreMes = substr($_POST["filtroPre"], 0, 4) . '-' . substr($_POST["filtroPre"], 4, 2);
            
            $sql .= " AND LEFT(P.data_cadastro,7) = '$filtroPreMes' ";
        }
        
        if(isset($_POST["filtroPreSub"]))
        {
                
            $filtro = $_POST["filtroPreSub"];
            
            if(isset($filtro["financeiraSafra"]) && !empty($filtro["financeiraSafra"]))
            {
                
                $financeiraSafra                     = $filtro["financeiraSafra"]                       ;

                $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;
            
                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;
            
            }
        
            if(isset($filtro["modalidadeSafra"]) && !empty($filtro["modalidadeSafra"]))
            {

//                $modalidades                     = join(",", $filtro["modalidadeSafra"])        ;
//                
                $modalidades                     = $filtro["modalidadeSafra"]                   ;

                $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }
        
            if(isset($filtro["politicaSafra"]) && !empty($filtro["politicaSafra"]))
            {

//                $politicaSafra = join(",", $filtro["politicaSafra"]);
                
                $politicaSafra = $filtro["politicaSafra"];

                $sql    .=  " AND EXISTS( "
                        .   "   SELECT * "
                        .   "   FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "   WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                        .   "   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   " )";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }
        
            if(isset($filtro["filiaisSafra"]) && !empty($filtro["filiaisSafra"]))
            {

//                $filiaisSafra                    = join(",", $filtro["filiaisSafra"])   ;
                
                $filiaisSafra                    = $filtro["filiaisSafra"]              ;

                $sql                            .= " AND F.id IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }
            
            if(isset($filtroPre["scoreSafra"      ]) && !empty($filtroPre["scoreSafra"      ]))
            {
                
                $scoreSafra = $filtroPre["scoreSafra"];
                
                $sql                            .= " AND `Fi.classe` IN ($scoreSafra) "      ;

                $filtroPreSub["scoreSafra"]    = $scoreSafra                        ;

            }

            if(isset($filtro["cidadesSafra"]) && !empty($filtro["cidadesSafra"]))
            {

                $cidadesSafra                    = strtoupper($filtro["cidadesSafra"])          ;

                $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "   ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                ;

            }
        
            if(isset($filtro["filtraAnalistas"]) && $filtro["filtraAnalistas"] == 1)
            {

                $filtroPreSub["filtraAnalistas"]    = 1;

                if(isset($filtro["analistasSafra"]) && !empty($filtro["analistasSafra"]))
                {

//                    $analistasSafra                 = strtoupper(join(",", $filtro["analistasSafra"]))  ;
                    $analistasSafra                 = $filtro["analistasSafra"]  ;

                    $sql                            .= " AND APSP.Analista_id IN ($analistasSafra) "       ;

                    $filtroPreSub["analistasSafra"]    = $analistasSafra                                    ;

                }
                else
                {
                    $sql                            .= " AND APSP.id IS NULL "       ;
                }

            }
            else
            {
                $filtroPreSub["filtraAnalistas"]    = 0;
            }
            
        }   
        
        $sql    .=  "GROUP BY  U.nome_utilizador,   U.id, Pa.seq "
                .   "ORDER BY  U.nome_utilizador,   U.id, Pa.seq ";
        
        try
        {
        
            $resultado  = Yii::app()->db->createCommand($sql)->queryAll()   ;
//            $mesSafra   = ""                                                ;
            $idUsuario  = ""                                                ;
            $linha      = []                                                ;

            $vlrTotSafra        = 0;
            $vlrTotSafraInad    = 0;

//            $seqParcela         = 0;
            
            foreach ($resultado as $r)
            {          

                if ($idUsuario !== $r["idUsuario"])
                {

                    if(count($linha) > 0)
                    {

                        $perTotalInad                   = ( ($vlrTotSafraInad/$vlrTotSafra) * 100)      ;

                        $percentualInadimplenciaSafra   = number_format($perTotalInad,2,",",".") . "%"  ;

                        $linha["geral"          ]   = $percentualInadimplenciaSafra                     ;
                        $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableAnalistasNucleo"   ;
//                        $linha["filtroPre"      ]   = $mesSafra                                         ;
                        $linha["filtroPre"      ]   = $idUsuario                                         ;
                        $linha["filtroPreSub"   ]   = $filtroPreSub                                     ;
                        $linha["tituloTable"    ]   = "Analista " . $linha["analista"] . " por Núcleo"     ;
                        
//                        $linha["vencimento"         ]   = $r["anoVencimento"] . $r["mesVencimento"]     ;

                        $dados[]                        = $linha                                        ;

                        $linha                          = []                                            ;

                        $vlrTotSafra                    = 0                                             ;
                        $vlrTotSafraInad                = 0                                             ;

//                        $seqParcela                     = 0                                             ;

                    }

                    $idUsuario              = $r["idUsuario"]                   ;

                    $btnDetalhar            = '<button style="border-radius: 20px" class="btn-success btnDetalharSafra" value="' . $r["idUsuario"] . '"> <i class="fa fa-plus"></i></button>' ;

//                    $mesExtenso             = $this->getMesExtenso($r["mes"])   ;

                    $linha["detalhar"   ]   = $btnDetalhar                      ;
//                    $linha["safra"      ]   = $mesExtenso . "/" . $r["ano"]     ;
                    $linha["analista"   ]   = $r["usuario"]                     ;

                }
                
//                $seqParcela++;
                
                $valorPago = $r["valorPago"];
                
                if($valorPago >= $r["valorTotal"])
                {
                    $valorPago = $r["valorTotal"]; 
                }
                
                $vlrInad                    = $r["valorTotal"] - $valorPago             ;

                $percInad                   = ( ($vlrInad/$r["valorTotal"]) * 100)      ;

                $percentualInadimplencia    = number_format($percInad,2,",",".") . "%"  ;

//                $percentualInadimplencia    =   number_format($percInad         , 2 , "," , ".") . "% (Total: R$ " . 
//                                                number_format($r["valorTotal"]  , 2 , "," , ".") .     "Pago: R$ " .
//                                                number_format($valorPago        , 2 , "," , ".") . ")";

//                $linha[$seqParcela . "x"]   = $percentualInadimplencia                  ;
                $linha[$r["seqParcela"] . "x"]   = $percentualInadimplencia                  ;


                $vlrTotSafra        += $r["valorTotal"] ;
                $vlrTotSafraInad    += $vlrInad         ;

            }

            if(count($linha) > 0)
            {

                $perTotalInad                   = ( ($vlrTotSafraInad/$vlrTotSafra) * 100)      ;

                $percentualInadimplenciaSafra   = number_format($perTotalInad,2,",",".") . "%"  ;

                $linha["geral"          ]   = $percentualInadimplenciaSafra                     ;
                $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableAnalistasNucleo"   ;
//                $linha["filtroPre"      ]   = $mesSafra                                         ;
                $linha["filtroPre"      ]   = $idUsuario                                         ;
                $linha["filtroPreSub"   ]   = $filtroPreSub                                     ;
                $linha["tituloTable"    ]   = "Analista " . $linha["analista"] . " por Núcleo"        ;
                        
//                $linha["vencimento"         ]   = $r["anoVencimento"] . $r["mesVencimento"]     ;
                
                $dados[]                        = $linha                                        ;

            }
            
        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "query" => $sql     ,
                                    "erro"  => $erro
                                ]
                            );
        
    }
    
    /*public function actionGetAnalistasNucleo()
    {
        
        $erro           = "";
        
        $dados          = [];
        
        $filtroPreSub   = [];
        
        $filtroPreMes   = "";
        
        $sql    = "SELECT   NF.id                                       AS idNucleo         ,                                                       "
                . "         NF.nome                                     AS nomeNucleo       ,                                                       "
                . "         Pa.seq                                      AS seqParcela       ,                                                       "
//              . "         MONTH(Pa.vencimento)                        AS mesVencimento    ,                                                       "
//              . "         YEAR(Pa.vencimento)                         AS anoVencimento    ,                                                       "
                . "         SUM(           Pa.valor     )               AS valorTotal       ,                                                       "
                . "         SUM( CASE WHEN B.id IS NULL THEN 0                                                                                      "
                . "                                     ELSE Pa.valor                                                                               "
                . "              END      )                             AS valorPago                                                                "
                . "FROM             Proposta                            AS P                                                                        "
                . "INNER    JOIN    Analise_de_Credito                  AS AC           ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id "
                . "INNER    JOIN    Titulo                              AS T            ON   T.habilitado   AND   T.Proposta_id             =  P.id "
                . "INNER    JOIN    Parcela                             AS Pa           ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id "
                . "INNER    JOIN    Tabela_Cotacao                      AS TC           ON  TC.habilitado   AND   P.Tabela_id               = TC.id "
                . "INNER    JOIN    Filial                              AS F            ON   F.habilitado   AND  AC.Filial_id               =  F.id "
                . "INNER    JOIN    Filial_has_Endereco                 AS FhE          ON FhE.habilitado   AND FhE.Filial_id               =  F.id "
                . "INNER    JOIN    Endereco                            AS E            ON   E.habilitado   AND FhE.Endereco_id             =  E.id "
                . "INNER    JOIN    NucleoFiliais                               AS NF   ON  NF.habilitado   AND   F.NucleoFiliais_id        = NF.id "
                . "INNER    JOIN    Analista_has_Proposta_has_Status_Proposta   AS APSP	ON APSP.habilitado  AND APSP.Proposta_id            =  P.id "
                . "LEFT     JOIN    Baixa                                       AS B    ON   B.baixado      AND   B.Parcela_id              = Pa.id "
                . "LEFT     JOIN    PropostaOmniConfig                          AS POC  ON POC.habilitado   AND POC.Proposta                =  P.id "
                . "WHERE P.habilitado AND POC.id IS NULL AND T.NaturezaTitulo_id = 1                                                                ";
        
        if(isset($_POST["filtroPre"]))
        {
            
            $filtroPre = $_POST["filtroPre"];
            
            $sql .= " AND APSP.Analista_id = '$filtroPre' ";
        }
        
        if(isset($_POST["filtroPreSub"]))
        {
                
            $filtro = $_POST["filtroPreSub"];
            
            if(isset($filtro["financeiraSafra"]) && !empty($filtro["financeiraSafra"]))
            {
                
                $financeiraSafra                     = $filtro["financeiraSafra"]                       ;

                $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;
            
                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;
            
            }
        
            if(isset($filtro["modalidadeSafra"]) && !empty($filtro["modalidadeSafra"]))
            {

//                $modalidades                     = join(",", $filtro["modalidadeSafra"])        ;
//                
                $modalidades                     = $filtro["modalidadeSafra"]                   ;

                $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }
        
            if(isset($filtro["politicaSafra"]) && !empty($filtro["politicaSafra"]))
            {

//                $politicaSafra = join(",", $filtro["politicaSafra"]);
                
                $politicaSafra = $filtro["politicaSafra"];

                $sql    .=  " AND EXISTS( "
                        .   "   SELECT * "
                        .   "   FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "   WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                        .   "   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   " )";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }
        
            if(isset($filtro["filiaisSafra"]) && !empty($filtro["filiaisSafra"]))
            {

//                $filiaisSafra                    = join(",", $filtro["filiaisSafra"])   ;
                
                $filiaisSafra                    = $filtro["filiaisSafra"]              ;

                $sql                            .= " AND F.id IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }

            if(isset($filtro["cidadesSafra"]) && !empty($filtro["cidadesSafra"]))
            {

                $cidadesSafra                    = strtoupper($filtro["cidadesSafra"])          ;

                $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "   ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                ;

            }
            
        }        
        
        $sql    .=  " GROUP BY NF.id    ,   NF.nome   , Pa.seq  "
                .   " ORDER BY              NF.nome   , Pa.seq  ";
        
        try
        {
        
            $resultado          = Yii::app()->db->createCommand($sql)->queryAll()   ;
            $nucleoID           = 0                                                 ;
            $linha              = []                                                ;

            $vlrTotNucleo       = 0                                                 ;
            $vlrTotNucleoInad   = 0                                                 ;
            
//          $seqParcela         = 0                                                 ;

            foreach ($resultado as $r)
            {
                
                if ($nucleoID !== $r["idNucleo"])
                {

                    if(count($linha) > 0)
                    {

                        $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)            ;

                        $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"          ;

//                      $linha["mesVencimento"  ]   = $r["mesVencimento"]                                   ;
//                      $linha["anoVencimento"  ]   = $r["anoVencimento"]                                   ;
                        $linha["geral"          ]   = $percentualInadimplenciaNucleo                        ;
                        $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraFilial"           ;
                        $linha["filtroPre"      ]   = ["nucleoID" => $nucleoID,"safra" => $filtroPreMes]    ;
                        $linha["filtroPreSub"   ]   = $filtroPreSub                                         ;
                        $linha["tituloTable"    ]   = "Safra do Núcleo " . $nomeNucleo . " por Filial"      ;

                        $dados[]                        = $linha                                                ;

                        $linha                          = []                                                    ;

                        $vlrTotNucleo                   = 0                                                     ;
                        $vlrTotNucleoInad               = 0                                                     ;
                        
//                      $seqParcela                     = 0                                                     ;

                    }

                    $nucleoID   =       $r["idNucleo"     ]     ;
                    $nomeNucleo = trim( $r["nomeNucleo"   ] )   ;

                    $btnDetalhar = "<button style='border-radius: 20px' class='btn-success btnDetalharNucleo' value=$nucleoID> <i class='fa fa-plus'></i></button>";

                    $linha["detalhar"   ]   = $btnDetalhar  ;
                    $linha["nucleoID"   ]   = $nucleoID     ;
                    $linha["nucleo"     ]   = $nomeNucleo   ;
                    
                }
                
//              $seqParcela++;
                
                $valorPago = $r["valorPago"];
                
                if($valorPago >= $r["valorTotal"])
                {
                    $valorPago = $r["valorTotal"]; 
                }

                $vlrInad                        = $r["valorTotal"] - $valorPago             ;

                $percInad                       = ( ($vlrInad/$r["valorTotal"]) * 100)      ;

                $percentualInadimplencia        = number_format($percInad,2,",",".") . "%"  ;

                $linha[$r["seqParcela"] . "x"]  = $percentualInadimplencia                  ;


                $vlrTotNucleo       += $r["valorTotal"] ;
                $vlrTotNucleoInad   += $vlrInad         ;
                
            }

            if(count($linha) > 0)
            {

                $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)            ;

                $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"          ;

//              $linha["mesVencimento"  ]   = $r["mesVencimento"]                                   ;
//              $linha["anoVencimento"  ]   = $r["anoVencimento"]                                   ;
                $linha["geral"          ]   = $percentualInadimplenciaNucleo                        ;
                $linha["urlMontaTable"  ]   = "/analiseInadimplencia/getTableSafraFilial"           ;
                $linha["filtroPre"      ]   = ["nucleoID" => $nucleoID,"safra" => $filtroPreMes]    ;
                $linha["filtroPreSub"   ]   = $filtroPreSub                                         ;
                $linha["tituloTable"    ]   = "Safra do Núcleo " . $nomeNucleo . " por Filial"      ;
                
                $dados[]                        = $linha                                                ;

            }
            
        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "query" => $sql     ,
                                    "erro"  => $erro
                                ]
                            );
        
    }*/
    
    public function actionGetSafraFilial()
    {
        
        $erro           = "";
        
        $dados          = [];
        
        $filtroPreSub   = [];
        
        $sql    = "SELECT    F.id                                       AS idFilial         ,                                                       "
                . "          F.nome_fantasia                            AS nomeFantasia     ,                                                       "
                . "          E.cidade                                   AS nomeCidade       ,                                                       "
                . "          E.uf                                       AS ufCidade         ,                                                       "
                . "         Pa.seq                                      AS seqParcela       ,                                                       "
//              . "         MONTH(Pa.vencimento)                        AS mesVencimento    ,                                                       "
//              . "         YEAR(Pa.vencimento)                         AS anoVencimento    ,                                                       "
                . "         SUM(           Pa.valor     )               AS valorTotal       ,                                                       "
                . "         SUM( CASE WHEN B.id IS NULL THEN 0                                                                                      "
                . "                                     ELSE Pa.valor                                                                               "
                . "              END      )                                     AS valorPago                                                                "
                . "FROM             Proposta                                    AS P                                                                        "
                . "INNER    JOIN    Analise_de_Credito                          AS AC           ON  AC.habilitado   AND   P.Analise_de_Credito_id   = AC.id "
                . "INNER    JOIN    Titulo                                      AS T            ON   T.habilitado   AND   T.Proposta_id             =  P.id "
                . "INNER    JOIN    Parcela                                     AS Pa           ON  Pa.habilitado   AND  Pa.Titulo_id               =  T.id "
                . "INNER    JOIN    Tabela_Cotacao                              AS TC           ON  TC.habilitado   AND   P.Tabela_id               = TC.id "
                . "INNER    JOIN    Filial                                      AS F            ON   F.habilitado   AND  AC.Filial_id               =  F.id "
                . "INNER    JOIN    Filial_has_Endereco                         AS FhE          ON FhE.habilitado   AND FhE.Filial_id               =  F.id "
                . "INNER    JOIN    Endereco                                    AS E            ON   E.habilitado   AND FhE.Endereco_id             =  E.id "
                . "INNER    JOIN    Analista_has_Proposta_has_Status_Proposta   AS APSP         ON APSP.habilitado  AND APSP.Proposta_id            =  P.id "
                . "LEFT     JOIN    Baixa                                       AS B            ON   B.baixado      AND   B.Parcela_id              = Pa.id "
                . "LEFT     JOIN    PropostaOmniConfig                          AS POC          ON POC.habilitado   AND POC.Proposta                =  P.id "
                . "INNER    JOIN    Cliente                                     AS C            ON   C.habilitado   AND  AC.Cliente_id              =  C.id "
                . "LEFT     JOIN    Ficha                                       AS Fi           ON                        C.Pessoa_id               = Fi.Pessoa_id "
                . "                                                                                                 AND   LEFT(P.data_cadastro,10) BETWEEN LEFT(Fi.data_cadastro,10) AND LEFT(Fi.data_expiracao,10) "
                . "WHERE P.habilitado AND POC.id IS NULL AND T.NaturezaTitulo_id = 1                                                                ";
        
        if(isset($_POST["filtroPre"]))
        {
            
            $nucleosID   = $_POST["filtroPre"]["nucleoID"   ]                   ;
            $safra       = $_POST["filtroPre"]["safra"      ]                   ;
            
            $sql        .= " AND F.NucleoFiliais_id IN (" . $nucleosID . ") "
                        .  " AND LEFT(P.data_cadastro,7) = '$safra'         "   ;
        }
        
        if(isset($_POST["filtroPreSub"]))
        {
                
            $filtro = $_POST["filtroPreSub"];
            
            if(isset($filtro["financeiraSafra"]) && !empty($filtro["financeiraSafra"]))
            {
                
                $financeiraSafra                     = $filtro["financeiraSafra"]                       ;

                $sql                                .= " AND P.Financeira_id IN ($financeiraSafra) "    ;
            
                $filtroPreSub["financeiraSafra"]     = $financeiraSafra                                 ;
            
            }
        
            if(isset($filtro["modalidadeSafra"]) && !empty($filtro["modalidadeSafra"]))
            {
//                
                $modalidades                     = $filtro["modalidadeSafra"]                   ;

                $sql                            .= " AND TC.ModalidadeId IN ($modalidades) "    ;

                $filtroPreSub["modalidadeSafra"] = $modalidades                                 ;

            }
        
            if(isset($filtro["politicaSafra"]) && !empty($filtro["politicaSafra"]))
            {
                
                $politicaSafra = $filtro["politicaSafra"];

                $sql    .=  " AND EXISTS( "
                        .   "   SELECT * "
                        .   "   FROM Filial_has_PoliticaCredito AS FhP  "
                        .   "   WHERE FhP.Filial_id = AC.Filial_id AND (P.data_cadastro >= FhP.dataInicio AND (FhP.dataFim IS NULL OR P.data_cadastro <= FhP.dataFim)) "
                        .   "   AND FhP.PoliticaCredito_id IN ($politicaSafra) "
                        .   " )";

                $filtroPreSub["politicaSafra"] = $politicaSafra;
            }
        
            if(isset($filtro["filiaisSafra"]) && !empty($filtro["filiaisSafra"]))
            {
                
                $filiaisSafra                    = $filtro["filiaisSafra"]              ;

                $sql                            .= " AND F.id IN ($filiaisSafra) "      ;

                $filtroPreSub["filiaisSafra"]    = $filiaisSafra                        ;

            }
            
            if(isset($filtro["scoreSafra"      ]) && !empty($filtro["scoreSafra"      ]))
            {
                
                $scoreSafra = $filtro["scoreSafra"];
                
                $sql                            .= " AND Fi.classe IN ($scoreSafra) "      ;

                $filtroPreSub["scoreSafra"]    = $scoreSafra                        ;

            }

            if(isset($filtro["cidadesSafra"]) && !empty($filtro["cidadesSafra"]))
            {

                $cidadesSafra                    = strtoupper($filtro["cidadesSafra"])          ;

                $sql                            .= " AND UPPER(E.cidade) IN ($cidadesSafra) "   ;

                $filtroPreSub["cidadesSafra"]    = $cidadesSafra                                ;

            }
        
            if(isset($filtro["filtraAnalistas"]) && $filtro["filtraAnalistas"] == 1)
            {

                $filtroPreSub["filtraAnalistas"]    = 1;

                if(isset($filtro["analistasSafra"]) && !empty($filtro["analistasSafra"]))
                {

//                    $analistasSafra                 = strtoupper(join(",", $filtro["analistasSafra"]))  ;
                    $analistasSafra                 = $filtro["analistasSafra"]  ;

                    $sql                            .= " AND APSP.Analista_id IN ($analistasSafra) "       ;

                    $filtroPreSub["analistasSafra"]    = $analistasSafra                                    ;

                }
                else
                {
                    $sql                            .= " AND APSP.id IS NULL "       ;
                }

            }
            else
            {
                $filtroPreSub["filtraAnalistas"]    = 0;
            }
            
        }        
        
        $sql    .=  " GROUP BY F.id ,   F.nome_fantasia  , E.cidade  , E.uf  , Pa.seq    "
                .   " ORDER BY          F.nome_fantasia  , E.cidade  , E.uf  , Pa.seq    ";
        
        try
        {
        
            $resultado          = Yii::app()->db->createCommand($sql)->queryAll()   ;
            $filialID           = 0                                                 ;
            $linha              = []                                                ;

            $vlrTotNucleo       = 0                                                 ;
            $vlrTotNucleoInad   = 0                                                 ;
            
//            $seqParcela         = 0                                                 ;

            foreach ($resultado as $r)
            {
                
                if ($filialID !== $r["idFilial"])
                {

                    if(count($linha) > 0)
                    {

                        $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)        ;

                        $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"      ;

//                      $linha["mesVencimento"      ]   = $r["mesVencimento"]                               ;
//                      $linha["anoVencimento"      ]   = $r["anoVencimento"]                               ;
                        $linha["geral"              ]   = $percentualInadimplenciaNucleo                    ;

                        $dados[]                        = $linha                                            ;

                        $linha                          = []                                                ;

                        $vlrTotNucleo                   = 0                                                 ;
                        $vlrTotNucleoInad               = 0                                                 ;
                        
//                        $seqParcela                     = 0                                                 ;

                    }

                    $filialID               = $r["idFilial"]                            ;
                    
                    $filial                 = Filial::model()->findByPk($filialID)      ;

                    $linha["filialID"   ]   =               $filialID                   ;
                    $linha["filial"     ]   = strtoupper(   $filial->getConcat()    )   ;
                    
                }
                
//                $seqParcela++;
                
                $valorPago = $r["valorPago"];
                
                if($valorPago >= $r["valorTotal"])
                {
                    $valorPago = $r["valorTotal"]; 
                }

                $vlrInad                        = $r["valorTotal"] - $valorPago             ;

                $percInad                       = ( ($vlrInad/$r["valorTotal"]) * 100)      ;

                $percentualInadimplencia        = number_format($percInad,2,",",".") . "%"  ;

                $linha[$r["seqParcela"] . "x"]  = $percentualInadimplencia                  ;


                $vlrTotNucleo       += $r["valorTotal"] ;
                $vlrTotNucleoInad   += $vlrInad         ;
                
            }

            if(count($linha) > 0)
            {

                $perTotalInad                   = ( ($vlrTotNucleoInad/$vlrTotNucleo) * 100)        ;

                $percentualInadimplenciaNucleo  = number_format($perTotalInad,2,",",".") . "%"      ;

//              $linha["mesVencimento"      ]   = $r["mesVencimento"]                               ;
//              $linha["anoVencimento"      ]   = $r["anoVencimento"]                               ;
                $linha["geral"              ]   = $percentualInadimplenciaNucleo                    ;
                
                $dados[                     ]   = $linha                                            ;

            }
            
        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }
        
        echo json_encode    (
                                [
                                    "data"  => $dados   ,
                                    "query" => $sql     ,
                                    "erro"  => $erro
                                ]
                            );
        
    }
    
    public function actionGetTableTrimestre()
    {
        
        if(isset($_POST["tituloTable"]))
        {
            $titulo = $_POST["tituloTable"];
        }
        
        if(isset($_POST["nomeAgrupamento"]))
        {
            $nomeAgrupamento = $_POST["nomeAgrupamento"];
        }
        
        if(isset($_POST["nomeTabela"]))
        {
            $nomeTabela = $_POST["nomeTabela"];
        }
        
        $htmlTabela =  "<div class='row'>
                            <div class='col-sm-12'>
                                <h4>$titulo</h4>
                            </div>
                        </div>

                        <div class='row'>

                            <table id='$nomeTabela' class='table table-striped' cellspacing='0' style='background-color: inherit; width : 100%!important;'>

                                <thead style=''>
                                    <tr class='trCabec' style='width : 100%!important;'>
                                        <th style='z-index: -1;'>
                                        </th>
                                        <th style='width: 350px'>
                                            $nomeAgrupamento
                                        </th>
                                        <th style='background-color: green; width: 100px'>
                                            Meta 1x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            1x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Deve 1x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Base 1x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Meta 123x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            123x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Deve 123x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Base 123x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Meta 456x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            456x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Deve 456x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Base 456x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Meta 789x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            789x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Deve 789x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Base 789x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Meta 101112x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            101112x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Deve 101112x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Base 101112x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Meta 131415x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            131415x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Deve 131415x
                                        </th>

                                        <th style='background-color: darkgreen; width: 100px'>
                                            Base 131415x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Meta 161718x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            161718x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Deve 161718x
                                        </th>

                                        <th style='background-color: green; width: 100px'>
                                            Base 161718x
                                        </th>
                                        <th>
                                            Geral
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>

                            </table>
                        </div>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false        ,
                            "data"              => "detalhar"   ,
                            "class"             => "detalhes"
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "agrupamento"       
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta1x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "1x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve1x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base1x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta123x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "123x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve123x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base123x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta456x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "456x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve456x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base456x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta789x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "789x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve789x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base789x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta101112x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "101112x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve101112x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base101112x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta131415x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "131415x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve131415x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base131415x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "meta161718x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "161718x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "deve161718x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "base161718x"     ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "geral"
                        ]
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                                      ,
                        "nomeTabela"    => $nomeTabela                                      ,
                        "urlBuscaDados" => "/analiseInadimplencia/getResultadoTrimestre"    ,
                        "colunas"       => $colunas                                         ,
                        "columnsDefs"   => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionGetTablePNP()
    {
        
        if(isset($_POST["tituloTable"]))
        {
            $titulo = $_POST["tituloTable"];
        }
        
        if(isset($_POST["nomeAgrupamento"]))
        {
            $nomeAgrupamento = $_POST["nomeAgrupamento"];
        }
        
        if(isset($_POST["nomeTabela"]))
        {
            $nomeTabela = $_POST["nomeTabela"];
        }
        
        $htmlTabela =  "<div class='row'>
                            <div class='col-sm-12'>
                                <h4>$titulo</h4>
                            </div>
                        </div>

                        <div class='row'>

                            <table id='$nomeTabela' class='table table-striped' cellspacing='0' style='background-color: inherit; width : 100%!important;'>

                                <thead style=''>
                                    <tr class='trCabec' style='width : 100%!important;'>
                                        <th style='z-index: -1;'>
                                        </th>
                                        <th style='width: 350px'>
                                            $nomeAgrupamento
                                        </th>
                                        <th>
                                            5
                                        </th>
                                        <th>
                                            10
                                        </th>
                                        <th>
                                            15
                                        </th>
                                        <th>
                                            20
                                        </th>
                                        <th>
                                            25
                                        </th>
                                        <th>
                                            30
                                        </th>
                                        <th>
                                            35
                                        </th>
                                        <th>
                                            40
                                        </th>
                                        <th>
                                            45
                                        </th>
                                        <th>
                                            50
                                        </th>
                                        <th>
                                            55
                                        </th>
                                        <th>
                                            60
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>

                            </table>
                        </div>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false            ,
                            "data"              => "detalhar"       ,
                            "class"             => "detalhes"
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "agrupamento"       
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate5"           ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate10"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate15"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate20"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate25"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate30"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate35"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate40"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate45"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate50"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate55"          ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false            ,
                            "data"              => "ate60"          ,
                            "defaultContent"    => "-"        
                        ],
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                              ,
                        "nomeTabela"    => $nomeTabela                              ,
                        "urlBuscaDados" => "/analiseInadimplencia/getResultadoPNP"  ,
                        "colunas"       => $colunas                                 ,
                        "columnsDefs"   => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionGetTableSafra()
    {
        
        $htmlTabela =   "<table id='gridSafra' class='table table-striped' cellspacing='0' style='background-color: inherit; width : 100%!important;'>
                                
                            <thead style=''>
                                <tr class='trCabec' style='background-color: orange; width : 100%!important;'>
                                    <th style='z-index: -1;'>
                                    </th>
                                    <th>
                                        Safra
                                    </th>
                                    <th>
                                        1x
                                    </th>
                                    <th>
                                        2x
                                    </th>
                                    <th>
                                        3x
                                    </th>
                                    <th>
                                        4x
                                    </th>
                                    <th>
                                        5x
                                    </th>
                                    <th>
                                        6x
                                    </th>
                                    <th>
                                        7x
                                    </th>
                                    <th>
                                        8x
                                    </th>
                                    <th>
                                        9x
                                    </th>
                                    <th>
                                        10x
                                    </th>
                                    <th>
                                        11x
                                    </th>
                                    <th>
                                        12x
                                    </th>
                                    <th>
                                        13x
                                    </th>
                                    <th>
                                        14x
                                    </th>
                                    <th>
                                        15x
                                    </th>
                                    <th>
                                        16x
                                    </th>
                                    <th>
                                        17x
                                    </th>
                                    <th>
                                        18x
                                    </th>
                                    <th>
                                        Geral
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false        ,
                            "data"              => "detalhar"   ,
                            "class"             => "detalhes"
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "safra"       
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "1x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "2x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "3x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "4x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "5x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "6x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "7x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "8x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "9x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "10x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "11x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "12x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "13x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "14x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "15x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "16x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "17x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "18x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "geral"
                        ]
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                                  ,
                        "nomeTabela"    => "gridSafra"                                  ,
                        "urlBuscaDados" => "/analiseInadimplencia/getResultadoSafra"    ,
                        "colunas"       => $colunas                                     ,
                        "columnsDefs"   => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionGetTableSafraNucleo()
    {
        
        $titulo = "";
        
        if(isset($_POST["tituloTable"]))
        {
            $titulo = $_POST["tituloTable"];
        }
        
        $htmlTabela =   "<div class='row'>
                            <div class='col-sm-12'>
                                <h4 style='float: none; text-align: center'>$titulo</h4>
                            </div>
                        </div>

                        <table id='gridSafraNucleo' class='table table-striped' cellspacing='0' width='100%' style='background-color: inherit;'>

                            <thead>
                                <tr class='trCabec' style='background-color:'>
                                    <th style='z-index: -1;'>

                                    </th>
                                    <th>
                                        Núcleo
                                    </th>
                                    <th>
                                        1x
                                    </th>
                                    <th>
                                        2x
                                    </th>
                                    <th>
                                        3x
                                    </th>
                                    <th>
                                        4x
                                    </th>
                                    <th>
                                        5x
                                    </th>
                                    <th>
                                        6x
                                    </th>
                                    <th>
                                        7x
                                    </th>
                                    <th>
                                        8x
                                    </th>
                                    <th>
                                        9x
                                    </th>
                                    <th>
                                        10x
                                    </th>
                                    <th>
                                        11x
                                    </th>
                                    <th>
                                        12x
                                    </th>
                                    <th>
                                        13x
                                    </th>
                                    <th>
                                        14x
                                    </th>
                                    <th>
                                        15x
                                    </th>
                                    <th>
                                        16x
                                    </th>
                                    <th>
                                        17x
                                    </th>
                                    <th>
                                        18x
                                    </th>
                                    <th>
                                        Geral
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false        ,
                            "data"              => "detalhar"   ,
                            "class"             => "detalhes"
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "nucleo"       
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "1x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "2x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "3x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "4x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "5x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "6x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "7x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "8x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "9x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "10x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "11x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "12x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "13x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "14x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "15x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "16x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "17x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "18x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "geral"
                        ]
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];
        
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                              ,
                        "nomeTabela"    => "gridSafraNucleo"                        ,
                        "urlBuscaDados" => "/analiseInadimplencia/getSafraNucleo"   ,
                        "colunas"       => $colunas                                 ,
                        "columnsDef"    => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionGetTableAnalistas()
    {
        
        $titulo = "";
        
        if(isset($_POST["tituloTable"]))
        {
            $titulo = $_POST["tituloTable"];
        }
        
        $htmlTabela =   "<div class='row'>
                            <div class='col-sm-12'>
                                <h4 style='float: none; text-align: center'>$titulo</h4>
                            </div>
                        </div>

                        <table id='gridAnalistasNucleo' class='table table-striped' cellspacing='0' width='100%' style='background-color: inherit;'>

                            <thead>
                                <tr class='trCabec' style='background-color:'>
                                    <th style='z-index: -1;'>

                                    </th>
                                    <th>
                                        Analistas
                                    </th>
                                    <th>
                                        1x
                                    </th>
                                    <th>
                                        2x
                                    </th>
                                    <th>
                                        3x
                                    </th>
                                    <th>
                                        4x
                                    </th>
                                    <th>
                                        5x
                                    </th>
                                    <th>
                                        6x
                                    </th>
                                    <th>
                                        7x
                                    </th>
                                    <th>
                                        8x
                                    </th>
                                    <th>
                                        9x
                                    </th>
                                    <th>
                                        10x
                                    </th>
                                    <th>
                                        11x
                                    </th>
                                    <th>
                                        12x
                                    </th>
                                    <th>
                                        13x
                                    </th>
                                    <th>
                                        14x
                                    </th>
                                    <th>
                                        15x
                                    </th>
                                    <th>
                                        16x
                                    </th>
                                    <th>
                                        17x
                                    </th>
                                    <th>
                                        18x
                                    </th>
                                    <th>
                                        Geral
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false        ,
                            "data"              => "detalhar"   ,
                            "class"             => "detalhes"
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "analista"       
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "1x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "2x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "3x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "4x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "5x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "6x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "7x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "8x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "9x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "10x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "11x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "12x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "13x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "14x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "15x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "16x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "17x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "18x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "geral"
                        ]
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];
        
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                                      ,
                        "nomeTabela"    => "gridAnalistasNucleo"                            ,
                        "urlBuscaDados" => "/analiseInadimplencia/getResultadoAnalistas"    ,
                        "colunas"       => $colunas                                         ,
                        "columnsDef"    => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionGetTableSafraFilial()
    {
        
        $titulo = "";
        
        if(isset($_POST["tituloTable"]))
        {
            $titulo = $_POST["tituloTable"];
        }
        
        $htmlTabela =   "<div class='row'>
                            <div class='col-sm-12'>
                                <h4 style='float: none; text-align: center'>$titulo</h4>
                            </div>
                        </div>

                        <table id='gridSafraFilial' class='table table-striped' cellspacing='0' width='100%' style='background-color: inherit;'>

                            <thead>
                                <tr class='trCabec' style='background-color: orange;'>
                                    <th style='z-index: -1;'>
                                        Filial
                                    </th>
                                    <th>
                                        1x
                                    </th>
                                    <th>
                                        2x
                                    </th>
                                    <th>
                                        3x
                                    </th>
                                    <th>
                                        4x
                                    </th>
                                    <th>
                                        5x
                                    </th>
                                    <th>
                                        6x
                                    </th>
                                    <th>
                                        7x
                                    </th>
                                    <th>
                                        8x
                                    </th>
                                    <th>
                                        9x
                                    </th>
                                    <th>
                                        10x
                                    </th>
                                    <th>
                                        11x
                                    </th>
                                    <th>
                                        12x
                                    </th>
                                    <th>
                                        13x
                                    </th>
                                    <th>
                                        14x
                                    </th>
                                    <th>
                                        15x
                                    </th>
                                    <th>
                                        16x
                                    </th>
                                    <th>
                                        17x
                                    </th>
                                    <th>
                                        18x
                                    </th>
                                    <th>
                                        Geral
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>";
        
        $colunas =  array
                    (
                        [
                            "orderable"         => false        ,
                            "data"              => "filial"       
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "1x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "2x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "3x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "4x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "5x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "6x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "7x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "8x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "9x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "10x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "11x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "12x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "13x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "14x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "15x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "16x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "17x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "18x"         ,
                            "defaultContent"    => "-"        
                        ],
                        [
                            "orderable"         => false        ,
                            "data"              => "geral"
                        ]
                    );
        
        $columnsDef =   [
                            [ 
                                "orderable" => false    , 
                                "targets"   => 0 
                            ]
                        ];        
        
        $retorno =  [
                        "htmlTabela"    => $htmlTabela                              ,
                        "nomeTabela"    => "gridSafraFilial"                        ,
                        "urlBuscaDados" => "/analiseInadimplencia/getSafraFilial"   ,
                        "colunas"       => $colunas                                 ,
                        "columnsDef"    => $columnsDef
                    ];
        
        echo json_encode
        (
            $retorno
        );
        
    }
    
    public function actionListarCidades()
    {
        
        $retorno    = []    ;
        
        $idFiliais  = null  ;
        
        if(isset($_POST["variavel"]) && !empty($_POST["variavel"]))
        {
            $idFiliais = $_POST["variavel"];
        }
        
        $cidades = $this->listarCidades($idFiliais);
        
        foreach ($cidades as $cidade)
        {
            
            $retorno[] =    [
                                "id"    => "'" .    $cidade["nome"] . "'"  ,
                                "text"  =>          $cidade["nome"]    
                            ];
            
        }
        
        echo json_encode($retorno);
        
    }
    
    public function listarCidades($filiaisId = null)
    {
        
        $query  =   "   SELECT      DISTINCT    TRIM(E.cidade)      AS nome 
                        FROM                    Endereco            AS E
                        INNER JOIN              Filial_has_Endereco AS FhE      ON FhE.habilitado AND FhE.Endereco_id   = E.id
                        INNER JOIN              Filial              AS F        ON   F.habilitado AND FhE.Filial_id     = F.id
                        WHERE E.habilitado ";
        
        if(isset($filiaisId))
        {
            
            $filiaisIDStr   = join(",", $filiaisId)             ;
            
            $query           .= " AND F.id IN ($filiaisIDStr) "   ;
            
        }
        
        $query .= " ORDER BY 1;";
        
        $dados  = Yii::app()->db->createCommand($query)->queryAll();
        
        return $dados;
        
    }

    public function actionCarregarBads()
    {
        
        $resultado          = []                                    ;
        
        $anoMeses           = [201510,201511,201512]                ;
        $anoMesesStr        = join(',', $anoMeses)                  ;
        
        $mesesVencimento    = $this->buscaMeses($anoMesesStr,true)  ;
        
        $dataHoje           = date("Y-m-d")                         ;
        
        foreach ($anoMeses as $anoMes)
        {
        
            $linha      = []                            ;
            $mesesPulo  = []                            ;
            $dados      = []                            ;

            $mes        = substr($anoMes, 4 , 2)        ;
            $ano        = substr($anoMes, 2 , 2)        ;
            $mesExtenso = $this->getMesExtenso($mes)    ;
            $mesAno     = $mesExtenso . "/" . $ano      ;
                        
            foreach ($mesesVencimento as $mes)
            {
                
                $dataBaixaLargada   = date("Y-m-d",                         strtotime(substr($mes["mes"],0,4) . substr($mes["mes"],4,2) . "01" )    );
                $dataBaixaLimite    = date("Y-m-d", strtotime("+1 month",   strtotime(substr($mes["mes"],0,4) . substr($mes["mes"],4,2) . "15" ) )  );
                
                $queryVencimento  =   "
                                SELECT 	ROUND(SUM(CASE WHEN  P.id IS NOT NULL THEN    P.valor   ELSE 0 END    ),2) AS VlrTotal    , 	
                                        ROUND(SUM(CASE WHEN  B.id IS NOT NULL THEN    P.valor   ELSE 0 END    ),2) AS VlrBx       , 	
                                        ROUND(SUM(CASE WHEN B2.id IS NOT NULL THEN   P2.valor   ELSE 0 END    ),2) AS VlrBxFut  
                                FROM        Proposta    AS Pr
                                INNER JOIN  Titulo 	AS T 	ON  T.habilitado    AND  T.Proposta_id  = Pr.id
                                LEFT  JOIN  Parcela     AS P	ON  P.habilitado    AND  P.Titulo_id	=  T.id 
                                                                    AND EXTRACT(YEAR_MONTH FROM P.vencimento) <= " . $mes['mes'] . " AND LEFT(P.vencimento,10) < '$dataHoje' 
                                LEFT  JOIN  Baixa	AS B    ON  B.baixado       AND  B.Parcela_id	=  P.id AND B.data_da_baixa BETWEEN '$dataBaixaLargada' AND '$dataBaixaLimite' 
                                LEFT  JOIN  Parcela     AS P2   ON P2.habilitado    AND P2.Titulo_id	=  T.id AND EXTRACT(YEAR_MONTH FROM  P.vencimento       ) > " . $mes['mes'] . " 
                                LEFT  JOIN  Baixa	AS B2   ON B2.baixado       AND B2.Parcela_id	= P2.id AND EXTRACT(YEAR_MONTH FROM B2.data_da_baixa    ) = " . $mes['mes'] . "
                                WHERE Pr.habilitado AND T.NaturezaTitulo_id IN (1) AND EXTRACT(YEAR_MONTH FROM Pr.data_cadastro) = $anoMes  ";
        
                $queryRowVencimento = Yii::app()->db->createCommand($queryVencimento)->queryRow();
                
                if(count($queryRowVencimento) > 0 && $queryRowVencimento["VlrTotal"] > 0)
                {
                    
                    $vlrInadimplencia = 0;
                    
                    if($anoMes == $mes["mes"] )
                    {
                        
                        $vlrInadimplenciaAux = $queryRowVencimento["VlrTotal"]-($queryRowVencimento["VlrBx"] + $queryRowVencimento["VlrBxFut"]);
                        
                        continue;
                        
                    }
                    else
                    {
                        $vlrInadimplencia       = $vlrInadimplenciaAux  ;
                        $vlrInadimplenciaAux    = 0                     ;
                    }
                    
                    $vlrInadimplencia   += $queryRowVencimento["VlrTotal"]-($queryRowVencimento["VlrBx"] + $queryRowVencimento["VlrBxFut"]) ;
                    $pct                 = ($vlrInadimplencia /$queryRowVencimento["VlrTotal"])*100                                         ;
                    $percentual          = number_format( $pct, 2, ",", ".") . "%"                                                          ;
                    
                }
                else
                {
                    
                    if($anoMes == $anoMeses[0])
                    {
                        $pct        = 0   ;
                        $percentual = ""  ;
                    }
                    else
                    {
                        continue;
                    }
                    
                }
                
                $dados[] = $percentual;
                
            }
            
            $linha["mes"] = $mesAno;
            
            for ($indice = 1; $indice < count($mesesVencimento); $indice++)
            {
                
                if($indice <= count($dados))
                {
                    $linha[$mesesVencimento[$indice]["mes"]] =  $dados[$indice-1];
                }
                else 
                {
                    $linha[$mesesVencimento[$indice]["mes"]] = "";
                }
                
            }
            
            $resultado[] = $linha;
            
        }
                
        echo json_encode(
                            [
                                "data"      => $resultado,
                            ]
                        );
        
    }
    
    public function actionGetMeses()
    {
        
        $retorno = [];
        
        if(isset($_POST["mesesPartida"]) && !empty($_POST["mesesPartida"]))
        {
            
            $mesesPartida = join(",", $_POST["mesesPartida"]);
        
            $rows = $this->buscaMeses($mesesPartida);

            $retorno[] = ["data"=>"mes"];
            
            foreach($rows as $row)
            {

                $mes        = $row["mes"];

                $retorno [] =   [
                                    "data"      => $mes           
                                ];

            }
            
        }
        
        echo json_encode(
                            [
                                "meses"     => $retorno,
                                "hasErrors" => false
                            ]
                        );
        
    }
    
    public function actionGetMesesColunas()
    {
                
        $retorno = [];
        
        if(isset($_POST["mesesPartida"]) && !empty($_POST["mesesPartida"]))
        {
        
            $mesesPartida = join(",",$_POST["mesesPartida"]);
            
            $rows = $this->buscaMeses($mesesPartida);
            
            $retorno[] = ["mes"=>"Mês"];
            
            $i = 0;
            
            foreach($rows as $row)
            {
                
                $i++;

/*                $mes        = substr($row["mes"], 4 , 2)    ;

                $ano        = substr($row["mes"], 2 , 2)    ;

                $mesExtenso = $this->getMesExtenso($mes)    ;

                $mesAno     = $mesExtenso . "/" . $ano      ;*/

                $retorno [] =   [
                                    "mes" => "Bad $i"
                                ];

            }
            
        }
        
        echo json_encode(
                            [
                                "colunas"   => $retorno,
                                "hasErrors" => false
                            ]
                        );
        
    }
    
    public function buscaMeses($mesesPartida, $primeiraBad = false)
    {
        
        $resultado = [];
        
        $dataHoje = date("Y-m-d");
        
        $mesesPartidaArray = split(",", $mesesPartida);
        
        $sql =  "
                    SELECT 	EXTRACT( YEAR_MONTH FROM P.vencimento) AS mes
                    FROM        Parcela     AS P
                    INNER JOIN  Titulo      AS T    ON  T.habilitado    AND P.Titulo_id     =  T.id
                    INNER JOIN  Proposta    AS Pr   ON Pr.habilitado    AND T.Proposta_id   = Pr.id
                    LEFT  JOIN  Baixa       AS B    ON  B.baixado       AND B.Parcela_id    =  P.id
                    WHERE P.habilitado AND T.NaturezaTitulo_id IN (1) AND EXTRACT( YEAR_MONTH FROM Pr.data_cadastro) IN ($mesesPartida)
                        AND LEFT(P.vencimento,10) < '$dataHoje'
                    GROUP BY EXTRACT( YEAR_MONTH FROM P.vencimento)
                    ORDER BY 1
                ";
        
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        
        foreach ($rows as $row)
        {
            
            if(!$primeiraBad && $row["mes"] == $mesesPartidaArray[0])
            {
                continue;
            }
            
            $resultado[] = ["mes" => $row["mes"]];
            
        }
        
        return $resultado;
        
    }
    
    public function getMesExtenso($mes)
    {
        
        $meses =    [
                        [
                            "numeral"   => "01"         , 
                            "descricao" => "jan"   
                        ],
                        [
                            "numeral"   => "02"         , 
                            "descricao" => "fev"   
                        ],
                        [
                            "numeral"   => "03"         , 
                            "descricao" => "mar"   
                        ],
                        [
                            "numeral"   => "04"         , 
                            "descricao" => "abr"   
                        ],
                        [
                            "numeral"   => "05"         , 
                            "descricao" => "mai"   
                        ],
                        [
                            "numeral"   => "06"         , 
                            "descricao" => "jun"   
                        ],
                        [
                            "numeral"   => "07"         , 
                            "descricao" => "jul"   
                        ],
                        [
                            "numeral"   => "08"         , 
                            "descricao" => "ago"   
                        ],
                        [
                            "numeral"   => "09"         , 
                            "descricao" => "set"   
                        ],
                        [
                            "numeral"   => "10"         , 
                            "descricao" => "out"   
                        ],
                        [
                            "numeral"   => "11"         , 
                            "descricao" => "nov"   
                        ],
                        [
                            "numeral"   => "12"         , 
                            "descricao" => "dez"   
                        ]
                    ];
            
        $key    = array_search($mes, array_column($meses, 'numeral'))   ;
        
        return $meses[$key]["descricao"];
            
    }
    
    public function getMesesPartida()
    {
        
        $retorno = [];
        
        $sql = "SELECT DISTINCT MONTH(P.data_cadastro) AS MES, YEAR(P.data_cadastro) AS ANO
                FROM Proposta AS P
                WHERE P.habilitado";
        
        $resultado = Yii::app()->db->createCommand($sql)->queryAll();
        
        foreach($resultado as $r)
        {
            
            $mes = $this->getMesExtenso($r["MES"]);
            
            $retorno[] =    [
                                "text"  => $mes         . "/" . $r["ANO"]    ,
                                "value" => $r["ANO"]    .       $r["MES"]
                            ];
            
        }
        
        return $retorno;
        
    }
    
}
