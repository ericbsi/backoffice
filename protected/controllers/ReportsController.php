<?php

class ReportsController extends Controller{

    public $layout = '';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return 
        [
            [
                'allow',
                'actions'       =>  ['index','relatorioPlanilha'],
                'users'         =>  ['@'],
                //'expression'    => 'Yii::app()->session["usuario"]->autorizado()'
            ],

            [
                'deny'                                                                  ,
                'users'         =>  [
                                        '*'
                                    ]
            ]
        ];
    }

    //sobrescreve a função do Controller, classe pai
    public function init()
    {
        
    }

    public function actionIndex()
    {
    	
        $colunas              = [
            ['A','Código'       ],
            ['B','Cliente'      ],
            ['C','CPF'          ], 
            ['D','Filial'       ], 
            ['E','Valor'        ], 
            ['F','Entrada'      ],
            ['G','Parcelas'     ],
            ['H','Financiado'   ],
            ['I','Repasse'      ],
            ['J','Data'         ],
            ['K','Status'       ]
        ];

        $linhas                 = [];
        $linhasCount            = 2;
        $rodape                 = NULL;
        $util                   = new Util;

        /*Query*/
        $resultQuery            = Yii::app()->db->createCommand($_POST['query'])->queryAll();

        foreach( $resultQuery as $r )
        {
            $obProposta         = null;

            if ($r['ambiente']  == '1')
            {
                $obProposta     = Proposta::model()->findByPk($r['id']);
            }

            else if ($r['ambiente'] == '2')
            {
                $obProposta = PropostaBeta::model()->findByPk($r['id']);
            }

            if( $obProposta != NULL ){
                $linhas[] = 
                [
                    ['A' . $linhasCount, $r['Codigo']                                                       ],
                    ['B' . $linhasCount, mb_strtoupper($r['nome'])                                          ],
                    ['C' . $linhasCount, $r['cpf']                                                          ],
                    ['D' . $linhasCount, mb_strtoupper($r['FilialLocal'])                                   ],
                    ['E' . $linhasCount, number_format($r['valor'], 2, ',', '.')                            ],
                    ['F' . $linhasCount, number_format($r['valor_entrada'], 2, ',', '.')                    ],
                    ['G' . $linhasCount,  $r['qtdParcelas']                                                 ],
                    ['H' . $linhasCount, number_format(( $r['valor'] - $r['valor_entrada']), 2, ',', '.')   ],
                    ['I' . $linhasCount, number_format($obProposta->valorRepasse(), 2, ',', '.')            ],
                    ['J' . $linhasCount, $util->bd_date_to_view(substr($r['data_cadastro'], 0, 10))         ],
                    ['K' . $linhasCount, mb_strtoupper(strip_tags($obProposta->renderBotaoStatus()))        ]
                ];
                $linhasCount++;
            }
        }

        $excel                = new Excel;
        $excel->export( $colunas, $linhas, $rodape );    
    }
    
    public function actionRelatorioPlanilha()
    {
        
        $coluna             =   "A"                                                     ;
        $linha              =   "1"                                                     ;
        $celula             =   ""                                                      ;
        
        $cabecalho          =   []                                                      ;
        $linhas             =   []                                                      ;
        
        $retorno            =   [
                                    "msg"       =>  "Tabela exportada com sucesso"  ,
                                    "type"      =>  "success"                       ,
                                    "hasErrors" =>  false
                                ]                                                       ;
        
        if(isset($_POST["planilha"]))
        {
            
            
            
        }
        else
        {
            
        }
        
        $s = 'W';
        
        for ($n = 0; $n < 6; $n++)
        {
            echo ++$s . PHP_EOL;
        }
        
        // Digit characters behave differently
        
        echo '== Digits ==' . PHP_EOL;
        
        $d = 'A8';
        
        for ($n = 0; $n < 6; $n++)
        {
            echo ++$d . PHP_EOL;
        }
        
        $d = 'A08';
        
        for ($n = 0; $n < 6; $n++)
        {
            echo ++$d . PHP_EOL;
        }
        
    }
    
}
