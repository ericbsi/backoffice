<?php

class PoliticaCreditoController extends Controller
{
  public function filters(){
    return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete' // we only allow deletion via POST request
    );
  }

  public function accessRules(){
    return 
    [
      [
        'allow',
        'actions'   => ['admin', 'listar', 'persist', 'addRegra', 'listarRegras', 'loadRegra'],
        'users'     => ['@'],
        'expression'=> 'Yii::app()->session["usuario"]->autorizado()'
      ],

      [
        'deny',
        'users'   => ['*']
      ]
    ];
  }

  public function actionLoadRegra()
  {
    $atributos  = [];

    $phr        = PoliticaCreditoHasRegra::model()->findByPk( $_POST['regraId'] );

    //$atributos  = array_filter($phr->regra->attributes);

    foreach (($phr->regra->attributes) as $atributo => $valor ){

      if( $valor !== NULL )
      {
        $atributos[] = ['campo'=> $atributo, 'valor'=> $valor];
      }

    }

    echo json_encode($atributos);
  }

  //sobrescreve a função do Controller, classe pai
  public function init(){

  }

  public function actionAdmin()
  {
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $this->layout = '//layouts/ubold';

    $cs->registerCssFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.css');
    $cs->registerCssFile('/css/login/font-awesome.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/datatables/scroller.bootstrap.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/custombox/dist/custombox.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/switchery/dist/switchery.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/select2/select2.css');
    $cs->registerCssFile('/assets/ubold/css/multipleselect.css');
    $cs->registerCssFile('/assets/ubold/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');
    $cs->registerCssFile('/assets/ubold/plugins/bootstrap-select/dist/css/bootstrap-select.min.css');

    
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.bootstrap.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/buttons.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/responsive.bootstrap.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/datatables/dataTables.scroller.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/select2/select2.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/bootstrap-select/dist/js/bootstrap-select.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/switchery/dist/switchery.min.js');
    $cs->registerScriptFile('/assets/ubold/plugins/notifyjs/dist/notify.min.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/notifications/notify-metro.js', CClientScript::POS_END);
    $cs->registerScriptFile('/assets/ubold/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/jquery.maskMoney.js', CClientScript::POS_END);
    $cs->registerScriptFile('/js/blockInterface.js', CClientScript::POS_END);
    $cs->registerScriptFile($baseUrl . '/js/limpaForm.js', CClientScript::POS_END);

    $cs->registerScriptFile('/assets/ubold/js/politicaCredito/fn-pcredito-admin.js', CClientScript::POS_END);

    $this->render('admin');
  }
  
  public function actionAddRegra()
  {
    $retorno                            = [];
    $erro                               = false;

    $criteria                           = new CDbCriteria;
    $criteria->with                     = ['regra' => ['alias' => 'r']];
    
    $criteria->addInCondition('r.habilitado',         [1],                          'AND' );
    $criteria->addInCondition('r.status',             [$_POST['Regra']['status']],  'AND' );
    $criteria->compare('r.score',                     $_POST['Regra']['score']            );
    $criteria->addInCondition('t.PoliticaCredito_id', [$_POST['pid']],              'AND' );

    $t                                  = Yii::app()->db->beginTransaction();

    /*Novo registro*/
    if( $_POST['id'] == '0' )
    {
      if( PoliticaCreditoHasRegra::model()->find( $criteria ) == NULL )
      {
        if( isset($_POST['pid'])        && $_POST['pid'] != '0' )
        {
          $politica                     = PoliticaCredito::model()->findByPk( $_POST['pid'] );

          $regra                      = new Regra;
          $regra->attributes          = $_POST['Regra'];

          if( !$regra->save() )
          {
            $erro                         = true;
            $retorno['msgConfig'][]       = [
              'tipo'                      => 'success',
              'titulo'                    => 'Operação realizada com sucesso: ',
              'mensagem'                  => 'Mensagem : Regra de análise cadastrada com sucesso.',
              'posicao'                   => 'top right'
            ];
          }
          else{
            $phr                      = new PoliticaCreditoHasRegra;
            $phr->PoliticaCredito_id  = $politica->id;
            $phr->Regra_id            = $regra->id;
            $phr->habilitado          = 1;
              
            if(!$phr->save()){
              $erro                   = true;
              $retorno['msgConfig'][]       = [
                'tipo'                      => 'error',
                'titulo'                    => 'Não foi possivel realizar a operação: ',
                'mensagem'                  => 'Mensagem : Entre em contato com o suporte.',
                'posicao'                   => 'top right'
              ];
            }
            else{
              $retorno['msgConfig'][]       = [
                'tipo'                      => 'success',
                'titulo'                    => 'Operação realizada com sucesso: ',
                'mensagem'                  => 'Mensagem : Regra de análise cadastrada com sucesso.',
                'posicao'                   => 'top right'
              ];
            }
          }
        }
      }

      else
      {
        $erro                           = true;
        $retorno['msgConfig'][]         = [
          'tipo'                        => 'error',
          'titulo'                      => 'Não foi possível cadastrar a regra: ',
          'mensagem'                    => 'Mensagem : Já existe uma regra com este Score e esta Ação cadastrada para esta política.',
          'posicao'                     => 'top right'
        ];
      }
    }
    
    /*Atualização*/
    else
    {
      /*Registro que sera atualizado*/
      $phrExistente                     = PoliticaCreditoHasRegra::model()->findByPk( $_POST['id'] );
      $phrExistente->regra->attributes  = $_POST['Regra'];

      
      $phrExistenteR                    = PoliticaCreditoHasRegra::model()->find( $criteria );
      
      /*Verifica se está tentando atualizar com condições ja existentes*/
      if( $phrExistenteR !== NULL && ( $phrExistenteR->id != $phrExistente->id ) ){
        $erro                           = true;
        $retorno['msgConfig'][]         = [
          'tipo'                        => 'error',
          'titulo'                      => 'Não foi possível atualizar a regra: ',
          'mensagem'                    => 'Mensagem : Já existem condições semelhantes cadastradas.',
          'posicao'                     => 'top right'
        ];
      }

      else{
        if( !$phrExistente->regra->update() ){
          $erro                         = true;
          $retorno['msgConfig'][]       = [
            'tipo'                      => 'error',
            'titulo'                    => 'Não foi possível atualizar a regra: ',
            'mensagem'                  => 'Mensagem : Entre em contato com o suporte.',
            'posicao'                   => 'top right'
          ];
        }
        else{
          $retorno['msgConfig'][]       = [
            'tipo'                      => 'success',
            'titulo'                    => 'Operação realizada com sucesso: ',
            'mensagem'                  => 'Mensagem : Regra atualizada com sucesso.',
            'posicao'                   => 'top right'
          ]; 
        }
      }

    }

    /*Commit ~ Rollback*/
    if(!$erro)
    {
      $t->commit();
    }
    else
    {
      $t->rollBack();
    }

    echo json_encode($retorno);
  }

  public function actionListarRegras()
  {
    $dados                = [];
    $total                = 0;
    $criteria             = new CDbCriteria;
    $acoes                = ['4'=>'MESA', '2'=>'APROVAR', '3'=>'NEGAR'];

    $criteria->addInCondition('t.PoliticaCredito_id', [ $_POST['politicaId'] ], 'AND');
    $criteria->addInCondition('t.habilitado',         [ 1 ],                    'AND');
    $criteria->with       = ['regra' => ['alias' => 'r']];

    $total                = count( PoliticaCreditoHasRegra::model()->findAll($criteria) );

    $criteria->limit      = $total;
    $criteria->offset     = 0;
    $criteria->order      = 'r.score ASC';

    foreach( PoliticaCreditoHasRegra::model()->findAll($criteria) as  $phr )
    {
      $cli_hist           = TTable::model()->findByPk($phr->regra->historico);
      $restricao          = TTable::model()->findByPk($phr->regra->restricao);
      $rg_anexado         = TTable::model()->findByPk($phr->regra->rg_anexado);
      $negacoes           = TTable::model()->findByPk($phr->regra->vendas_negadas);
      $acao               = $acoes[$phr->regra->status];
      //$acao               = StatusProposta::model()->findByPk($phr->regra->status);
      $alerta             = Alerta::model()->findByPk($phr->regra->alerta);

      $dados[]             = [
        "score"           => $phr->regra->score,
        "acao"            => $acao,
        "cli_hist"        => ($phr->regra->historico != NULL) ? $cli_hist->flag : "",
        "val_de"          => ($phr->regra->valor_de != NULL)  ? number_format($phr->regra->valor_de,2,',','.') : "0",
        "val_ate"         => ($phr->regra->valor_ate != NULL) ? number_format($phr->regra->valor_ate,2,',','.') : "0",
        "restricao"       => ($phr->regra->restricao != NULL) ? $restricao->flag : "",
        "comp_renda"      => $phr->regra->comprometimento_da_renda.'%',
        "cont_abertos"    => $phr->regra->contratos_em_aberto.'%',
        "alerta"          => ($phr->regra->alerta != NULL)    ? $alerta->alerta : "",
        "passagens_casa"  => $phr->regra->max_passagens_casa,
        "negacoes"        => ($phr->regra->vendas_negadas != NULL) ? $negacoes->flag : "",
        "rg"              => ($phr->regra->rg_anexado != NULL) ? $rg_anexado->flag : "",
        "passagens_spc"   => $phr->regra->max_passagens_spc,
        "btn_load_info"   => "<button data-phr-id='".$phr->id."' style='padding: 4px 8px!important; font-size:10px!important' class='btn btn-icon waves-effect waves-light btn-success btn-load-regra'> <i class='fa fa-pencil'></i> </button>"
      ];
    }

    echo json_encode([
      'draw'              => 1,
      'data'              => $dados,
      'recordsTotal'      => count($dados),
      'recordsFiltered'   => $total,
    ]);
  }

  public function actionListar()
  {
    $dados                = [];
    $criteria             = new CDbCriteria;
    $criteria->offset     = $_POST['start'];
    $criteria->limit      = 10;

    $total                = count( PoliticaCredito::model()->findAll() );

    foreach( PoliticaCredito::model()->findAll( $criteria ) as $p )
    {
      $dados[]            = 
      [
        'legenda'         => '<p style="background-color : '.$p->cor.'!important">&nbsp;</p>',
        'descricao'       => mb_strtoupper($p->descricao),
        'btn_r_a'         => '<a data-politica="'.$p->id.'" data-politica-nome="'.$p->descricao.'" style="padding:1px 7px!important;" data-bind-show="#wrapper-politicas-regras" data-bind-hide="#wrapper-politicas-grid" class="btn btn-info waves-effect waves-light btn-move-panel btn-load-politica-regras"> <span class="btn-label"><i class="fa fa-reorder"></i></span>Visualizar </a>'
      ];
    }

    echo json_encode([
      'draw'              => $_POST['draw'],
      'data'              => $dados,
      'recordsTotal'      => count($dados),
      'recordsFiltered'   => $total,
    ]);
  }

  public function actionPersist()
  {
    $politica                   = new PoliticaCredito;
    $politica->attributes       = $_POST['PoliticaCredito'];
    $politica->data_cadastro    = date('Y-m-d H:i:s');

    $retorno                    = ['hasErrors' => false];

    $t                          = Yii::app()->db->beginTransaction();

    if( !$politica->save() )
    {
      $retorno['hasErrors']     = true;
      $retorno['errors']        = true;

      foreach ( $politica->getErrors() as $erro )
      {
        $retorno['msgConfig'][] = [
          'tipo'                => 'error',
          'titulo'              => 'Não foi possível realizar a operação: ',
          'mensagem'            => 'Motivo : ' . ($erro[0]),
          'posicao'             => 'top right'
        ];
      }

    }
    
    if ($retorno['hasErrors'] === true)
    {
      $t->rollBack();
    }

    else
    {
      $t->commit();

      $retorno['msgConfig'][]   = [
        'tipo'                  => 'success',
        'titulo'                => 'Operação realizada com sucesso: ',
        'mensagem'              => 'Mensagem : Perfil cadastrado, já disponível para consulta.',
        'posicao'               => 'top right'
      ];
    }

    echo json_encode($retorno);
  }
}
