$(function(){

  $('#filiais_select').multiselect({

      buttonWidth     : '200px',
      numberDisplayed :2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Parceiros <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Parceiro Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Parceiros Selecionados <b class="caret"></b>'
         }
      },

  });

	var tableRepasses = $('#grid_repasses').DataTable({
		
        "processing": true,
        
        "serverSide": true,
        
        "ajax": {
            url: '/repasse/getRepasses/',
            type: 'POST',
            "data": function (d) {
                d.filiais    = $("#filiais_select").val(),
                d.data_de    = $("#data_de").val(),
                d.data_ate	 = $("#data_ate").val()
            },
        },
        
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        
        "columns": [
            {"data": "nome"},
            {"data": "valor"},
        ],
        /*"drawCallback" : function(settings) {
            $('#tfoot-total-atraso').html( settings.json.customReturn.totalAtrasoFilter + ' (de ' + settings.json.customReturn.totalAtraso +')' );
		    }*/
	});

	$('#btn-filter').on('click',function(){
		tableRepasses.draw();
		return false;
	});

});