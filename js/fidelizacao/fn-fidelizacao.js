$(document).ready(function ()
{

    $(document).on('click', '.btnAddCarrinho', function ()
    {

        var idPremio    = $(this        ).val() ;
        var idUsuario   = $('#idUsuario').val() ;

        var estaAtivo   = $(this).hasClass('active')    ;
        var funcao      = ''                            ;
        
        var elemento    = $(this)   ;
        
        console.log($('#qtdItensCarrinho').text());

        if (estaAtivo)
        {
            funcao = "remover";
        }
        else
        {
            funcao = "adicionar";
        }


        $.ajax({
            type: "POST",
            url: "/carrinho/" + funcao + "/",
            data: {
                "idPremio": idPremio,
            },
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if(!retorno.hasErrors)
            {
                
                $('#qtdItensCarrinho').text(retorno.qtdItensCarrinho);
                
                if (estaAtivo)
                {                
                    elemento.removeClass('active');
                    elemento.removeClass('btn btn-red');
                    elemento.addClass('btn btn-green');
                    elemento.html('<i class="fa fa-plus-square"></i> <i class="fa fa-shopping-cart"></i> Carrinho');
                }
                else
                {                
                    elemento.addClass('active');
                    elemento.removeClass('btn btn-green');
                    elemento.addClass('btn btn-red');
                    elemento.html('<i class="fa fa-minus-square"></i> <i class="fa fa-shopping-cart"></i> Carrinho');
                }
                
                if(retorno.qtdItensCarrinho > 0)
                {
                    $('#linkQtdItens').removeAttr('style');
                }
                else
                {
                    $('#linkQtdItens').attr('style','display : none');
                }
                
            }
            else
            {
               console.log(retorno);
            }

        })

    })

})