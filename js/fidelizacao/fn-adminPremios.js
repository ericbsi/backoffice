$(document).ready(function () {

   $('.select2').select2();

   var premiosTable = $('#gridPremios').DataTable({
      "processing": true,
      "serverSide": true,
        "pagingType": "full_numbers",
      "ajax":
              {
                 url: '/premios/listarPremios',
                 type: 'POST',
              },
      "columns": [
         {
            "data": "imagem",
            "className": "imagemPremio",
         },
         {
            "data": "descricao",
            "className": "descricaoPremio tdEdita",
         },
         {
            "data": "tipo",
            "className": "tipoProduto",
         },
         {
            "data": "estoque",
            "className": "estoqueProduto",
         },
         {
            "data": "quantidade",
            "className": "quantidadeEstoque tdEdita",
         },
         {
            "data": "valor",
            "className": "valorPremio tdEdita",
         },
         {"data": "btn"},
      ],
   });

   $('#btnSalvar').on('click', function () {

      $.ajax({
         type: "POST",
         url: "/premios/incluirPremio/",
         data: {
            "descricao": $('#inputDescricaoProduto').val(),
            "tipo": $('#tipoProduto').val(),
            "estoque": $('#estoqueProduto').val(),
            "quantidade": $('#inputQuantidadeProduto').val(),
            "pontos": $('#inputValorPremio').val(),
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         console.log(retorno);

         $.pnotify({
            title: 'Notificação',
            text: retorno.msg,
            type: retorno.pntfyClass
         });

         if (!(retorno.hasErrors)) {

            $('#inputDescricaoProduto').val('');
            $('#tipoProduto').val(0).change();
            $('#estoqueProduto').val(0).change();
            $('#inputQuantidadeProduto').val('');
            $('#inputValorPremio').val('');

            $('#premioId').val(retorno.premioId);

            console.log(retorno.premioId);
            console.log($('#premioId').val());

            $('#modal_form_new_att').modal('show');

            premiosTable.draw();

         }

      })

   });

   /*Formulario de anexo de documento*/
   $('#form-add-att').ajaxForm(
           {
              beforeSubmit: function () {

                 if ($('#imagemProduto').val() != '')
                 {
                    if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-att').valid())
                    {
                       var fsize = $('#imagemProduto')[0].files[0].size;
                       var ftype = $('#imagemProduto')[0].files[0].type;

                       switch (ftype)
                       {
                          case 'image/png':
                          case 'image/gif':
                          case 'image/jpeg':
                          case 'image/pjpeg':
                          case 'application/pdf':
                             break;
                          default:
                             alert("Tipo de arquivo não permitido!");
                             return false;
                       }

                       if (fsize > 5242880)
                       {
                          alert("Arquivo muito grande!");
                          return false
                       }
                    }

                    else
                    {
                       alert("Revise o formulário!");
                    }
                 }
              },
              success: function (response, textStatus, xhr, form) {
                 var retorno = $.parseJSON(response);

                 $.pnotify(
                         {
                            title: 'Notificação',
                            text: retorno.msg,
                            type: retorno.pntfyClass
                         });

                 $('#modal_form_new_att').modal('hide');

                 $('#inputDescricaoProduto').val('');
                 $('#tipoProduto').val(0).change();
                 $('#estoqueProduto').val(0).change();
                 $('#inputQuantidadeProduto').val('');
                 $('#inputValorPremio').val('');

                 premiosTable.draw();

              }
           });

   $(document).on('click', '.btnAtivar', function ()
   {
      var tr = $(this).closest('tr');
      var row = premiosTable.row(tr);
      var rowData = row.data();
      var idPremio = rowData.idPremio;
      var elemento = $(this);

      $.ajax({
         type: "POST",
         url: "/premios/ativarPremio/",
         data: {
            "idPremio": idPremio,
            "ativo": elemento.hasClass('ativo'),
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify({
            title: 'Notificação',
            text: retorno.msg,
            type: retorno.pntfyClass
         });

         premiosTable.draw();

      });

   })

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdEdita', function () {

      var conteudoOriginal = $(this).text()        ; //resgate o conteúdo atual da célula
      var tr               = $(this).closest('tr') ;
      var row              = premiosTable.row(tr)  ;
      var rowData          = row.data()            ;
      var idPremio         = rowData.idPremio      ;
      var elemento         = $(this)               ;
      var tipo             = 0                     ;


      if(elemento.hasClass('descricaoPremio'))
      {
         tipo = 1;
      }

      //transforme o elemento em um input
      $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/premios/alterar",
               data: {
                  "idPremio"  : idPremio     ,
                  "conteudo"  : novoConteudo ,
                  "tipo"      : tipo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });

            })

            $(this).parent().text(novoConteudo);

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

});