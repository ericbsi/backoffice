var FormWizard = function () {

    var wizardContent = $('#wizard');
    var wizardForm = $('#formFilial');

    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };

    var animateBar = function (val) {

        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };

        numberOfSteps = $('.swMain > ul > li').length;
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');

    };

    var initValidator = function () {

        jQuery.validator.addMethod("cnpj", function(cnpj, element) {

          cnpj = jQuery.trim(cnpj);// retira espaços em branco
          // DEIXA APENAS OS NÚMEROS
            cnpj = cnpj.replace('/','');
            cnpj = cnpj.replace('.','');
            cnpj = cnpj.replace('.','');
            cnpj = cnpj.replace('-','');

            var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
            
          digitos_iguais = 1;

            if (cnpj.length < 14 && cnpj.length < 15){
                return false;
            }

            for (i = 0; i < cnpj.length - 1; i++){
              if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
                 digitos_iguais = 0;
                 break;
              }
            }

         if (!digitos_iguais){
                
                tamanho = cnpj.length - 2
                numeros = cnpj.substring(0,tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;

                for (i = tamanho; i >= 1; i--){
                    
                    soma += numeros.charAt(tamanho - i) * pos--;
                    
                    if (pos < 2){
                        pos = 9;
                    }
                }

                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                
                if (resultado != digitos.charAt(0)){
                    return false;
                }

                tamanho = tamanho + 1;
                numeros = cnpj.substring(0,tamanho);
                soma = 0;
                pos = tamanho - 7;

                for (i = tamanho; i >= 1; i--){

                    soma += numeros.charAt(tamanho - i) * pos--;

                    if (pos < 2){
                        pos = 9;
                    }
                }

                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

                if (resultado != digitos.charAt(1)){
                    return false;
                }
                return true;
             }

         else{
                 return false;
             }

         }, "Informe um CNPJ válido.");

        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
            
            rules: {
                'Filial[nome_fantasia]' : {
                    required : true
                },
                'Filial[cnpj]' : {
                    required : true,
                    cnpj : true,
                },
                'Filial[inscricao_estadual]' : {
                    required : true,
                },
                'Telefone[numero]' :{
                    required : true,
                },
                'Email[email]' : {
                    required : true,
                    email : true,
                },
            },
            
            messages: {
                firstname: "Please specify your first name"
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    
    var displayConfirm = function () {
        $('.display-value', formFilial).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {
                $(this).html(input.filter(":checked").parent('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            }
        });
    };
    
    var onShowStep = function (obj, context) {
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
    };
    
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
   
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('selected').addClass('done');
            //wizardForm.submit();
        }
    };
   
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        if (numberOfSteps !== nextstep) {
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else {
            displayConfirm();
            animateBar(nextstep);
            return true;
        };
    };
    
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
   
    return {
        init: function () {
            initWizard();
        }
    };
}();