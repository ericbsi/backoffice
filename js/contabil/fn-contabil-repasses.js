$(function(){
  $('#nucleos_select').multiselect({

      buttonWidth             : '300px',
      numberDisplayed           : 2,
        enableFiltering                   : true,
        enableCaseInsensitiveFiltering    : true,

        buttonText:function(options, select){

          if (options.length == 0)
          {
              return 'Selecionar Núcleos <b class="caret"></b>'
          }
          else if (options.length == 1)
          {
              return '1 Núcleo Selecionado <b class="caret"></b>'
          }
          else
          {
              return options.length + ' Núcleos Selecionados <b class="caret"></b>'
          }
        }
    });

  $.validator.setDefaults({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).closest('.help-block').removeClass('valid');
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
      },
      unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      success: function (label, element) {
        label.addClass('help-block valid');
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
      }
    });

    var tablePropostas = $('#grid_propostas').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax":
    {
        url     :   '/lotePagamento/getPropostas',
        type    :   'POST',
        data    :  function(d)  {
          d.Nucleos   = $('#nucleos_select').val(),
          d.data_de = $('#data_de').val(),
          d.data_ate = $('#data_ate').val()
        }, 
    },

    "columns":  [
                                { 
                                    "orderable" : false         ,
                                    "data"      : "dataProposta"
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "codigo"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "filial"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "cliente"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "inicial"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "entrada"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "carencia"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "financiado"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "repasse"      
                                },
                                { 
                                    "orderable" : false         ,
                                    "data"      : "parcelas"      
                                }
                            ],

    "drawCallback" : function(settings) {
      $('#queryXlsExport').val(settings.json.queryXlsExport  );
    }
        
  });

  $('#btn-filter').on('click',function(){
    tablePropostas.draw();
    return false;
  });

});