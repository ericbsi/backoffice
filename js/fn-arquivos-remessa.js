$(function(){

    $('#bancos_select, #bancos_select2').multiselect({
      buttonWidth                     : '230px',
      enableFiltering                 : true,
      enableCaseInsensitiveFiltering  : true,            
    });
  
  	var gridArquivosRemessa = $('#grid_arquivos_remessa').DataTable({
  		
          "processing": true,
          "serverSide": true,
          
          "ajax": {
              url: '/financeiro/getArquivosRemessa/',
              type: 'POST',
              "data" : function(d)
              {
                d.banco       = $('#bancos_select').val(),
                d.data_de     = $("#data_de").val(),
                d.data_ate    = $("#data_ate").val()
              }
          },
          
          "language": {
              "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
          },
          
          "columnDefs": [{
              "orderable": false,
              "targets": "no-orderable"
          }],
          
          "columns": [
              {"data" :"arquivo"},
              {"data" :"usuario"},
              {"data" :"banco"},
          ],
          /*"drawCallback" : function(settings) {
            $('#th-total').html( settings.json.customReturn.totalRepasse );
          }*/
  	});
	
    $('#btn-filter').on('click',function(){
      	gridArquivosRemessa.draw();
      	return false;
    });

    /*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { 
            $(element).closest('.form-group').removeClass('has-error');

        },

        success: function (label, element) {
            label.addClass('help-block valid');

            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $('#form-add-remessa').validate({
        
        submitHandler : function(){
          
          var request = $.ajax({

              url  : '/financeiro/gerarNovaRemessa/',
              type : 'POST',
              data : $('#form-add-remessa').serialize()

          });

          request.done(function(dRt){
              var retorno = $.parseJSON(dRt);
              console.log(retorno);
          });

          return false;
        }
    });

});