$(document).ready(function () {

    var regionaisTable;

    var nucleosTable = $('#grid_nucleo').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/grupoFiliais/listarNucleos',
                    type: 'POST',
                },
        "columns": [
            {
                "className"       : 'details-control' ,
                "orderable"       : false             ,
                "data"            : null              ,
                "defaultContent"  : ''
            },
/*            {
                "data": "filiais",
                "className": "tableFiliais",
            },
/*          {
                "data": "idNucleo",
            },*/
            {
                "data": "nomeGrupo",
                "className": "tdGrupo",
            },
            {
                "data": "nome",
                "className": "tdNome",
            },
            {
                "data": "banco",
                "className": "tdBanco",
            },
            {
                "data": "agencia",
                "className": "tdAgencia",
            },
            {
                "data": "conta",
                "className": "tdConta",
            },
            {
                "data": "operacao",
                "className": "tdOperacao",
            },
            {"data": "btn"},
        ],
    });

    $('.grupoFilial').select2();

    $('.bancoNucleo').select2();
    
    // Add event listener for opening and closing details
    $('#grid_nucleo tbody').on('click', 'td.details-control', function () {

        var tr          = $(this).closest('tr')         ;
        var row         = nucleosTable.row(tr)          ;
        var rowsTam     = nucleosTable.rows()[0].length ;
        var rowData     = row.data()                    ;
        var idNucleo    = rowData.idNucleo              ;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {

            for (i = 0; i < rowsTam; i++)
            {

                if (nucleosTable.row(i).child.isShown()) {
                    nucleosTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);
            
            regionaisTable = $('#regionais').dataTable(
            {
                "processing"  : true,
                "serverSide"  : true,
                "ajax"        : {
                                    url     :   '/grupoFiliais/getRegionaisNucleo'   ,
                                    type    :   'POST'                              ,
                                    data    :   function(d) {
                                                                d.idNucleo = idNucleo
                                                            }
                  
                                },
                "columns":  [
                                { 
                                    "data"      : "regional"      
                                },
                                { 
                                    "data"      : "btnRemover"      
                                }
                            ]
            });

        }
    });

    $('#btnSalvar').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/grupoFiliais/salvarNucleo/",
            data: {
                "grupoFilial": $('#grupoFilial').val(),
                "nomeNucleo": $('#inputNomeNucleo').val(),
                "bancoNucleo": $('#bancoNucleo').val(),
                "agenciaNucleo": $('#inputAgencia').val(),
                "contaNucleo": $('#inputConta').val(),
                "operacaoNucleo": $('#inputOperacao').val()
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if (!(retorno.hasErrors)) {
                $('#grupoFilial').val(0).change();
                $('#bancoNucleo').val(0).change();
                $('#inputNomeNucleo').val('');
                $('#inputAgencia').val('');
                $('#inputConta').val('');
                $('#inputOperacao').val('');
                nucleosTable.draw();
            }

        })

    });

    $(document).on('click', '#btnAddRegional', function () 
    {

        $.ajax({
            type: "POST",
            url: "/grupoFiliais/salvarRegional/",
            data: {
                "idNucleo"      : $(this).val()                 ,
                "nomeRegional"  : $('#inputNomeRegional').val() ,
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.type
            });

            if (!(retorno.hasErrors)) 
            {
                
                $('#inputNomeRegional').val('');
                
                regionaisTable.api().draw();
                
            }

        })

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdNome', function () {

        var tr          = $(this).closest('tr');
        var row         = nucleosTable.row(tr);
        var rowData     = row.data();
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarNomeNucleo",
                    data: {
                        "idNucleo"  : idNucleo,
                        "nomeNucleo": novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                    if (!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdAgencia', function () {

        var tr          = $(this).closest('tr');
        var row         = nucleosTable.row(tr);
        var rowData     = row.data();
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input id='mudaAgencia' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarAgenciaNucleo",
                    data: {
                        "idNucleo"      : idNucleo,
                        "agenciaNucleo" : novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                    if (!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdConta', function () {

        var tr          = $(this).closest('tr');
        var row         = nucleosTable.row(tr);
        var rowData     = row.data();
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input id='mudaConta' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarContaNucleo",
                    data: {
                        "idNucleo"      : idNucleo,
                        "contaNucleo"   : novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                    if (!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdOperacao', function () {

        var tr          = $(this).closest('tr') ;
        var row         = nucleosTable.row(tr)  ;
        var rowData     = row.data()            ;
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input id='mudaOperacao' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarOperacaoNucleo",
                    data: {
                        "idNucleo"      : idNucleo,
                        "operacaoNucleo": novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                    if (!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdGrupo', function () {

        var tr          = $(this).closest('tr');
        var row         = nucleosTable.row(tr);
        var rowData     = row.data();
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        $.ajax
                (
                        {
                            type: "POST",
                            url: "/grupoFiliais/listarSelect",
                            data: {
                                "idNucleo": idNucleo,
                            },
                        }
                ).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            elemento.html(retorno.html);

            elemento.children().first().focus(); //atribua o foco ao elemento criado

            elemento.on
                    ('change', function ()
                    {

                        index = elemento.children()[0].selectedIndex;
                        idGrupo = elemento.children().val();

                        $.ajax
                                (
                                        {
                                            type: "POST",
                                            url: "/grupoFiliais/mudarGrupoNucleo",
                                            data: {
                                                "idNucleo": idNucleo,
                                                "idGrupo": idGrupo
                                            }
                                        }
                                ).done(function (dRt)
                        {

                            var retorno = $.parseJSON(dRt);

                            $.pnotify({
                                title: 'Notificação',
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            });

                            if (!retorno.hasErrors)
                            {
                                elemento.text(elemento.children()[0][index].text);
                            }

                        }
                        );


                    }
                    );

            //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
            elemento.children().first().blur(function () {

                //devolva o conteúdo original
                elemento.text(conteudoOriginal);

            });

        }

        );

    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdBanco', function () {

        var tr          = $(this).closest('tr');
        var row         = nucleosTable.row(tr);
        var rowData     = row.data();
        var idNucleo    = rowData.idNucleo;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

//        var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento = $(this);

        $.ajax
                (
                        {
                            type: "POST",
                            url: "/grupoFiliais/listarSelectBanco",
                            data: {
                                "idNucleo": idNucleo,
                            },
                        }
                ).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            elemento.html(retorno.html);

            elemento.children().first().focus(); //atribua o foco ao elemento criado

            elemento.on
                    ('change', function ()
                    {

                        index = elemento.children()[0].selectedIndex;
                        idBanco = elemento.children().val();

                        $.ajax
                                (
                                        {
                                            type: "POST",
                                            url: "/grupoFiliais/mudarBancoNucleo",
                                            data: {
                                                "idNucleo": idNucleo,
                                                "idBanco": idBanco
                                            }
                                        }
                                ).done(function (dRt)
                        {

                            var retorno = $.parseJSON(dRt);

                            $.pnotify({
                                title: 'Notificação',
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            });

                            if (!retorno.hasErrors)
                            {
                                elemento.text(elemento.children()[0][index].text);
                            }

                        }
                        );


                    }
                    );

            //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
            elemento.children().first().blur(function () {

                //devolva o conteúdo original
                elemento.text(conteudoOriginal);

            });

        }

        );

    });

});

function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data =  '<h5><i class="clip-list"></i> Regionais</h5>' +
                '<div class="row">' +
                '   <div  class="col-sm-12">' +
                '       <table id="regionais" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                '           <thead>' +
                '              <tr>' +
                '                   <th class="no-orderable">' +
                '                       <input id="inputNomeRegional" class="form form-control" />' +
                '                   </th>' +
                '                   <th class="no-orderable" width="3%">' +
                '                       <button id="btnAddRegional" class="btn btn-green" value="' + d.idNucleo + '">' +
                '                           <i class="fa fa-save"></i>' +
                '                       </button>' +
                '                   </th>' +
                '              </tr>' +
                '              <tr>' +
                '                   <th>Regional</th>' +
                '                   <th></th>' +
                '              </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>'+
                '       </table>'+
                '   </div>'+
                '</div>';

    row.child(data).show();
}