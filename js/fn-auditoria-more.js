$(function()
{

	var grid_observacoes 	= $('#grid_observacoes').DataTable({
	    
	    "processing": true,
	    "serverSide": true,
	    "ajax": {
	        url: '/auditoria/getObservacoes/',
	        type: 'POST',
	        "data": function (d) {
                d.AuditoriaId    = $("#Auditoria_id").val()
            },
	    },
	    
	    "columnDefs" : [
	      {
	        "orderable" : false,
	        "targets"   : "no-orderable"
	      }
    	],

	    "columns":[
	        {"data": "escrita_por"	},
	        {"data": "data_criacao"	},
	        {"data": "conteudo"	}
	    ]    
	});

	var grid_anexos			= $('#grid_anexos').DataTable({

		"processing": true,
	    "serverSide": true,
	    "ajax": {
	        url: '/auditoria/getAnexos/',
	        type: 'POST',
	        "data": function (d) {
                d.AuditoriaId    = $("#Auditoria_id").val()
            },
	    },
	    
	    "columnDefs" : [
	      {
	        "orderable" : false,
	        "targets"   : "no-orderable"
	      }
    	],

	    "columns":[
	        {"data": "descricao"	},
	        {"data": "visualizar"	},
	    ]    

	});

	/*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

	$("#form-add-obs").validate({

		submitHandler : function()
		{
			var post = $.ajax({

				type : 'POST',
				url  : '/auditoria/addObs/',
				data : $('#form-add-obs').serialize(),

			});

			post.done(function( drt ){

				var retorno = $.parseJSON(drt);

				$('#modal_form_new_obs').modal('hide');

				//console.log(retorno);
               
                grid_observacoes.draw();
			});

			return false;
		}

	});


	$('#form-add-att').ajaxForm({

        beforeSubmit    : function(){

            if( $('#ComprovanteFile').val() != '')
            {
                if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-att').valid() )
                {
                    var fsize = $('#ComprovanteFile')[0].files[0].size;
                    var ftype = $('#ComprovanteFile')[0].files[0].type;
                    
                    switch( ftype )
                    {
                        case 'image/png': 
                        case 'image/gif':
                        case 'image/jpeg': 
                        case 'image/pjpeg':
                        case 'application/pdf':
                        break;
                        default:
                            alert("Tipo de arquivo não permitido!");
                        return false;
                    }

                    if( fsize > 5242880 )
                    {
                        alert("Arquivo muito grande!");
                        return false
                    }
                }

                else
                {
                    alert("Revise o formulário!");
                }
            }
        },

        success         : function( response, textStatus, xhr, form ) {
            var retorno = $.parseJSON(response);
            $('#modal_form_new_att').modal('hide');
            grid_anexos.draw();
        }
    });
})