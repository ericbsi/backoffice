$(document).ready(function()
{
    
//    $('.selectpicker').selectpicker();
    
    var sinteticoAnalitico = new Switchery  ( 
                                                $("#analiticoSintetico")            , 
                                                {   color           : '#1db198' , 
                                                    secondaryColor  : '#ff0000' ,
                                                    size            : 'small'   ,
                                                    speed           : '10s'
                                                }
                                            );

    $("#analiticoSintetico").on('change', function()
    {
        
        var checked = sinteticoAnalitico.element[0].checked;
        
        if(checked)
        {
            $("#analiticoText").html("Analítico");
        }
        else
        {
            $("#analiticoText").html("Sintético");
        }
        
    });
    
    $(document).on('click','.btnDiv',function()
    {
        
        var ativo                   = false                                                                     ;
        var divTable                = $("#" +   $(this                      ).attr("data-divNome"       )   )   ;
        var urlTable                =           $(this                      ).attr("data-urlTable"      )       ;
        var tituloTable             =           $(this                      ).attr("data-tituloTable"   )       ;
        var filtroPre               =           $(this                      ).attr("data-filtroPre"     )       ;
        var divTitulo               =           $("#divTituloGrid"          )                                   ;
        var divAnaliticoSintetico   =           $("#divAnaliticoSintetico"  )                                   ;
        
        console.log("oi");
        
        if($(this).hasClass("active"))
        {
            ativo = true;
        }
        
        $(".btnDiv").removeClass("active");
        
        if(!ativo)
        {
            $(this              )   .addClass   ("active"       );
            
            $(".divTableCustom" )   .hide       ("slow"         );
            
            divTable                .show       ("slow"         );
            divAnaliticoSintetico   .show       ("slow"         );
            
            divTitulo               .text       (tituloTable    );
            
/*            $(".table"                      ).css("width","100%");
            $(".dataTables_scrollHeadInner" ).css("width","100%");*/
            
//            estruturarTabela                    (divTable, urlTable, filtroPre, null, tituloTable   );
            
        }
        else
        {
            divTable                .hide("slow"    );
            divAnaliticoSintetico   .hide("slow"    );
            divTitulo               .text(""        );
        }
        
        
        
    });
    
    $('.multiselectCustom').selectpicker(
    {
        buttonWidth                     : '100%'                ,
        numberDisplayed                 : 1                     ,
        liveSearch                      : true                  ,
//      enableFiltering                 : true                  ,
//      enableCaseInsensitiveFiltering  : true                  ,
//      includeSelectAllOption          : true                  ,
        actionsBox                      : true                  ,
        liveSearchPlaceholder           : 'Buscar...'           ,
        selectAllText                   : 'Todos'               ,
        deselectAllText                 : 'Nenhum'              ,
        noneSelectedText                : 'Nada selecionado'    ,
        selectAllValue                  : 0                     ,
        nSelectedText                   : 'Selecionados!'       ,
        selectedClass                   : 'multiselect-selected'
    });
    
    $('#analistasSafra').selectpicker(
    {
        buttonWidth                     : '100%'                ,
//        numberDisplayed                 : 30                    ,
        liveSearch                      : true                  ,
        actionsBox                      : true                  ,
        liveSearchPlaceholder           : 'Buscar...'           ,
        selectAllText                   : 'Todos'               ,
        deselectAllText                 : 'Nenhum'              ,
        noneSelectedText                : 'Nada selecionado'    ,
        selectAllValue                  : 0                     ,
        nSelectedText                   : 'Selecionados!'       ,
        selectedClass                   : 'multiselect-selected'
    });
    
    $("#analistasSafra").on("change",function()
    {
        console.log($(this).val());
    });
        

    $('.multiselectCustom').on('change', function () 
    {
        
        if( typeof $(this).attr("data-Alvo") !== typeof undefined && $(this).attr("data-Alvo") !== false)
        {
            filtrarSelectAlvo($(this));
        }
        
    });
    
    function filtrarSelectAlvo(selectOrigem)
    {
        
        var url         = selectOrigem.attr("data-url"  )   ;
        var alvo        = selectOrigem.attr("data-alvo" )   ;
        
        var objAlvo     = $("#" + alvo                  )   ;

        var selected    = "selected"                        ;
        
        var valor       = selectOrigem.val()                ;

        if( (typeof valor === typeof undefined) || valor == null)
        {
            selected    = "";
        }

        $.ajax(
        {
            url     : url   ,
            type    : 'POST'                                    ,
            data    : 
            {
                variavel : selectOrigem.val()
            }

        }).done(function (dRt)
        {
            var retorno = $.parseJSON(dRt);
			
            objAlvo.find('option').each(function()
            {
                $(this).remove();
            });

            $.each(retorno, function(key, value) 
            {
                objAlvo.append('<option value="' + retorno[key].id + '" ' + selected + '>' + retorno[key].text + '</option>').selectpicker('refresh');
            });
            
            objAlvo.selectpicker("rebuild");
            
            $(".btn.dropdown-toggle").removeClass("btn-default");
            
            if(typeof objAlvo.attr("data-Alvo") !== typeof undefined && objAlvo.attr("data-Alvo") !== false)
            {
                filtrarSelectAlvo(objAlvo);
            }

        });
    }
    
    $(document).on("click",".btnMostrarFiltro",function()
    {
        
        var divFiltro = $(this).attr("data-class-filtro");
        
        if($(this).hasClass("active"))
        {   
            
            $(this              ).removeClass   ("active"                                                       );
            
            $(this              ).css           ("background-color"                             , "orangered"   );
            
            $(this              ).html          ("<b style='color : inherit'>Exibir Filtro</b>"                 );
            
            $("." + divFiltro   ).hide          ("slow"                                                         );
            
        }
        else
        {
            
            $(this              ).addClass      ("active"                                                       );
            
            $(this              ).css           ("background-color"                                 , "orange"  );
            
            $(this              ).html          ("<b style='color : inherit'>Esconder Filtro</b>"               );
            
            $("." + divFiltro   ).show          ("slow"                                                         );
        }
            
        $(this).css("text-height"   , "bold"    );
        
    });
    
    $(document).on("click",".btnFiltrar",function()
    {
        
        $(".tableCustom").DataTable().draw();
        
    });
    
    var gridSafra = $("#gridSafra").DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "scrollX"       : true  ,
        "ajax":
        {
            url     : '/analiseInadimplencia/getResultadoSafra',
            type    : 'POST',
            data    : function(d)
            {
                d.mesesPartida      = $("#mesesPartidaSafra"    ).val() ;
                d.financeiraSafra   = $("#financeiraSafra"      ).val() ;
                d.modalidadeSafra   = $("#modalidadeSafra"      ).val() ;
                d.politicaSafra     = $("#politicaSafra"        ).val() ;
                d.scoreSafra        = $("#scoreSafra"           ).val() ;
                d.filiaisSafra      = $("#filiaisSafra"         ).val() ;
                d.cidadesSafra      = $("#cidadesSafra"         ).val() ;
                d.scoreSafra        = $("#scoreSafra"           ).val() ;
                d.filtraAnalistas   = 0                                 ;
            }
        },
        "columns": 
        [
            {
                "orderable"         : false         ,
                "data"              : "detalhar"    ,
                "class"             : "detalhes"
            },
            {
                "orderable"         : false         ,
                "data"              : "safra"       
            },
            {
                "orderable"         : false         ,
                "data"              : "1x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "2x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "3x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "4x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "5x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "6x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "7x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "8x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "9x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "10x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "11x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "12x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "13x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "14x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "15x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "16x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "17x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "18x"         ,
                "defaultContent"    : "-"          
            },
            {
                "orderable"         : false         ,
                "data"              : "geral"
            }
        ],
    });
    
    var gridAnalistas = $("#gridAnalistas").DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "scrollX"       : true  ,
        "ajax":
        {
            url     : '/analiseInadimplencia/getResultadoSafra',
            type    : 'POST',
            data    : function(d)
            {
                d.mesesPartida      = $("#mesesPartidaSafra"    ).val() ;
                d.financeiraSafra   = $("#financeiraSafra"      ).val() ;
                d.modalidadeSafra   = $("#modalidadeSafra"      ).val() ;
                d.politicaSafra     = $("#politicaSafra"        ).val() ;
                d.scoreSafra        = $("#scoreSafra"           ).val() ;
                d.filiaisSafra      = $("#filiaisSafra"         ).val() ;
                d.cidadesSafra      = $("#cidadesSafra"         ).val() ;
                d.scoreSafra        = $("#scoreSafra"           ).val() ;
                d.filtraAnalistas   = 1                                 ;
                d.analistasSafra    = $("#analistasSafra"       ).val() ;
            }
        },
        "columns": 
        [
            {
                "orderable"         : false         ,
                "data"              : "detalhar"    ,
                "class"             : "detalhes"
            },
            {
                "orderable"         : false         ,
                "data"              : "safra"       
            },
            {
                "orderable"         : false         ,
                "data"              : "1x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "2x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "3x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "4x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "5x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "6x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "7x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "8x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "9x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "10x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "11x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "12x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "13x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "14x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "15x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "16x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "17x"         ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "18x"         ,
                "defaultContent"    : "-"          
            },
            {
                "orderable"         : false         ,
                "data"              : "geral"
            }
        ],
    });
    
    var gridPNP = $("#gridPNP").DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
//        "scrollX"       : true  ,
        "scrollX"       : "100%"  ,
        "bSortable"     : false ,
        "ajax":
        {
            url     : '/analiseInadimplencia/getResultadoPNP',
            type    : 'POST',
            data    : function(d)
            {
                d.filtroPreSub =    {
                                        agrupamento         :   "REPLACE(LEFT(`P.data_cadastro`,7),'-','') "          ,
                                        campoFiltro         :   "REPLACE(LEFT(`P.data_cadastro`,7),'-','') "          ,
                                        aliasFiltro         :   "mesPartida"                                        ,
                                        nivelTable          :   "0"                                                 ,
                                        mesesPartida        : $("#mesesPartidaSafra"                        ).val() ,
                                        financeiraSafra     : $("#financeiraSafra"                          ).val() ,
                                        modalidadeSafra     : $("#modalidadeSafra"                          ).val() ,
                                        politicaSafra       : $("#politicaSafra"                            ).val() ,
                                        scoreSafra          : $("#scoreSafra"                               ).val() ,
                                        filiaisSafra        : $("#filiaisSafra"                             ).val() ,
                                        cidadesSafra        : $("#cidadesSafra"                             ).val() ,
                                        scoreSafra          : $("#scoreSafra"                               ).val() 
                                    };
            }
        },
        "columns": 
        [
            {
                "orderable"         : false         ,
                "data"              : "detalhar"    ,
                "class"             : "detalhes"
            },
            {
                "orderable"         : false         ,
                "data"              : "agrupamento"       
            },
            {
                "orderable"         : false         ,
                "data"              : "ate5"        ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate10"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate15"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate20"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate25"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate30"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate35"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate40"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate45"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate50"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate55"       ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "ate60"       ,
                "defaultContent"    : "-"        
            },
        ],
        "aoColumnDefs": 
        [
            { 
                'bSortable' : false, 
                'aTargets'  : [ 0, 1 ] 
            }
        ]
    });
    
    var gridTrimestre = $("#gridTrimestre").DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
//        "scrollX"       : true  ,
        "scrollX"       : "100%"  ,
        "bSortable"     : false ,
        "ajax":
        {
            url     : '/analiseInadimplencia/getResultadoTrimestre',
            type    : 'POST',
            data    : function(d)
            {
                d.filtroPreSub =    {
                                        agrupamento         :   "REPLACE(LEFT(P.data_cadastro,7),'-','') "          ,
                                        campoFiltro         :   "REPLACE(LEFT(P.data_cadastro,7),'-','') "          ,
                                        aliasFiltro         :   "mesPartida"                                        ,
                                        nivelTable          :   "0"                                                 ,
                                        mesesPartida        : $("#mesesPartidaSafra"                        ).val() ,
                                        financeiraSafra     : $("#financeiraSafra"                          ).val() ,
                                        modalidadeSafra     : $("#modalidadeSafra"                          ).val() ,
                                        politicaSafra       : $("#politicaSafra"                            ).val() ,
                                        scoreSafra          : $("#scoreSafra"                               ).val() ,
                                        filiaisSafra        : $("#filiaisSafra"                             ).val() ,
                                        cidadesSafra        : $("#cidadesSafra"                             ).val() ,
                                        scoreSafra          : $("#scoreSafra"                               ).val() 
                                    };
            }
        },
        "columns": 
        [
            {
                "orderable"         : false         ,
                "data"              : "detalhar"    ,
                "class"             : "detalhes"
            },
            {
                "orderable"         : false         ,
                "data"              : "agrupamento"       
            },
            {
                "orderable"         : false         ,
                "data"              : "meta1x"      ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "1x"          ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve1x"      ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base1x"      ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta123x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "123x"        ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve123x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base123x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta456x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "456x"        ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve456x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base456x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta789x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "789x"        ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve789x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base789x"    ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta101112x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "101112x"     ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve101112x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base101112x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta131415x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "131415x"     ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve131415x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base131415x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "meta161718x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "161718x"     ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "deve161718x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "base161718x" ,
                "defaultContent"    : "-"        
            },
            {
                "orderable"         : false         ,
                "data"              : "geral"
            }
        ],
        "aoColumnDefs": 
        [
            { 
                'bSortable' : false, 
                'aTargets'  : [ 0, 1 ] 
            }
        ]
    });
    
    $("#exportarXLS").on("click", function()
    {
        
        var arrayCabec  = []                                                        ;
        var arrayLinhas = []                                                        ;
        var arrayTh     = $(gridTrimestre.table().header()).children().children()   ;
        
        arrayTh.each(function (index, element) 
        {
            console.log($.trim($(element).text()));
            
            arrayCabec.push($(element).text());

        });
        
        var arrayTr = $(gridTrimestre.table().body()).children();
        
        arrayTr.each(function (index, element) 
        {
            
            linha = [];
            
            $(element).children().each(function(i,e)
            {
                
                console.log($(e).text());
                
                linha.push($(e).text());
                
            });
            
            arrayLinhas.push(linha);

        });
        
        
        /*$.ajax(
        {
            url     :   "reports/relatorioPlanilha" ,
            type    :   "POST"                      ,
            data    :   function(d)
            {
                "planilha"  =   ["cabecalho"    => arrayCabec,  "linhas"    => arrayLinhas];
            }
        }).done(function(dRt)
        {
            
            var retorno = $.parseJSON(dRt);
            
            $.pnotify   
            ({
                    title   : "Notificação" ,
                    text    : retorno.nsg   ,
                    type    : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                
            }
            
        });*/
            
        
    });
    
    $(".divTableCustom"         ).hide("slow");
    $("#divAnaliticoSintetico"  ).hide("slow");
    
    $(document).on("click",".btn-group", function()
    {
        $(this).addClass("show-tick");
    });

    function SubDetalhes(row) 
    {
        
        var data = row.data();
        
        console.log(data);
        
        estruturarTabela(row, data.urlMontaTable, data.filtroPre, data.filtroPreSub, data.tituloTable, false, data.agrupamento, data.nomeAgrupamento, data.nomeTabela);
        
    }
    
    function estruturarTabela(elemento, url, filtroPre, filtroPreSub = {}, tituloTable, ehDiv = true, agrupamento, nomeAgrupamento, nomeTabela)
    {
        
        $.ajax(
        {
            url     : url       ,
            type    : 'POST'    ,
            data    :
            {
                agrupamento     : agrupamento       ,
                filtroPre       : filtroPre         ,
                tituloTable     : tituloTable       ,
                nomeAgrupamento : nomeAgrupamento   ,
                nomeTabela      : nomeTabela
            }
        }).done(function (dRt)
        {
            
            var retorno = $.parseJSON(dRt)      ;
            
            if (ehDiv)
            {
                elemento.html(retorno.htmlTabela);
            }
            else //if(elemento.is("tr"))
            {
                elemento.child(retorno.htmlTabela).show();
            }

            $('#' + retorno.nomeTabela).DataTable(
            {
                
                "processing"    : true              ,
                "serverSide"    : true              ,
                "paging"        : false             ,
                "bFilter"       : false             ,
                "bInfo"         : false             ,
                "scrollX"       : true              ,
                "responsive"    : false             ,
                "ajax"          :
                {
                    url     : retorno.urlBuscaDados ,
                    type    : 'POST'                ,
                    data    :
                    {
                        filtroPre       : filtroPre     ,
                        filtroPreSub    : filtroPreSub
                    }
                },
                "columns"       : retorno.colunas       ,
                "columnsDef"    : retorno.columnsDef    
            });

        });
        
    }
    
    $('.table tbody').on('click', 'td.detalhes', function () 
    {
        
        console.log("oi");
        
        var table       = $(this).offsetParent().find("table").DataTable()  ;
        var tableNome   = $(this).offsetParent().find("table").attr("id")   ;
        var tr          = $(this).closest('tr')                             ;
        var row         = table.row(tr)                                     ;
        var rowsTam     = table.rows()[0].length                            ;
//        var desfoque    = $(this).parent().find('button')                   ;

        if ($(this).parent().find('button').hasClass('btn-success')) 
        {

            $(this).parent().find('button'  ).removeClass ('btn-success'    );
            $(this).parent().find('button'  ).addClass    ('btn-warning'    );

            $(this).parent().find('i'       ).removeClass ('fa-plus'        );
            $(this).parent().find('i'       ).addClass    ('fa-minus'       );

        } 
        else 
        {

            $(this).parent().find('button'  ).addClass    ('btn-success'    );
            $(this).parent().find('button'  ).removeClass ('btn-warning'    );

            $(this).parent().find('i'       ).removeClass ('fa-minus'       );
            $(this).parent().find('i'       ).addClass    ('fa-plus'        );

        }

        if (row.child.isShown()) 
        {
            
            row.child.hide();

            $('#' + tableNome + ' tbody tr').each(function (index, element) 
            {
                $(element).show();

            });

        } 
        else
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (table.row(i).child.isShown()) 
                {
                    table.row(i).child.hide('slow');
                }
            }

            $('#' + tableNome + ' tbody tr').each(function (index, element) 
            {

                if ( $(element).index('tr') !== $(tr).index('tr') ) 
                {
                    $(element).hide();
                }
            });

            SubDetalhes(row);

        }

    });
    
    $(document).on("click",'button.dropdown-toggle',function()
    {
        
        console.log("e aí");
        
        $(this).parent().find("div.dropdown-menu").css("height" , "250px"   );
        $(this).parent().find( "ul.dropdown-menu").css("height" , "160"     );
        
        $(this).parent().find("div.dropdown-menu").css("width"  , "100%"    );
        $(this).parent().find( "ul.dropdown-menu").css("width"  , "100%"    );
        
    })
    
    //---------------------------so CSS---------------------------//
    
    $(".btn.dropdown-toggle"        ).removeClass   ("btn-default"                          );
    
    $(".trCabec"                    ).css           ("background-color" , "orange"          );
    
    $(".bootstrap-select.btn-group" ).css           ("max-width"        , "100%!important"  );
    $(".dropdown-menu"              ).css           ("max-width"        , "100%!important"  );
    $(".bootstrap-select.btn-group" ).css           ("max-height"       , "100%!important"  );
    $(".dropdown-menu"              ).css           ("max-height"       , "100%!important"  );
    
    //---------------------------so CSS---------------------------//
    
})