$(document).ready(function ()
{
    
    getColunasNomes([201510,201511,201512]);
    
    var badsTable;
    
    getColunasData([201510,201511,201512]);
    
    function getColunasData(mesesPartida)
    {
        
        $.ajax(
        {
            type    : "POST"            ,
            url     : "/analiseInadimplencia/getMeses/" ,
            data    : 
            {
               'mesesPartida': mesesPartida,
            }
            
         }).done(function (dRt) {

            var retJSON = $.parseJSON(dRt);
            
            if (retJSON.hasErrors)
            {
                $.pnotify
                ({
                    title   : retJSON.msgConfig.titulo  ,
                    text    : retJSON.msgConfig.texto   ,
                    type    : retJSON.msgConfig.tipo
                });
            }
            else
            {
                
                badsTable = $('#gridBads').DataTable(
                {
                    "processing"    : true  ,
                    "serverSide"    : true  ,
                    "ordering"      : false ,
                    "ajax":
                    {
                        url     : '/analiseInadimplencia/carregarBads'  ,
                        type    : 'POST'                                ,
                    },
                    "columns": retJSON.meses,
                });
                
            }
    
        });
        
    }
    
    function getColunasNomes(mesesPartida)
    {
        
        $.ajax(
        {
            type    : "POST"            ,
            url     : "/analiseInadimplencia/getMesesColunas/" ,
            data    : 
            {
               'mesesPartida': mesesPartida,
            }
            
         }).done(function (dRt) {

            var retJSON = $.parseJSON(dRt);
            
            if (retJSON.hasErrors)
            {
                $.pnotify
                ({
                    title   : retJSON.msgConfig.titulo  ,
                    text    : retJSON.msgConfig.texto   ,
                    type    : retJSON.msgConfig.tipo
                });
            }
            else
            {
                
                var html = "";
                
                $.each(retJSON.colunas,function(i,coluna)
                {
                    html += "<th>" + coluna.mes + "</th>";
                });
                
                $("#cabecBads").html(html);
                
            }
    
         })
        
    }

})