$(function(){

	var tableRecebimentos = $('#grid_recebimentos').DataTable({
		
        "processing": true,
        
        "serverSide": true,
        
        "ajax": {
            url: '/filial/getRecebimentos/',
            type: 'POST',
            "data": function (d) {
                d.Filial_id  = $("#Filial_id").val(),
                d.data_de    = $("#data_de").val(),
                d.data_ate	 = $("#data_ate").val()
            },
        },
        
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        
        "columns": [
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "seq_parcela"},
            {"data": "vencimento_da_parcela"},
            {"data": "data_do_pgto"},
            {"data": "data_da_baixa"},
            {"data": "valor_parcela"},
            {"data": "valor_pago"},
            {"data": "proposta"},
        ],
        "drawCallback" : function(settings) {
            $('#tfoot-total-atraso').html( settings.json.customReturn.totalAtrasoFilter + ' (de ' + settings.json.customReturn.totalAtraso +')' );
        }
	});

	$('#btn-filter').on('click',function(){
		tableRecebimentos.draw();
		return false;
	});
});