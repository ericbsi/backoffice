$(function(){

	$('#submit-form-editar-endereco-cliente').on('click',function(){

		$.ajax({

			type 	:  'POST',
            url  	: '/endereco/edit/',
			data 	: $('#formEditarEndereco').serialize(),

			success	: function( data ){
				console.log( data )
			}
		})

		window.parent.$('#modalFormEndereco').modal('hide')
		
		window.parent.$('#td_logradouro').html($('#logradouro_cliente').val())
		window.parent.$('#td_bairro').html($('#bairro_cliente').val())
		window.parent.$('#td_numero').html($('#numero_end_cliente').val())
		window.parent.$('#td_cidade').html($('#cidade_cliente').val())
		window.parent.$('#td_cep').html($('#cep_cliente').val())
		window.parent.$('#td_uf').html($('#EnderecoCliente_uf option:selected').text())
		window.parent.$('#td_complemento').html($('#complemento_end_cliente').val())

		return false;
	})
})