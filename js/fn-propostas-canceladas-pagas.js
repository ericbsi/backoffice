$(function () {

   $('.select2').select2();

   var tablePropostas = $('#grid_propostas').DataTable({
      "dom": 'T<"clear">lfrtip',
      "tableTools":
              {
                 "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
              },
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/empresa/getCancelamentosPagos/',
         type: 'POST',
         "data": function (d) {
            d.codigo_filter      = ''                             ,
            d.nome_filter        = ''                             ,
            d.filtroGrupoFilial  = $('#filtroGrupoFilial').val()
         }
      },
      "columns": [
         {"data": "codigo"},
         {"data": "data"},
         {"data": "cliente"},
         {"data": "bordero"},
         {"data": "repasse"},
         {"data": "parceiro"}
      ],
      "drawCallback": function (settings) {

         var api = this.api(), data;

         $(api.column(4).footer()).html(settings.json.customReturn.total);
      }
   });
   
   $('#filtroGrupoFilial').on('change', function()
   {
      tablePropostas.draw();
   });
   
});