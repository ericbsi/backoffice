function bloquear_ctrl_j() {

   if (window.event.ctrlKey && window.event.keyCode == 74)
   {
      event.keyCode = 0;
      event.returnValue = false;
   }
}

function select2Focus() {

   var select2 = $(this).data('select2');

   setTimeout(function () {

      if (!select2.opened()) {
         select2.open();
      }
   }, 0);

}

$(function () {

   var tableAnexos = $('#grid_anexos').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/cliente/getAnexos/',
         type: 'POST',
         "data": function (d) {
            d.clienteId = $('#cliente_id').val(),
                    d.canDelete = '0'
         }
      },
      "columns": [
         {"data": "descricao"},
         {"data": "ext"},
         {"data": "data_envio"},
         {"data": "btn"},
      ],
   });



   $('body').tooltip({
      selector: '[rel=tooltip]'
   });

   $.validator.setDefaults({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
         $(element).closest('.help-block').removeClass('valid');
         $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-success').addClass('ico-validate-error');
      },
      unhighlight: function (element) {
         $(element).closest('.form-group').removeClass('has-error');
      },
      success: function (label, element) {
         label.addClass('help-block valid');
         $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-error').addClass('ico-validate-success');
      },
   });

   $('.select2').select2().select2().one('select2-focus', select2Focus).on("select2-blur", function () {
      $(this).one('select2-focus', select2Focus);
   });

   $('#cpf').focus();

   jQuery.fn.deslizar = function (target)
   {
      return this.each(function () {
         $('html,body').animate({scrollTop: $("#" + target).offset().top}, 'slow');
      });
   }

   $.limparForm = function (form)
   {
      $(form)[0].reset();
      $(form + " .select2").select2("val", "");
      $(form).closest('.form-group').removeClass('has-success').find('.symbol').removeClass('ok').addClass('required');
   }

   $.mostrar = function (element)
   {
      if (!$('#' + element).is(':visible'))
      {
         $('#' + element).show();
      }
   }

   $.changeStatusDoCadastro = function (statusCadastro)
   {
      if (statusCadastro == 1)
      {
         $('#msgsReturn').deslizar('initial_place');
         $('#msgsReturn').html('');
         $('#msgsReturn').append('<div style="padding: 5px !important; margin-bottom: 9px !important; display: block;" class="alert alert-success"> <button style="font-size:16px;" data-dismiss="alert" class="close">×</button><i class="fa fa-check-circle"></i> O cliente possui cadastro regular! Uma proposta pode ser enviada!</div>');
         $('#tabForm a[href="#tab4_detalhes_proposta"]').tab('show');
      }
   }

   $.listenCadastroStatus = function (clienteId)
   {
      var request = $.ajax({
         url: '/cliente/checkStatusDoCadastro/',
         type: 'POST',
         data: {
            clienteId: clienteId
         },
      });

      request.done(function (dRt) {
         var retorno = $.parseJSON(dRt);
         $.changeStatusDoCadastro(retorno.status);
      });
   }

   $('#msgsReturn').deslizar('initial_place');

   var selectCidades = $("#selectCidades").select2({
      data: [{id: 0, text: 'Carregando..'}],
      placeholder: "Selecione uma cidade...",
   });

   var selectCidades2 = $("#selectCidades2").select2({
      data: [{id: 0, text: 'Carregando..'}],
      placeholder: "Selecione uma cidade...",
   });

   var selectQtdParcelas = $("#selectQtdParcelas").select2({
      data: [{id: 0, text: 'Carregando..'}],
      placeholder: "Quantidade de parcelas...",
   });

   var selectCarencias = $("#selectCarencias").select2({
      data: [{id: 0, text: 'Carregando..'}],
      placeholder: "Carencia...",
   });

   $('#cpf').on('blur', function () {

      if ($('#cpf').valid())
      {
         var cpf_antigo = $('#cpf_antigo').val();

         if (cpf_antigo != $(this).val())
         {
            $.limparForm('#form-informacoes-basicas');
            $.limparForm('#form-conjuge');
            $.limparForm('#form-dados-profissionais');
            $.limparForm('#form-endereco');
            $.limparForm('#form-contato');
            $.limparForm('#form-referencia');

            $('#cpf_antigo').val($(this).val())

            var request = $.ajax({
               type: 'POST',
               url: '/cliente/situacaoCadastralCPF',
               data: {
                  cpf: $(this).val()
               },
               beforeSend: function ()
               {
                  $.bloquearInterface('<h4>Buscando informações...</h4>');
               }
            });

            request.done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.each(retorno.abasConfig.hasNoError, function (v, abaNE) {
                  $(abaNE).removeClass('hasError').addClass('hasNoError');
               });

               $.each(retorno.abasConfig.hasError, function (v, aba) {
                  $(aba).removeClass('hasNoError').addClass('hasError');
               });

               $('#msgsReturn').deslizar('initial_place');
               $('#msgsReturn').html('');
               $.desbloquearInterface();

               $.each(retorno.formConfig, function (f, config) {

                  $.each(config.fields, function (c, field) {

                     if (field.type == 'select')
                     {
                        if (field.value != null)
                        {
                           $('#' + field.input_bind).select2("val", field.value);
                        }
                     }
                     else
                     {
                        $('#' + field.input_bind).val(field.value);
                     }
                  });
               });

               $.each(retorno.otherFormsConfig.formsToShow, function (fts, f) {
                  $.each(f, function (i, form) {
                     $.mostrar(form);
                  });
               });

               $.each(retorno.msgConfig, function (mensagem, mensagemConfig) {
                  $('#msgsReturn').append($('<div id="' + mensagemConfig.divId + '" style="padding:5px!important; margin-bottom:9px!important;" class="' + mensagemConfig.cssClass + '"> <button style="font-size:16px;" data-dismiss="alert" class="close">×</button>' + mensagemConfig.icon + ' ' + mensagemConfig.content + '</div>').hide().fadeIn(1000));
               });

               if (retorno.cadastrado)
               {

                  tableAnexos.draw();

                  if (retorno.statusDoCadastro.atualizar)
                  {
                     $('#btn-submit-informacoes-basicas').attr('disabled', false);
                     $('#cadastro-precisa-atualizar').modal('show');
                  }
                  else
                  {
                     $('#btn-submit-informacoes-basicas').attr('disabled', true);
                  }

                  var estado = retorno.formConfig[0].fields.Pessoa_naturalidade.value;

                  $.ajax({
                     type: 'GET',
                     url: '/cidade/listCidades/',
                     data: {
                        uf: estado
                     },
                  }).done(function (dRt) {

                     var jsonReturn = $.parseJSON(dRt);

                     selectCidades.select2({
                        data: jsonReturn
                     });
                  });

                  selectCidades.select2("data", {id: retorno.formConfig[0].fields.naturalidade_cidade.value, text: "Some Text"});

                  if (retorno.statusDoCadastro.status == 1)
                  {
                     $('#msgsReturn').deslizar('initial_place');
                     $('#tabForm a[href="#tab4_detalhes_proposta"]').tab('show');
                     $('#valor_da_compra').focus();
                  }

                  else
                  {
                     $('#tabForm a[href="#tab4_detalhes_proposta"]').tab('show');
                  }
               }

               else
               {
                  var valCPF = $('#cpf').val();

                  $('#cpf').val(valCPF)

                  $('#tabForm a[href="#tab4_detalhes_proposta"]').tab('show');
                  $('#valor_da_compra').focus();
                  $('#li-aba-dados-conjuge').removeClass('hasNoError').removeClass('hasError');
                  $('#btn-submit-informacoes-basicas').attr('disabled', false);
               }
            });
         }
      }
   });

   $('#form-cpf').on('submit', function () {
      return false;
   });

   $('#form-cpf').validate({
      submitHandler: function () {
         return false;
      }
   });

   $('#Pessoa_naturalidade').on('change', function () {
      $.ajax({
         type: 'GET',
         url: '/cidade/listCidades/',
         data: {
            uf: $(this).val()
         },
         beforeSend: function ()
         {
            $.bloquearInterface('<h4>Listando cidades...</h4>');
         }

      }).done(function (dRt) {

         $.desbloquearInterface();

         var jsonReturn = $.parseJSON(dRt);

         selectCidades.select2({
            data: jsonReturn
         });
      });
   });

   $('#Conjuge_naturalidade').on('change', function () {
      $.ajax({
         type: 'GET',
         url: '/cidade/listCidades/',
         data: {
            uf: $(this).val()
         },
         beforeSend: function ()
         {
            $.bloquearInterface('<h4>Listando cidades...</h4>');
         }

      }).done(function (dRt) {

         $.desbloquearInterface();

         var jsonReturn = $.parseJSON(dRt);

         selectCidades2.select2({
            data: jsonReturn
         });
      });
   });

   $('#cpf_conjuge').on('blur', function () {

      if ($('#cpf_conjuge').valid() && $('#cpf_conjuge_hidden').val() != $('#cpf_conjuge').val())
      {
         var request = $.ajax({
            type: 'POST',
            url: '/conjuge/checkStatus/',
            data: {
               cpfConj: $(this).val(),
               clienteId: $('#cliente_id').val(),
            },
            beforeSend: $.bloquearInterface('<h4>Buscando informações...</h4>')

         });

         request.done(function (dRt) {

            $.desbloquearInterface();

            var retorno = $.parseJSON(dRt);

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });

            if (retorno.enableSubmit)
            {
               $('#btn-submit-dados-conjuge').attr('disabled', false);
            }
            else
            {
               $('#btn-submit-dados-conjuge').attr('disabled', true);
            }

            $.each(retorno.formConfig, function (configs, fconfig) {

               $.each(fconfig.fields, function (c, v) {

                  if (v.type == 'select')
                  {
                     if (v.value != null)
                     {
                        $('#' + v.input_bind).select2("val", v.value);
                     }
                  }
                  else
                  {
                     $('#' + v.input_bind).val(v.value);
                  }
               });
            });


            if (retorno.hasErrors)
            {
               $('#cpf_conjuge').val('');
               $('#cpf_conjuge').focus();
               $.limparForm('#form-conjuge');
            }
         });
      }
   });

   $("#form-informacoes-basicas").validate({
      ignore: null,
      rules:
              {
                 'Pessoa[nascimento]': {
                    remote: {
                       url: '/cliente/clienteIdadeApto/',
                       type: 'POST',
                       data: {
                          nascimento: function () {
                             return $('#nascimento').val()
                          }
                       },
                    }
                 },
              },
      messages:
              {
                 'Pessoa[nascimento]': {
                    remote: "Cliente precisa ter entre 21 e 80 anos de idade!"
                 },
                 /*'RG[numero]'            : {
                  remote : "RG já cadastrado no sistema!"
                  },*/
              },
      submitHandler: function () {

         var formData = $('#form-informacoes-basicas').serialize();

         var post = $.ajax({
            type: "POST",
            url: "/cliente/clientePersist/",
            data: formData + '&CPF[numero]=' + $('#cpf').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
            }
         });

         post.done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.desbloquearInterface();

            $('#msgsReturn').html('');

            $.each(retorno.formConfig.fields, function (c, v) {
               $('#' + v.input_bind).attr('value', v.value);
            });

            $.each(retorno.abasConfig.hasError, function (v, aba) {
               $(aba).removeClass('hasNoError').addClass('hasError');
            });

            $.each(retorno.abasConfig.hasNoError, function (v, abaNE) {
               $(abaNE).removeClass('hasError').addClass('hasNoError');
            });

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });

            if (!retorno.hasErrors)
            {
               $.mostrar('form-endereco');
               $.mostrar('form-contato');
               $.mostrar('form-referencia');
               $.mostrar('form-dados-profissionais');

               if ($('#Cadastro_conjugue_compoe_renda').val() == 1)
               {
                  $.mostrar('form-conjuge');
               }

               $('#tabForm a[href="#tab4_endereco"]').tab('show');
            }

         });
      }
   });

   $('#form-conjuge').validate({
      submitHandler: function ()
      {

         var formData = $('#form-conjuge').serialize();

         var request = $.ajax({
            url: '/conjuge/persist/',
            type: 'POST',
            data: formData + '&clienteId=' + $('#cliente_id').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
            }
         });

         request.done(function (dRt) {

            $.desbloquearInterface();

            var retorno = $.parseJSON(dRt);

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });



            if (!retorno.hasErrors)
            {
               $.limparForm('#form-conjuge');
               $('#tabForm a[href="#tab4_referencias"]').tab('show');
               $('#li-aba-dados-conjuge').removeClass('hasError').addClass('hasNoError');
               $('#aviso_conjuge').hide();

               $.listenCadastroStatus($('#cliente_id').val());
            }
         });
      }
   });

   $('#form-dados-profissionais').validate({
      submitHandler: function ()
      {
         var formData = $('#form-dados-profissionais').serialize();

         var request = $.ajax({
            url: '/dadosProfissionais/persist/',
            type: 'POST',
            data: formData + '&clienteId=' + $('#cliente_id').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando dados profissionais. Aguarde...</h4>');
            }

         });

         request.done(function (dRt) {

            $.desbloquearInterface();

            var retorno = $.parseJSON(dRt);

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });


            if (!retorno.hasErrors)
            {
               $('#aviso_dados_profissionais').hide();
               $('#li-aba-dados-profissionais').removeClass('hasError').addClass('hasNoError');
               //$.limparForm('#form-dados-profissionais');

               if ($('#Cadastro_conjugue_compoe_renda').val() == 1)
               {
                  $('#tabForm a[href="#tab4_dados_conjuge"]').tab('show');
               }
               else
               {
                  $('#tabForm a[href="#tab4_referencias"]').tab('show');
               }

               $.changeStatusDoCadastro(retorno.statusDoCadastro);
            }
         });
      }
   });

   $("#form-endereco").validate({
      submitHandler: function () {

         var formData = $('#form-endereco').serialize();

         var request = $.ajax({
            url: '/endereco/persist/',
            type: 'POST',
            data: formData + '&clienteId=' + $('#cliente_id').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando endereço. Aguarde...</h4>');
            }

         });

         request.done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.desbloquearInterface();

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });

            if (!retorno.hasErrors)
            {
               $('#aviso_endereco').hide();
               //$.limparForm('#form-endereco');
               $('#li-aba-endereco').removeClass('hasError').addClass('hasNoError');
               $('#tabForm a[href="#tab4_contato"]').tab('show');
               $.changeStatusDoCadastro(retorno.statusDoCadastro);
            }
         });
      }
   });

   $('#form-contato').validate({
      rules:
              {
                 'TelefoneCliente2[Tipo_Telefone_id]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#telefone2_numero').val().length > 0;
                               }
                            }
                         },
                 'TelefoneCliente3[Tipo_Telefone_id]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#telefone3_numero').val().length > 0;
                               }
                            }
                         }
              },
      submitHandler: function () {

         var formData = $('#form-contato').serialize();

         var request = $.ajax({
            url: '/telefone/persist/',
            type: 'POST',
            data: formData + '&clienteId=' + $('#cliente_id').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando informações. Aguarde...</h4>');
            }
         });

         request.done(function (dRt) {

            $.desbloquearInterface();

            var retorno = $.parseJSON(dRt);

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });

            if (!retorno.hasErrors)
            {
               $('#aviso_contato').hide();
               $('#li-aba-contato').removeClass('hasError').addClass('hasNoError');
               //$.limparForm('#form-contato');
               $('#tabForm a[href="#tab4_dados_profissionais"]').tab('show');
               $.changeStatusDoCadastro(retorno.statusDoCadastro);


               $.each(retorno.formConfig, function (f, config) {

                  $.each(config.fields, function (c, field) {

                     if (field.type == 'select')
                     {
                        if (field.value != null)
                        {
                           $('#' + field.input_bind).select2("val", field.value);
                        }
                     }
                     else
                     {
                        $('#' + field.input_bind).val(field.value);
                     }
                  });
               });
            }
         });
      }
   });

   $('#form-referencia').validate({
      rules:
              {
                 'Referencia2[nome]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia2_parentesco').val().length > 0;
                               }
                            }
                         },
                 'TelefoneReferencia2[numero]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia2_parentesco').val().length > 0;
                               }
                            }
                         },
                 'Referencia2[Tipo_Referencia_id]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia2_parentesco').val().length > 0;
                               }
                            }
                         },
                 'Referencia3[nome]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia3_parentesco').val().length > 0;
                               }
                            }
                         },
                 'TelefoneReferencia3[numero]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia3_parentesco').val().length > 0;
                               }
                            }
                         },
                 'Referencia3[Tipo_Referencia_id]':
                         {
                            required: {
                               depends: function (element)
                               {
                                  return $('#referencia3_parentesco').val().length > 0;
                               }
                            }
                         },
              },
      submitHandler: function ()
      {
         var formData = $('#form-referencia').serialize();

         var request = $.ajax({
            url: '/referencia/persist/',
            type: 'POST',
            data: formData + '&clienteId=' + $('#cliente_id').val(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Salvando referência. Aguarde...</h4>');
            }
         });

         request.done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.desbloquearInterface();

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {

               $.pnotify({
                  title: conteudoNotificacao.titulo,
                  text: conteudoNotificacao.texto,
                  type: conteudoNotificacao.tipo
               });
            });

            if (!retorno.hasErrors)
            {
               $('#aviso_referencia').hide();
               $('#li-aba-referencias').removeClass('hasError').addClass('hasNoError');
               //$.limparForm('#form-referencia');
               $('#tabForm a[href="#tab4_anexos"]').tab('show');

               $.each(retorno.formConfig, function (f, config) {

                  $.each(config.fields, function (c, field) {

                     if (field.type == 'select')
                     {
                        if (field.value != null)
                        {
                           $('#' + field.input_bind).select2("val", field.value);
                        }
                     }
                     else
                     {
                        $('#' + field.input_bind).val(field.value);
                     }
                  });
               });

               $.changeStatusDoCadastro(retorno.statusDoCadastro);
            }
         });
      }
   });

   /*Enviando proposta*/
   $('#btn-enviar-proposta').on('click', function () {

      if ($('#form-proposta').valid())
      {
         if ($('#Proposta_qtd_parcelas').val() != '0')
         {
            $('#btn-enviar-proposta').attr('disabled', true);

            var formData = $('#form-proposta').serialize();

            var request = $.ajax({
               url: '/proposta/persist/',
               type: 'POST',
               data: formData + '&clienteId=' + $('#cliente_id').val(),
               beforeSend: function ()
               {
                  $.bloquearInterface('<h4>Aguarde... Processando informações</h4>');
               }
            });

            request.done(function (dRt) {

               $.desbloquearInterface();
               var retorno = $.parseJSON(dRt);

               if (retorno.hasErrors)
               {
                  $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
                     $.pnotify({
                        title: conteudoNotificacao.titulo,
                        text: conteudoNotificacao.texto,
                        type: conteudoNotificacao.tipo
                     });
                  });

                  $('#btn-enviar-proposta').attr('disabled', false);
               }

               else
               {
                  $('#span-codigo-proposta').text(retorno.propostaConfig.codigo);
                  $('#ipt-hdn-id-proposta').val(retorno.propostaConfig.id);
                  $('#sucess-return').modal('show');
               }
            });
         }
         else
         {
            alert('Escolha um plano!');
            $('#valor_da_compra').focus();
         }
      }

      else
      {
         $('#tabForm a[href="#tab4_detalhes_proposta"]').tab('show');
      }

      return false;
   });

   $(document).on('click', '.btn-show-form', function () {

      $.limparForm('#' + $(this).data('form-show'));
      $.mostrar($(this).data('form-show'));
      $(document).deslizar($(this).data('form-show'));
      $('#' + $(this).data('form-input-init')).focus();
      $('#tabForm a[href="' + $(this).data('go-to-tab') + '"]').tab('show');
      return false;

      /*if( !$('#'+$(this).data('form-show')).is(":visible") )
       {
       $('#'+$(this).data('form-show')).show();
       }
       
       $(document).deslizar( $(this).data('form-show') )
       
       $('#'+$(this).data('form-input-init')).focus();*/


      /*
       
       data-go-to-tab="#tab4_contato"
       data-form-show="form-contato"
       data-form-input-init="telefone_numero"
       
       */

      /*$.mostrar($(this).data('form-show'));
       $('#'+$(this).data('form-input-init')).focus();
       //$('#tabForm a[href="#tab4_contato"]').tab('show');
       $('#tabForm a[href="#'+$(this).data('go-to-tab')+'"]').tab('show');*/
   });


   $('#Cadastro_conjugue_compoe_renda').on('change', function () {

      if ($(this).val() == 1)
      {
         /*if( !$('#form-conjuge').is(":visible") && $('#ControllerAction').val() == 'update' )
          {
          $('#form-conjuge').show();
          $(document).deslizar('form-conjuge');
          $('#cpf_conjuge').focus();
          }*/

         $.mostrar('form-conjuge');
         $('#li-aba-dados-conjuge').removeClass('hasNoError').addClass('hasError');
      }

      else
      {
         $('#li-aba-dados-conjuge').removeClass('hasError hasNoError');
         $('#form-conjuge').hide();
      }

   });

   $(document).on('click', '.bind-form-add', function () {

      $.limparForm('#' + $(this).data('form-bind'));
      $.mostrar($(this).data('form-bind'));
      $(document).deslizar($(this).data('form-bind'));
      $('#' + $(this).data('form-focus')).focus();
      $('#tabForm a[href="' + $(this).data('go-to-tab') + '"]').tab('show');

   });

   $('#Cotacao_id').on('change', function () {

      if ($('#Cotacao_id').valid())
      {
         var request = $.ajax({
            type: 'POST',
            url: '/tabelaCotacao/getCondicoes/',
            data: $('#form-proposta').serialize(),
            beforeSend: function ()
            {
               $.bloquearInterface('<h4>Buscando informações da tabela...</h4>');
            }

         });

         request.done(function (dRt) {

            $.desbloquearInterface();

            var retorno = $.parseJSON(dRt);

            selectQtdParcelas.select2({
               data: retorno.parcelas
            });

            selectCarencias.select2({
               data: retorno.carencias
            });

            $('#opcoes-parcelas').html('');

            $('#valor_financiado').val('');
            $('#valor_financiado_final').val('');
            $('#data_pri_parc').val('');
            $('#data_ult_parc').val('');

            $('#Proposta_qtd_parcelas').attr('value', 0);
            
         });
      }
      else
      {
         selectCarencias.select2({
            data: [{id: null, text: 'Carregando..'}],
            placeholder: "Carencia...",
         });


         selectQtdParcelas.select2({
            data: [{id: null, text: 'Carregando..'}],
            placeholder: "Parcelas...",
         });
      }

   });

   $.simularProposta = function ()
   {
      return this.each(function () {

         $('#Proposta_qtd_parcelas').attr('value', 0);

         if ($('#valor_da_compra').valid() && $('#valor_de_entrada').valid() && $('#Cotacao_id').valid() && $('#selectCarencias').valid())
         {
            var dataForm = $('#form-proposta').serialize();

            var request = $.ajax({
               url: '/proposta/simular/',
               type: 'POST',
               data: dataForm,
               beforeSend: function ()
               {
                  $.bloquearInterface('<h4>Simulando...Aguarde.</h4>');
               }

            });

            request.done(function (dRt) {

               $.desbloquearInterface();

               var retorno = $.parseJSON(dRt);

               $('#opcoes-parcelas').html('');

               $('#valor_financiado').val('');
               $('#valor_financiado_final').val('');
               $('#data_pri_parc').val('');
               $('#data_ult_parc').val('');

               $('#Proposta_qtd_parcelas').attr('value', 0);

               $.each(retorno.parcelas, function (parcela, pConfig) {

                  var msgToolTip = 'Primeira Parcela: ' + retorno.globalConfig.dataPriParc;
                  msgToolTip += ' - Última Parcela: ' + pConfig.dataUltParc;
                  msgToolTip += ' - Valor Final: ' + pConfig.valorFinal;

                  var htmlAppend = '<div class="col-sm-2">';
                  htmlAppend += '    <div class="div-condicoes-parcelamento selectDefault">';
                  htmlAppend += '        <p data-ult-parc="' + pConfig.dataUltParc + '" data-pri-parc="' + retorno.globalConfig.dataPriParc + '" data-valor-financiado-final="' + pConfig.valorFinal + '" data-valor-financiado="' + retorno.globalConfig.valFinanciado + '" data-qtd-parcelas="' + pConfig.qtdParcelas + '" class="selectPlan" rel="tooltip" data-original-title="' + msgToolTip + '" data-placement="top">' + pConfig.qtdParcelas + ' x R$ ' + pConfig.valorView + '</p>';
                  htmlAppend += '    </div>';
                  htmlAppend += '</div>';

                  $('#opcoes-parcelas').append(htmlAppend);
               });
            });
         }
      });
   }

   $(document).on('click', '.selectPlan', function () {

      $('.div-condicoes-parcelamento').each(function (i, obj) {
         $(obj).removeClass('selectedIn').addClass('selectedOut');
      });

      $(this).parent().removeClass('selectedOut').addClass('selectedIn');
      $('#Proposta_qtd_parcelas').val($(this).data('qtd-parcelas'));

      $('#valor_financiado').val(' R$ ' + $(this).data('valor-financiado'));
      $('#valor_financiado_final').val(' R$ ' + $(this).data('valor-financiado-final'));
      $('#data_pri_parc').val($(this).data('pri-parc'));
      $('#data_ult_parc').val($(this).data('ult-parc'));
   });

   $(document).on('blur', '.triggerSimularInput', $.simularProposta());
   $(document).on('change', '.triggerSimularSelect', $.simularProposta());

   $('#tabForm a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      $("#" + e.target.dataset.formBind).focus();
   });

   $(document).on('click', '.btn-step-back', function () {
      $('#tabForm a[href="' + $(this).data('go-to-step') + '"]').tab('show');
      return false;
   });

   $('#form-anexos').ajaxForm({
      beforeSubmit: function () {

         $('#Cliente_id_fup').val($('#cliente_id').val());

         if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-anexos').valid())
         {
            var fsize = $('#FileInput')[0].files[0].size;
            var ftype = $('#FileInput')[0].files[0].type;

            switch (ftype)
            {
               case 'image/png':
               case 'image/gif':
               case 'image/jpeg':
               case 'image/pjpeg':
               case 'application/pdf':
                  break;
               default:
                  $('#cadastro_anexo_msg_return').html("Tipo de arquivo não permitido!").show();
                  $('#cadastro_anexo_msg_return').fadeOut(4000);
                  return false;
            }

            if (fsize > 5242880)
            {
               $('#cadastro_anexo_msg_return').html("Arquivo muito grande!").show();
               $('#cadastro_anexo_msg_return').fadeOut(4000);
               return false
            }
         }

         else
         {
            alert("Revise o formulário!");
         }

      },
      uploadProgress: function (event, position, total, percentComplete) {

         $('#progressbox').show();
         $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
         $('#statustxt').html(percentComplete + '%'); //update status text

         if (percentComplete > 50)
         {
            $('#statustxt').css('color', '#000'); //change status text to white after 50%
         }
      },
      success: function (response, textStatus, xhr, form) {

         var retorno = $.parseJSON(response);

         $.pnotify({
            title: 'Ação realizada com sucesso',
            text: 'O Arquivo foi enviado com sucesso!',
            type: 'success',
         });

         $('#progressbox').fadeOut(3000);

         tableAnexos.draw();

      },
      error: function (xhr, textStatus, errorThrown) {

      },
      resetForm: true
   });

   $('#btn-go-to-informacoes-basicas').on('click', function () {

      if ($('#form-proposta').valid())
      {
         $('#li-aba-detalhes-proposta').removeClass('hasError').addClass('hasNoError');
      }
      else
      {
         $('#li-aba-detalhes-proposta').removeClass('hasNoError').addClass('hasError');
      }

      $('#tabForm a[href="#tab4_dados_do_cliente"]').tab('show');
      return false;
   });

   $('#btn-go-to-dados-pessoais-update').on('click', function () {
      $('#tabForm a[href="#tab4_dados_do_cliente"]').tab('show');
      $('#cadastro-precisa-atualizar').modal('hide');
   });
});