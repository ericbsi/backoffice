$(function () {

   $('#cpfcliente').focus(); // cursor setado no campo de pesquisa assim que a página é carregada

   $.validator.setDefaults({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
         $(element).closest('.help-block').removeClass('valid');
         $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-success').addClass('ico-validate-error');
      },
      unhighlight: function (element) {
         $(element).closest('.form-group').removeClass('has-error');
      },
      success: function (label, element) {
         label.addClass('help-block valid');
         $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-error').addClass('ico-validate-success');
      },
   });

   $('#form-boletos').validate();

   var tableBoletos = $('#grid_parcelas').DataTable({
      "columnDefs": [
         {
            "orderable": false,
            "targets": "no-orderable"
         }
      ]
   });

   $('.check_print').on('change', function () {

      var ipthdnId = 'iptn_hdn_parcela_' + $(this).val();

      if (this.checked) {

         if ($('#' + ipthdnId).length == 0)
         {
            $('#form_titulos_segunda_via').append('<input id="' + ipthdnId + '" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
         }
      }

      else
      {
         if ($('#' + ipthdnId).length != 0)
         {
            $('#' + ipthdnId).remove();
         }
      }

      if ($('.check_print:checked').length == 0)
      {
         $("#btn-print-selects").prop("disabled", true);
      }
      else
      {
         $("#btn-print-selects").prop("disabled", false);
      }
      
   });

   $('#check_select_all').on('change', function () {
      this.checked ? $(this).checkAll() : $(this).unCheckAll();
   });

   jQuery.fn.checkAll = function () {

      return this.each(function () {

         $(".check_print").each(function () {

            var inputHdn = $('#iptn_hdn_parcela_' + $(this).val());

            if (inputHdn.length == 0)
            {

               $('#form_titulos_segunda_via').append('<input id="iptn_hdn_parcela_' + $(this).val() + '" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
            }
         })

         $(".check_print").prop('checked', true);
         
         $("#btn-print-selects").prop("disabled", false);
         
      })
   }

   jQuery.fn.unCheckAll = function () {

      return this.each(function () {

         $(".check_print").each(function () {

            var inputHdnId = 'iptn_hdn_parcela_' + $(this).val();

            if ($('#' + inputHdnId).length != 0)
            {
               $('#' + inputHdnId).remove();
            }
         })

         $(".check_print").prop('checked', false);
         
         $("#btn-print-selects").prop("disabled", true);
      })

   }

});