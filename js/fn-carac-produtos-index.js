$(function(){

    var gridEstoques = $('#grid_caracteristicas').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/caracteristicaProduto/getCaracteristicas/',
            type: 'POST'            
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            {"data": "descricao"},
            {"data": "qtd_itens"},
            {"data": "btn"},
        ],
    });

	jQuery.fn.clearFormInputs = function(form){
        return form.find("input[type=text]").val("");
    }

    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
            
        unhighlight: function (element) { 
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');            
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

	$('#form-add-carac-produto').validate({

		submitHandler : function(){

			$("#cadastro_carac_produto_msg_return").hide().empty();

            var formData = $('#form-add-carac-produto').serialize();

			$.ajax({
				url  : '/caracteristicaProduto/add/',
				type : 'POST',
				data: formData,

				beforeSend: function() {

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando caracteristica...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function() {
                        $('.panel').unblock();
                    }, 1000);
                }
			})
			.done(function(dRt){

				var retorno = $.parseJSON(dRt);
				
                if( !$('#carac_produto_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });

                    $('#modal_form_new_carac_produto').modal('hide');
                }
                else
                {
                    $('#cadastro_carac_produto_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-carac-produto').clearFormInputs($('#form-add-carac-produto'));
                
                gridEstoques.draw();
                
			});

		}
	});
});