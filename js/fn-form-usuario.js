$(document).ready(function(){

	if ($('#Usuario_tipo').val() == 'analista_de_credito') {
		$('#select_grupo_de_analista_wrapper').show();
		$('#input_limite_de_credito').show();
	}

	else{
		$('#input_filial').show();
	}


	$('#Usuario_tipo').on('change', function(){

		if( $(this).val() =='analista_de_credito')
		
		{
			$('#select_grupo_de_analista_wrapper').show();
			$('#input_limite_de_credito').show();
			$('#input_filial').hide();
		}

		else if ( $(this).val() == 'crediarista' || $(this).val() == 'vendedor' || $(this).val() == 'caixa' ) {

			$('#select_grupo_de_analista_wrapper').hide();
			$('#input_limite_de_credito').hide();
			$('#input_filial').show();
		}

		else
		{
			$('#input_filial').hide();
			$('#select_grupo_de_analista_wrapper').hide();
			$('#input_limite_de_credito').hide();
		}
		
	});

	if ( $('#Usuario_tipo').val() == 'empresa_admin' ) {
		$('#input_filial').hide();
	}

});