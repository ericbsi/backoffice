$(document).ready(function() {

    // DataTable
    var oTable = $("#tableReservas").DataTable({
        "sDom": 'T<"clear">lfrtip',
        "retrieve": true,
        "tableTools"     : {
            "sSwfPath"   : "/swf/copy_csv_xls_pdf.swf"
        },
    });

    /*$('#teste').on('click', function () {
        $(document).reload();
    })*/


    var rowCount = $('#tableReservas tr').length;

    if (rowCount > 0) {

        // Setup - add a text input to each footer cell
        $('#tableReservas thead th.searchable').each(function() {
            var title = $('#tableReservas thead th').eq($(this).index()).text();
            $(this).html($(this).html() + '<input style="width:100%" type="text" placeholder="Buscar ' + title + '" />');
        });

        oTable.columns().eq(0).each(function(colIdx) {
            $('input', oTable.column(colIdx).header()).on('keyup', function() {
                oTable.column(colIdx).search(this.value).draw();
            })
        })

    }
});
 
 $(document).on('dblclick', '.td-qtd-gerencia', function () {

    var conteudoOriginal = $(this).text().trim();
    var prod             = $(this).attr("data-rese-cod"); //atraves do atributo item (data), recupera a posicao do item na tabela
    var pBtn             = $('[data-p-btn="' + prod + '"]');
    var tipo             = $(this).attr('[data-td-tipo]');
    var outroTipo        = "";
    var valMax           = 0;
    var cHtml            = "";
    var qtdReq           = $("[data-rese-cod=" + prod + "][data-td-tipo='qtd']").text().trim();

    if (tipo == "efe")
    {
        outroTipo = $("[data-rese-cod=" + prod + "][data-td-tipo='rec']").text().trim();
    }else{
        outroTipo = $("[data-rese-cod=" + prod + "][data-td-tipo='efe']").text().trim();
    }
    
    valMax = (parseFloat(qtdReq) - parseFloat(outroTipo) + parseFloat(conteudoOriginal));

    cHtml += "<input type='number' value='" + conteudoOriginal + "' min='0' max='" + valMax + "' />"

    $(this).html(cHtml);

    $(this).children().first().focus();

    $(this).children().first().keypress(function (e) {

        if (e.which == 13) {

            var novoConteudo = $(this).val();

            if (novoConteudo < 0)
            {
                novoConteudo = '0';
                pBtn.attr('hidden');
            }else if((parseFloat(qtdReq)-(parseFloat(novoConteudo) + parseFloat(outroTipo))) == 0){
                pBtn.removeAttr('hidden');
            }

            $(this).parent().text(novoConteudo);

        }
    });

    $(this).children().first().blur(function () {

        $(this).parent().text(conteudoOriginal);
    });
    
});

//////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 26-12-2014///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.btn-ger-reserva', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var rese   = $(this).attr("data-rese-btn"); //atraves do atributo item (data), recupera a posicao do item na tabela
    var qtdEfe = $('[data-rese-cod="' + rese + '"][data-td-tipo="efe"]').text();
    var qtdRec = $('[data-rese-cod="' + rese + '"][data-td-tipo="rec"]').text();
    
    $.ajax({
        type: 'POST',
        url: '/gerenciarReserva/gravar/',
        data: {
            'rese': rese,
            'qtd': {
                'efe' : parseFloat(qtdEfe),
                'rec' : parseFloat(qtdRec)
            }
        }

    }).done(function (dRt) {
        
/*        var retorno = $.parseJSON(dRt);
        console.log(retorno);*/
        window.location.reload(true);

    });
    
 });