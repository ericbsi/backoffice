$(document).ready(function(){

	$('.btn-edit-att').on('click',function(){
		 $('#iframe_1').attr( 'src', $(this).attr('modal-iframe-uri') );
		 $('#iframe_1').attr( 'height', $(this).attr('modal-iframe-height') );
		 $('#iframe_1').attr( 'width', $(this).attr('modal-iframe-width') );
	})

	$(document.body).on('click','.edit-customer-contact-info',function(){
		$('#iframe_2').attr( 'src', $(this).attr('modal-iframe-uri') );
	})

	$('.edit-customer-contact-add-info').on('click', function(){
		$('#iframe_3').attr( 'src', $(this).attr('modal-iframe-uri') );		
	})

	$('.edit-customer-professional-info').on('click', function(){
		$('#iframe_4').attr( 'src', $(this).attr('modal-iframe-uri') );
	})

	$('.edit-customer-address-info').on('click', function(){
		$('#iframeFormEndereco').attr( 'src', $(this).attr('modal-iframe-uri') );
	});
	
})