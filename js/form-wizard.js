var FormWizard = function () {

    var wizardContent   = $('#wizard');
    var wizardForm      =    $('#form');

    var initWizard = function () {
        
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });

        var numberOfSteps = 0;

        animateBar();

        initValidator();

    };

    var animateBar = function (val) {

        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };

        numberOfSteps = $('.swMain > ul > li').length;
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');

    };

    var initValidator = function () {

        $.validator.setDefaults({
            errorElement: "span", 
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            
            ignore: ':hidden',
            
            rules: {
                
                'DocumentoCliente[numero]':{
                    required: true
                },
                'Cadastro[nome_da_mae]': {
                    required: true
                },
                'Pessoa[nascimento]': {
                    required: true
                },
                'EnderecoCliente[cep]':{
                    required: true
                },
                'EnderecoCliente[logradouro]':{
                    required: true
                },
                'EnderecoCliente[cidade]':{
                    required: true
                },
                'EnderecoCliente[bairro]':{
                    required: true
                },
                'EnderecoCliente[tempo_de_residencia]':{
                    required: true
                },
                'EnderecoCliente[numero]':{
                    required: true  
                },
                'TelefoneCliente[numero]':{
                    required: true
                },
            
                'Conjuge[nome]':{
                    required : function (){
                        return $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }
                },
                'DadosProfissionaisConjuge[renda_liquida]':{
                    required : function (){
                        return $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }
                },
                'DadosProfissionaisConjuge[mes_ano_renda]':{
                    required : function (){
                        return $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }
                },
                'CPF_Conjuge[numero]':{
                    required : function (){
                        return $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }
                },
                'EnderecoConjuge[cep]':{
                    required : function (){
                        return $('#MesmoEndereco').val() == '0' && $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }
                },
                'EnderecoConjuge[logradouro]':{
                    required : function (){
                        return $('#MesmoEndereco').val() == '0' && $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }  
                },
                'EnderecoConjuge[cidade]':{
                    required : function (){
                        return $('#MesmoEndereco').val() == '0' && $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }  
                },
                'EnderecoConjuge[bairro]':{
                    required : function (){
                        return $('#MesmoEndereco').val() == '0' && $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }  
                },
                'EnderecoConjuge[numero]':{
                    required : function (){
                        return $('#MesmoEndereco').val() == '0' && $('#Cadastro_conjugue_compoe_renda').val() == '1';
                    }  
                },
                'DadosProfissionais[numero_do_beneficio]':{
                    required : function(){
                        return $('#DadosProfissionais_Classe_Profissional_id').val() == '1'
                    }
                },
                'TelefoneCliente[numero]' : {
                    required : true
                },
                'Referencia1[nome]' : {
                    required : true
                },
                'TelefoneRef1[numero]' : {
                    required : true
                },                
                'DadosProfissionais[cnpj_cpf]' : {
                    required : true
                }
            },
            
            messages: {
                'Conjuge[nome]': "Campo obrigatório, caso cônjuge componha a renda",
                'DadosProfissionaisConjuge[renda_liquida]': "Campo obrigatório, caso cônjuge componha a renda",
                'DadosProfissionaisConjuge[mes_ano_renda]': "Campo obrigatório, caso cônjuge componha a renda",
                'CPF_Conjuge[numero]': "Campo obrigatório, caso cônjuge componha a renda",
                'EnderecoConjuge[cep]':"Campo obrigatório, caso use endereço próprio",
                'EnderecoConjuge[logradouro]':"Campo obrigatório, caso use endereço próprio",
                'EnderecoConjuge[cidade]':"Campo obrigatório, caso use endereço próprio",
                'EnderecoConjuge[bairro]':"Campo obrigatório, caso use endereço próprio",
                'EnderecoConjuge[numero]':"Campo obrigatório, caso use endereço próprio",
                'DadosProfissionais[numero_do_beneficio]' : "Campo obrigatório para Aposentados/Pensionistas"
            },
            
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    
    var displayConfirm = function () {
        $('.display-value', form).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', form);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {
                $(this).html(input.filter(":checked").parent('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
            }
        });
    };
    
    var onShowStep = function (obj, context) {
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
    };
    
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
   
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('selected').addClass('done');
            //wizardForm.submit();
        }
    };
   
    var validateSteps = function (stepnumber, nextstep) {

        var isStepValid = false;
        
        if (numberOfSteps !== nextstep) {
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else {
            displayConfirm();
            animateBar(nextstep);
            return true;
        };
    };
    
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
   
    return {
        init: function () {
            initWizard();
        }
    };
}();