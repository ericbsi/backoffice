$(document).ready(function(){

	var form1 = $('#empresaForm');

	var errorHandler1 = $('.errorHandler', form1);
        
  var successHandler1 = $('.successHandler', form1);


    jQuery.validator.addMethod("cnpj", function(cnpj, element) {

  	  cnpj = jQuery.trim(cnpj);// retira espaços em branco
  	  // DEIXA APENAS OS NÚMEROS
  		cnpj = cnpj.replace('/','');
  		cnpj = cnpj.replace('.','');
  		cnpj = cnpj.replace('.','');
  		cnpj = cnpj.replace('-','');

  		var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
     	
      digitos_iguais = 1;

   		if (cnpj.length < 14 && cnpj.length < 15){
      		return false;
   		}

   		for (i = 0; i < cnpj.length - 1; i++){
	      if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
	         digitos_iguais = 0;
	         break;
	      }
   		}

   	 if (!digitos_iguais){
		    
		    tamanho = cnpj.length - 2
		    numeros = cnpj.substring(0,tamanho);
		    digitos = cnpj.substring(tamanho);
		    soma = 0;
		    pos = tamanho - 7;

		    for (i = tamanho; i >= 1; i--){
		    	
		    	soma += numeros.charAt(tamanho - i) * pos--;
		    	
		    	if (pos < 2){
            		pos = 9;
         		}
		    }

		    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		    
		    if (resultado != digitos.charAt(0)){
         		return false;
      		}

      		tamanho = tamanho + 1;
      		numeros = cnpj.substring(0,tamanho);
      		soma = 0;
      		pos = tamanho - 7;

      		for (i = tamanho; i >= 1; i--){

      			soma += numeros.charAt(tamanho - i) * pos--;

      			if (pos < 2){
            		pos = 9;
         		}
      		}

      		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

      		if (resultado != digitos.charAt(1)){
         		return false;
      		}
      		return true;
 		 }

     else{
 		     return false;
 		 }

	 }, "Informe um CNPJ válido.");

	$('#empresaForm').validate({

		errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
        	if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
            } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
            }
        },
       
        ignore: "",

		rules : {
			'Empresa[nome_fantasia]' :{
				required : true
			},
			'Empresa[cnpj]': {
				required: true,
				cnpj: true
			},
			'Empresa[inscricao_estadual]': {
				required: true
			},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
    },
    highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
                // display OK icon
        	$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
    },
            
    unhighlight: function (element) { 
        	// revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
    },
    success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
    },
	});

});