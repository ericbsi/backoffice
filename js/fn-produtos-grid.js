$(document).ready(function() {

    $('.multipleselect').multiselect({

        buttonWidth                 : '480px',
        numberDisplayed             : 2,
        enableFiltering             : true,
        includeSelectAllOption      : true,
        selectAllText               : "Selecionar todos",
        selectAllValue              : '0',

        buttonText:function(options, select){

             if (options.length == 0) {
                return 'Selecione <b class="caret"></b>'
             }

             else if (options.length == 1) {
                return '1 Selecionado(a) <b class="caret"></b>'
             }

             else{
                return options.length + ' Selecionados(as) <b class="caret"></b>'
             }
        },
    });

    jQuery.fn.clearFormInputs = function(form){
        return form.find("input[type=text]").val("");
    }

    var selectCategorias    = $(".selectCategorias").select2({
        placeholder         : "Selecione uma categoria...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/categoriaProduto/searchCategorias/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {
                        q           : term,
                        showPai     : $(this).data('show-no-father'),
                };
            },
            results         : function(data){
                return {results:data};
            }
        }
    });
    
    var selectNcms          = $("#Produto_NCM_id").select2({
        placeholder         : "Selecione um ncm...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/ncm/searchNcm/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {q   : term};
            },
            results         : function(data){
                return {results:data};
            }
        }
    });

    var selectMarcas        = $("#Modelo_marca_id").select2({
        placeholder         : "Selecione uma marca...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/marca/searchMarcas/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {q   : term};
            },
            results         : function(data){
                return {results:data};
            }
        }
    });

    var selectModelos       = $("#Produto_Modelo_id").select2({
        placeholder         : "Selecione uma marca...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/modelo/searchModelo/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {q   : term};
            },
            results         : function(data){
                return {results:data};
            }
        }
    });

    var selectFabricantes   = $("#Marca_Fabricante_id").select2({
        placeholder         : "Selecione uma marca...",
        minimumInputLength  : 1,
        formatSearching     : function(){
            return "Pesquisando..."
        },
        ajax                : {
            url             : '/fabricante/searchFabricantes/',
            json            : 'jsonp',
            type            : 'post',
            data            : function(term){
                return {q   : term};
            },
            results         : function(data){
                return {results:data};
            }
        }
    });

    $('#btn_modal_form_new_produto').on('click', function() {
        $("#cadastro_msg_return").hide().empty();
    })

    var produtosTable = $('#grid_produtos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/produto/getProdutos',
            type: 'GET',
        },
        "columns": [
            {"data": "descricao"},
            {"data": "tipo"},
            /*{"data": "categoria"},
            {"data": "modelo"},*/
            {"data": "btn"}
        ],
        "columnDefs": [
            {
                "orderable": false,
                "targets": "no-orderable"
            }
        ],
    });

    $('#grid_produtos tfoot th.searchable').each(function() {
        var title = $('#grid_produtos thead th').eq($(this).index()).text();
        $(this).html('<input style="width:100%" type="text" placeholder="Buscar ' + title + '" />');
    });

    produtosTable.columns().eq(0).each(function(colIdx) {
        $('input', produtosTable.column(colIdx).footer()).on('change', function() {
            produtosTable.column(colIdx).search(this.value).draw();
        })
    })

    $('#grid_produtos_wrapper .dataTables_length select').select2();
    $('.select2').select2();

    /*
     var mySelect = $("#selectCidades").select2({
     data:[{id:0,text:'Carregando..'}],
     placeholder: "Selecione uma cidade...",
     });
     
     $.ajax({
     
     type  :  'GET',
     url   :  '/cidade/listCidades/',        
     })
     
     .done(function(dRt){
     var jsonReturn = $.parseJSON(dRt);
     mySelect.select2({
     data:jsonReturn 
     })
     })
     
     $('#Pessoa_naturalidade').on('change',function(){
     $.ajax({
     
     type  :  'GET',
     url   :  '/cidade/listCidades/',
     data  :  {
     uf : $(this).val()
     },
     beforeSend : function(){
     
     $('.panel').block({
     overlayCSS: {
     backgroundColor: '#fff'
     },
     message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Listando cidades do ' + $('#Pessoa_naturalidade').val(),
     css: {
     border: 'none',
     color: '#333',
     background: 'none'
     }
     });
     window.setTimeout(function () {
     $('.panel').unblock();
     }, 1000);
     }
     })
     
     .done(function(dRt){
     
     var jsonReturn = $.parseJSON(dRt);
     
     mySelect.select2({
     data:jsonReturn
     });
     })
     })*/

    /*Validação formulário*/
    $.validator.setDefaults({

        ignore: ':hidden:not(".multipleselect")',
        ignore: null,
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function(element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function(element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function(label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-produto").validate({

        ignore: null,
    
        submitHandler: function() {

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-produto').serialize();

            $.ajax({
                
                type: "POST",
                url: "/produto/add/",
                data: formData,

                beforeSend: function() {

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando produto...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function() {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt) {

                //console.log(dRt);

                $('#form-add-produto').trigger("reset");

                $('.has-success').each(function(index) {
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index) {
                    $(this).removeClass('ok');
                })

                $("#cadastro_msg_return").prepend('<p>Produto cadastrado com suscesso!</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                
                $('#modal_form_new_produto').modal('hide');
                
                $('#form-add-produto').clearFormInputs($('#form-add-produto'));

                produtosTable.draw();
            })

            return false;
        }
    });
    
    $('#Produto_Categoria_id').on('change',function(){

        var fieldProdutoDesc = $('#produto_descricao');
        var desc             = '';
        
        if ( $('#Produto_Modelo_id').select2('data').text.length > 0 && $(this).select2('data').text.length > 0 )
            fieldProdutoDesc.attr('value', $.trim( $(this).select2('data').text ) + ' ' +$.trim($('#Produto_Modelo_id').select2('data').text) )
        else
            fieldProdutoDesc.attr('value','')
    })

    $('#Produto_Modelo_id').on('change',function(){
        
        var fieldProdutoDesc = $('#produto_descricao');
    
        if ( $('#Produto_Categoria_id').select2('data').text.length > 0 && $(this).select2('data').text.length > 0 )
            fieldProdutoDesc.attr('value', $.trim( $('#Produto_Categoria_id').select2('data').text ) + ' ' + $.trim($(this).select2('data').text) )
        else
            fieldProdutoDesc.attr('value','');
    });

    $('#form-add-categoria-produto').validate({
        submitHandler : function(){

            var formData = $('#form-add-categoria-produto').serialize();

            $('#cadastro_categorias_msg_return').hide().empty();
            
            $.ajax({
                
                type    : "POST",
                url     : '/categoriaProduto/add',
                data    : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                            
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);        
                },

            }).done(function(dRt){

                var retorno = $.parseJSON(dRt);
                
                if( !$('#categorias_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.classNtfy
                    });

                    $('#modal_form_new_categoria').modal('hide');
                }
                else
                {
                    $('#cadastro_categorias_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-categoria-produto').clearFormInputs($('#form-add-categoria-produto'));
            });

            return false;
        }
    });
    
    $('#form-add-ncm').validate({
        submitHandler : function(){

            var formData = $('#form-add-ncm').serialize();

            $('#cadastro_ncm_msg_return').hide().empty();
            
            $.ajax({
                
                type    : "POST",
                url     : '/ncm/add',
                data    : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                            
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);        
                },

            }).done(function(dRt){

                var retorno = $.parseJSON(dRt);
                
                if( !$('#ncms_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.classNtfy
                    });

                    $('#modal_form_new_ncm').modal('hide');
                }

                else
                {
                    $('#cadastro_ncm_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-ncm').clearFormInputs($('#form-add-ncm'));
            });

            return false;
        }
    });
    
    $('#form-add-modelo').validate({

        submitHandler : function(){

            var formData = $('#form-add-modelo').serialize();

            $('#cadastro_modelo_msg_return').hide().empty();
            
            $.ajax({
                
                type    : "POST",
                url     : '/modelo/add/',
                data    : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                            
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);        
                },

            }).done(function(dRt){

                var retorno = $.parseJSON(dRt);
                
                //console.log(retorno);

                if( !$('#modelo_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.classNtfy
                    });

                    $('#modal_form_new_modelo').modal('hide');
                }

                else
                {
                    $('#cadastro_modelo_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-modelo').clearFormInputs($('#form-add-modelo'));
            });

            return false;
        }
    });
    
    $('#form-add-marca').validate({

        submitHandler : function(){

            var formData = $('#form-add-marca').serialize();

            $('#cadastro_marca_msg_return').hide().empty();
            
            $.ajax({
                
                type    : "POST",
                url     : '/marca/add/',
                data    : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                            
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);        
                },

            }).done(function(dRt){

                var retorno = $.parseJSON(dRt);
                
                //console.log(retorno);

                if( !$('#marca_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.classNtfy
                    });

                    $('#modal_form_new_marca').modal('hide');
                }

                else
                {
                    $('#cadastro_marca_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-marca').clearFormInputs($('#form-add-marca'));

            });

            return false;
        }
    });

})