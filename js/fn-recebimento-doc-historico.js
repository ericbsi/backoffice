$(function(){

    $('#filiais_select').multiselect({

      buttonWidth                     : '400px',
      enableFiltering                 : true,
      enableCaseInsensitiveFiltering  : true,      

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Parceiros <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Parceiro Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Parceiros Selecionados <b class="caret"></b>'
         }
      },
    });
  
  	var tableRecebimentosDeContratos = $('#grid_historico_recebimentos_contratos').DataTable({
  		
          "processing": true,
          "serverSide": true,
          
          "ajax": {
              url: '/recebimentoDeDocumentacao/getRecebimentos/',
              type: 'POST',
              /*"data": function (d) {
                  d.filiais    = $("#filiais_select").val(),
                  d.data_de    = $("#data_de").val(),
                  d.data_ate	 = $("#data_ate").val()
              },*/
              "data" : function(d)
              {
                d.filial      = $('#filiais_select').val(),
                d.data_de    = $("#data_de").val(),
                d.data_ate   = $("#data_ate").val()
              }
          },
          
          "language": {
              "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
          },
          
          "columnDefs": [{
              "orderable": false,
              "targets": "no-orderable"
          }],
          
          "columns": [
              {"data" :"codigo"},
              {"data" :"data_criacao"},
              {"data" :"operador"},
              {"data" :"parceiro"},
              {"data" :"count_aprovadas"},
              {"data" :"count_pendentes"},
              {"data" :"total_aprovado"},
              {"data" :"total_pendente"},
              {"data" :"repasse_aprovado"},
              {"data" :"repasse_pendente"},
              {"data" :"btn_print"}
          ],
          "drawCallback" : function(settings) {
            $('#th-total').html( settings.json.customReturn.totalRepasse );
          }
  	});
	
    $('#btn-filter').on('click',function(){
      	tableRecebimentosDeContratos.draw();
      	return false;
    });
});