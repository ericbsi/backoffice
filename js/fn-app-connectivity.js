//AppConnectivityStatus

$(function(){

	var uiBlocked = false;

	window.setInterval(function(){
		
		var request = $.ajax({
			cache 	: false,
			type  	: 'GET',
			url   	: '/app/appConnectivityStatus/',
			timeout : 1000,
		});

		request.fail(function( jqXHR, textStatus ){

			if( !uiBlocked )
			{	
				uiBlocked = true;
				$.bloquearInterface('<h4>Problemas com a conexão... Aguarde...</h4>');
			}

		});

		request.done(function(dRt,textStatus){
			var retorno = $.parseJSON(dRt);
			
			console.log(textStatus);

			if( retorno.requestStatus != 'alive' )
			{
				if( !uiBlocked )
				{	
					uiBlocked = true;
					$.bloquearInterface('<h4>Problemas com a conexão... Aguarde...</h4>');
				}
			}
			else
			{
				if( uiBlocked )
				{	
					uiBlocked = false;
					$.desbloquearInterface();
				}	
			}
		});

	},2000);

	/*$.blockUI({
	   message: 'Mensagem',
	   css: { 
	        borderColor: '#d6e9c6', 
	        padding: '15px', 
	        backgroundColor: '#dff0d8', 
	        '-webkit-border-radius': '4px', 
	        '-moz-border-radius': '4px', 
	        opacity: .5, 
	    	color: '#3c763d' 
	    } 
	});*/

	

});
