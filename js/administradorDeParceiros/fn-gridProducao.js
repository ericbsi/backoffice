$(document).ready(function()
{
    
    var selectFiliais = $('#selectFiliais').multiselect(
    {
        buttonWidth                     : '300px'   ,
        numberDisplayed                 : 2         ,
        enableFiltering                 : true      ,
        enableCaseInsensitiveFiltering  : true      ,
        buttonText: function (options, select) 
        {

            if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
            }
            
        },
    });
    
    var selectNucleoFiliais = $('#nucleoFiliaisSelect').multiselect(
    {
        buttonWidth                     : '300px'   ,
        numberDisplayed                 : 2         ,
        enableFiltering                 : true      ,
        enableCaseInsensitiveFiltering  : true      ,
        buttonText: function (options, select) 
        {

            if (options.length == 0) {
                return 'Selecionar Núcleo de Filiais <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Núcleo Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Núcleos Selecionados <b class="caret"></b>'
            }
            
        },
    });
    
    var selectGrupoFiliais = $('#selectGrupoFiliais').multiselect(
    {
        buttonWidth                     : '300px'   ,
        numberDisplayed                 : 2         ,
        enableFiltering                 : true      ,
        enableCaseInsensitiveFiltering  : true      ,
        buttonText: function (options, select) 
        {

            if (options.length == 0) {
                return 'Selecionar Grupo de Filiais <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Grupo Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Grupo Selecionados <b class="caret"></b>'
            }
            
        },
    });
    
    selectNucleoFiliais.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getGrupoFilialUsuario/"   ,
            
            data: 
            {
                "idNucleos"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                selectFiliais       .val(retorno.dataFiliais    );
                
                selectGrupoFiliais  .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        })
        
    });
    
    selectGrupoFiliais.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getNucleoFilialUsuario/"   ,
            
            data: 
            {
                "idGrupos"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos );
                selectFiliais       .val(retorno.dataFiliais );
                
                selectNucleoFiliais .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        })
        
    });
    
    selectFiliais.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                            ,
            url     : "/grupoFiliais/getFilialUsuario/" ,
            
            data: 
            {
                "idFiliais"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos    );
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                
                selectNucleoFiliais .multiselect("refresh");
                selectGrupoFiliais  .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        })
        
    });
    
});