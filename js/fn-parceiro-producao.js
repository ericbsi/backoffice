$(function(){

	$.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });
	
	$('#grid_producao').DataTable({

		"columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
      	}],

	});

	$('#form-filter').validate();


	jQuery.fn.checkAll = function (){

        return this.each(function(){

            $(".check_print").each(function(){

                var inputHdn = $('#proposta_'+$(this).val());

                if( inputHdn.length == 0)
                {
                    $('#form-print').append('<input id="proposta_'+$(this).val()+'" type="hidden" name="idsPropostas[]" value="' + $(this).val() + '">');
                }
            })

            $(".check_print").prop('checked', true);
        })
    }

    jQuery.fn.unCheckAll = function (){

        return this.each(function(){

            $(".check_print").each(function(){

                var inputHdnId = 'proposta_'+$(this).val();

                if( $('#'+inputHdnId).length != 0)
                {
                    $('#'+inputHdnId).remove();
                    
                }
            });

            $(".check_print").prop('checked', false);
        });

    }

	$('.check_print').on('change',function(){
        
        var ipthdnId  = 'proposta_'+$(this).val();

        if( this.checked ){
            
            if( $('#'+ipthdnId).length == 0 )
            {
                $('#form-print').append('<input id="'+ipthdnId+'" type="hidden" name="idsPropostas[]" value="' + $(this).val() + '">');
            }
        }

        else
        {
            if( $('#'+ipthdnId).length != 0 )
            {
                $('#'+ipthdnId).remove();
            }   
        }
    });

	$('#check_select_all').on('change',function(){
        this.checked ? $(this).checkAll() : $(this).unCheckAll();
    });

    $('#btn-print-rel').on('click',function(){
        
        $('#dataDeHdn').attr('value', $('#data_de').val() );
        $('#dataAteHdn').attr('value', $('#data_ate').val() );

        $('#form-print').submit();
        return false;
    });

});