$(function(){

	$('.select2').select2();

    /*$('.multipleselect').multiselect({

        buttonWidth: '200px',
        numberDisplayed:2,

        buttonText:function(options, select){

             if (options.length == 0) {
                return 'Filtrar <b class="caret"></b>'
             }

             else if (options.length == 1) {
                return '1 Selecionado(a) <b class="caret"></b>'
             }

             else{
                return options.length + ' Selecionados(as) <b class="caret"></b>'
             }

        },
    });*/

    var gridFabricantes = $('#grid_fabricantes').DataTable({
		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/fabricante/getFabricantes/',
            type: 'POST',
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            {"data": "fabricante"},
            {"data": "cnpj"},
            {"data": "btn"},
        ],
        /*"drawCallback" : function(settings) {
            console.log(settings.json);
        }*/
	});

    /*Validar cnpj*/
    jQuery.validator.addMethod("cnpj", function(cnpj, element) {

          cnpj = jQuery.trim(cnpj);// retira espaços em branco
          // DEIXA APENAS OS NÚMEROS
            cnpj = cnpj.replace('/','');
            cnpj = cnpj.replace('.','');
            cnpj = cnpj.replace('.','');
            cnpj = cnpj.replace('-','');

            var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
            
          digitos_iguais = 1;

            if (cnpj.length < 14 && cnpj.length < 15){
                return false;
            }

            for (i = 0; i < cnpj.length - 1; i++){
              if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
                 digitos_iguais = 0;
                 break;
              }
            }

         if (!digitos_iguais){
                
                tamanho = cnpj.length - 2
                numeros = cnpj.substring(0,tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;

                for (i = tamanho; i >= 1; i--){
                    
                    soma += numeros.charAt(tamanho - i) * pos--;
                    
                    if (pos < 2){
                        pos = 9;
                    }
                }

                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                
                if (resultado != digitos.charAt(0)){
                    return false;
                }

                tamanho = tamanho + 1;
                numeros = cnpj.substring(0,tamanho);
                soma = 0;
                pos = tamanho - 7;

                for (i = tamanho; i >= 1; i--){

                    soma += numeros.charAt(tamanho - i) * pos--;

                    if (pos < 2){
                        pos = 9;
                    }
                }

                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

                if (resultado != digitos.charAt(1)){
                    return false;
                }
                return true;
             }

         else{
                 return false;
             }

    }, "Informe um CNPJ válido.");

    /*Validação formulário*/
    $.validator.setDefaults({
        
        errorElement: "span", 
        
        errorClass: 'help-block',
        
        rules: {                
            'Fabricante[cnpj]' : {
                required : true,
                cnpj : true,
            },
        },

        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-fabricante").validate({

        submitHandler   :   function(){

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-fabricante').serialize();

            $.ajax({
                
                type        : "POST",
                url         : "/fabricante/add/",
                data        : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://s1.sigacbr.com.br/js/loading.gif" /> Salvando estoque...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt){

                var retorno = $.parseJSON( dRt );
                
                if( !$('#checkbox_continuar').is(':checked') )
                {
                    $('#modal_form_new_fabricante').modal('hide');
                    $('#form-add-fabricante').trigger("reset");

                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });
                }

                else
                {
                    $("#cadastro_msg_return").prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }
                
                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                })
                
                $('#form-add-fabricante').trigger("reset");

                gridFabricantes.draw();
            })

            return false;
        }
    });
});