var grid_reanalises = $('#grid_reanalises').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        url: '/reanalise/ultimasPropostas/',
        type: 'POST'
    },

    "columns": [
        {"data": "btn-more"         }  ,
        {"data": "codigo"           }  ,
        {"data": "cliente"          }  ,
        {"data": "filial"           }  ,
        {"data": "politicaCredito"  }  ,
        {"data": "valor"            }  ,
        {"data": "qtd_parcelas"     }  ,
        {"data": "btnAtualizacoes"  }  ,
        {"data": "btn"              }  ,
        {"data": "data_cadastro"    }
    ],
    "drawCallback": function (settings) {
        setTimeout(function () {
            grid_reanalises.draw();
        }, 15000)
    }
});