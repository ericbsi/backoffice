function format ( d ) {
    return $.each(d.moreDetails, function(index)
    {

    });
}

var grid_reanalises = $('#grid_reanalises').DataTable({

    "processing": true,
    "serverSide": true,
    "ajax": {
        url: '/reanalise/getPropostasNegadas/',
        type: 'POST'
    },
    "columns":[
        { 
            "class"                 :"details-control",
            "orderable"             : false,
            "data"                  : null,
            "defaultContent"        : ""
        },
        {"data": "codigo"           }   ,
        {"data": "cliente"          }   ,
        {"data": "dprofissa"        }   ,
        {"data": "filial"           }   ,
        {"data": "valor"            }   ,
        {"data": "qtd_parcelas"     }   ,
        {"data": "btn"              }   ,
        {"data": "data_cadastro"    }
    ]    
});

var detailRows = [];

$('#grid_reanalises tbody').on('click', 'tr td:first-child', function(){

    var tr      = $(this).closest('tr');
    var row     = grid_reanalises.row( tr );
    var idx     = $.inArray( tr.attr('id'), detailRows );

    if ( row.child.isShown() ) 
    {
        tr.removeClass( 'details' );
        row.child.hide();
        detailRows.splice( idx, 1 );
    }

    else 
    {
        tr.addClass( 'details' );
        row.child( format( row.data() ) ).show();
            
        if ( idx === -1 )
        {
            detailRows.push( tr.attr('id') );
        }
    }

});

grid_reanalises.on('draw', function(){

    $.each(detailRows, function ( i, id ) {
        $('#'+id+' td:first-child').trigger( 'click' );
    });

});

$(document).on('click','.exibir-dados-profissionais', function(){
    
    var get     = $.ajax({
        type            :  'GET',
        url             :  '/dadosProfissionais/load/',
        data            :  {
            entity_id   : $(this).data('dados')
        },
        beforeSend : function()
        {
            $.bloquearInterface('<h4>Buscando Dados...</h4>');
        }
    });

    get.done(function(dRt)
    {
        $('#td_empresa').text($.parseJSON(dRt).grid.empresa);
        $('#td_renda').text($.parseJSON(dRt).grid.renda);
        $('#td_admissao').text($.parseJSON(dRt).grid.admissao);
        $('#td_mes_ano_renda').text($.parseJSON(dRt).grid.mes_ano_renda);
        $('#td_profissao').text($.parseJSON(dRt).grid.profissao);
        $('#modal_dados_profissionais').modal('show');

        $.desbloquearInterface();
    });

    return false;
});