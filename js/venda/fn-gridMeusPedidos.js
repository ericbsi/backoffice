$(document).ready(function ()
{

   var resgatesTable = $('#grid_resgates').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 "url": '/venda/gridResgates',
                 "type": 'POST'

              },
      "columnDefs":
              [
                 {
                    "orderable": false,
                    "targets": "no-orderable"
                 }
              ],
      "columns":
              [
                 {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                 },
                 {
                    "data": "codigo"
                 },
                 {
                    "data": "dataResgate"
                 },
                 {
                    "data": "totalPontos"
                 }
              ]
   });

   // Add event listener for opening and closing details
   $('#grid_resgates tbody').on('click', 'td.details-control', function () {

      var tr = $(this).closest('tr');
      var row = resgatesTable.row(tr);
      var rowsTam = resgatesTable.rows()[0].length;

      if (row.child.isShown()) {

         // This row is already open - close it
         row.child.hide();
         tr.removeClass('shown');
      }
      else {

         for (i = 0; i < rowsTam; i++)
         {

            if (resgatesTable.row(i).child.isShown()) {
               resgatesTable.row(i).child.hide();
            }
         }

         $('td.details-control').closest('tr').removeClass('shown');

         formatSubTable(row.data(), tr, row);

      }
   });
 /*  
   $(document).on('click', '.btnAcao', function ()
   {
      
      var elemento = $(this);

      $.ajax({
         type: "POST",
         url: "/venda/gerarOrdemEntrega/",
         data: {
            'idItemVenda'  : $(this).val()   ,
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify(
                 {
                    title: retorno.title,
                    text: retorno.msg,
                    type: retorno.type
                 }
         );
 
         if(!retorno.hasErrors)
         {
            elemento.removeClass('btn-green');
            elemento.addClass('btn-red');
            elemento.attr('disabled','disabled');
         }
 
      })

   })
*/
   
   $(document).on('keyup', '#inputMensagem', function()
   {
      
      if($.trim($(this).val()) !== "")
      {
         $('#enviarMensagem').removeAttr('disabled');
      }
      else
      {
         $('#enviarMensagem').attr('disabled','disabled');
      }
      
   })

   $(document).on('click','.btnCancelar',function()
   {
      
      if(!$(this).hasClass('disabled'))
      {
         if(confirm('Você tem certeza de que deseja excluir esse item?'))
         {
         
         $.ajax(
                  {
                     type  : "POST",
                     url   : "/venda/cancelarResgate",
                     data  : 
                              {
                                 "idItemVenda"   : $(this).attr("value")
                              },
         }).done(function (dRt)
         {

            var retorno = $.parseJSON(dRt);
           
            $.pnotify(
                    {
                       title  : retorno.titulo     ,
                       text   : retorno.msg        ,
                       type   : retorno.pntfyClass
                    }
            );
    
            if(!retorno.hasErrors)
            {
               resgatesTable.draw();
            }

         });
         
      }
      }
      
   });
   
   $(document).on('click','#enviarMensagem',function()
   {
        enviarMensagem($('#inputMensagem').val(),$('#enviarMensagem').val());
   })
   
   function enviarMensagem(mensagem,idVenda)
   {
       
        $.ajax(
        {
          type  :   "POST"                  ,
          url   :   "/venda/enviarMensagem" ,
          data  :   {
                        "mensagem"  : mensagem  ,
                        "idVenda"   : idVenda
                    },
        }).done(function (dRt)
         {

            var retorno = $.parseJSON(dRt);

            $.pnotify
            (
                {
                   text : retorno.msgReturn     ,
                   type : retorno.classNotify
                }
            );
    
            if(!retorno.hasErrors)
            {
                resgatesTable.draw();
            }
             
         });
       
   }

})

function formatSubTable(d, tr, row) {

   $.ajax(
           {
              type: "POST",
              url: "/venda/getInformacoesResgate",
              data: {
                 "idVenda": d.idVenda
              },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');

      // `d` is the original data object for the row
      var data =  '<h5><i class="clip-list"></i> Itens do Pedido</h5>' +
                  '<div class="row">' +
                  '   <div  class="col-sm-12">' +
                  '       <table class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                  '           <thead>' +
                  '              <tr>' +
                  '                 <th class="no-orderable" width="30px"></th>' +
                  '                 <th class="no-orderable"            >Produto</th>' +
                  '                 <th class="no-orderable"            >Quantidade</th>' +
                  '                 <th class="no-orderable"            >Status</th>' +
                  '                 <th class="no-orderable"            >Pontos</th>' +
                  '              </tr>' +
                  '           </thead>' +
                  '           <tbody>';
                  
                  $.each(retorno.dados.itensVenda, function(indice, valor)
                  {
                     data +=
                             
                     '              <tr>' + 
                     '                 <th>' + 
                                             valor.imgItemCarrinho +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.descProduto +
                     '                 </th>' +
                     '                 <th>' + 
                                             valor.quantidade +
                     '                 </th>' +
                     '                 <th>' +
                                             valor.status +
                     '                 </th>' +
                     '                 <th>' +
                                             valor.valor +
                     '                 </th>' +
                     '              </tr>';
                     
                     if(valor.statusId < 4)
                     {
                        data += 
                        
                        '              <tr>' +
                        '                 <td colspan="5">' +
                        '                    <div class="swMain">' +
                        '                       <ul class="anchor">' +
                        '                          <li>' +
                        '                             <a value="' + valor.idItemVenda + '" ';

                        if(valor.statusId == 0)
                        {
                           data += 'class="selected        btnCancelar"  isdone="1" rel="1" href="#" >';
                        }
                        else if(valor.statusId == 1)
                        {
                           data += 'class="          done  btnCancelar"  isdone="1" rel="1" href="#" >';
                        }
                        else if(valor.statusId > 1)
                        {
                           data += 'class="disabled  done  btnCancelar"  isdone="1" rel="1">';
                        }
                        else
                        {
                           data += 'class="disabled         btnCancelar"  isdone="0" rel="1">';
                        }

                        data+=

                        '                                <div class="stepNumber">' +
                        '                                   1' +
                        '                                </div>' +
                        '                                <span class="stepDesc"> '+
                        '                                  Aguardando Aprovação' +
                        '                                   <br>' +
                        '                                   <small style="color : red">' +
                        '                                      <i class="clip-cancel-circle"> ' +
                        '                                      </i>' +
                        '                                         Cancelar Resgate' + 
                        '                                   </small>' + 
                        '                                </span>' +
                        '                             </a>' +
                        '                          </li>' +
                        '                          <li>' +
                        '                            <a ';

                        if(valor.statusId == 1)
                        {
                           data += 'class="selected         btnCancelar"  isdone="1" rel="2" href="#" >';
                        }
                        else if(valor.statusId > 1)
                        {
                           data += 'class="disabled   done  btnCancelar"  isdone="1" rel="2" >';
                        }
                        else
                        {
                           data += 'class="disabled         btnCancelar"  isdone="0" rel="2">';
                        }

                        data+= 

                        '                                <div class="stepNumber">' +
                        '                                   2' +
                        '                                </div>' +
                        '                                <span class="stepDesc"> ' +
                        '                                   Em Separação no Estoque' +
                        '                                   <br>' +
                        '                                   <small style="color : red">' +
                        '                                      <i class="clip-cancel-circle"> ' +
                        '                                      </i>' +
                        '                                         Cancelar Resgate' +
                        '                                  </small>' + 
                        '                                </span>' +
                        '                             </a>' +
                        '                          </li>' +
                        '                          <li>' +
                        '                          <a ';
                        
                        if(valor.statusId == 2 || valor.statusId == 3)
                        {
                           data += 'href="http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=' +
                                                            valor.rastreamento + '" target="blank" '; 
                        }
                                                 

                        if(valor.statusId == 2)
                        {
                           data += 'class="           done  btnRastreamento"  isdone="1" rel="3">';
                        }
                        else if(valor.statusId > 2)
                        {
                           data += 'class="disabled   done  btnRastreamento"  isdone="1" rel="3">';
                        }
                        else
                        {
                           data += 'class="disabled         btnRastreamento"  isdone="0" rel="3">';
                        }

                        data +=
                        '                             <div class="stepNumber">' +
                        '                                3' +
                        '                             </div>' +
                        '                            <span class="stepDesc">' + 
                        '                                Produto Despachado' +
                        '                                <br>' +
                        '                                <small style="color : blue">' +
                        '                                   <i class="clip-search"> ' +
                        '                                   </i>' +
                        '                                   Rastrear Entrega' +
                        '                                </small>' + 
                        '                             </span>' +
                        '                          </a>' +
                        '                      </li>' +
                        '                       <li>' +
                        '                          <a  '; 
                        
                        if(valor.statusId == 3)
                        {
                           data += 'href="#" ';
                        }

                        if(valor.statusId == 3)
                        {
                           data += 'class="done       btnRecebido"   isdone="1" rel="4">';
                        }
                        else if(valor.statusId == 2)
                        {
                           data += 'class="           btnRecebido"   isdone="1" rel="4">';
                        }
                        else
                        {
                           data += 'class="disabled   btnRecebido"   isdone="0" rel="4">';
                        }

                        data +=

                        '                             <div class="stepNumber">' +
                        '                                4' +
                        '                             </div>' +
                        '                             <span class="stepDesc">' +
                        '                                Produto Entregue' +
                        '                                <br>' +
                        '                                <small style="color : green">' +
                        '                                   <i class="clip-thumbs-up"> ' +
                        '                                   </i>' +
                        '                                   Confirmar Recebimento' +
                        '                                </small>' + 
                        '                            </span>' +
                        '                          </a>' +
                        '                       </li>' +
                        '                    </ul>' +
                        '                 </div>' +
                        '              </td>' +
                        '           </tr>';
                     }
                  });
                  
                  data +=
                          
                  '           </tbody>' +
                  '       </table>' +
                  '   </div>' +
                  '</div>' +
                  '<br>' +
                  '<h5><i class="clip-envelope"></i> Mensagens</h5>' +
                  '<div class="row">' +
                  '   <div  class="col-sm-12">' +
                  '     <table class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                  '         <thead>' +
                  '             <tr>' +
                  '                 <th colspan="2">' +
                  '                                ' +
                  '                 </th>' +
                  '                 <th width="80%">' +
                  '                     <div class="row">' +
                  '                         <div class="col-sm-11">' +
                  '                             <input class="form form-control" id="inputMensagem"/>' +
                  '                         </div>' +
                  '                         <div class="col-sm-1">' +
                  '                             <button disabled class="btn btn-green" id="enviarMensagem" value="' + d.idVenda + '">' +
                  '                                 <i class="fa fa-mail-forward"></i> ' +
                  '                                 <i class="fa fa-envelope"></i> ' +
                  '                             </button>' +
                  '                         </div>' +
                  '                     </div>' +
                  '                 </th>' +
                  '             </tr>' +
                  '             <tr>' +
                  '                 <th width="10%">' +
                  '                     Remetente' +
                  '                 </th>' +
                  '                 <th width="10%">' +
                  '                     Data' +
                  '                 </th>' +
                  '                 <th width="80%">' +
                  '                     Mensagem' +
                  '                 </th>' +
                  '             </tr>' +
                  '         </thead>' +
                  '         <tbody>' ;
          
                  $.each(retorno.dados.mensagens, function(indice, valor)
                  {
                     
                     data +=
                     '                                  <tr>' +
                     '                                      <td>' +
                                                                     valor.remetente +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.dataCriacao +
                     '                                      </td>' +
                     '                                      <td>' +
                                                                     valor.conteudoMensagem +
                     '                                      </td>' +
                     '                                  </tr>';
                     
                  });
                  
                  data +=
                  '         </tbody>' +
                  '     </table>' +
                  '   </div>' +
                  '</div>';

      row.child(data).show();

   });
}

