$(document).ready(function () {

    var mySelect = $("#FP_id").select2({
        data: [{
                id: 0,
                text: 'Carregando..'
            }],
        placeholder: "Selecione uma Forma de Pagamento...",
    });

    $('#btn-salvar-compra').on('click', function () {
        $('#form-add-compra').submit();
    })

    $('#btn-modalPgCompra-show').on('click', function () {

        var infoPg = $(document).getInfoPG();

        var valFP = infoPg['valFP'];
        var fpUtil = infoPg['fpUtil'];
        var parUtil = infoPg['parUtil'];

        if (valFP > 0) {

            $.ajax({
                type: 'POST',
                url: '/compra/listFP/',
                data: {
                    formPGs: fpUtil,
                    parcelas: parUtil
                }

            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                $('#valorFP').attr('value', valFP);

                mySelect.select2({
                    data: retorno
                });

                $('#modal_new_Compra_CP').modal('show');

            });
        } else {
            console.log('eita');
        }


    });

    $('.form-compra').on('change', function ()
    {
        var fornecedor = $('#Compra_Fornecedor_id').val();
        var filial     = $('#Filial_id'           ).val();
        var comprador  = $('#Compra_Comprador_id' ).val();
        
        if (fornecedor > 0 && filial > 0 && comprador > 0 && $('#Compra_Data').empty())
        {
            $('#grid_Compra_Item_da_Compra').removeAttr('hidden');
        } else {
            $('#grid_Compra_Item_da_Compra').attr('hidden', 'true');
        }
    });
    
    $('#Filial_id').on('change', function()
    {
        var filialId = $(this).val();
       
        $.ajax(
        {
            type : 'POST',
            url  : '/Estoque/buscaPorFilial',
            data : {
                estoque :filialId,
            },
        }).done(function(dRt)
        {
           var retorno = $.parseJSON(dRt);
            
           $('#Estoque_id').select2({
                      data : retorno
                   });
        });
       
    });

    $('#FP_id').on('change', function () {

        var fpHasCp = $(this).val();

        $.ajax({
            type: 'POST',
            url: '/FormaDePagamentoHasCondicaoDePagamento/getIntervalo/',
            data: {
                id: fpHasCp
            }

        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $('#parcelas').attr('value', retorno['de']);

            if (retorno['de'] == retorno['ate'])
            {
                $('#parcelas').attr('disabled', 'true');
            } else {
                $('#parcelas').removeAttr('disabled');

            }
            
            $('#parcelas').attr('min',retorno['de']);
            $('#parcelas').attr('max',retorno['ate']);
            
            $('#parcelas').val(retorno['de']);

        });

    });

    $('#Item_da_Compra_id').on('change', function () {

        $('#quantidade').attr('value', 1);
        
        $('#preco_compra').attr('value', 0.1);
        $('#preco_compra').attr('min',   0.1);

        $('#desconto').attr('value', 0);
        $('#desconto').attr('max',   100);
        $('#desconto').attr('min',   0);

        $(document).setPrecoCompra($('#quantidade').val(),$('#preco_compra').val(),$('#desconto').val());


    });

/*    $('#quantidade').on('change', function () {

        $('#desconto').attr('value', 0);

        if (!($('#quantidade').val() > 0)) {
            $('#quantidade').attr('value', 1);
        }

        $(document).setPrecoCompra($('#quantidade').val(),$('#preco_compra').val(),$('#desconto').val());

    })*/

    $('#quantidade').on('change', function () {

        $('#desconto').attr('value', 0);

        if (!($('#quantidade').val() > 0)) {
            $('#quantidade').attr('value', 1);
        }

        $(document).setPrecoCompra($('#quantidade').val(),$('#preco_compra').val(),$('#desconto').val());

    })

    $('#preco_compra').on('change', function () {

        if ($('#preco_compra').val() <= 0) {
            $('#preco_compra').val(0.1);
        }

        $(document).setPrecoCompra($('#quantidade').val(), $('#preco_compra').val(), $('#desconto').val())

    })

    $('#btn-add-item').on('click', function () {
        
        var estoqueId = $('#Estoque_id').val();

        var itCmp = $('#grid_Compra_Item_da_Compra tr').length;
        
        var valMerc = (parseFloat($('#quantidade').val())*parseFloat($('#preco_compra').val()));

        $.ajax({
            type: 'POST',
            url: '/estoque/getDescricao/',
            data: {
                id: estoqueId
            }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            var htmlAdd = '';
            
            htmlAdd =   '<tr id="trItCmp' + itCmp + '" data-tr-item="' + itCmp + '">' +
                            '<td data-td-tipo="item"   data-itCmp="'   + itCmp + '">' + itCmp + '</td>' +
                            '<td data-td-tipo="prod"   data-itCmp="'   + itCmp + '">' + $('#Item_da_Compra_id option:selected').text() + '</td>' +
                            '<td data-td-tipo="est"    data-itCmp="'   + itCmp + '">' + retorno + '</td>' +
                            '<td data-td-tipo="qtd"    data-itCmp="'   + itCmp + '">' + $('#quantidade').val() + '</td>' +
                            '<td data-td-tipo="prc"    data-itCmp="'   + itCmp + '">' + $('#preco_compra').val() + '</td>' +
                            '<td data-td-tipo="dsc"    data-itCmp="'   + itCmp + '">' + $('#desconto').val() + '</td>' +
                            '<td data-td-tipo="prv"    data-itCmp="'   + itCmp + '">' + $('#precoTotal').val() + '</td>' +
                            '<td>' +
                            '<a  style="padding:1px 5px; font-size:12px;" ' +
                            'id="btn-rm-item' + itCmp + '" ' +
                            'data-item=' + itCmp + ' ' +
                            'type="submit" ' +
                            'class="btn btn-red rmItCmp">' +
                            '-' +
                            '</a>' +
                            '</td>' +
                        '</tr>';

            $('#grid_Compra_Item_da_Compra tbody').append(htmlAdd);
            
        });

        $('#form-add-compra').append('<input class="itemCompra" id="itC' + itCmp + '" type="hidden" name="Item_da_Compra_id_hdn[]" data-item-id='  + itCmp + ' value="' + $('#Item_da_Compra_id').val() + '">');
        $('#form-add-compra').append('<input                    id="qtd' + itCmp + '" type="hidden" name="quantidade_hdn[]"        data-item-qtd=' + itCmp + ' value="' + $('#quantidade'       ).val() + '">');
        $('#form-add-compra').append('<input                    id="est' + itCmp + '" type="hidden" name="estoque_hdn[]"           data-item-est=' + itCmp + ' value="' + $('#Estoque_id'       ).val() + '">');
        $('#form-add-compra').append('<input                    id="dsc' + itCmp + '" type="hidden" name="desconto_hdn[]"          data-item-dsc=' + itCmp + ' value="' + $('#desconto'         ).val() + '">');
        $('#form-add-compra').append('<input                    id="prc' + itCmp + '" type="hidden" name="precoTotal_hdn[]"        data-item-prc=' + itCmp + ' value="' + $('#precoTotal'       ).val() + '">');
        $('#form-add-compra').append('<input class="valMerHdn"  id="vlr' + itCmp + '" type="hidden"                                data-item-vlr=' + itCmp + ' value="' + valMerc                       + '">');

        $('#grid_Compra_CP').removeAttr('hidden');
        
        $(document).setPrecoGeralCompra();

        $('#modal_new_Compra_Item_da_Compra').modal('hide');

        return false;
    })

    $('#btn-add-fp').on('click', function () {

        var fpHasCP_id = $('#FP_id').val();

        $.ajax({
            type: 'POST',
            url: '/formaDePagamentoHasCondicaoDePagamento/getDescricao/',
            data: {
                id: fpHasCP_id
            }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            var htmlAdd = '';

            htmlAdd += '<tr id="trItCp' + fpHasCP_id + '" class="tr-fp" data-tr-fp="' + fpHasCP_id + '">' +
                        '<td>' + retorno.text + '</td>' +
                        '<td>' + $('#parcelas').val() + '</td>' +
                        '<td>' + $('#valorFP').val() + '</td>' +
                        '<td>' +
                            '<a  style="padding:1px 5px; font-size:12px;" ' +
                            'id="btn-rm-cp' + fpHasCP_id + '" ' +
                            'data-fpCp-id=' + fpHasCP_id + ' ' +
                            'type="submit" ' +
                            'class="btn btn-red rmItCp">' +
                            '-' +
                            '</a>' +
                        '</td>' +
                        '</tr>';

            $('#grid_Compra_CP tbody').append(htmlAdd);

        });

        $('#form-add-compra').append('<input type="hidden" data-fp-id="' + fpHasCP_id + '" class="hdn-fp" name="FP_id_hdn[]"    value="' + $('#FP_id').val() + '">');
        $('#form-add-compra').append('<input type="hidden" data-fp-pa="' + fpHasCP_id + '" class="hdn-fp" name="parcelas_hdn[]" value="' + $('#parcelas').val() + '">');
        $('#form-add-compra').append('<input type="hidden" data-fp-vl="' + fpHasCP_id + '" class="hdn-fp" name="valorFP_hdn[]"  value="' + $('#valorFP').val() + '">');

        $(document).setPrecoGeralCompra();

        $('#modal_new_Compra_CP').modal('hide');

        return false;
    })

})

/////////////////////////////////////////////////////////////////////////////////////////////
//autor : Andre Willams // data : 25-09-2014/////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//funcao que tem como objetivo trazer todas as informacoes referentes aos valores da compra//
/////////////////////////////////////////////////////////////////////////////////////////////
$.fn.getInfoPG = function ()
{

    var valores = $('#form-add-compra');

    var form = valores[0];
    var valItemTot = 0;
    var valPgtoTot = 0;
    var valMerc    = 0;

    var valFP = 0;

    var fpUtil = [];
    var parUtil = [];
    
    var valMerHdn = $('.valMerHdn');
    
    for (i = 0; i < valMerHdn.length; i++)
    {
        valMerc += parseFloat(valMerHdn[i].value);
    }

    for (i = 0; i < form.length; i++) {

        if (form[i].name == 'precoTotal_hdn[]') {
            valItemTot += parseFloat(form[i].value);
        }

        if (form[i].name == 'valorFP_hdn[]') {
            valPgtoTot += parseFloat(form[i].value);
        }

        if (form[i].name == 'FP_id_hdn[]') {
            fpUtil.push(parseInt(form[i].value));
        }

        if (form[i].name == 'parcelas_hdn[]') {
            parUtil.push(parseInt(form[i].value));
        }

    }

    valFP = (valItemTot - valPgtoTot);

    return {
        'valFP': valFP.toFixed(2), 'valItemTot': valItemTot.toFixed(2), 'valPgtoTot': valPgtoTot.toFixed(2), 'valMerc' : valMerc.toFixed(2),
        'fpUtil': fpUtil,
        'parUtil': parUtil
    };

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//autor : Andre Willams // data : 10-09-2014///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//funcao que tem como objetivo gatilhar o preco final de Compra, de acordo com preco unitario, qtd e desconto//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
$.fn.setPrecoCompra = function (qtd, prc, desc) {

    var q = parseFloat(qtd);
    var p = parseFloat(prc);
    var d = (p * q) * (parseFloat(desc) / 100);
    var pv = ((q * p) - d);

    $('#precoTotal').attr('value', pv);
}

$.fn.setPrecoGeralCompra = function()
{
    var infoPG = $(document).getInfoPG();
    
    $('#valFin').val(infoPG['valItemTot']);
    
    $('#valPag').val(infoPG['valFP']);
    
    $('#valMer').val(infoPG['valMerc']);
    
    $('#valDes').val((parseFloat(infoPG['valMerc'])-parseFloat(infoPG['valItemTot'])).toFixed(2));

    if (infoPG['valFP'] > 0) //verifica se o valor a pagar eh maior q 0
    {

        $('#div-btn-salvar'  ).attr('hidden', 'true'); //esconde o botao de salvar

    } else { //caso o valor de pagamento tenha atigindo o valor total da compra

        if (infoPG['valItemTot'] > 0) //verifica se ja tem item com valor a ser pago
        {

            $('#grid_Compra_CP').removeAttr('hidden'); //mostra a tabela de pagamentos

            $('#div-btn-salvar').removeAttr('hidden'); //mostre o botao salvar

        } else { //se nao houver item com valor a ser pago
            $('#grid_Compra_CP').attr('hidden', 'true'); //esconda a tabela de pagamentos
            $('#div-btn-salvar').attr('hidden', 'true'); //esconde o botao de salvar
        }
    }
    
}

//////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 26/09/2014///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//trecho referente, inicialmente, para habilitar a exclusao de um item da compra//
//////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.rmItCmp', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var posIt = $(this).attr("data-item"); //atraves do atributo item (data), recupera a posicao do item na tabela

    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var itemCompraHdn  = $('[data-item-id="'  + posIt + '"]');
    var quantidadeHdn  = $('[data-item-qtd="' + posIt + '"]');
    var estoqueHdn     = $('[data-item-est="' + posIt + '"]');
    var descontoHdn    = $('[data-item-dsc="' + posIt + '"]');
    var precoTotalHdn  = $('[data-item-prc="' + posIt + '"]');
    var valorCompraHdn = $('[data-item-vlr="' + posIt + '"]');

    //encontra a linha da FP atraves de seu id (formado de acordo com sua posicao na tabela)
    var itemCompraLin = $('#trItCmp' + posIt);

    //remove todos os input's[hidden] com informacoes do item da compra
     itemCompraHdn.remove();
     quantidadeHdn.remove();
        estoqueHdn.remove();
       descontoHdn.remove();
     precoTotalHdn.remove();
    valorCompraHdn.remove();

    //esconde a linha do item corrente. para evitar problema com as posicoes de cada item, decidiu-se nao excluir a linha
    //apenas esconde-la, para manter o tamanho da tabela, bem como a sequencia dos itens (respectivos posicionamentos)
    itemCompraLin.attr('hidden', true);
    
    $(document).setPrecoGeralCompra();

    $(document).limpaPG();

});

//////////////////////////////////////////////////////////////////////////////////
//autor: Andre Willams // Data : 26/09/2014///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//trecho referente, inicialmente, para habilitar a exclusao de um item da compra//
//////////////////////////////////////////////////////////////////////////////////
$(document).on('click', '.rmItCp', function () { //captura o evento de click, no botao de remover item, a partir de sua classe

    var posFp = $(this).attr("data-fpCp-id"); //atraves do atributo item (data), recupera a posicao do item na tabela

    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var fpHdn = $('[data-fp-id="' + posFp + '"]');
    var paHdn = $('[data-fp-pa="' + posFp + '"]');
    var vlHdn = $('[data-fp-vl="' + posFp + '"]');

    //encontra a linha da FP atraves de seu id (formado de acordo com sua posicao na tabela)
    var cpLin = $('#trItCp' + posFp);

    //remove todos os input's[hidden] com informacoes do item da compra
    fpHdn.remove();
    paHdn.remove();
    vlHdn.remove();

    //esconde a linha do item corrente. para evitar problema com as posicoes de cada item, decidiu-se nao excluir a linha
    //apenas esconde-la, para manter o tamanho da tabela, bem como a sequencia dos itens (respectivos posicionamentos)
    cpLin.remove();
    
    $(document).setPrecoGeralCompra();

});

$.fn.limpaPG = function ()
{

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    //para evitar complicacoes futuras com a condicao de pagamento, em todo caso que um item for excluido//
    //apagamos todas as formas de pagamento utilizadas na compra                                          //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    var trFp = $('.tr-fp');  //encontra todas as de formas de pagamento, ja incluidas, atraves de sua classe
    //encontra todos input's[hidden] com os valores das de formas de pagamento, ja incluidas, atraves de 
    //sua classe
    var hdnFp = $('.hdn-fp');

    //apga todas as linhas referentes as FP, junto com os input's[hidden] respectivos
    trFp.remove();
    hdnFp.remove();

    $(document).setPrecoGeralCompra();

}

$(document).on('dblclick', 'td', function () {

    var tipo             = $.trim($(this).attr('data-td-tipo'));
    var conteudoOriginal = $(this).text();
    var itemCmp          = $(this).attr('data-itCmp');
//    var retorno = null;

    if (tipo !== 'item')
    {
        if (tipo == 'prod')
        {
            
            $(this).html('<input required="true" placeholder="Selecione" type="hidden" id="Produto_id" class="form-control search-select">');
            
            $.ajax({
                type: 'POST',
                url: '/compra/listProdutos/',
                data: null,

            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                var selectProd = $("#Produto_id").select2({
                    data: [{
                            id: 0,
                            text: 'Carregando..'
                        }],
                    placeholder: "Selecione um Produto...",
                });
                
                selectProd.select2({
                    data : retorno,    
                });
                
                selectProd.on('change', function () {
                   
                    itemId = selectProd.val();
                   
                    $.ajax({
                        type: 'POST',
                        url: '/produto/getAttributes/',
                        data: {Produtos : itemId}	

                    }).done(function (dRt) {
                        var retItem = $.parseJSON(dRt);
                        
                        $(document).setProdutoTD(itemCmp,retItem);
                        
                    });
                    
                });
            
            });
            
        } else if(tipo !== 'est') {
            
            var cHtml = "<input type='number' value='" + conteudoOriginal + "' ";
            
            if (tipo == 'prc')
            {
                cHtml += " min='0.1' ";
            }else{
                cHtml += " min='0' ";
            }
                
            
            cHtml += " />";

            $(this).html(cHtml);

            $(this).children().first().focus();

            $(this).children().first().keypress(function (e) {

                if (e.which == 13) {

                    var novoConteudo = $(this).val();
                    
                    if (parse.float(novoConteudo) <= 0)
                    {
                        if(tipo == 'prc')
                        {
                            novoConteudo = '0.1';
                        }else{
                            novoConteudo = '0';
                        }
                    }

                    $(this).parent().text(novoConteudo);
                    
                    $(document).atualizaItemC(itemCmp);
                    
                }
            });
        }
    }

    $(this).children().first().blur(function () {

        $(this).parent().text(conteudoOriginal);
    });
});

///////////////////////////////////////////////////////////////////////////////////////////
//autor : andre willams // data : 30-09-14 ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//funcao criada com o intuito de atualizar o td com a descricao do produto selecionado na//
//alteracao do produto.                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////
$.fn.setProdutoTD = function(itemCmp,produto)
{
    
    //resgata os valores da linha corrente, atraves do tipo da coluna e o item da compra
    var tdProd   = $('td[data-td-tipo="prod"][data-itCmp="' + itemCmp + '"]');
    var tdQtd    = $('td[data-td-tipo="qtd" ][data-itCmp="' + itemCmp + '"]');
    var tdEst    = $('td[data-td-tipo="est" ][data-itCmp="' + itemCmp + '"]');
    var tdPrc    = $('td[data-td-tipo="prc" ][data-itCmp="' + itemCmp + '"]');
    var tdDesc   = $('td[data-td-tipo="dsc" ][data-itCmp="' + itemCmp + '"]');
    
    var produtoHdn = $('[data-item-id="' + itemCmp + '"]');

    tdProd.text(produto.descricao); //atribui o valor do td como a descricao do produto
     tdQtd.text(1);                 //seta, como padrao, a quantidade de compra = 1
     tdEst.text('VE');              //
     tdPrc.text(0.1);               //coloca o preco de compra
    tdDesc.text(0);                 //zera o desconto do produto
    
    produtoHdn.val(produto.id); //atualiza o input (hidden)
    
    $(document).atualizaItemC(itemCmp); //atualiza os valores da linha

}

//////////////////////////////////////////////////////////////////////////////////////////////
//autor : andre willams // data : 29-09-14 ///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//funcao criada com o intuito de calcular os novos valores do item alterado, bem como limpar//
//os pagamentos ja informados                                                               //
//////////////////////////////////////////////////////////////////////////////////////////////
$.fn.atualizaItemC = function (itemCmp) {
    
    //resgata os valores da linha corrente, atraves do tipo da coluna e o item da compra
    var tdQtd    = $('td[data-td-tipo="qtd"][data-itCmp="' + itemCmp + '"]');
    var tdPrc    = $('td[data-td-tipo="prc"][data-itCmp="' + itemCmp + '"]');
    var tdEst    = $('td[data-td-tipo="est"][data-itCmp="' + itemCmp + '"]');
    var tdDesc   = $('td[data-td-tipo="dsc"][data-itCmp="' + itemCmp + '"]');
    var tdPrecoC = $('td[data-td-tipo="prv"][data-itCmp="' + itemCmp + '"]');
    
    //com a posicao do item na tabela, encontramos todos os inputs[hidden] com os valores do item
    var quantidadeHdn  = $('[data-item-qtd="'  + itemCmp + '"]');
    var descontoHdn    = $('[data-item-dsc="'  + itemCmp + '"]');
    var precoCompraHdn = $('[data-item-prc="'  + itemCmp + '"]');
    var valorCompraHdn = $('[data-item-vlr="'  + itemCmp + '"]');

    var q = parseFloat(tdQtd.text()); //pega o conteudo da celula e transforma para double
    var p = parseFloat(tdPrc.text()); //pega o conteudo da celula e transforma para double
    
    //transforma o valor da celula do desconto (apos ser transformado em double) em valor
    //percentual para descobrir o valor total do item
    var d = (p * q) * (parseFloat(tdDesc.text()) / 100);

    var pc = ((q * p) - d);//calcula o valor total do item de acordo com a qtd e desconto

    tdPrecoC.text(pc); //atribue o valor na celula correspondente

    //atribue valores resgatados no input's hidden
     quantidadeHdn.val(q); 
       descontoHdn.val(d);
    precoCompraHdn.val(pc);
    valorCompraHdn.val(p*q);
    
    $(document).setPrecoGeralCompra();

    //chama a funcao para limpar os pagamentos para evitar problemas de recalculo
    $(document).limpaPG();

};