$(function(){

    $('.selectpicker').selectpicker(
    {
        buttonWidth: '100%',
        numberDisplayed: 1,
        liveSearch: true,
        actionsBox: true,
        liveSearchPlaceholder: 'Buscar...',
        selectAllText: 'Todos',
        deselectAllText: 'Nenhum',
        noneSelectedText: 'Nada selecionado',
        selectAllValue: 0,
        nSelectedText: 'Selecionados!',
        selectedClass: 'multiselect-selected',
        countSelectedText: function (num)
        {
            if (num == 1) {
                return "{0} ITEM SELECIONADO ";
            }
            else{
                return "{0} ITENS SELECIONADOS ";
            }
        }
    });

    $('.date').datepicker({
        format: "dd/mm/yyyy"
    });

    var gridProducao = $('#grid_producao').DataTable(
    {
        "ajax": {
            url: '/gestorDeFiliais/gridSeguro/',
            type: 'POST',
            "data": function (d)
            {
                d.de = $("#de").val(),
                d.ate = $("#ate").val(),
                d.filial = $('#selectFilial').val()
            }
        },
        serverSide: true,
        processing: "Processando...",
        searching: false,
        ordering: false,
        "columns": [
            {"data": "codigo"       },
            {"data": "filial"       },
            {"data": "data_cadastro"},
            {"data": "crediarista"  },
            {"data": "v_inicial"    },
            {"data": "v_seguro"     },
            {"data": "v_financiado" },
        ],
        "columnDefs": [
            {
                "orderable": false,
                "targets": "no-orderable"
            }
        ],
        "language": {
            "info": "Exibindo _START_ até _END_ de _TOTAL_ registros",
            "paginate": {
                "first": "Primeiro",
                "last": "Último",
                "next": "Pŕoximo",
                "previous": "Anterior"
            }
        },
        "drawCallback": function (settings){
            $('#th-total-financiado').html(settings.json.totalF);
            $('#th-total-seguro').html(settings.json.totalS);
            $('#th-total-soli').html(settings.json.totalSoli);
        }

    });

    $('#selectNucleo').on('change', function () {

        var selecteds = [];

        var getFiliais = $.ajax({
            url: '/gestorDeFiliais/listarFiliaisFiltro/',
            type: 'POST',
            data:
                    {
                        nucleoId: $(this).val()
                    }

        });

        getFiliais.done(function (dRt)
        {
            var retorno = $.parseJSON(dRt);

            $('#selectFilial').val(selecteds).change();

            $('#selectFilial option').each(function () {
                $(this).remove();
            });

            $('#selectFilial').selectpicker('refresh');

            $.each(retorno, function (key, value) {
                $('#selectFilial').append('<option selected value="' + retorno[key].id + '">' + retorno[key].text + '</option>').selectpicker('refresh');
            });
        });
    });

    $('#btn_filter').on('click', function () {
        gridProducao.draw();
    });
});