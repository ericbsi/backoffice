function bloquear_ctrl_j() {

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;
        event.returnValue = false;
    }
}

$(document).ready(function () {

    var bipados = [];

    $('.multiselectCustom').selectpicker(
            {
                buttonWidth: '100%',
                numberDisplayed: 1,
                liveSearch: true,
                actionsBox: true,
                liveSearchPlaceholder: 'Buscar...',
                selectAllText: 'Todos',
                deselectAllText: 'Nenhum',
                noneSelectedText: 'Nada selecionado',
                selectAllValue: 0,
                nSelectedText: 'Selecionados!',
                selectedClass: 'multiselect-selected'
            });

    $('#contrato').focus();

    $('#recebimento_btn').on('click', function () {
        $('.divRecebimentos').toggle('slow');
        $('.divBorderos').hide('slow');
        $('.divMalotes').hide('slow');
    });

    $('#borderos_btn').on('click', function () {
        $('.divBorderos').toggle('slow');
        $('.divRecebimentos').hide('slow');
        $('.divMalotes').hide('slow');
    });

    $('#malotes_btn').on('click', function () {
        $('.divBorderos').hide('slow');
        $('.divRecebimentos').hide('slow');
        $('.divMalotes').toggle('slow');
    });

    var propostasTable = $('#grid_propostas').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/documentacao/getBipadas',
                    type: 'POST',
                    data: function (d) {
                        d.bips = bipados.join();
                    }
                },
        "columns": [
            {
                "orderable": false,
                "data": "modalidade"
            },
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "filial"
            },
            {
                "orderable": false,
                "data": "cliente"
            },
            {
                "orderable": false,
                "data": "cpf"
            },
            {
                "orderable": false,
                "data": "data"
            },
            {
                "orderable": false,
                "data": "financiado"
            },
            {
                "orderable": false,
                "data": "parcelas"
            },
            {
                "orderable": false,
                "data": "repasse"
            }
        ],
        "drawCallback": function (settings) {
            $('#qtd_props').text(settings.json.qtd);
            $('#total_fin').text(settings.json.totalFin);
            $('#total_rep').text(settings.json.totalRep);
        }
    });

    $('#contrato').on('change', function () {
        if ($('#contrato').val() != '') {
            $.ajax({
                type: "POST",
                url: "/documentacao/validarRecebimento",
                data: {
                    codigo: $('#contrato').val()
                }
            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);

                if (!retorno.invalido) {
                    if($('#contrato').val().length > 12){
                        bipados.push($('#contrato').val().slice(1, -2));
                    }else{
                        bipados.push($('#contrato').val());
                    }
                    console.log(bipados);
                    propostasTable.ajax.reload();
                } else {
                    $.Notification.autoHideNotify(
                            'error',
                            'top right',
                            'Informações',
                            retorno.mensagem
                            );
                }
                $('#contrato').val('');
            });
        }
    });

    var recebidosTable = $('#grid_recebidos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/documentacao/recebidos',
                    type: 'POST',
                    data: function (d) {
                        d.filiais = $('#filiais').val();
                    }
                },
        "columns": [
            {
                "orderable": false,
                "data": "marcar"
            },
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "filial"
            },
            {
                "orderable": false,
                "data": "user"
            },
            {
                "orderable": false,
                "data": "data"
            },
            {
                "orderable": false,
                "data": "imprimir"
            },
        ],
        "drawCallback": function (settings) {
            var elem = document.querySelectorAll('.check_recebido');
            var i;
            for (i = 0; i < elem.length; i++) {
                var init = new Switchery(elem[i], {color: '#1db198', size: 'small'});
            }
        }
    });

    $('#gerar_recebimento').on('click', function () {
        $.ajax({
            type: "POST",
            url: "/documentacao/gerarRecebimentos",
            data: {
                bips: bipados.join()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            $.Notification.autoHideNotify(
                    retorno.tipo,
                    'top right',
                    'Informações',
                    retorno.msg
                    );

            bipados = [];
            propostasTable.ajax.reload();
            recebidosTable.ajax.reload();
        });
    });

    $('#filtrar_recebidos').on('click', function () {
        recebidosTable.ajax.reload();
    });

    $('.multiselectCustom').on('change', function () {
        if (typeof $(this).attr("data-Alvo") !== typeof undefined && $(this).attr("data-Alvo") !== false) {
            filtrarSelectAlvo($(this));
        }
    });

    function filtrarSelectAlvo(selectOrigem)
    {

        var url = selectOrigem.attr("data-url");
        var alvo = selectOrigem.attr("data-alvo");

        var objAlvo = $("#" + alvo);

        var selected = "selected";

        var valor = selectOrigem.val();

        if ((typeof valor === typeof undefined) || valor == null) {
            selected = "";
        }

        $.ajax({
            url: url,
            type: 'POST',
            data:
                    {
                        variavel: selectOrigem.val()
                    }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            objAlvo.find('option').each(function () {
                $(this).remove();
            });

            $.each(retorno, function (key, value) {
                objAlvo.append('<option value="' + retorno[key].id + '" ' + selected + '>' + retorno[key].text + '</option>').selectpicker('refresh');
            });

            objAlvo.selectpicker("rebuild");

            if (typeof objAlvo.attr("data-Alvo") !== typeof undefined && objAlvo.attr("data-Alvo") !== false) {
                filtrarSelectAlvo(objAlvo);
            }
        });
    }

    var recebimentos = [];

    $('#marcar_todos').on('change', function () {
        var elem = document.querySelectorAll('.check_recebido');
        var i;
        if ($(this).prop('checked')) {
            for (i = 0; i < elem.length; i++) {
                if (!$(elem[i]).parent().find('input').prop('checked')) {
                    $(elem[i]).click();
                    if ($(elem[i]).parent().find('input').prop('checked')) {
                        $(elem[i]).parent().parent().css("background-color", "#343d47");
                    } else {
                        $(elem[i]).parent().parent().css("background-color", "");
                    }
                }
            }
        } else {
            for (i = 0; i < elem.length; i++) {
                if ($(elem[i]).parent().find('input').prop('checked')) {
                    $(elem[i]).click();
                    if ($(elem[i]).parent().find('input').prop('checked')) {
                        $(elem[i]).parent().parent().css("background-color", "#343d47");
                    } else {
                        $(elem[i]).parent().parent().css("background-color", "");
                    }
                }
            }
        }
    });

    $(document).on('change', '.check_recebido', function () {
        if ($(this).prop('checked')) {
            $(this).parent().parent().css("background-color", "#343d47");
        } else {
            $(this).parent().parent().css("background-color", "");
            $(this).parent().parent().find('td').each(function (index, val) {
                $(val).css('color', '');
            });
        }
        var element = $(this).parent().find('input');
        marcar(element);
    });

    function marcar(element) {
        if (element.prop('checked') && !element.hasClass('checkall')) {
            recebimentos.push(element.val());
        } else {
            var indice = recebimentos.indexOf(element.val());
            recebimentos.splice(indice, 1);
        }
        if(recebimentos.length > 0){
            $('.floating_btn').fadeIn('fast');
        }else{
            $('.floating_btn').fadeOut('fast');
        }
        $('#id_rebs').val(recebimentos);
    }

    $('#gerar_malotes').on('click', function () {
        $.ajax({
            type: "POST",
            url: "/documentacao/novaDocumentacao",
            data: {
                selecionados: recebimentos
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            $.Notification.autoHideNotify(
                    retorno.tipo,
                    'top right',
                    'Informações',
                    retorno.msg
                    );

            recebimentos = [];
            recebidosTable.ajax.reload();
            documentosTable.ajax.reload();
        });
    });

    var documentosTable = $('#grid_documentacao').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/documentacao/documentacoes',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "data"
            },
            {
                "orderable": false,
                "data": "user"
            },
            {
                "orderable": false,
                "data": "print"
            }
        ]
    });

});