$(function()
{
	$.validator.messages.required = function (param, input)
  {
    return 'O campo ' + $(input).data('nome-msg') + ' é obrigatório ';
	}
	$('.selectpicker').selectpicker(
  {
    buttonWidth           : '100%',
    numberDisplayed       : 1,
    liveSearch            : true,
    actionsBox            : true,
    liveSearchPlaceholder : 'Buscar...',
    selectAllText         : 'Todos',
    deselectAllText       : 'Nenhum',
    noneSelectedText      : 'Nada selecionado',
    selectAllValue        : 0,
    nSelectedText         : 'Selecionados!',
    selectedClass         : 'multiselect-selected',
    countSelectedText     : function (num){
      if (num == 1){
        return "{0} ITEM SELECIONADO ";
      }
      else{
        return "{0} ITENS SELECIONADOS ";
      }
    }
  });
	$('#PoliticaCredito_nome').focus();

	$('.colorpicker-default').colorpicker({format: 'hex'});
	$('.colorpicker-rgba').colorpicker();

	var grid_politicas = $('#datatable-politicas').DataTable({

		ordering              : false,
    searching             : false,
    serverSide            : true,

		"ajax": {
      url                 : "/politicaCredito/listar/",
      type                : 'POST'            
    },

    "columns"             : [
      {"data"             : "legenda"    	},
      {"data"             : "descricao"  	},
      {"data"             : "btn_r_a"  	},
    ],

    "language"            :{
      "info"              : "Exibindo _START_ até _END_ de _TOTAL_ registros",
        "paginate"        : {
        "first"         	:   "Primeiro",
        "last"          	:   "Último",
        "next"          	:   "Pŕoximo",
        "previous"      	:   "Anterior"
      },
    }
	});
  var grid_regras = $('#grid_regras').DataTable(
  {
    ordering              : false,
    searching             : false,  
    "columns"             : [
      {"data"             : "score"           },
      {"data"             : "acao"            },
      {"data"             : "cli_hist"        },
      {"data"             : "val_de"          },
      {"data"             : "val_ate"         },
      {"data"             : "restricao"       },
      {"data"             : "comp_renda"      },
      {"data"             : "cont_abertos"    },
      {"data"             : "alerta"          },
      {"data"             : "passagens_casa"  },
      {"data"             : "negacoes"        },
      {"data"             : "rg"              },
      {"data"             : "passagens_spc"   },
      {"data"             : "btn_load_info"   }
    ]
  
  });
	$('#form-politica').validate({

		submitHandler : function()
		{

			var post 	= $.ajax({
				url 	: '/politicaCredito/persist',
				type 	: 'POST',
				data 	: $('#form-politica').serialize(),
			});

			post.done(function(resp){

				var r = $.parseJSON(resp);

				$.each(r.msgConfig, function (noti, conteudoNotificacao){
	       $.Notification.autoHideNotify(
            conteudoNotificacao.tipo,
	          conteudoNotificacao.posicao,
	          conteudoNotificacao.titulo,
	          conteudoNotificacao.mensagem
	       );
        });
				
				if( typeof r.errors == 'undefined')
				{
					limpaForm( $('#form-politica') );
					grid_politicas.draw(false);
				}

			});
		}
	});
	$('#form-add-regra').validate({
		  ignore: "",
		  errorLabelContainer: "#erros",
  		wrapper: "li",

  		submitHandler 	: function()
  		{
  			var post 	= $.ajax({
  				url 	: '/politicaCredito/addRegra/',
  				type 	: 'POST',
  				data 	: $('#form-add-regra').serialize(),
  			});

  			post.done(function(retorno)
        {
          var r = $.parseJSON(retorno);

          $.each(r.msgConfig, function (noti, conteudoNotificacao){
            $.Notification.autoHideNotify(
              conteudoNotificacao.tipo,
              conteudoNotificacao.posicao,
              conteudoNotificacao.titulo,
              conteudoNotificacao.mensagem
            );
          });
        
          if( typeof r.errors == 'undefined')
          {
            updateGridRegras();
          }

          $('#id').attr('value', '0');

  			});
  			return false;
  		}
	});
	$(document).on('click', '.btn-load-politica-regras', function()
	{
		$('#pid').val($(this).data('politica'));
		$('.span_politica_nome').html($(this).data('politica-nome'));
    updateGridRegras();

	});
  $(document).on('click', '.btn-load-regra', function(){

    var get = $.ajax({
      url         : '/politicaCredito/loadRegra/',
      type        : 'POST',
      data        : {
        regraId   : $(this).data('phr-id')
      }
    });

    get.done(function( retorno ){

      var r = $.parseJSON(retorno);

      $(r).each(function(){
        
        if( $('#'+$(this)[0].campo).val() !== undefined ){
          $('#'+$(this)[0].campo).val($(this)[0].valor);
          $('#'+$(this)[0].campo).selectpicker('refresh');
        }
      });

    });

  });
  var updateGridRegras = function(){

    var post = $.ajax({
      url : '/politicaCredito/listarRegras/',
      type : 'POST',
      data :{ 'politicaId' : $('#pid').val() }
    });

    post.done(function(retorno)
    {
      var r = $.parseJSON(retorno);
      grid_regras.clear().draw();
      grid_regras.rows.add(r.data).draw();
      limparSelectsRegras();
    });

  };
  var limparSelectsRegras = function(){
    
    $('#form-add-regra .selectpicker').each(function(el){
      $(this).val("");
      $(this).selectpicker('refresh');
    });
  };
});