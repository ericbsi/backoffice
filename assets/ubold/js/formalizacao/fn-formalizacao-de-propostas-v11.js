function bloquear_ctrl_j() {

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;
        event.returnValue = false;
    }
}

$(function () {

    var propostasTable;
    var inicializada = false;
    var update = false;
    $("#select-modo").on('change', function () {

        if ($(this).val() == '1' || $(this).val() == '2') {
            $('#gcpl').show('slow');
            $('#filtros_pl').hide('slow');
            $('#filtros_pl2').hide('slow');
            $('#filtros_imagem').hide('slow');
            $('#filtros_imagem2').hide('slow');
        }

        update = true;
    });

    $('.multiselectCustom').selectpicker(
            {
                buttonWidth: '100%',
                numberDisplayed: 1,
                liveSearch: true,
                actionsBox: true,
                liveSearchPlaceholder: 'Buscar...',
                selectAllText: 'Todos',
                deselectAllText: 'Nenhum',
                noneSelectedText: 'Nada selecionado',
                selectAllValue: 0,
                nSelectedText: 'Selecionados!',
                selectedClass: 'multiselect-selected'
            });

    $('.multiselectCustom').on('change', function () {
        if (typeof $(this).attr("data-Alvo") !== typeof undefined && $(this).attr("data-Alvo") !== false) {
            filtrarSelectAlvo($(this));
        }
    });

    function filtrarSelectAlvo(selectOrigem)
    {

        var url = selectOrigem.attr("data-url");
        var alvo = selectOrigem.attr("data-alvo");

        var objAlvo = $("#" + alvo);

        var selected = "selected";

        var valor = selectOrigem.val();

        if ((typeof valor === typeof undefined) || valor == null) {
            selected = "";
        }

        $.ajax({
            url: url,
            type: 'POST',
            data:
                    {
                        variavel: selectOrigem.val()
                    }

        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            objAlvo.find('option').each(function () {
                $(this).remove();
            });

            $.each(retorno, function (key, value) {
                objAlvo.append('<option value="' + retorno[key].id + '" ' + selected + '>' + retorno[key].text + '</option>').selectpicker('refresh');
            });

            objAlvo.selectpicker("rebuild");

            if (typeof objAlvo.attr("data-Alvo") !== typeof undefined && objAlvo.attr("data-Alvo") !== false) {
                filtrarSelectAlvo(objAlvo);
            }
        });
    }

    $("#valor_max").maskMoney({decimal: ",", thousands: "."});

    $("#geracao_cnabs").on('click', function () {
        $('#filtros_imagem').toggle('slow');
        $('#filtros_imagem2').toggle('slow');
        $('#filtros_pl').hide('slow');
        $('#filtros_pl2').hide('slow');
        $('#filtros_contratos').hide('slow');
        if (!inicializada) {
            inicializada = true;
            propostasTable = $('#grid_propostas').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":
                        {
                            url: '/recebimento/propostasBipagem',
                            type: 'POST',
                            data: function (d) {
                                d.selects = selecionados.join();
                                d.filiais = $('#selectFilial').val();
                                d.dt_ini = $('#data_de').val();
                                d.dt_fin = $('#data_ate').val();
                                d.contrato = $('#contrato').val();
                                d.valor_max = $('#valor_max').val();
                                d.fisico = $('#select-modo').val();
                            }
                        },
                "columns": [
                    {
                        "orderable": false,
                        "data": "marcar"
                    },
                    {
                        "orderable": false,
                        "data": "modalidade"
                    },
                    {
                        "orderable": false,
                        "data": "codigo"
                    },
                    {
                        "orderable": false,
                        "data": "filial"
                    },
                    {
                        "orderable": false,
                        "data": "cliente"
                    },
                    {
                        "orderable": false,
                        "data": "cpf"
                    },
                    {
                        "orderable": false,
                        "data": "data"
                    },
                    {
                        "orderable": false,
                        "data": "financiado"
                    },
                    {
                        "orderable": false,
                        "data": "parcelas"
                    },
                    {
                        "orderable": false,
                        "data": "repasse"
                    }
                ],
                "drawCallback": function (settings) {

                    var elem = document.querySelectorAll('.pfin');
                    var i;
                    for (i = 0; i < elem.length; i++) {
                        var init = new Switchery(elem[i], {color: '#1db198', size: 'small'});
                    }

                    $('#qtd_props').text(settings.json.qtd);
                    $('#total_fin').text(settings.json.totalFin);
                    $('#total_rep').text(settings.json.totalRep);

                    if ($('#marcar_todos').prop('checked')) {
                        $('#marcar_todos').prop('checked', false);
                        checkall.setPosition(false);
                    }

                    if ($('#contrato').val() != '') {
                        $('.checkall').click();
                        atualizar();
                    }

                    $('#contrato').val('');

                    update = false;
                }
            });
        } else {
            if (update) {
                propostasTable.ajax.reload();

            }
        }
    });

    $("#pre_lotes").on('click', function () {
        $('#filtros_pl').toggle('slow');
        $('#filtros_pl2').toggle('slow');
        $('#filtros_imagem').hide('slow');
        $('#filtros_imagem2').hide('slow');
        $('#filtros_contratos').hide('slow');
    });

    $("#dev_propostas").on('click', function () {
        $('#filtros_contratos').toggle('slow');
        $('#filtros_pl').hide('slow');
        $('#filtros_pl2').hide('slow');
        $('#filtros_imagem').hide('slow');
        $('#filtros_imagem2').hide('slow');
    });

    var plotesTable = $('#grid_plotes').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/recebimento/getPreLotes',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable": false,
                "class": "details",
                "data": "deta"
            },
            {
                "orderable": false,
                "data": "data"
            },
            {
                "orderable": false,
                "data": "qtdb"
            },
            {
                "orderable": false,
                "data": "qtdp"
            },
            {
                "orderable": false,
                "data": "user"
            },
            {
                "orderable": false,
                "data": "fina"
            },
            {
                "orderable": false,
                "data": "repa"
            },
            {
                "orderable": false,
                "data": "stat"
            },
            {
                "orderable": false,
                "data": "ipep"
            },
            {
                "orderable": false,
                "class":"glotes",
                "data": "cons"
            }
        ]
    });

    var devsTable = $('#grid_devs').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/recebimento/devContrato',
                    type: 'POST',
                    data: function (d) {
                        d.ident = $('#contrato_dev').val();
                    }
                },
        "columns": [
            {
                "orderable": false,
                "data": "data_pl"
            },
            {
                "orderable": false,
                "data": "codigo"
            },
            {
                "orderable": false,
                "data": "parceiro"
            },
            {
                "orderable": false,
                "data": "cliente"
            },
            {
                "orderable": false,
                "data": "cpf"
            },
            {
                "orderable": false,
                "data": "financiado"
            },
            {
                "orderable": false,
                "data": "repasse"
            },
            {
                "orderable": false,
                "class": "btn_dev",
                "data": "devolver"
            }
        ],
        "drawCallback": function (settings) {
            $('#contrato_dev').val('');
        }
    });

    $('#filtrar_dev').on('click', function () {
        devsTable.ajax.reload();
    });

    $('#grid_plotes tbody').on('click', 'td.details', function () {
        var tr = $(this).closest('tr');
        var row = plotesTable.row(tr);
        var rowsTam = plotesTable.rows()[0].length;
        var dt = $(this).parent().find('button').val();
        var desfoque = $(this).parent().find('button');
        if ($(this).parent().find('button').hasClass('btn-success')) {
            $(this).parent().find('button').removeClass('btn-success');
            $(this).parent().find('button').addClass('btn-warning');
        } else {
            $(this).parent().find('button').addClass('btn-success');
            $(this).parent().find('button').removeClass('btn-warning');
        }

        if (row.child.isShown()) {
            row.child.hide();
        } else
        {
            for (i = 0; i < rowsTam; i++)
            {

                if (plotesTable.row(i).child.isShown()) {
                    plotesTable.row(i).child.hide();
                }
            }

            SubDetalhes(row.data(), tr, row, dt);
        }

        $('#grid_plotes tbody tr').each(function (index, element) {
            if (desfoque.hasClass('btn-warning') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2 && $(element).index('tr') !== $(tr).index('tr') + 3) {
                $(element).hide();
            } else {
                $(element).show();
            }
        });

    });

    $('#grid_devs tbody').on('click', 'td.btn_dev', function () {

        $.ajax({
            type: "POST",
            url: "/recebimento/devolver",
            data: {
                idlc: $(this).parent().find('input#idlc').val(),
                idib: $(this).parent().find('input#idib').val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            $.Notification.autoHideNotify(
                    retorno.tipo,
                    'top right',
                    'Informações',
                    retorno.msg
                    );
        });
        
        devsTable.ajax.reload();
        plotesTable.ajax.reload();
    });
    
    $('#grid_plotes tbody').on('click', 'td.glotes', function () {

        $.ajax({
            type: "POST",
            url: "/recebimento/gerarLotes",
            data: {
                id_arq: $(this).parent().find('button').val()
            }
        }).done(function (dRt) {
            $.bloquearInterface('<h4>Aguarde... gerando lotes</h4>');
            var retorno = $.parseJSON(dRt);
            
            $.Notification.autoHideNotify(
                    retorno.tipo,
                    'top right',
                    'Informações',
                    retorno.msg
                    );
            
            $.desbloquearInterface();
        });

        plotesTable.ajax.reload();
    });
    
    function SubDetalhes(d, tr, row, dt) {
        var data = '<div class="col-md-12">' +
                '       <table style="font-size: 11px!important; width: 100%" id="arquivo-detalhado" class="table table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="9" style="background-color: #81c868; text-align:center;">Arquivo Detalhado</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th></th>' +
                '                   <th>Núcleo</th>' +
                '                   <th>Data</th>' +
                '                   <th>Qtd Bord.</th>' +
                '                   <th>Qtd Prop.</th>' +
                '                   <th>Usuário</th>' +
                '                   <th>Financiado</th>' +
                '                   <th>Repasse</th>' +
                '                   <th>Status</th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';

        row.child(data).show();

        var sub_arquivo = $('#arquivo-detalhado').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":
                    {
                        url: '/recebimento/arquivoDetalhado',
                        type: 'POST',
                        "data": {
                            "id_arq": dt
                        }
                    },
            "columns": [
                {
                    "orderable": false,
                    "class": "details2",
                    "data": "deta"
                },
                {
                    "orderable": false,
                    "data": "nucl"
                },
                {
                    "orderable": false,
                    "data": "data"
                },
                {
                    "orderable": false,
                    "data": "qtdb"
                },
                {
                    "orderable": false,
                    "data": "qtdp"
                },
                {
                    "orderable": false,
                    "data": "user"
                },
                {
                    "orderable": false,
                    "data": "fina"
                },
                {
                    "orderable": false,
                    "data": "repa"
                },
                {
                    "orderable": false,
                    "data": "stat"
                }
            ]
        });

        $('#arquivo-detalhado tbody').on('click', 'td.details2', function () {
            var tr = $(this).closest('tr');
            var row = sub_arquivo.row(tr);
            var rowsTam = sub_arquivo.rows()[0].length;
            var dt = $(this).parent().find('button').val();
            var desfoque = $(this).parent().find('button');
            if ($(this).parent().find('button').hasClass('btn-success')) {
                $(this).parent().find('button').removeClass('btn-success');
                $(this).parent().find('button').addClass('btn-warning');
            } else {
                $(this).parent().find('button').addClass('btn-success');
                $(this).parent().find('button').removeClass('btn-warning');
            }

            if (row.child.isShown()) {
                row.child.hide();
            } else
            {
                for (i = 0; i < rowsTam; i++)
                {

                    if (sub_arquivo.row(i).child.isShown()) {
                        sub_arquivo.row(i).child.hide();
                    }
                }

                SubDetalhes2(row.data(), tr, row, dt);
            }

            $('#arquivo-detalhado tbody tr').each(function (index, element) {
                if (desfoque.hasClass('btn-warning') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2 && $(element).index('tr') !== $(tr).index('tr') + 3) {
                    $(element).hide();
                } else {
                    $(element).show();
                }
            });

        });
    }

    function SubDetalhes2(d, tr, row, dt) {
        var data = '<div class="col-md-12">' +
                '       <table style="font-size: 11px!important; width: 100%" id="pl-detalhado" class="table table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="6" style="background-color: #81c868; text-align:center;">Pré-Lote Detalhado</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th></th>' +
                '                   <th>Filial</th>' +
                '                   <th>Qtd Bord.</th>' +
                '                   <th>Qtd Prop.</th>' +
                '                   <th>Financiado</th>' +
                '                   <th>Repasse</th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';

        row.child(data).show();

        var sub_pl = $('#pl-detalhado').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":
                    {
                        url: '/recebimento/preLoteDetalhado',
                        type: 'POST',
                        "data": {
                            "id_pl": dt
                        }
                    },
            "columns": [
                {
                    "orderable": false,
                    "class": "details3",
                    "data": "deta"
                },
                {
                    "orderable": false,
                    "data": "fili"
                },
                {
                    "orderable": false,
                    "data": "qtdb"
                },
                {
                    "orderable": false,
                    "data": "qtdp"
                },
                {
                    "orderable": false,
                    "data": "fina"
                },
                {
                    "orderable": false,
                    "data": "repa"
                }
            ]
        });

        $('#pl-detalhado tbody').on('click', 'td.details3', function () {
            var tr = $(this).closest('tr');
            var row = sub_pl.row(tr);
            var rowsTam = sub_pl.rows()[0].length;
            var dt = $(this).parent().find('button').val();
            var desfoque = $(this).parent().find('button');
            if ($(this).parent().find('button').hasClass('btn-success')) {
                $(this).parent().find('button').removeClass('btn-success');
                $(this).parent().find('button').addClass('btn-warning');
            } else {
                $(this).parent().find('button').addClass('btn-success');
                $(this).parent().find('button').removeClass('btn-warning');
            }

            if (row.child.isShown()) {
                row.child.hide();
            } else
            {
                for (i = 0; i < rowsTam; i++)
                {

                    if (sub_pl.row(i).child.isShown()) {
                        sub_pl.row(i).child.hide();
                    }
                }

                SubDetalhes3(row.data(), tr, row, dt);
            }

            $('#pl-detalhado tbody tr').each(function (index, element) {
                if (desfoque.hasClass('btn-warning') && $(element).index('tr') !== $(tr).index('tr') && $(element).index('tr') !== $(tr).index('tr') + 1 && $(element).index('tr') !== $(tr).index('tr') + 2 && $(element).index('tr') !== $(tr).index('tr') + 3) {
                    $(element).hide();
                } else {
                    $(element).show();
                }
            });

        });
    }

    function SubDetalhes3(d, tr, row, dt) {
        var data = '<div class="col-md-12">' +
                '       <table style="font-size: 11px!important; width: 100%" id="bordero-detalhado" class="table table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th colspan="9" style="background-color: #81c868; text-align:center;">Borderô Detalhado</th>' +
                '               </tr>' +
                '               <tr>' +
                '                   <th>Data</th>' +
                '                   <th>Código</th>' +
                '                   <th>Cliente</th>' +
                '                   <th>Inicial</th>' +
                '                   <th>Entrada</th>' +
                '                   <th>Carencia</th>' +
                '                   <th>Financiado</th>' +
                '                   <th>Repasse</th>' +
                '                   <th>Parcelas</th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>';

        row.child(data).show();

        var sub_bord = $('#bordero-detalhado').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":
                    {
                        url: '/recebimento/BorderoDetalhado',
                        type: 'POST',
                        "data": {
                            "id_bord": dt
                        }
                    },
            "columns": [
                {
                    "orderable": false,
                    "data": "data"
                },
                {
                    "orderable": false,
                    "data": "codi"
                },
                {
                    "orderable": false,
                    "data": "clie"
                },
                {
                    "orderable": false,
                    "data": "inic"
                },
                {
                    "orderable": false,
                    "data": "entr"
                },
                {
                    "orderable": false,
                    "data": "care"
                },
                {
                    "orderable": false,
                    "data": "fina"
                },
                {
                    "orderable": false,
                    "data": "repa"
                },
                {
                    "orderable": false,
                    "data": "parc"
                }
            ]
        });
    }

    var selecionados = [];

    $('#filtrar').on('click', function () {
        propostasTable.ajax.reload();

    });
    var checkall;
    var elem = document.querySelectorAll('.checkall');
    var i;
    for (i = 0; i < elem.length; i++) {
        checkall = new Switchery(elem[i], {color: '#1db198', size: 'small'});
    }

    $('#marcar_todos').on('change', function () {
        var elem = document.querySelectorAll('.pfin');
        var i;
        if ($(this).prop('checked')) {
            for (i = 0; i < elem.length; i++) {
                if (!$(elem[i]).parent().find('input').prop('checked')) {
                    $(elem[i]).click();
                    if ($(elem[i]).parent().find('input').prop('checked')) {
                        $(elem[i]).parent().parent().css("background-color", "#343d47");
                    } else {
                        $(elem[i]).parent().parent().css("background-color", "");
                    }
                }
            }
        } else {
            for (i = 0; i < elem.length; i++) {
                if ($(elem[i]).parent().find('input').prop('checked')) {
                    $(elem[i]).click();
                    if ($(elem[i]).parent().find('input').prop('checked')) {
                        $(elem[i]).parent().parent().css("background-color", "#343d47");
                    } else {
                        $(elem[i]).parent().parent().css("background-color", "");
                    }
                }
            }
        }
    });

    $(document).on('click', '.switchery', function () {
        atualizar();
    });

    $(document).on('change', '.pfin', function () {
        if ($(this).parent().find('input').prop('checked')) {
            $(this).parent().parent().css("background-color", "#343d47");
        } else {
            $(this).parent().parent().css("background-color", "");
            $(this).parent().parent().find('td').each(function (index, val) {
                $(val).css('color', '');
            });
        }
        var element = $(this).parent().find('input');
        marcar(element);
    });

    function atualizar() {
        $.ajax({
            type: "POST",
            url: "/recebimento/selecionadas",
            data: {
                selecionadas: selecionados
            }
        }).done(function (dRt) {
            var result = $.parseJSON(dRt);
            $('#props_selects').text(result.qtd_props);
            $('#props_fin').text(result.totalFin);
            $('#props_rep').text(result.totalRep);
            $('#props_ncl').text(result.qtdNcl);
        });
    }

    function marcar(element) {
        if (element.prop('checked') && !element.hasClass('checkall')) {
            selecionados.push(element.val());
        } else {
            var indice = selecionados.indexOf(element.val());
            selecionados.splice(indice, 1);
        }
        //atualizar();
    }

    $('#gerar_cnab').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/recebimento/lotesCNABS",
            data: {
                marcadas: selecionados,
                imagem: $('#select-modo').val()
            }
        }).done(function (dRt) {
            $.bloquearInterface('<h4>Aguarde... gerando pré lotes e arquivo</h4>');
            var retorno = $.parseJSON(dRt);

            $.Notification.autoHideNotify(
                    retorno.data.tipo,
                    'top right',
                    'Informações',
                    retorno.data.msg
                    );

            if (retorno.data.tipo !== 'error') {
                $.ajax({
                    type: "POST",
                    url: "/recebimento/gerarArquivoLecca",
                    data: {
                        propostasMarcadas: selecionados
                    }
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.Notification.autoHideNotify(
                            retorno.type,
                            'top right',
                            retorno.title,
                            retorno.msg
                            );

                    if (!retorno.hasErrors) {

                        var link = document.createElement("a");
                        link.download = retorno.nomeArquivo;
                        link.href = retorno.url;
                        link.click();

                        propostasTable.ajax.reload();
                        plotesTable.ajax.reload();
                        if ($('#marcar_todos').prop('checked')) {
                            $('#marcar_todos').click();
                        }

                        selecionados = [];
                        atualizar();
                        $.desbloquearInterface();
                    }
                });
            }
        });
    });

    $("[type=file]").on("change", function () {
        var file = this.files[0].name;
        var dflt = $(this).attr("placeholder");
        if ($(this).val() != "") {
            $(this).next().text(file);
        } else {
            $(this).next().text(dflt);
        }
    });

    $('#form_importacao').ajaxForm({
        beforeSubmit: function () {
            $.bloquearInterface('<h4>Aguarde... O arquivo está sendo importado</h4>');
        },
        //em caso de sucesso
        success: function (response) {

            var data = $.parseJSON(response);

            $.Notification.autoHideNotify(
                    data.tipo,
                    'top right',
                    'Informações',
                    data.msg
                    );

            $("#anexo_importacao").empty();
            $("#anexo_label").text('Selecionar Arquivo');

            $.ajax({
                type: "POST",
                url: "/recebimento/lotesDP"
            }).done(function (dRt) {

                var retorno = $.parseJSON(dRt);

                $.Notification.autoHideNotify(
                        retorno.tipo,
                        'top right',
                        'Informações',
                        retorno.msg
                        );

                $.desbloquearInterface();

                plotesTable.ajax.reload();
                propostasTable.ajax.reload();
            });
        }
    });
});