$(function(){

    $("#form-mudar-primeira-senha").modal("show");

	$("#senha_atual").focus();

	$('#formulario-senha').validate({
		
		ignore: [],

		rules: {            
            nova_senha_novamente: {
                equalTo: "#nova_senha"
            }
        },
        messages: {
            nova_senha_novamente: {
                equalTo: "As senhas não coincidem. Por favor, corrija."
            },
        },

        submitHandler: function() {

            var post = $.ajax({
                url: '/user/changePassword/',
                type: 'POST',
                data: $('#formulario-senha').serialize(),
            });
			
            post.done(function(dRt) {
                
                var retorno = $.parseJSON(dRt);

                $.each(retorno.msgConfig, function (noti, conteudoNotificacao){

                    $.Notification.autoHideNotify(
                        conteudoNotificacao.tipo,
                        conteudoNotificacao.posicao,
                        conteudoNotificacao.titulo,
                        conteudoNotificacao.mensagem
                    );
                });
                
                if ( !retorno.erro )
                {
                    $("#form-mudar-primeira-senha").modal("hide");
                    limpaForm( $('#formulario-senha') );   
                }
            });
        }

	});
});