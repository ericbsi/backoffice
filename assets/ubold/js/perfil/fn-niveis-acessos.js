$(function(){

	var funcoesGrid;

	var gridNiveis = $('#datatable-niveis-acessos').DataTable({

		searching 		: false,
		ordering 		: false,
		"ajax": {
            url 		: '/nivelRole/dataTable/',
            type 		: 'POST',
            "data": function (d) {
                d.Role_Id = $("#Role_Id").val()
            }
        },
		serverSide 		: true,
		ordering 		: false,
		searching 		: false,

		"columns": [
			{
                "className" 	: 'details-control',
                "orderable"		: false,
                "data" 			: null,
                "defaultContent": ''
            },
	        {"data" 			: "nome"		},
	        {"data" 			: "descricao"	},
	        {"data" 			: "status"		},
	        {"data" 			: "btn_editar"	},
    	],    	
		"columnDefs": [
            {
                "orderable" 	: false,
                "targets" 		: "no-orderable"
            }
        ],
		"language" 				:{
    		"info"  			: "Exibindo _START_ até _END_ de _TOTAL_ registros",
    		"paginate"  		: {
		        "first" 		:   "Primeiro",
		        "last"  		:   "Último",
		        "next"  		:   "Pŕoximo",
		        "previous"		:   "Anterior"
    		},
    	}
	});

	$('#datatable-niveis-acessos tbody').on('click', 'td.details-control', function () {

		var tr 				= $(this).closest('tr');
        var row 			= gridNiveis.row(tr);
        var rowsTam 		= gridNiveis.rows()[0].length; // quantidade de linhas
        var rowData	 		= row.data(); // Object [prop:val]
        var elemento 		= $(this);

        /*Se a linha estiver aberta, feche-a*/
        if ( row.child.isShown() )
        {
        	row.child.hide();
            tr.removeClass('shown');
        }
        else
        {
        	for (i = 0; i < rowsTam; i++)
            {

                if (gridNiveis.row(i).child.isShown()) {
                    gridNiveis.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            funcoesGrid = $('#funcoesGrid').DataTable({

            	"ajax": {
		            url 		: '/nivelRoleHasRoleHasFuncao/dataTable/',
		            type 		: 'POST',
		            "data": function (d) {
		                d.nivelId 	= rowData.idNivel
		            }
        		},
				serverSide 		: true,
				ordering 		: false,
				searching 		: false,

				"columns": [
			        {
                        "data": "checkFilial",
                        "orderable": false,
                        "className": "checkFilial"
                    },
                    {
                        "data": "exibirMenu",
                        "orderable": false,
                        "className": "CheckExibeMenu"
                    },
			        {"data" 		: "label"		},
			        {"data" 		: "descricao"	},
		    	],

		    	"language" 			:{
		    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
		    		"paginate"  	: {
				        "first" 	:   "Primeiro",
				        "last"  	:   "Último",
				        "next"  	:   "Pŕoximo",
				        "previous"	:   "Anterior"
		    		},
		    	}

            });
        }
	});
	
	$(document).on('change', '.input_check', function(){

		var post = $.ajax({

			url 			: '/nivelRoleHasRoleHasFuncao/nivelRoleRoleHasFuncao/',
			type 			: 'POST',
			data 			:{
               nivelId 		: $(this).data('nivel-id'),
               funcaoId		: $(this).data('role-has-funcao-id'),
               hab			: $(this).data('hab')
            }
		});

		post.done(function(dRt){
			
			//console.log( $.parseJSON( dRt ) );

			funcoesGrid.draw(false);
		});
	});

	$(document).on('change', '.input_check_exibir', function(){
		
		var post = $.ajax({

			url 			: '/nivelRoleHasRoleHasFuncao/exibirMenu/',
			type 			: 'POST',
			data 			:{
               nivelId 		: $(this).data('nivel-id'),
               funcaoId		: $(this).data('role-has-funcao-id'),
               hab			: $(this).data('hab')
            }
		});

		post.done(function(dRt){

			funcoesGrid.draw(false);
		});
	});	

	$(document).on('click', '.btn-load-form', function(){

		var getDados 	= $.ajax({
			type 		: 'POST',
			url  		: $(this).data('source'),
			data 		: {
				id 		: $(this).data('entity-id')
			}
		});

		getDados.done(function( dRt, statusText, xhr ){
			
			var retorno = $.parseJSON( dRt );

			$.each(retorno.fields, function(f,field){

				$('#'+field.input_bind).val(field.value);

                if (field.type == 'select')
                {
                	if( field.value != null )
                    {
                    	$('#' + field.input_bind).select2("val", field.value);
                    }
                }
                else
                {
                	$('#'+field.input_bind).val(field.value);
                }				
            });

			//$('#nivelrole_id').attr('value', $(this).data('entity-id'));
            
            $.desbloquearInterface();
			$('#con-close-modal').modal('show');
		});
	});

	function formatSubTable(d, tr, row) {

    	tr.addClass('shown');

	    var data = '<h5><i class="clip-list"></i> Funções</h5>' +
	            '<div class="row">' +
	            '   <div  class="col-sm-12">' +
	            '       <table id="funcoesGrid" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">' +
	            '           <thead>' +
	            '              <tr>' +
	            '                   <th class="no-orderable" width="20px" id="thCheckFilial">' +
	            '                       <i id="checkFiliais" class="clip-checkbox-partial"></i>' +
	            '                   Ativo?</th>' +
	            '                   <th class="no-orderable" width="100px" id="thCheckExibeMenu">' +
	            '                       <i id="checkExibeMenu" class="clip-checkbox-partial"></i>' +
	            '                   Exibir no menu?</th>' +
	            '                   <th class="no-orderable">Função</th>' +
	            '					<th class="no-orderable">Descrição</th>' +
	            '              </tr>' +
	            '           </thead>' +
	            '           <tbody>' +
	            '           </tbody>' +
	            '       </table>' +
	            '   </div>' +
	            '</div>';

	    row.child(data).show();
	}

	$('#form-add-novo-nivel').validate({

		submitHandler: function () {

			$.bloquearInterface('<p>Aguarde, salvando informações...</p>');

			var post = $.ajax({
				type : 'POST',
				url  : '/nivelRole/persist/',
				data : $('#form-add-novo-nivel').serialize(),
			});
			
			post.done(function( dRt ){
				
				$.desbloquearInterface();

				var retorno = $.parseJSON( dRt );

				if( typeof retorno.errors == 'undefined')
				{
					limpaForm( $('#form-add-novo-nivel') );
					gridNiveis.draw();
				}
				
				$.each(retorno.msgConfig, function (noti, conteudoNotificacao){

	                $.Notification.autoHideNotify(
	                	conteudoNotificacao.tipo,
	                	conteudoNotificacao.posicao,
	                	conteudoNotificacao.titulo,
	                	conteudoNotificacao.mensagem
	                );
            	});
				
				$('#label-niveis-count').html(retorno.countRoleNiveis);
				$('#con-close-modal').modal('hide');

			});
		}
	});

	$('#label-niveis-count').on('click',function(){
		$('#nivelrole_id').attr('value','0');
		limpaForm( $('#form-add-novo-nivel') );
	});
});