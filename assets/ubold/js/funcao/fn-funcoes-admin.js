$(function(){

	var gridFuncoes 	= $('#datatable-funcoes').DataTable({
		ajax 			: "/funcao/dataTable/",
		serverSide 		: true,
		ordering 		: false,
		searching 		: false,

		"columns": [
	        {"data" 		: "label"		},
	        {"data" 		: "descricao"	},
	        {"data" 		: "btn_editar"	},
    	],

    	"language" 			:{
    		"info"  		: "Exibindo _START_ até _END_ de _TOTAL_ registros",
    		"paginate"  	: {
		        "first" 	:   "Primeiro",
		        "last"  	:   "Último",
		        "next"  	:   "Pŕoximo",
		        "previous"	:   "Anterior"
    		},
    	}
	});
	
	$('#form-funcao').validate({
		
		submitHandler: function () {

			$.bloquearInterface('<p>Aguarde, salvando informações...</p>');

			var post = $.ajax({
				type : 'POST',
				url  : '/funcao/persist/',
				data : $('#form-funcao').serialize(),
			});

			post.done(function( dRt, statusText, xhr ){
				
				$.desbloquearInterface();

				$('#funcao_id').attr('value', '0');

				/*Conversão necessária, pois é retornado um array formatado pelo json_encode do php*/
				var retorno = $.parseJSON( dRt );

				/*Não houveram erros, limpo o formulário*/
				/*E dá um refresh na table*/
				if( typeof retorno.errors == 'undefined')
				{
					limpaForm( $('#form-funcao') );
					gridFuncoes.draw();
				}

				/*For each nas mensagens retornadas pelo servidor [erro, sucesso, informação, etc]*/
				$.each(retorno.msgConfig, function (noti, conteudoNotificacao){

	                $.Notification.autoHideNotify(
	                	conteudoNotificacao.tipo,
	                	conteudoNotificacao.posicao,
	                	conteudoNotificacao.titulo,
	                	conteudoNotificacao.mensagem
	                );
            	});
			});
		}
	});

	$(document).on('click','.btn-load-form',function(){
		
		$.bloquearInterface('<p>Aguarde, carregando informações...</p>');

		$($(this).data('collapse-div')).collapse('show');

		$($(this).data('form-load')+ ' :input:enabled:visible:first').focus();

		var getDados = $.ajax({
			type : 'POST',
			url  : $(this).data('source'),
			data : {
				id : $(this).data('entity-id')
			}
		});

		getDados.done(function( dRt, statusText, xhr ){
			
			var retorno = $.parseJSON( dRt );

			$.each(retorno.fields, function(f,field){

				$('#'+field.input_bind).val(field.value);

                if (field.type == 'select')
                {
                	if( field.value != null )
                    {
                    	$('#' + field.input_bind).select2("val", field.value);
                    }
                }
                else
                {
                	$('#'+field.input_bind).val(field.value);
                }				
            });

			$('#funcao_id').attr('value', $(this).data('entity-id'));
            
            $.desbloquearInterface();
		});
	});

});
